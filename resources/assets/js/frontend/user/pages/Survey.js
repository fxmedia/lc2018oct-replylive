import React from 'react';
import { inject, observer } from 'mobx-react';

import OpenAnswer from '../components/games/OpenAnswer';
import RangeAnswer from '../components/games/RangeAnswer';
import InBetweenScreen from '../components/games/InBetweenScreen';
import ABCD from '../components/games/ABCD';
import ABCDMultiple from '../components/games/ABCDMultiple';
import Question from '../components/games/Question';
import SubmitButton from '../components/games/SubmitButton';
import Loader from '../components/Loader';
import ProgressBar from '../components/games/ProgressBar';

@inject('stores', 't') @observer
export default class SurveyPage extends React.Component {

    handleAnswer(input) {
        this.props.stores.survey.submitAnswer(input);
    }

    handleNextQuestion() {
        this.props.stores.survey.setNextQuestion()
    }

    handleOpenAnswer() {
        const { activeOpenAnswer } = this.props.stores.survey;
        this.props.stores.survey.submitAnswer(activeOpenAnswer);
    }

    handleOpenAnswerChange(answer) {
        this.props.stores.survey.updateOpenAnswer(answer)
    }

    renderQuestionData() {
        const { activeQuestion, isLastQuestion, activeOpenAnswer } = this.props.stores.survey;

        if(!activeQuestion) return null;

        switch(activeQuestion.type) {
            case "open":
                return (
                    <OpenAnswer
                        isLast={isLastQuestion}
                        handleAnswer={this.handleOpenAnswer.bind(this)}
                        handleChange={this.handleOpenAnswerChange.bind(this)}
                        value={activeOpenAnswer}
                    />
                )
            case "range":
                return (
                    <RangeAnswer
                        isLast={isLastQuestion}
                        handleAnswer={this.handleAnswer.bind(this)}
                        data={activeQuestion.data}
                    />
                )
            case "abcd":
                return (
                    <ABCD
                        handleAnswer={this.handleAnswer.bind(this)}
                        data={activeQuestion.data}
                    />
                )
            case "abcd-multiple":
                return (
                    <ABCDMultiple
                        isLast={isLastQuestion}
                        handleAnswer={this.handleAnswer.bind(this)}
                        data={activeQuestion.data}
                    />
                )
            case "in-between-screen":
                return (
                    <InBetweenScreen
                        handleNextQuestion={this.handleNextQuestion.bind(this)}
                        data={activeQuestion.data}
                    />
                )
            default:
                console.log("This question type does not exist..");
                return null;
        }
    }

    renderIntro() {
        const { user } = this.props.stores.main;
        return (
            <div className="text-wrapper">
                <h2>{this.props.t.general.dear} {user.name}</h2>
                <p dangerouslySetInnerHTML={{ __html: this.props.stores.survey.data.intro}} />
                <SubmitButton
                    text={this.props.t.survey.start}
                    handleSubmit={() => this.props.stores.survey.setPhase("questions")}
                />
            </div>
        )
    }

    renderOutro() {
        const { user } = this.props.stores.main;
        return (
            <div className="text-wrapper">
                <h2>{this.props.t.general.dear} {user.name}</h2>
                <p dangerouslySetInnerHTML={{ __html: this.props.stores.survey.data.outro}} />
                <SubmitButton
                    text={this.props.t.survey.end}
                    handleSubmit={() => this.props.stores.main.closeTakeover()}
                />
            </div>
        )
    }

    renderQuestions() {
        const { activeQuestion, currentQuestionIdx, data } = this.props.stores.survey;
        if(!activeQuestion || !data) return null;

        const numRealQuestions = data.questions
            .filter(question => question.type != "in-between-screen").length;

        const currentRealQuestionIdx = currentQuestionIdx - data.questions
            .filter((question, idx) =>
                question.type == "in-between-screen" && currentQuestionIdx >= idx
            ).length

        return (
            <div className="active-question">
                <Question text={activeQuestion.title}>
                    <ProgressBar
                        currentNum={currentRealQuestionIdx}
                        totalNum={numRealQuestions}
                        hide={activeQuestion.type == "in-between-screen"}
                    />
                </Question>
                {this.renderQuestionData()}
            </div>
        )
    }

    renderPhase(phase) {
        switch(phase) {
            case "intro":
                return this.renderIntro();
            case "outro":
                return this.renderOutro();
            default:
                return this.renderQuestions();
        }
    }

    render() {
        const { phase, isLoading } = this.props.stores.survey;

        return (
            <div className="SurveyPage game-wrapper">
                {this.renderPhase(phase)}
                {(isLoading) ? <Loader /> : null}
            </div>
        )
    }
}
