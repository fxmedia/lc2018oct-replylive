<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOpenQuestionLinkTablesAndTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('open_questions', function(Blueprint $table){
            $table->string('tags')->nullable()->after('settings');
        });

        Schema::create('group_open_question', function(Blueprint $table){
            $table->integer('open_question_id')->unsigned();
            $table->foreign('open_question_id')->references('id')->on('open_questions')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onUpdate('cascade')->onDelete('cascade');

            $table->index( ['open_question_id', 'group_id'] );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('open_questions', function(Blueprint $table){
            $table->dropColumn('tags');
        });

        Schema::dropIfExists('group_open_question');
    }
}
