<?php

namespace App\Export;

class PledgeExport extends BaseExport
{
    private $filename;
    private $headers = ['Registratienaam', 'E-mail', 'Bedrijfsnaam', 'Bedrag', 'Volledige naam', 'Gemaakt op'];
    private $rows;

    public function __construct($pledges, $filename = 'all')
    {
        $this->filename = 'export-pledges-'.$filename;

        foreach($pledges as $pledge){
            $company = isset($pledge->user->extra['company']) ? $pledge->user->extra['company'] : "";
            $this->rows[] = [
                $pledge->name,
                $pledge->email,
                $company,
                $pledge->amount,
                $pledge->user->name,
                $pledge->created_at
            ];
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}
