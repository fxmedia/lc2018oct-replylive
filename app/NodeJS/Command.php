<?php

namespace App\NodeJS;

use App\Setting;
use Log;

class Command
{

    public static function make($action, $data = [])
    {
        $entryPoint = self::getEntryPoint('action');  //->getEntryPoint();

        $command = [
            "action" => $action,
            "data" => $data
        ];

        return self::send($entryPoint, $command);
    }

    public static function status($action, $data = [])
    {
        $entryPoint = self::getEntryPoint('status');

        $command = [
            "action" => $action,
            "data" => $data
        ];

        return self::send($entryPoint, $command);
    }

    private static function getEntryPoint($uri)
    {
        $settings = Setting::getSettingsAsObject();

        return ( ($settings->nodejs_port == 443) ? 'https://' : 'http://' ) .
            $settings->nodejs_domain .
            (
                $settings->nodejs_port != 443
                && $settings->nodejs_port != 80
            ? ':' . $settings->nodejs_port : '' ) .
            config('app.node_js.'.$uri);
    }

    private static function send($entryPoint, $command)
    {
        $json_command = json_encode($command);
        $ch = curl_init( $entryPoint );

        curl_setopt($ch, CURLOPT_URL, $entryPoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_command );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json_command)
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }
}
