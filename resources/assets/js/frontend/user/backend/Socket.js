import socketio from 'socket.io-client';
import config from './../../config';

export default class Socket {
    constructor(namespace) {
        this.connection = socketio(config.websocket +'/'+ namespace);
        this.connection.on('connect', user => {
            console.log("The connection is live! :D", this.connection.id);
            this.connection.emit('token_set', this.parseToken());
        });
    }

    parseToken() {
        return config.token;
    }
}
