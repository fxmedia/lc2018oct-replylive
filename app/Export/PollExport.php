<?php

namespace App\Export;

class PollExport extends BaseExport
{
    private $filename;
    private $headers = ['id', 'question', 'tags', 'groups', 'choice', 'value'];
    private $rows;

    public function __construct($polls, $filename = 'all')
    {
        $this->filename = 'export-polls-'.$filename;


        foreach($polls as $poll){
            $groups = '';
            foreach($poll->groups as $group){
                $groups .= $group->name.', ';
            }
            if(strlen($groups) > 2) {
                $groups = substr($groups, 0, -2);
            }
            $this->rows[] = [
                $poll->id,
                $poll->question,
                $poll->tags,
                $groups
            ];

            foreach($poll->choices as $c){
                $this->rows[] = [
                    $poll->id,
                    '',
                    '',
                    '',
                    $c['choice'],
                    $c['value']
                ];
            }
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}