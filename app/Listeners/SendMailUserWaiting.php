<?php

namespace App\Listeners;

use App\Events\UserSetWaiting;
use App\Senders\EmailSender;

//use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailUserWaiting
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserSetWaiting $event
     * @return void
     */
    public function handle(UserSetWaiting $event)
    {
        //
        $user = $event->getUser();
        EmailSender::sendAction($user, 'waiting');
    }
}
