<?php

namespace App\Export;

class SurveyExport extends BaseExport
{
    private $filename;
    private $headers = ['id', 'survey', 'tags', 'takeover', 'intro', 'outro', 'groups', 'question' , 'type', 'order' ,'data'];
    private $rows;

    public function __construct($surveys, $filename = 'all')
    {
        $this->filename = 'export-surveys-'.$filename;


        foreach($surveys as $survey){
            $groups = '';
            foreach($survey->groups as $group){
                $groups .= $group->name.', ';
            }
            if(strlen($groups) > 2) {
                $groups = substr($groups, 0, -2);
            }

            $this->rows[] = [
                $survey->id,
                $survey->name,
                $survey->tags,
                $survey->takeover,
                $survey->intro,
                $survey->outro,
                $groups
            ];

            foreach($survey->questions as $question){
                $this->rows[] = [
                    $survey->id,
                    '', // survey_name
                    '', // survey_tags
                    '', // survey_takeover
                    '', // survey_intro
                    '', // survey_outro
                    '', // survey_groups
                    $question->title,
                    $question->type,
                    $question->order,
                    json_encode($question->data)

                ];
            }
        }
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }

}