<?php

namespace App\Report;

use App\User;
use Maatwebsite\Excel\Facades\Excel;

class QuizResultReport extends BaseReport{

    private $quizzes;

    public function __construct($quizzes)
    {
        $this->quizzes = $quizzes;
    }

    public function getFilename()
    {
        return 'quiz_report_' . date('Y_m_d_h_m_s');
    }

    public function report(){
        Excel::create($this->getFilename(), function($excel){

            foreach($this->quizzes as $quiz){

                $name = substr(trim(preg_replace('/[\*|\:|\\|\/|\?|\[|\]]/', '', $quiz->name)), 0, 31);
                $excel->sheet($name, function($sheet) use ($quiz){
                    $index = 1;

                    $sheet->row($index, ['Quiz Name: ' . $quiz->name ]);
                    $sheet->row($index, function ($row) {
                        $row->setFontSize(18);
                        $row->setFontWeight('bold');
                    });
                    $index++;

                    foreach($quiz->questions as $question) {
                        $sheet->row($index, ['Question: ' . $question->title ]);

                        $sheet->row($index, function ($row) {
                            $row->setFontSize(14);
                            $row->setFontWeight('bold');
                        });
                        $index++;

                        $sheet->row($index, ['', 'User', 'Choice', 'Correct?']);

                        $index++;

                        foreach($question->answers as $a){
                            $answer = json_decode($a);
                            $user = User::find($answer->user_id);
                            $correct = ($answer->choice === $question->correct_answer) ? "correct" : "wrong";

                            $name = $user->firstname . " " . $user->lastname;
                            $sheet->row($index,[ '', $name, $answer->choice,  $correct ]);
                            $index++;
                        }
                        $index++;
                    }
                });
            }

        })->export('xls');
    }
}
