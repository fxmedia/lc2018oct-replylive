(function(scope, $){
    console.log('Pushes.js');

    class _Push {
        constructor(){
            this.routes = scope.api_routes;

            $('.toggle-push-start-stop').on('click', this.togglePush.bind(this));
        }

        getRoute(push_id, key){
            return this.routes[push_id][key];
        }

        togglePush(event){
            var el = $(event.currentTarget),
                pushId = el.parent().data('push-id'),
                isActive = el.hasClass('btn-danger');

            isActive
                ? this.stopPush(el, pushId)
                : this.startPush(el, pushId);
        }

        stopPush(el, pushId){
            var route = this.getRoute(pushId, 'stop');

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                el.toggleClass('btn-danger btn-primary')
                    .find('i').toggleClass('fa-stop fa-play')
                    .parent().find('span').text('Start Push Module');
                // .parent().parent().find('.toggle-pause')
                // .addClass('hidden')
                // .addClass('btn-info').removeClass('btn-success')
                // .find('i').removeClass('glyphicon-play').addClass('glyphicon-pause')
                // .parent().find('span').text('Pause');
            }).error(this.error);
        }

        startPush(el, pushId){
            var route = this.getRoute(pushId, 'start');

            var currentActive = $('.toggle-push-start-stop.btn-danger').length > 0;

            if(currentActive && !confirm('There is already a push module active, do you wish to proceed to this push module?')) {
                return;
            }

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                $('.toggle-poll-start-stop')
                    .addClass('btn-primary').removeClass('btn-danger')
                    .find('i').addClass('fa-play').removeClass('fa-stop')
                    .parent().find('span').text('Start Push Module');
                    // .parent().parent().find('.toggle-pause').addClass('hidden');

                el.toggleClass('btn-danger btn-primary')
                    .find('i').toggleClass('fa-stop fa-play')
                    .parent().find('span').text('Stop Push Module')
                    .parent().parent().find('.toggle-pause').removeClass('hidden');

            }).error(this.error);
        }

        error(xhr, status, errorThrown) {
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }
    }

    scope.push = new _Push();
})(this,$);