<?php

use App\Setting;

if(! function_exists('get_javascript_cache_version')){
    /**
     * Get the JavaScript Cache Version
     *
     * @return int
     */
    function get_javascript_cache_version()
    {
        return Setting::getJSCacheVersion();
    }
}

if(! function_exists('get_css_cache_version')){
    /**
     * Get the CSS Cache Version
     *
     * @return int
     */
    function get_css_cache_version()
    {
        return Setting::getCSSCacheVersion();
    }
}