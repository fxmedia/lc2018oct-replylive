<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-plus"></i> Create</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.quizzes.create') }}">Add Quiz</a></li>
    </ul>
    <div class="panel-heading"><i class="fa fa-upload"></i> Upload</div>
    <div class="panel-body panel-uploader">
        {!! Form::open(['route' => 'backend.quizzes.import', 'files' => true]) !!}
        {!! Form::file( 'quiz_upload', ['class' => 'importfile'] ) !!}
        <br>
        {!! Form::submit( 'Upload', ['class' => 'btn btn-sm btn-primary'] ) !!}
        {!! Form::close() !!}
    </div>
    <div class="panel-heading"><i class="fa fa-download"></i> Download</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.quizzes.export') }}">Download all Quizzes</a></li>
        <li class="list-group-item"><a href="{{ route('backend.quizzes.export.images') }}">Download Images</a></li>
        <li class="list-group-item"><a href="{{ route('backend.quizzes.report') }}">Download Results</a></li>
    </ul>

    <div class="panel-heading"><i class="glyphicon glyphicon-wrench"></i> Actions</div>
    <ul class="list-group">
        <li class="list-group-item">
            <form action="{{ URL::route('backend.quizzes.reset') }}" method="post" onsubmit="return confirm('Are you sure you wish to reset all scores?')">
                {!! csrf_field() !!}
                <button type="submit" class="btn-link">Reset all quiz scores</button>
            </form>
        </li>
        <li class="list-group-item">
            <form action="{{ URL::route('backend.quizzes.remove.all') }}" method="post" onsubmit="return confirm('Are you sure you wish to remove all quizzes?')">
                {!! csrf_field() !!}
                <button type="submit" class="btn-link">Remove all quizzes</button>
            </form>
        </li>
    </ul>

    <div class="panel-heading"><i class="glyphicon glyphicon-stats"></i> Statistics</div>
    <ul class="list-group">
        <li class="list-group-item"><span class="pull-right badge">{{ count($quizzes) }}</span>Total quizzes:</li>
        @if(isset($amountQuestions))
            <li class="list-group-item"><span class="pull-right badge">{{ $amountQuestions }}</span>Total Questions:</li>
        @endif
        @if(isset($totalAnswers))
            <li class="list-group-item"><span class="pull-right badge">{{ $totalAnswers }}</span>Total Answers:</li>
        @endif
    </ul>
</div>
