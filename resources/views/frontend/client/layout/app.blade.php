<!DOCTYPE html>
<html>
<head>
    <title>{{ $eventName }}</title>

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('stylesheets')
</head>
<body>

@yield('header')

@yield('content')

@yield('scripts')
</body>
</html>
