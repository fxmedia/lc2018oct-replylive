<?php

namespace App\Http\Controllers\Backend;

use App\Export\KnockoutExport;
use App\Import\KnockoutImport;
use App\Report\KnockoutResultReport;
use App\NodeJS\Command;
use App\Knockout;
use App\Group;
use App\KnockoutAnswer;
use App\KnockoutQuestion;
use Exception;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KnockoutsController extends Controller
{
    //
    public function index(){
        $knockouts = Knockout::with('groups', 'questions')->get();
        $totalAnswers = KnockoutAnswer::count();
        $amountQuestions = KnockoutQuestion::count();

        try {
            $status = json_decode( Command::status('knockout_status') ,true);

            foreach($knockouts as $knockout){
                if($status['knockout_id'] && $status['knockout_id'] == $knockout->id){
                    $knockout->isActive = true;
                    $knockout->activeData = $status;
                }
            }

        } catch(Exception $e){}


        return view('backend.knockouts.index', compact('knockouts', 'totalAnswers', 'amountQuestions'));
    }


    public function show($id){
        $knockout = Knockout::find($id);

        if($knockout === null){
            return redirect()->back()->withErrors(['message' => 'Unable to show non existing Knockout']);
        }

        return view('backend.knockouts.show', compact('knockout'));
    }

    public function create(){
        $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

        return view('backend.knockouts.create', compact('groups'));
    }

    public function store(Request $request){
        $validators = config('validation.knockouts.store');

        $data = $request->validate($validators);

        $knockout = new Knockout;
        $knockout->fill($data);


        if(!$knockout->save()){
            return redirect()->back()->withErrors(['message'=>'Error saving new knockout']);
        }

        $groups = [];
        if($request->input('groups')){
            $groups = (array)$request->input('groups');
        }
        $knockout->groups()->sync($groups);

        return redirect()->route('backend.knockouts.edit', [$knockout->id])->with(['message' => 'Successfully added new knockout. Go edit knockout to add questions']);
    }

    public function edit($id){
        $knockout = Knockout::find($id);

        if($knockout !== null){
            $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

            return view('backend.knockouts.edit', compact('knockout','groups'));
        }

        return redirect()->back()->withErrors(['message' => 'Unable to find selected Knockout']);
    }

    public function update(Request $request, $id){
        $knockout = Knockout::find($id);

        if($knockout === null){
            return redirect()->back()->withErrors(['message' => 'Unable to update non existing Knockout']);
        }

        $validators = config('validation.knockouts.store');

        $data = $request->validate($validators);

        if($knockout->update($data)){
            $groups = [];
            if($request->input('groups')){
                $groups = (array)$request->input('groups');
            }
            $knockout->groups()->sync($groups);

            return redirect()->route('backend.knockouts.index')->with(['message' => 'Sucessfully updated knockout']);
        }

        return redirect()->back->withErrors(['message' => 'Error saving updates to knockout']);
    }

    public function remove($id){
        $knockout = Knockout::find($id);

        if($knockout === null){
            return redirect()->route('backend.knockouts.index')->withErrors(['message' => 'Unable to remove non existing knockout']);
        }

        /**
         * check first maybe if current poll is active? and deactivate it
         */

        try{
            $removed = $knockout->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Knockout has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing knockout.']);
    }

    public function reset(){
        $errors = [];
        try{
            KnockoutAnswer::truncate();
            $success = true;
        } catch(Exception $err) {
            $success = false;
            $errors = $err->getMessage();
        }

        if($success) return redirect()->back()->with('message', 'All knockouts have been reset');

        return redirect()->back()->withErrors($errors);
    }

    public function resetSingle($id){
        $knockout = Knockout::find($id);

        if($knockout === null){
            return redirect()->route('backend.knockouts.index')->withErrors(['message' => 'Unable to reset non existing knockout']);
        }
        $totalRemoved = 0;
        $totalFailed = 0;

        foreach($knockout->questions as $question) {
            foreach ($question->answers as $answer) {
                try {
                    $s = $answer->delete();
                    if ($s == true) $totalRemoved++;
                } catch (Exception $e) {
                    $totalFailed++;
                }
            }
        }

        return redirect()->back()->with('message', 'Removed '.$totalRemoved. ' answers and skipped '. $totalFailed .' from knockout '. $knockout->id);
    }

    /**
     * TODO function not being used yet
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function perform(Request $request){
        $message = '';
        $count = 0;
        $knockoutIds = $request->input('knockout');

        if($knockoutIds && is_array($knockoutIds)){
            $knockouts = Knockout::find($knockoutIds);

            /**
             * TODO: build stuff here
             */

            switch($request->input('perform')){
                case 'export':
                    $message = 'Exported ';
                    break;
                case 'clear_score':
                    $message = 'Cleared scores of ';
                    break;
                case 'report':
                    $message = 'Build Report of ';
                    break;
                case 'delete':
                    $message = 'Deleted ';
                    break;
            }
        }

        return redirect()->back()->with('message', $message . $count . ' knockouts');
    }

    public function export(){
        $knockouts = Knockout::all();
        $count = count($knockouts);

        if($count > 0){
            $export = new KnockoutExport($knockouts,'all');
            $export->export();

        }

        return redirect()->back()->with('message','Exporting '.$count.' knockouts');
    }

    public function import(Request $request){
        if ($request->hasFile('knockout_upload') && $request->file('knockout_upload')->isValid()) {

            $import = new KnockoutImport($request->file('knockout_upload'));

            if ($import->isValid()) {

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors(['no_file' => 'No or wrong file uploaded']);
    }

    public function report(){
        $knockouts = Knockout::all();

        if(count($knockouts) > 0){
            $report = new KnockoutResultReport($knockouts);

            $report->report();
        }

        return redirect()->back()->with(['message', 'No knockouts to report results of']);
    }

    public function reportSingle($id){
        return redirect()->back()->with('message','Download Result does not yet exist for Knockout');
    }
}
