<?php

use Illuminate\Database\Seeder;

class OpenQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->command->info('Seeding the database with random open questions');
        factory('App\OpenQuestion', 5)->create()->each(function($oq) {
            for($i = 0; $i < 15; $i++) {
                $oq->answers()->save(factory('App\OpenQuestionAnswer')->make());
            }
        });

    }
}
