<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderColumnQuizSurveyKnockoutTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('quiz_questions', function(Blueprint $table){
            $table->integer('order')->default(0)->after('choices');
        });

        Schema::table('survey_questions', function(Blueprint $table){
            $table->integer('order')->default(0)->after('data');
        });

        Schema::table('knockout_questions', function(Blueprint $table){
            $table->integer('order')->default(0)->after('choices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('quiz_questions', function(Blueprint $table){
            $table->dropColumn('order');
        });

        Schema::table('survey_questions', function(Blueprint $table){
            $table->dropColumn('order');
        });

        Schema::table('knockout_questions', function(Blueprint $table){
            $table->dropColumn('order');
        });
    }
}
