@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Emails</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.emails.index') }}">Emails</a></li>
        <li>Log {{ $email->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">

        <table class="table table-hover">
            <thead>
            <th>Batch id</th>
            <th>Created at</th>
            <th>Users</th>
            <th>Still Processing</th>
            <th>Failed</th>
            </thead>
            <tbody>
            @foreach($logs as $batch => $log)
                <tr data-toggle="modal" data-target='#modal-batch-{{$batch}}'>
                    <td>{{ $batch }}</td>
                    <td>{{ $log['created_at'] }}</td>
                    <td>{{ $log['users'] }}</td>
                    <td>{{ $log['processing'] }}</td>
                    <td>{{ $log['failures'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="form-group">
            <div class="col-sm-12 breadcrumb breadcrumb-footer">
                <a href="{{ URL::route('backend.emails.index') }}" class="btn btn-default">Back</a>
            </div>
        </div>
    </div>
    @foreach( $logs as $batch => $data )

        <div class="modal fade" id="modal-batch-{{$batch}}">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Batch {{$batch}}</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover" id="emails">
                            <thead>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Error</th>
                            </thead>
                            <tbody>
                            @foreach($data['rows'] as $row)
                                @if(!is_object($row) || !is_object($row->user))
                                    @continue
                                @endif
                                <tr data-toggle="modal" data-target='#modal-batch-{{$batch}}'>
                                    <td>{{ $row->user->name }}</td>
                                    <td>{{ $row->user->email }}</td>
                                    <td>{{ $row->status }}</td>
                                    <td>{{ $row->error }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('scripts')
@endsection