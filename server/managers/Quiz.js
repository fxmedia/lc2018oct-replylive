const GameManager = require('./Game');

class QuizManager extends GameManager {
    constructor(Admin) {
        super(Admin, "quiz");
        this.currentState = { questionIdx: 0, locked: false };
        this.privateState = { answers: [] };
    }

    addAnswer(answer) {
        const existingAnswerIdx = this.privateState.answers.findIndex((_answer) => answer.user_id == _answer.user_id);
        if(existingAnswerIdx === -1) {
            this.privateState.answers.push(answer);
        } else {
            this.privateState.answers[existingAnswerIdx] = answer;
        }
    }

    checkAnswer(action, data) {
        this.currentState.locked = true;
        this.broadcastToUsers(action);
        this.Admin.broadcastToChannel("display", action);
    }

    handle(action, data) {
        switch(action) {
            case 'quiz_start':
                this.start(action, data);
                break;
            case 'quiz_show_question':
                this.showQuestion(action, data);
                break;
            case 'quiz_check_answer':
                this.checkAnswer(action, data);
                break;
            case 'quiz_stop':
                this.stop(action);
                break;
            case 'quiz_toggle_results':
                this.toggleResults(action);
                break;
        }
    }

    reset() {
        this.currentState = { questionIdx: 0, locked: false };
    }

    toggleResults(action) {
        const results = this.Admin.managers.user.getUsers()
        .filter((user) => user.attending && user.attending.status == "attending")
        .map((user) => {
            return {
                points: (user.extra && user.extra.points) ? user.extra.points : 0,
                name: user.name,
                image_path: user.image_path,
                id: user.id
            };
        })
        .sort((a, b) => parseInt(b.points) - parseInt(a.points));

        this.Admin.broadcastToChannel("display", action, { results });
    }

    showQuestion(action, data) {
        this.currentState.questionIdx = parseInt(data.question_index);
        this.currentState.locked = false;
        this.privateState.answers = [];
        this.broadcastToUsers(action, data);
        this.Admin.broadcastToChannel("display", action, data);
    }

    start(action, data) {
        this.reset();
        this.setBroadcastLevel(data.quiz);
        const extraction = {
            user: this.extract(data, [
                {key: 'quiz', keys: ["questions", "id"]},
                {key: 'question_index', keys: null}
            ]),
            display: this.extract(data, [
                {key: 'quiz', keys: ["questions", "id"]},
                {key: 'question_index', keys: null}
            ])
        };
        this.broadcastToUsers(action, extraction.user);
        this.Admin.broadcastToChannel("display", action, extraction.display);
        this.Admin.setActiveAction({ action, data: extraction });
    }

    status() {
        if(!this.Admin.activeAction || this.Admin.getManager(this.Admin.activeAction.action) !== this.namespace) {
            return { quiz_id: false };
        }
        return {
            quiz_id: this.Admin.activeAction.data.user.quiz.id,
            question_index: this.currentState.questionIdx,
            locked: this.currentState.locked
        };
    }

    stop(action) {
        if(this.isActive()) {
            this.broadcastToUsers(action);
            this.Admin.broadcastToChannel("display", action);
            this.Admin.setActiveAction(null);
        }
    }

    submitAnswer(answer, userSocket) {
        this.Admin.save('/quiz/answer', answer, userSocket.id)
        .catch((err) => {
            this.Admin.sendError(userSocket, err);
            const userId = this.Admin.getConnection("user", userSocket.id).id;
            this.Admin.broadcastToUsers([userId], 'quiz_reset_answer');
        })
        .then((result) => {
            if(result.data.success) {
                this.addAnswer({choice: result.data.answer.choice, user_id: result.data.answer.user_id});
                this.Admin.broadcastToUsers([result.data.answer.user_id], 'quiz_set_answer', answer.choice);
                this.Admin.broadcastToChannel("display", "quiz_add_answer");
                const activeQuestion = this.Admin.activeAction.data.user.quiz.questions[this.currentState.questionIdx];
                if (result.data.answer.choice == activeQuestion.correct_answer) {
                    this.Admin.managers.user.addPoints(result.data.answer.user_id, activeQuestion.points);
                }
            } else {
                this.Admin.sendError(userSocket, "The answer could not be saved..");
                const userId = this.Admin.getConnection("user", userSocket.id).id;
                this.Admin.broadcastToUsers([userId], 'quiz_reset_answer');
            }
        });
    }
}

module.exports = QuizManager;
