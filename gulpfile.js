var gulp        = require('gulp'),
// sass
    sass        = require('gulp-sass'),
    autoprefix  = require('gulp-autoprefixer'),
    sourcemaps  = require('gulp-sourcemaps'),
// js
    uglify      = require('gulp-uglify'),
    webpack     = require('webpack-stream'),//require('gulp-webpack'),
    webpack2    = require('webpack');
// images
    imagemin    = require('gulp-imagemin'),
// utils
    plumber     = require('gulp-plumber'),
    rename      = require('gulp-rename'),
    replace     = require('gulp-replace'),
    env         = require('dotenv').config({path: './.env'}).parsed,
//del         = require('del'),
// minify_css  = require('gulp-minify-css'),
    path        = require('path');

console.log('starting gulp with env:');

var paths = {
    // watch
    WATCH_SASS_PATH_BACKEND:    './resources/assets/sass/backend/**/*.scss',
    WATCH_SASS_PATH_FRONTEND:   './resources/assets/sass/frontend/**/*.scss',
    WATCH_JS_PATH_BACKEND:      './resources/assets/js/backend/**/*.*', // include everything because TinyMCE needs it's css and fonts
    WATCH_JS_PATH_FRONTEND:     './resources/assets/js/frontend/**/*.{js,jsx}',
    WATCH_JS_PATH_LOADER:       './resources/assets/js/loader/**/*.{js,jsx}',
    WATCH_JS_PATH_HELPDESK:     './resources/assets/js/helpdesk/**/*.{js,jsx}',
    WATCH_JS_PATH_SCANNER:       './resources/assets/js/scanner/**/*.{js,jsx}',
    WATCH_IMAGES_PATH:          './resources/assets/images/**/*.*',
    WATCH_CSS_PATH:             './resources/assets/css/**/*.css', // css library files which are being used by the backend. we might want to get rid of these in the future
    WATCH_FONTS_PATH:           './resources/assets/fonts/',
    WATCH_ROOT_PATH:            './resources/assets/root_files/',
    // Destinations
    DEST_CSS:                   './public/css',
    DEST_JS:                    './public/js',
    DEST_JS_BACKEND:            './public/js/backend',
    DEST_IMAGES:                './public/images',
    DEST_FONTS:                 './public/fonts',
    DEST_ROOT:                  './public',
    // webpack
    WP_ENTRY_USER:              './resources/assets/js/frontend/user.js',
    WP_ENTRY_DISPLAY:           './resources/assets/js/frontend/display.js',
    WP_ENTRY_HELPDESK:           './resources/assets/js/helpdesk/helpdesk.js',
    WP_ENTRY_SCANNER:           './resources/assets/js/scanner/qrscanner.js'
};

var webpack_conf_frontend = {
    entry: {
        user: paths.WP_ENTRY_USER,
        polyfill: "babel-polyfill",
        display: paths.WP_ENTRY_DISPLAY
    },
    output: {
        filename: '[name].js'
    },
    devtool: 'eval',
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    }
};

var webpack_conf_frontend_dist = {
    entry: {
        polyfill: "babel-polyfill",
        user: paths.WP_ENTRY_USER,
        display: paths.WP_ENTRY_DISPLAY
    },
    output: {
        filename: '[name].js'
    },
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    },
    plugins: [
        new webpack2.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack2.optimize.UglifyJsPlugin()
    ]
};

var webpack_conf_helpdesk = {
    entry: {
        polyfill: "babel-polyfill",
        helpdesk: paths.WP_ENTRY_HELPDESK
    },
    output: {
        filename: '[name].js'
    },
    devtool: 'eval',
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    }
};

var webpack_conf_helpdesk_dist = {
    entry: {
        polyfill: "babel-polyfill",
        helpdesk: paths.WP_ENTRY_HELPDESK,
    },
    output: {
        filename: '[name].js'
    },
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    },
    plugins: [
        new webpack2.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack2.optimize.UglifyJsPlugin()
    ]
};


var webpack_conf_scanner = {
    entry: {
        polyfill: "babel-polyfill",
        qrscanner: paths.WP_ENTRY_SCANNER
    },
    output: {
        filename: '[name].js'
    },
    devtool: 'eval',
    node: {
        fs: "empty"
    },
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    }
};

var webpack_conf_scanner_dist = {
    entry: {
        polyfill: "babel-polyfill",
        qrscanner: paths.WP_ENTRY_SCANNER
    },
    output: {
        filename: '[name].js'
    },
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    },
    node: {
        fs: "empty"
    },
    plugins: [
        new webpack2.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('production')
        }),
        //new webpack2.optimize.UglifyJsPlugin()
    ]
};

var webpack_conf_backend = {
    entry: {
        polyfill: "babel-polyfill",
        main: './resources/assets/js/backend/main.js'
    },
    output: {
        filename: '[name].js'
    },
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    },
    plugins: [
        new webpack2.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack2.optimize.UglifyJsPlugin()
    ]
};

//_____General_Functions_______________________________________________________________________________________________/
var replaceCssPlaceholders = function(){
    return replace('[[!!BASE_PATH!!]]', env.APP_URL);
};


//_____Build_Dev_Tasks_________________________________________________________________________________________________/
gulp.task('build_css_dev_backend', function(){
    gulp.src(paths.WATCH_SASS_PATH_BACKEND)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefix(['last 1 version', '> 1%', 'ie 9']))
        .pipe(replaceCssPlaceholders())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.DEST_CSS))

});
gulp.task('build_css_dev_frontend', function(){
    gulp.src(paths.WATCH_SASS_PATH_FRONTEND)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefix(['last 1 version', '> 1%', 'ie 9']))
        .pipe(replaceCssPlaceholders())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.DEST_CSS))

});

gulp.task('build_js_dev_backend', function(){
    gulp.src(paths.WATCH_JS_PATH_BACKEND)
        .pipe(plumber())
        //.pipe(webpack(webpack_conf_backend))
        .pipe(gulp.dest(paths.DEST_JS_BACKEND));

});
gulp.task('build_js_dev_frontend', function(){
    gulp.src(paths.WATCH_JS_PATH_FRONTEND)
        .pipe(webpack(webpack_conf_frontend))
        .pipe(gulp.dest(paths.DEST_JS));

});
gulp.task('build_js_dev_loader', function(){
    gulp.src(paths.WATCH_JS_PATH_LOADER)
        .pipe(plumber())
        .pipe(gulp.dest(paths.DEST_JS));

});

gulp.task('build_js_dev_helpdesk', function(){
    gulp.src(paths.WATCH_JS_PATH_HELPDESK)
        .pipe(webpack(webpack_conf_helpdesk))
        .pipe(gulp.dest(paths.DEST_JS));
});

gulp.task('build_js_dev_scanner', function(){
    gulp.src(paths.WATCH_JS_PATH_SCANNER)
        .pipe(webpack(webpack_conf_scanner))
        .pipe(gulp.dest(paths.DEST_JS));
});

gulp.task('place_images', function(){
    gulp.src(paths.WATCH_IMAGES_PATH)
        .pipe(gulp.dest(paths.DEST_IMAGES));
});

gulp.task('place_css', function(){
    gulp.src(paths.WATCH_CSS_PATH)
        .pipe(plumber())
        .pipe(gulp.dest(paths.DEST_CSS));
});

//_____Build_Dist_Tasks________________________________________________________________________________________________/
gulp.task('place_root_files', function(){
    gulp.src([paths.WATCH_ROOT_PATH+'**/*.*', '!'+ paths.WATCH_ROOT_PATH +'htaccess.txt'])
        .pipe(plumber())
        .pipe(gulp.dest(paths.DEST_ROOT));

    gulp.src(paths.WATCH_ROOT_PATH+'htaccess.txt')
        .pipe(rename('.htaccess'))
        .pipe(gulp.dest('public/'));
});
gulp.task('place_font_files', function(){
    gulp.src([paths.WATCH_FONTS_PATH+'**/*.**'])
        .pipe(plumber())
        .pipe(gulp.dest(paths.DEST_FONTS));
});

gulp.task('build_css_dist', function(){
    gulp.src(paths.WATCH_SASS_PATH_BACKEND)
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefix(['last 1 version', '> 1%', 'ie 9']))
        .pipe(replaceCssPlaceholders())
        .pipe(gulp.dest(paths.DEST_CSS));

    gulp.src(paths.WATCH_SASS_PATH_FRONTEND)
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefix(['last 1 version', '> 1%', 'ie 9']))
        .pipe(replaceCssPlaceholders())
        .pipe(gulp.dest(paths.DEST_CSS));

});

gulp.task('build_js_dist', function(){
    gulp.src(paths.WATCH_JS_PATH_BACKEND)
        .pipe(plumber())
        //.pipe(webpack(webpack_conf_backend))
        //.pipe(uglify()) // uglify here doesn't work??
        .pipe(gulp.dest(paths.DEST_JS_BACKEND));

    gulp.src(paths.WATCH_JS_PATH_FRONTEND)
        .pipe(webpack(webpack_conf_frontend_dist))
        .pipe(uglify())
        .pipe(gulp.dest(paths.DEST_JS));

    gulp.src(paths.WATCH_JS_PATH_HELPDESK)
        .pipe(webpack(webpack_conf_helpdesk_dist))
        //.pipe(uglify())
        .pipe(gulp.dest(paths.DEST_JS));

    gulp.src(paths.WATCH_JS_PATH_SCANNER)
        .pipe(webpack(webpack_conf_scanner_dist))
        //.pipe(uglify())
        .pipe(gulp.dest(paths.DEST_JS));

    gulp.src(paths.WATCH_JS_PATH_LOADER)
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest(paths.DEST_JS));

    gulp.src(paths.WATCH_JS_PATH_SCANNER)
        .pipe(plumber())
        //.pipe(uglify())
        .pipe(gulp.dest(paths.DEST_JS));

});

gulp.task('place_and_minify_images', function(){
    gulp.src(paths.WATCH_IMAGES_PATH)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.DEST_IMAGES));

});

gulp.task('place_and_uglify_css', function(){
    gulp.src(paths.WATCH_CSS_PATH)
        .pipe(plumber())
        //.pipe(minify_css({compatibility: 'ie9'})) // do we want an uglifier here?
        .pipe(gulp.dest(paths.DEST_CSS));
});
//_____Dist_Tasks______________________________________________________________________________________________________/
gulp.task('clean', function(){
    //return del( paths.CLEANUP_PATHS );
});

gulp.task('build_dist', ['place_root_files', 'place_font_files', 'build_css_dist', 'build_js_dist', 'place_and_minify_images', 'place_and_uglify_css']);

//_____Dev_Tasks______________________________________________________________________________________________________/
gulp.task('build_dev', ['place_root_files', 'place_font_files','build_css_dev_backend', 'build_css_dev_frontend', 'build_js_dev_backend', 'build_js_dev_frontend', 'build_js_dev_loader', 'build_js_dev_helpdesk', 'build_js_dev_scanner','place_images', 'place_css']);

gulp.task('watch', function(){
    gulp.watch(paths.WATCH_ROOT_PATH, ['place_root_files']);
    gulp.watch(paths.WATCH_FONTS_PATH, ['place_font_files']);

    gulp.watch(paths.WATCH_SASS_PATH_BACKEND, ['build_css_dev_backend']);
    gulp.watch(paths.WATCH_SASS_PATH_FRONTEND, ['build_css_dev_frontend']);

    gulp.watch(paths.WATCH_JS_PATH_BACKEND, ['build_js_dev_backend']);
    gulp.watch(paths.WATCH_JS_PATH_FRONTEND, ['build_js_dev_frontend']);
    gulp.watch(paths.WATCH_JS_PATH_LOADER, ['build_js_dev_loader']);
    gulp.watch(paths.WATCH_JS_PATH_HELPDESK, ['build_js_dev_helpdesk']);
    gulp.watch(paths.WATCH_JS_PATH_SCANNER, ['build_js_dev_scanner']);

    gulp.watch(paths.WATCH_IMAGES_PATH, ['place_images']);

    gulp.watch(path.WATCH_CSS_PATH, ['place_css']);
});

//_____Public_Tasks____________________________________________________________________________________________________/
gulp.task('dev', ['build_dev', 'watch']);
gulp.task('dist', ['clean', 'build_dist']);
//gulp.task('server', ['node_server']);
gulp.task('default', ['watch']);
