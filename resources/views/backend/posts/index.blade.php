@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Posts</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Posts</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.posts._partials.aside')
    </div>
    <div class="col-xs-9">
        <div class="show-wall">
            <h3>Show Posts on Display</h3>
            <div class="form-switch">
                <input class="toggle-display on" id="postsOn" type="radio" name="showDisplay" value="1" @if($showDisplayPosts == '1') checked @endif>
                <input class="toggle-display off" id="postsOff" type="radio" name="showDisplay" value="0" @if(!$showDisplayPosts == '1') checked @endif>
                <div class="switch">
                    <label class="on" for="postsOff">On</label>
                    <label class="off" for="postsOn">Off</label>
                </div>
            </div>
        </div>
        @include('backend._partials.errors')
        <div class="posts">
            <h3>Posts by Users</h3>
            {!! Form::open(['route'=>['backend.posts.perform'], 'id'=>'postsTable', 'method'=>'POST']) !!}
            <table class="table table-hover">
                <thead>
                    @include('backend._partials.table-top-actions')
                    <tr>
                        <th>&nbsp;</th>
                        <th>Visibility</th>
                        <th>Highlight</th>
                        <th>Message</th>
                        <th>Image</th>
                        <th>Type</th>
                        <th>User</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                        <tr id="post_{{ $post->id }}" class="{{ (!$post->visible ? 'greyed-out' : '') }}">
                            <td>{!! Form::checkbox('post[]', $post->id, false, ['class' => 'selectable', 'id' => 'checkbox'.$post->id]) !!}</td>
                            <td><a class="toggle-post-visibility" href="{{ route('backend.posts.toggle_visibility',[$post->id]) }}" data-post-id="{{ $post->id }}"><i class="fa fa-eye{{ (!$post->visible)? '-slash' : '' }}"></i></a></td>
                            <td><a class="toggle-post-highlight btn @if($post->isHighlight) btn-success @else btn-primary @endif btn-xs" data-post-id="{{ $post->id }}"><i class="fa @if($post->isHighlight) fa-heart @else fa-heart-o @endif"></i></a></td>
                            <td>{{ $post->message }}</td>
                            <td>{{ $post->image }}</td>
                            <td>{{ $post->type }}</td>
                            <td>{{ $post->user->name }}</td>
                            <td>
                                {{--<a class="btn btn-xs btn-success" href="{{ route('backend.posts.edit', [$post->id] ) }}">--}}
                                    {{--<span class="glyphicon glyphicon-edit"></span>--}}
                                {{--</a>--}}
                                <a class="btn btn-xs btn-danger form-action" href="#"
                                   data-action="delete"
                                   data-username="{{ $post->message }}"
                                   data-id="{{ $post->id }}"
                                   data-form="postsTable">
                                    <span class="glyphicon glyphicon-trash"></span>
                                    Remove
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                @include('backend.posts._partials.table-bottom-action')
            </table>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        var api_routes = {
            'other': {
                lowlight: '{{ route('api.post.highlight') }}',
                toggle_display: '{{ route('api.post.toggle-display') }}'
            }
        };
        @foreach($posts as $post)
            api_routes['{{ $post->id }}'] = {
                highlight: '{{ route('api.post.highlight', [$post->id]) }}',
                toggle_visibility: '{{ route('api.post.toggle-visibility', [$post->id]) }}'
        };
        @endforeach
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/posts.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>
@endsection
