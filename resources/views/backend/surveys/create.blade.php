@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Surveys</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.surveys.index') }}">Surveys</a></li>
        <li>New Survey</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.surveys._partials.survey-form')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>
@endsection