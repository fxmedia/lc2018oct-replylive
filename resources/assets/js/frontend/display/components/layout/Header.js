import React from 'react';
import config from '../../../config';

export default function Header(props) {

    const renderSecondaryLogo = () => {
        return (
            <div className="logo-wrapper secondary">
                <img src={"/images/logo-secondary.png"} />
            </div>
        )
    }

    return (
        <div className="Header">
            <div className="ad">
                <img src={`${config.base_url}/images/reply-live-logo.png`} />
            </div>
            <div className="logo-wrapper">
                <img src={"/images/logo-display.png"} />
            </div>
        </div>
    )
}
