import React from 'react';
import config from '../../../config';

export default class Ad extends React.Component {
    constructor(props) {
        super(props);
        this.state = { expand: false };
    }

    expand() {
        this.setState({ expand: true });
    }

    close() {
        this.setState({ expand: false });
    }

    renderBigAd() {
        return (
            <div className={(this.state.expand) ? "big-ad expand" : "big-ad"}>
                <div className="close" onClick={this.close.bind(this)}>
                    <img src={`${config.base_url}/images/close.png`} />
                </div>
                <div className="image-wrapper">
                    <img src={`${config.base_url}/images/replylive-big.png`} />
                </div>
                <div className="body">
                    <p className="highlight">
                        <strong>Reply.live is the new platform for live events</strong>
                    </p>
                    <p>
                        Create the ultimate event experience for your visitors. Reply.live is a platform on which visitors, speakers and the organisation of the event get connected in real-time.
                        <br /><br />
                        Reply.live is powered by FX: <a href="https://fxmedia.nl" target="_blank">https://fxmedia.nl</a>
                    </p>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className={(this.state.expand) ? "Ad expand" : "Ad"} onClick={(!this.state.expand) ? this.expand.bind(this) : false}>
                {this.renderBigAd()}
                <div className="message">
                    <p>powered by</p>
                    <img src={`${config.base_url}/images/replylive.png`} />
                    <img src={`${config.base_url}/images/info-circle.png`} />
                </div>
            </div>
        )
    }

}
