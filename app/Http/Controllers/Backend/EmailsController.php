<?php

namespace App\Http\Controllers\Backend;

use App\Email;
use App\EmailLog;
use App\Export\EmailExport;
use App\Group;
use App\Helpers\ImageHelper;
use App\Import\EmailImport;
use App\Report\EmailLogReport;
use App\Senders\EmailSender;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Session;
use Storage;

class EmailsController extends Controller
{
    //
    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $emails = Email::all();
        $sent = EmailLog::whereStatus('done')->whereNull('error')->count();
        $processing = EmailLog::where('status' ,'!=' ,'done')->count();
        $failed = EmailLog::whereNotNull('error')->count();

        return view('backend.emails.index', compact('emails','sent', 'processing','failed'));
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function setup($id){
        $email = Email::find($id);

        if($email !== null){
            $users = User::with('groups:id')->get();
            $groups = Group::all();

            return view('backend.emails.setup', compact('email','users','groups'));
        }

        return redirect()->back()->withErrors(['message' => 'unable to setup non existing email']);
    }

    public function preview(Request $request, $id){
        $email = Email::find($id);

        if($email === null){
            return redirect()->back()->withErrors(['message' => 'Oops, something went wrong with the selected email.']);
        }

        $idListString = $request->input('id_list');
        if($idListString === null){
            return redirect()->back()->withErrors(['message' => 'No users are selected to send an email to.']);
        }

        $users = User::find(explode(',', $idListString));
        if($users === null){
            return redirect()->back()->withErrors(['message' => 'Error finding selected users in the database.']);
        }

        $sendUsers = [];
        $skipUsers = [];
        $sendUserIds = [];
        foreach($users as $user){
            if($user->email === null){
                $skipUsers[] = $user;
            } else {
                $sendUsers[] = $user;
                $sendUserIds[] = $user->id;
            }
        }

        if(count($sendUsers) === 0){
            return redirect()->back()->withErrors(['message' => 'No users were selected with an email address']);
        }

        $request->session()->put('email.users', $sendUserIds);

        return view('backend.emails.preview', compact('email','sendUsers', 'skipUsers'));
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function log($id){
        $email = Email::find($id);

        if($email !== null){
            $data = [];
            $ls = EmailLog::with('user')->where('email_id',$email->id)->get();

            foreach ( $ls as $log ) {
                if ( ! isset( $data[ $log->batch ] ) ) {
                    $data[ $log->batch ] = [
                        'created_at' => $log->created_at,
                        'users'      => 0,
                        'processing' => 0,
                        'failures'   => 0,
                        'rows'       => []
                    ];
                }
                if ( $log->error ) {
                    $data[ $log->batch ]['failures'] ++;
                }
                if ( $log->status !== 'done') {
                    $data[ $log->batch ]['processing'] ++;
                }
                $data[ $log->batch ]['users'] ++;
                $data[ $log->batch ]['rows'][] = $log;
            }

            $logs = array_reverse($data, true);
            return view('backend.emails.log', compact('email', 'logs'));
        }

        return redirect()->back()->withErrors(['message' => 'unable to read log of non existing email']);
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){
        $actions = Email::getActions();
        $templateViews = Email::getLayoutOptions();
        $settings = Setting::getSettingsAsObject();
        $images = Email::getImageList();
        $attachments = Email::getAttachmentsList();

        return view('backend.emails.create', compact('actions', 'templateViews', 'settings', 'images', 'attachments'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $validators = config('validation.emails.store');

        $data = $request->validate($validators);

        $email = new Email($data);

        $this->handleUploads($request, $email);

        if(!$email->save()){
            return redirect()->back()->withErrors(['message'=>'Error saving new email']);
        }

        return redirect()->route('backend.emails.index')->with(['message' => 'Successfully created new Email']);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id){
        $email = Email::find($id);

        if($email !== null){
            $actions = Email::getActions();
            $templateViews = Email::getLayoutOptions();
            $settings = Setting::getSettingsAsObject();
            $images = Email::getImageList();
            $attachments = Email::getAttachmentsList();

            return view('backend.emails.edit', compact('email', 'actions', 'templateViews', 'settings', 'images', 'attachments'));
        }

        return redirect()->back()->withErrors(['message' => 'Unable to find selected Email']);
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id){
        $email = Email::find($id);

        if($email === null){
            return redirect()->back()->withErrors(['message' => 'Unable to update non existing Email']);
        }

        $validators = config('validation.emails.store');

        $data = $request->validate($validators);

        $email->fill($data);
        $this->handleUploads($request, $email);

        if($email->save()){
            return redirect()->route('backend.emails.index')->with(['message' => 'Successfully updated email']);
        }

        return redirect()->back()->withErrors(['message' => 'Error saving updates to email']);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function remove($id){
        $email = Email::find($id);

        if($email === null){
            return redirect()->route('backend.emails.index')->withErrors(['message' => 'Unable to remove non existing email']);
        }

        try{
            $removed = $email->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Email has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing email.']);
    }

//    public function perform(Request $request){
//
//    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function export(){
        $emails = Email::all();
        $count = count($emails);

        if($count > 0){
            $export = new EmailExport($emails, 'all');
            $export->export();
        }

        return redirect()->back()->with('message','Exporting '.$count.' emails');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function import(Request $request){
        if ($request->hasFile('email_upload') && $request->file('email_upload')->isValid()) {

            $import = new EmailImport($request->file('email_upload'));

            if ($import->isValid()) {

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors(['no_file' => 'No or wrong file uploaded']);
    }

    public function report(){
        $emails = Email::all();

        if(count($emails) > 0){
            $report = new EmailLogReport($emails);

            $report->report();
        }

        return redirect()->back()->with(['message'=>'No Emails to report results of']);
    }

    public function reportSingle($id){
        $email = Email::find($id);

        if($email !== null){
            $report = new EmailLogReport([$email]);
            $report->report();
        }

        return redirect()->back()->with(['message' => 'No Email to report results of']);
    }

    public function ajaxSend(Request $request, $id){
        $userIds = $request->session()->get('email.users');

        if($userIds === null || count($userIds) === 0){
            return response()->json(['success' => false, 'error' => ['message' => 'No users found in session to send an email to']]);
        }

        $users = User::find($userIds);
        if($users === null){
            return response()->json(['success' => false, 'error' => ['message' => 'Unable to find selected users in the database']]);
        }

        $email = Email::find($id);
        if($email === null){
            return response()->json(['success' => false, 'error' => ['message' => 'Oops, email is invalid']]);
        }

        if($batch = EmailSender::batch($email, $users)){
            return response()->json(['success'=>true,
                'batch' => $batch,
                'email_id' => $email->id,
                'statusUrl' => route('backend.emails.ajax-batch-status'),
                'logUrl' => route('backend.emails.log', [$email->id]),
                'totalUsers' => count($users)
            ]);
        }

        return response()->json(['success'=>false, 'error'=> ['message' => 'Failed creating an email batch']]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxBatchStatus(Request $request){
        $email_id = $request->input('email_id');
        $batch = $request->input('batch');
        $logs = EmailLog::with('user:id,firstname,lastname,email')
            ->whereEmailId($email_id)
            ->whereBatch($batch)
            ->get();

        if(count($logs)){
            return response()->json(['success' => true, 'logs'=>$logs->toArray()]);
        }

        return response()->json(['success'=>false, 'error'=>['message' => 'Unable to get logs from email '.$email_id.' with batch '.$batch]]);
    }

    /**
     * @param Request $request
     * @param Email $email
     */
    private function handleUploads(Request $request, Email $email){
        foreach(['header_image_upload', 'footer_image_upload', 'attachment_upload'] as $name){
            // should we do a request->validate here first?
            if($request->hasFile($name) && $request->file($name)->isValid()){
                $file = $request->file($name);
                $name = str_replace('_upload','', $name);

                if($name === 'attachment'){
                    $subFolder =  config('storage.email_attachments');
                    $path = storage_path('uploads/' . $subFolder . '/' );
                    if(!Storage::disk('uploads')->has($subFolder) ){
                        Storage::disk('uploads')->makeDirectory($subFolder);
                    }

                    $filename = substr( md5( rand() . time() ), 0, 3 ) . '_' . $file->getClientOriginalName();
                    $file->move($path, $filename );

                    $email->{$name} = $filename;
                } else {
                    $imageHelper = new ImageHelper($file, 'email_images');

                    if( $request->input('resize_'.$name) !== null){
                        $imageHelper->convert(false)
                            ->resizeWallUpload( Setting::getValue('email_image_size') )
                            ->moveToStorage();
                    } else {
                        $imageHelper->convert(false)
                            ->moveToStorage();

                    }
                    //
                    $email->{$name} = $imageHelper->getFilename();
                }
            }
        }
    }
}
