<?php

namespace App\Export;



class PostExport extends BaseExport
{
    private $filename;
    private $headers = ['type', 'message', 'image', 'user', 'created_at', 'visible'];
    private $rows;

    /**
     * TODO: should this be a Report instead of an Export?
     */

    public function __construct($posts, $filename = 'all')
    {
        $this->filename = 'export-posts-'.$filename;


        foreach($posts as $post){
            $this->rows[] = [
                $post->type,
                $post->message,
                $post->image,
                $post->user->name,
                $post->created_at,
                $post->visible
            ];
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}