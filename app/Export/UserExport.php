<?php

namespace App\Export;

class UserExport extends BaseExport
{
    private $filename;
    private $headers = [
        'Firstname',
        'Lastname',
        'Email',
        'Phonenumber',
        'Token',
        'Created',
        'Attending status',
        'Groups',
        'Test user'
    ];
    private $rows;

    public function __construct($users, $filename)
    {
        foreach($users as $user){

            $groups = '';
            foreach($user->groups as $group){
                $groups .= $group->name.', ';
            }
            if(strlen($groups) > 2) {
                $groups = substr($groups, 0, -2);
            }

            $data = [
                'Firstname'     => $user->firstname,
                'Lastname'      => $user->lastname,
                'Email'         => $user->email,
                'Phonenumber'   => $user->phonenumber,
                'Token'         => $user->token,
                'Attending status' => $user->attending->status,
                'Created'       => $user->created_at,
                'Groups'        => $groups,
                'Test user'     => $user->test_user
            ];

            $extra = $user->extra;
            if(is_array($extra)) {

                // Loop all extra to headers and add extra if not there yet
                foreach ($extra as $key => $value) {
                    $headerOffset = array_search($key, $this->headers);
                    if($headerOffset === false) {
                        $this->headers[] = $key;
                    }

                    is_array($value)
                        ? $data[$key] = implode(', ', $value)
                        : $data[$key] = $value;
                }
            }

            $row = [];
            foreach($this->headers as $rowName){
                $row[] = isset($data[$rowName])? $data[$rowName] : '';
            }

            $this->rows[] = $row;
        }


        $this->filename = 'export-users-'.$filename;
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}