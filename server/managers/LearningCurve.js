const GameManager = require('./Game');

class LearningCurveManager extends GameManager {
    constructor(Admin) {
        super(Admin, "curve");
        this.currentState = { filter: ['users'], users: {} };
        this.privateState = { totalAnswers: [], totalUsers: 0 };
        this.phase = null;
    }

    handle(action, data) {
        switch(action) {
            case 'curve_start':
                this.start(action, data);
                break;
            case 'curve_stop':
                this.stop(action, data);
                break;
        }
    }

    calcParticipatingUsers() {
        const users = this.Admin.managers.user.getUsers();
        return users.filter((user) => {
            const userGroupIds = (user.groups) ? user.groups.map((group) => group.id) : [];
            return this.Admin.managers.user.hasMatchingGroup(this.group_ids, userGroupIds)
                && this.Admin.managers.user.isAttending(user);
        });
    }

    start(action, data) {
        this.setBroadcastLevel(data);
        const totalAnswers = Array(data.curve.questions.length).fill(0);
        data.totalUsers = this.calcParticipatingUsers().length;
        data.totalAnswers = totalAnswers.slice();
        this.privateState.totalAnswers = totalAnswers.slice();
        this.privateState.totalUsers = data.totalUsers;
        const extraction = {
            user: this.extract(data, [
                {key: 'curve', keys: ["questions", "id"]},
                {key: 'phase', keys: null}
            ]),
            display: this.extract(data, [
                {key: 'curve', keys: ["questions", "id", "name"]},
                {key: 'totalUsers', keys: null},
                {key: 'totalAnswers', keys: null}
            ])
        };
        this.phase = data.phase;
        this.broadcastToUsers(action, extraction.user);
        this.Admin.broadcastToChannel("display", action, extraction.display);
        this.Admin.setActiveAction({ action, data: extraction });
    }

    status() {
        if(!this.Admin.activeAction || this.Admin.getManager(this.Admin.activeAction.action) != this.namespace) {
            return { curve_id: false };
        }

        return { curve_id: this.Admin.activeAction.data.user.curve.id, phase: this.Admin.activeAction.data.user.phase };
    }

    resetUserQuestionCount() {
        Object.keys(this.currentState.users).map((userId) => {
            Object.keys(this.currentState.users[userId]).map((curveId) => {
                this.currentState.users[userId][curveId].question_index = 0;
            });
        });
    }

    reset() {
        if(this.phase === "post") {
            this.currentState.users = {};
        } else {
           this.resetUserQuestionCount();
        }
        this.privateState = { totalAnswers: [], totalUsers: 0 };
    }

    stop(action, data) {
        if(this.isActive()) {
            this.reset();
            this.broadcastToUsers(action, this.Admin.activeAction.data.user);
            this.Admin.broadcastToChannel("display", action);
            this.Admin.setActiveAction(null);
        }
    }

    submitAnswer(answer, userSocket) {
        console.log("sending: ", answer);
        this.Admin.save('/curve/answer', answer, userSocket.id)
        .catch((err) => {
            this.Admin.sendError(userSocket, "The backend has some problems, contact someone at FX..");
        })
        .then((result) => {
            if(result.data.success) {
                this.Admin.broadcastToUsers([result.data.answer.user_id], 'curve_add_answer', result.data.answer);
                const userId = result.data.answer.user_id;
                const curveId = result.data.curve_id;

                if(!this.currentState.users[userId]) this.currentState.users[userId] = {};
                if(!this.currentState.users[userId][curveId]) this.currentState.users[userId][curveId] = {question_index: 0, answers: []};
                this.currentState.users[userId][curveId].answers.push(result.data.answer.choice);

                const currentQuestionIdx = this.currentState.users[userId][curveId].question_index;

                this.Admin.broadcastToChannel("display", 'curve_display_add_answer', { question_index: currentQuestionIdx });
                this.privateState.totalAnswers[currentQuestionIdx]++;
                this.currentState.users[userId][curveId].question_index++;
            } else {
                this.Admin.sendError(userSocket, "The Answer could not be submitted, please try again..");
                const userId = this.Admin.getConnection("user", userSocket.id).id;
                this.Admin.broadcastToUsers([userId], 'curve_reset_answer');
            }
        });
    }
}

module.exports = LearningCurveManager;
