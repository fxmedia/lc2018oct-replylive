[TOC]

# The New New Reply.live #

This Reply.live project is the latest version of which all interactive elements are seperated from the Event Website. The event website only has to include one small piece of JavaScript, and it will load all elements of Reply.Live on it (or at least that is the plan...)


## Setup as Developer ##

- Clone Repository to your local environment
- change to the root-directory of your local environment and run ....

```
npm install
composer install
gulp dev
nodemon server/server.js
```
- `cp .env.example .env` and fill in your own local url & database connection data
- `php artisan key:generate`
- `php artisan migrate`
- for now, create a public/uploads/encoded directory (for uploaded gids) @todo: catch error and create directory and move to storage path
- server dependencies (mac) -> windows users: https://www.ffmpeg.org/download.html
```
brew install ffmpeg
```

### Folder structure modules ###
The folder structure can be a bit overwhelming, but all modules and components follow the same structure. So if you know the structure of one module, you know all of them. Below is an example of the tipical structure of a module in reply.live

```
replylive3.0
.
+- app
|	+- Export
|	|	+-- [module]Export.php
|	|
|	+- Http
|	|	+- Controllers
|	|		+- Api
|	|		|	+-- [module]Controller.php
|	|		|
|	|		+- Backend
|	|			+-- [module]Controller.php
|	|
|	+- Import
|	|	+-- [module]Import.php
|	|
|	+-	Report
|	|	+-- [module]Report.php
|	|
|	+-- [module].php // the Model
|
+- config
|	+-- [module].php // not for every module neccasary
|
+- database
|	+- factories
|	|	+-- [module]Factory.php // for testing to build random db-fields
|	|
|	+- migrations
|	|	+-- xxxx_xx_xx_xxxxx_create_[module]_table.php // and/or other migrations
|	|
|	+- seeds
|		+-- [module]Seeder.php // to seed db with random or default fields
|
+- resources
|	+- js
|	|	// stuff to fill here
|	|
|	+- sass
|	|	// stuff to fill here
|	|
|	+- views
|		+- backend
|			+- [module]
|				+-- // here you find all blade templates for backend
|
+- routes
|	+-- api.php // write here all api call routes
|	+-- web.php // write here all your backend routes
|
+- server
	+ [module].js
```

# Changelog
##v1.2.10
- Fix: Reply.live attending status check was broken.

##v1.2.9
- Refactor: Add CustomEvent polyfill for ticker scanned event in IE > 10

##v1.2.8
- Fix/Temp: Disable user scanned Event for IE fix.

##v1.2.7
- Add 'Present' status filter to Email/SMS Setup

##v1.2.6
- Feat: Add Timezone setting to Reply.live to format scanned timestamp on Helpdesk and Scanner
- Refactor: Set helpdesk scanned to timezone Europe/Amsterdam

##v1.2.5
- Improvement: Better QR Scanner garbage collection

##v1.2.4
- Fix: Fixed a bug where QR Scanner wouldn't show feedback due to JS error

##v1.2.3
- Refactor: Cancel Request animation frame on stop scanning in QR Scanner

##v1.2.2
- Fix: Helpdesk 'Set present' button eventlistener now correctly delegates on DOM hidden elements
- Feat: Helpdesk when user is set present, set Present badge and scanned timestamp to 'Helpdesk'
- Feat: QR Scanner garbage collection through .stop() on localStream


##v1.2.1
- Fix: On eventsite register/update check if groups isset before syncing groups.


##v1.2.0
- Feat: Allow group (un)assignment from eventsite


##v1.1.0
- Refactor: Change eventsite extra field prefix from '@' to '_'
- Refactor: QR Scanner uses different library
- Feature: Add wysiwyg to email
- Fix: Bij het bewerken van een mailing wordt het veld van de afzender altijd overschreven met de default waarde.


##v1.0.6
- Feat/Refactor: New Scanner page that uses camera to scan QR codes

##v1.0.5
- Fix: Calculation for knockout/assessment participating users when started for group(s)
- Fix: Bug in survey result export


##v1.0.4
- Feat: Add Re:Assesment totals to result export and progress to display
- Feat: Remove emoji's from exports
- Refactor: Added labels to buttons in backend
- Fix: standalone register, now checks for cookie
- Fix: Range slider
- Fix: Survey export


## v1.0.3
- Feat: Added user attending status to the user export
- Feat: Quiz Answer total on Display
- Fix: Fixed a bug where standalone registration users didn't get user attending status
- Refactor: Knockout
- Fix: Poll stability on Display (refresh)

## v1.0.2
- Fixed a bug where Email attachments were not added to the email

## v.1.0.1
- Added custom stringOrArray validation rule
- Fixed a bug where, if a user was saved in the backend, exta fields with multiple values would only store the last value.

## v1.0.0
- Release