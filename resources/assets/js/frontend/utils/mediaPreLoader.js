const MAX_TIMEOUT_TIME = 2000;// in ms
const USE_LOCAL_STORAGE = true;
const DEBUG = true;

import Debug from './debug'

let allowVideoCaching = false;
function iOSversion() {
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
        // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
        let v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
    }
    else return [0];
}
// function isBlackBerry(){
//     return /BlackBerry|BB10|PlayBook/i.test(navigator.userAgent);
// }
// function isWindowsPhone(){
//     return /Windows Phone/i.test(navigator.userAgent);
// }
function detectIE() {
    let ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …

    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

    // Edge 12 (Spartan)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    // Edge 13
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    let msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    let trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        let rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    let edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return 0;
}


function getAndroidVersion(ua) {
    ua = (ua || navigator.userAgent).toLowerCase();
    let match = ua.match(/android\s([0-9.]*)/);
    return match ? match[1] : "";
}

function getChromeVersion () {
    let raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);

    return raw ? parseInt(raw[2], 10) : false;
}
function checkAllovVideoCaching(){
    if(iOSversion()[0] >= 9) allowVideoCaching = true;

    if(/Android/i.test(navigator.userAgent)){
        let androidVersion = getAndroidVersion();
        let chromeVersion = getChromeVersion();

        if( androidVersion &&
            chromeVersion  &&
            androidVersion.split('.')[0] >= 6 &&
            chromeVersion >= 53
        ){
            allowVideoCaching = true;
        }
    }

    if( detectIE() >= 11) allowVideoCaching = true;
}
checkAllovVideoCaching();


let _debug = function(message){
    if(DEBUG){
        Debug.logL('mediaPreLoader', message);
        // let parent = document.getElementById('cacheConsole');
        // if(parent){
        //     let p = document.createElement('p');
        //     p.innerHTML = message;
        //     parent.appendChild( p );
        // }
    }
};

let _parseJSON = function(string){
    let obj = false;
    try {
        obj = JSON.parse(string);
    } catch(e) {
        return false;
    }
    return obj;
};

//_____PreLoader_Classes_______________________________________________________/
let ImageLoader = function(url, id) {

    /**
     * Loader for images.
     *
     * Dispatches JOJO.Events.DataEvent
     *
     * @class JOJO.Net.ImageLoader
     * @constructor
     * @param {String} url URL for the image
     * @param {String} id Id for the image
     */

    let _img = document.createElement("img");
    let _url = url;

    let _id = id || "image_" + Math.random() * 10000000;
    let _isLoaded = false;
    let _cached = false;
    let _timeout;

    let _errorCallbacks = [];
    let _successCallbacks = [];
    let _timeoutCallbacks = [];

    let STD_MAX_TIME = 10000;

    _img.id = _id;
    _img.onload = function() {
        _isLoaded = true;
        clearTimeout(_timeout);
        _trigger('success');
    };

    let _onError = function() {
        clearTimeout(_timeout);
        _trigger('error');
    };

    _img.onerror = _onError;
    _img.onabort = _onError;


    let _load = function() {
        _img.src = _url;
        if(_img.complete){
            // image already in cache
            _cached = true;
            // console.log('Already in cache', _url);
            _debug('Already cached! '+ _url);
        }
        _timeout = setTimeout(_doTimeout, STD_MAX_TIME);
    };

    let _doTimeout = function() {
        _img.onload = null;
        _trigger('timeout');
    };

    let _trigger = function(eventName){
        let arr = [];
        switch(eventName){
            case "success":
                arr = _successCallbacks.concat();
                break;
            case "error":
                arr = _errorCallbacks.concat();
                break;
            case "timeout":
                arr = _timeoutCallbacks.concat();
                break;
            default:
                // is no default
                break;
        }

        for(let i=0; i<arr.length; i++){
            arr[i](_public, _cached);
        }
    };

    let _public = {
        /**
         * Returns the image element.
         *
         * @method element
         * @return {Object} Returns the image element.
         */
        element: function() {
            return _img;
        },

        /**
         * Returns the image id.
         *
         * @method id
         * @return {Object} Returns the image id.
         */
        id: function() {
            return _id;
        },

        /**
         * Checks if the image is loaded.
         *
         * @method isLoaded
         * @return {Boolean} Returns true if the image is loaded.
         */
        isLoaded: function() {
            return _isLoaded;
        },

        /**
         * Starts loading.
         *
         * @method load
         * @return null
         */
        load: function() {
            _load();
        },

        /**
         * Returns the image url.
         *
         * @method url
         * @return {String} Returns the image url.
         */
        url: function() {
            return _url;
        },

        on: function(eventName, callback){
            switch(eventName){
                case 'success':
                    _successCallbacks.push(callback);
                    break;
                case 'error':
                    _errorCallbacks.push(callback);
                    break;
                case 'timeout':
                    _timeoutCallbacks.push(callback);
                    break;
                default:
                    // illegal event name
                    break;
            }
        },

        off: function(){
            _successCallbacks = [];
            _errorCallbacks = [];
            _timeoutCallbacks = [];
        }
    };

    return _public;
};
let VideoLoader = function(url, id, options){

    /**
     * Loader for videos.
     *
     * Dispatches JOJO.Events.DataEvent
     *
     * @class JOJO.Net.VideoLoader
     * @constructor
     * @param {String} url URL for the video
     * @param {String} id Id for the video
     * @param {Object} options Options for the video; e.g. controls, muted, etc.
     */

    let _vid = document.createElement('video');
    let _url = url;
    let _id  = id || "video_" + Math.random() * 10000000;
    let _isLoaded = false;
    let _cached = false;
    let _timeout;

    let _errorCallbacks = [];
    let _successCallbacks = [];
    let _timeoutCallbacks = [];

    let STD_MAX_TIME = 10000;

    _vid.id = _id;

    if(options){
        for(let k in options){
            if(options.hasOwnProperty(k))_vid.setAttribute(k,options[k]);
        }
    }

    let _trigger = function(eventName){
        let arr = [];
        switch(eventName){
            case "success":
                arr = _successCallbacks.concat();
                break;
            case "error":
                arr = _errorCallbacks.concat();
                break;
            case "timeout":
                arr = _timeoutCallbacks.concat();
                break;
            default:
                // is no default
                break;
        }

        for(let i=0; i<arr.length; i++){
            arr[i](_public, _cached);
        }
    };

    // video events on order:
    // loadstart
    // durationchange
    // loadedmetadata
    // loadeddata
    // progress
    // canplay
    // canplaythrough
    _vid.oncanplay = function(){
        _isLoaded = true;
        clearTimeout(_timeout);
        _trigger('success');
    };

    let _onError = function(){
        clearTimeout(_timeout);
        _trigger('error');
    };

    _vid.onerror = _onError;
    _vid.onabort = _onError;

    let _load = function(){
        _vid.src = _url;
        _timeout = setTimeout(_doTimeout, STD_MAX_TIME);
    };

    let _doTimeout = function(){
        _vid.oncanplay = null;
        _trigger('timeout');
    };

    let _public = {
        /**
         * Returns the video element.
         *
         * @method element
         * @return {Object} Returns the video element.
         */
        element: function() {
            return _vid;
        },

        /**
         * Returns the video id.
         *
         * @method id
         * @return {Object} Returns the video id.
         */
        id: function() {
            return _id;
        },

        /**
         * Checks if the video is loaded.
         *
         * @method isLoaded
         * @return {Boolean} Returns true if the video is loaded.
         */
        isLoaded: function() {
            return _isLoaded;
        },
        /**
         * Gets given datatype of video
         *
         * @method getData
         * @param {String} type Datatype you want to get from the video
         * @return {*} Returns value of the datatype
         */
        getData: function(type) {
            return _vid[type];
        },

        /**
         * Set given datatype of video
         *
         * @method setData
         * @param {String} type Datatype you want to set from the video
         * @param {*} value Value you want to set for the given datatype.
         * @return null;
         */
        setData: function(type,value) {
            switch(type){
                case "autoplay":
                case "controls":
                case "currentTime":
                case "defaultMuted":
                case "defaultPlaybackRate":
                case "loop":
                case "mediaGroup":
                case "muted":
                case "playbackRate":
                case "preload":
                case "volume":
                    _vid[type] = value;
                    break;
                case "src":
                    /// oh noes! url/src want so be changed so do something else with it!!!
                    break;
            }
        },


        /**
         * Starts loading.
         *
         * @method load
         * @return null
         */
        load: function() {
            _load();
        },

        /**
         * Returns the video url.
         *
         * @method url
         * @return {String} Returns the video url.
         */
        url: function() {
            return _url;
        },

        /**
         * Pause the video
         *
         * @method pause
         */
        pause: function(){
            _vid.pause();
        },

        /**
         * Plays the video
         *
         * @method play
         */
        play: function(){
            _vid.play();
        },

        /**
         * Add an eventListener to the video element
         *
         * @method addListener
         * @param {String} eventName Name of the event type
         * @param {Function} callback Callback Function for when the event happens
         * @param {Boolean} [useCapture=false] Optional. A Boolean value that specifies whether the event should be executed in the capturing or in the bubbling phase
         * @return null;
         */
        addListener: function(eventName, callback, useCapture){
            useCapture = (useCapture === true);
            _vid.addEventListener(eventName, callback, useCapture);
        },

        /**
         * Remove an eventListener to the video element
         *
         * @method removeListener
         * @param {String} eventName Name of the event type
         * @param {Function} callback Callback Function that needs to be removed from the event
         * @param {Boolean} [useCapture=false] Optional. A Boolean value that specifies the event phase to remove the event handler from.
         * @return null;
         */
        removeListener: function(eventName, callback, useCapture){
            useCapture = (useCapture === true);
            _vid.removeEventListener(eventName, callback, useCapture);
        },


        on: function(eventName, callback){
            switch(eventName){
                case 'success':
                    _successCallbacks.push(callback);
                    break;
                case 'error':
                    _errorCallbacks.push(callback);
                    break;
                case 'timeout':
                    _timeoutCallbacks.push(callback);
                    break;
                default:
                    // illegal event name
                    break;
            }
        },

        off: function(){
            _successCallbacks = [];
            _errorCallbacks = [];
            _timeoutCallbacks = [];
        }
    };
    return(_public);
};

let VideoCacher = function(url, id){

    let _url = url;
    let _id  = id || "video_" + Math.random() * 10000000;
    let _isLoaded = false;
    let _timeout;

    let _errorCallbacks = [];
    let _successCallbacks = [];
    let _timeoutCallbacks = [];

    let STD_MAX_TIME = 20000;

    let req;
    let reader;

    let _load = function(){
        req = (window.XMLHttpRequest)? new XMLHttpRequest() :(window.ActiveXObject)? new ActiveXObject('Microsoft.XMLHTTP') : false;
        reader = (window.FileReader)? new window.FileReader() : false;

        if(!req || !reader){
            _trigger('error');
            return;
        }

        req.open('GET', _url, true);
        req.responseType = 'blob';
        req.onload = function(){

            if(req.status === 200){
                clearTimeout(_timeout);

                _convertToBase64(req.response);


            } else {
                _trigger('error');
            }
        };
        req.onerror = function(){
            _trigger('error');
        };


        req.send();
        _timeout = setTimeout(_doTimeout, STD_MAX_TIME);

    };

    let _convertToBase64 = function(blob){
        reader.onloadend = function(){
            reader.onloadend = null;

            localStorage.setItem(_url, reader.result);
            _isLoaded = true;
            _trigger('success');
        };
        reader.readAsDataURL(blob);
    };

    let _doTimeout = function(){
        req.onload = null;
        _trigger('error');
    };


    let _trigger = function(eventName){
        let arr = [];
        switch(eventName){
            case "success":
                arr = _successCallbacks.concat();
                break;
            case "error":
                arr = _errorCallbacks.concat();
                break;
            case "timeout":
                arr = _timeoutCallbacks.concat();
                break;
            default:
                // is no default
                break;
        }

        for(let i=0; i<arr.length; i++){
            arr[i](_public);
        }
    };


    let _public = {
        /**
         * Returns the image element.
         *
         * @method element
         * @return {Object} Returns the image element.
         */
        element: function() {
            return '';
        },

        /**
         * Returns the image id.
         *
         * @method id
         * @return {Object} Returns the image id.
         */
        id: function() {
            return _id;
        },

        /**
         * Checks if the image is loaded.
         *
         * @method isLoaded
         * @return {Boolean} Returns true if the image is loaded.
         */
        isLoaded: function() {
            return _isLoaded;
        },

        /**
         * Starts loading.
         *
         * @method load
         * @return null
         */
        load: function() {
            _load();
        },

        /**
         * Returns the image url.
         *
         * @method url
         * @return {String} Returns the image url.
         */
        url: function() {
            return _url;
        },

        on: function(eventName, callback){
            switch(eventName){
                case 'success':
                    _successCallbacks.push(callback);
                    break;
                case 'error':
                    _errorCallbacks.push(callback);
                    break;
                case 'timeout':
                    _timeoutCallbacks.push(callback);
                    break;
                default:
                    // illegal event name
                    break;
            }
        },

        off: function(){
            _successCallbacks = [];
            _errorCallbacks = [];
            _timeoutCallbacks = [];
        }
    };

    return _public;
};

let dummyObject = function(url){
    let _url = url;

    return {
        off: function(){},
        url: function(){
            return _url;
        }
    };
};

class MediaPreLoader {
    constructor(){
        this.toLoadArray = [];
        this.loadedCache = {};
        this.currentlyLoadingId = 0;
        this.currentlyLoading = false;
        // this.currentLoad = null;
    }

    _startLoading(){
        if(!this.currentlyLoading){
            if(this.currentlyLoadingId >= this.toLoadArray.length) {
                // done
                this._loadingDone();
            } else {
                this.currentlyLoading = true;
                // load next
                this._setLoadNextTimeout();
            }
        }
    }

    _setLoadNextTimeout(quickLoad){
        let timeoutTime = (quickLoad)? 0 :  Math.round(Math.random() * MAX_TIMEOUT_TIME);

        // let fn = this._loadNextElement;
        setTimeout(() => {
            // console.log('new load after', timeoutTime, 'ms');
            _debug('loading next after '+timeoutTime+'ms');
            // fn();
            this._loadNextElement();
        },timeoutTime);
    }

    _loadNextElement(){
        let cached = false;
        let loadObj = this.toLoadArray[this.currentlyLoadingId];
        let loadURI = encodeURIComponent(loadObj.url);

        let cachedObject = this.loadedCache[loadURI];
        // console.log('laodNextElement',loadObj.url);
        _debug('start loading '+ this.currentlyLoadingId);

        if(cachedObject){
            // already loaded, no need for reload
            // console.log('already loaded', loadObj.url);
            _debug('already loaded '+ this.currentlyLoadingId);
            this._loadSuccess(cachedObject, true);
            cached = true;
        } else if (USE_LOCAL_STORAGE) {
            let storeArr = _parseJSON(localStorage.getItem('MediaPreLoader')) || [];
            if(storeArr.indexOf(loadURI) !== -1) {
                // cached! what to do now?
                _debug('already loaded according to localStorage! '+ loadObj.url);
                this._loadSuccess(dummyObject(loadObj.url), true);
                cached = true;
            }
        }

        if(!cached){
            // console.log('loading', loadObj.url);
            let _set = false;
            switch(loadObj.type){
                case 'image':
                    this.currentLoad = ImageLoader(loadObj.url);
                    _set = true;
                    break;
                case 'video':
                    this.currentLoad = VideoLoader(loadObj.url);
                    _set = true;
                    break;
                case 'cache':
                    this.currentLoad = VideoCacher(loadObj.url);
                    _set = true;
                    break;
                default:
                    // switch for different file types to load. E.g. video or audio. Not build in yet.
                    break;
            }
            if(_set){
                this.currentLoad.on('success', this._loadSuccess.bind(this));
                this.currentLoad.on('error', this._loadFailed.bind(this));
                this.currentLoad.on('timeout', this._loadFailed.bind(this));
                this.currentLoad.load();
            }
        }
    }

    _loadFailed(loadedObj) {
        loadedObj.off();
        // log error or something like that
        // console.log('loading failed', loadedObj.url());
        _debug('loading failed '+ this.currentlyLoadingId);
        this._elementDone(true);
    }

    _loadSuccess(loadedObj, quickLoad) {
        loadedObj.off();
        // console.log('loading success', this.currentlyLoadingId);
        _debug('loading success '+ loadedObj.url());

        let urlToLoad = encodeURIComponent(loadedObj.url());
        if(!this.loadedCache[urlToLoad]){
            this.loadedCache[urlToLoad] = loadedObj;
        }
        if(USE_LOCAL_STORAGE) {
            let storeArr = _parseJSON(localStorage.getItem('MediaPreLoader')) || [];
            if(storeArr.indexOf(urlToLoad) === -1) {
                storeArr.push(urlToLoad);
                localStorage.setItem('MediaPreLoader', JSON.stringify( storeArr ) );
            }
        }

        this._elementDone(quickLoad);
    }

    _elementDone(quickLoad) {
        this.currentlyLoadingId++;
        // this.currentLoad = null;
        if (this.currentlyLoadingId >= this.toLoadArray.length) {
            this._loadingDone();
        } else {
            this._setLoadNextTimeout(quickLoad);
        }
    }

    _loadingDone(){
        this.currentlyLoading = false;
        // emit done event
        _debug('PreLoading done!');
    }

    /**
     * @method loadImage pre-load images into cache
     * @param {String|Array} url URL/Array of images to pre-load
     */
    loadImage(url){
        if(url) {
            if (typeof url === "string") {
                this.toLoadArray.push({
                    type: 'image',
                    url: url
                });
            } else {
                for (let i = 0; i < url.length; i++) {
                    this.toLoadArray.push({
                        type: 'image',
                        url: url[i]
                    });
                }
            }
            this._startLoading();
        }
    }

    /**
     * @method loadVideos Note that this function uses the default video tag to preload and triggers done on 'canplay'
     * @param {String|Array} url URL/Array of urls to pre-load videos.
     */
    loadVideo(url){
        if(url) {
            if (typeof url === "string") {
                this.toLoadArray.push({
                    type: 'video',
                    url: url
                });
            } else {
                for (let i = 0; i < url.length; i++) {
                    this.toLoadArray.push({
                        type: 'video',
                        url: url[i]
                    });
                }
            }
            this._startLoading();
        }
    }

    /**
     * @method cacheVideo Note that this function fully loads the video into the localStorage. Max video size for this is 10mb (preferably below 8)
     * @param {String|Array} url URL/Array of urls to load videos from with caching
     */
    cacheVideo(url){
        if(allowVideoCaching && url){
            if(typeof url === 'string'){
                // check if already cached first;
                if(!localStorage.getItem(url)){
                    this.toLoadArray.push({
                        type: 'cache',
                        url: url
                    });
                }
            } else {
                for(let i=0; i< url.length; i++){
                    if(!localStorage.getItem(url[i])){
                        this.toLoadArray.push({
                            type: 'cache',
                            url: url[i]
                        });
                    }
                }
            }
            this._startLoading();
        }
    }
}
export default new MediaPreLoader();