const GameManager = require('./Game');

const initialCurrentState = {
    questionIdx: 0,
    locked: false,
    phase: "multiple_choice",
    players_left: 999,
    knockoutsPerRound: 0
};

class KnockoutManager extends GameManager {
    constructor(Admin) {
        super(Admin, "knockout");
        this.participants = [];
        this.currentState = Object.assign({}, initialCurrentState);
        // create private state so it will not be sent to users
        this.privateState = {
            answers: [],
            knockouts: [],
            deciding_answers: []
        };
    }

    addAnswer(answer) {
        const existingAnswerIdx = this.privateState.answers.findIndex((_answer) =>
            answer.user_id == _answer.user_id
        );
        if(existingAnswerIdx === -1) {
            this.privateState.answers.push(answer);
        } else {
            this.privateState.answers[existingAnswerIdx] = answer;
        }
    }

    addDecidingAnswer(answer) {
        this.privateState.deciding_answers.push(answer);
    }

    showWinner() {
        const winner = this.participants.find((p) =>
            this.privateState.knockouts.indexOf(p.id) === -1
        );
        console.log("winner: ", winner);
        this.broadcastToUsers("knockout_set_winner", { winner: [winner] });
        this.Admin.broadcastToChannel("display", "knockout_set_winner", { winner: [winner] });
        this.Admin.setActiveAction({
            action: "knockout_set_winner",
            data: {
                user: {
                    winner: [winner],
                    knockout: {id: this.Admin.activeAction.data.user.knockout.id }
                },
                display: {
                    winner: [winner],
                    knockout: {id: this.Admin.activeAction.data.user.knockout.id }
                }
            }
        });
    }

    knockoutPlayers(action, players_left) {
        this.broadcastToUsers(action);
        this.Admin.broadcastToChannel(
            "display",
            "knockout_set_score",
            { score: this.calcFinal(), players_left }
        );
    }

    knockoutInactiveUsers() {
        this.participants.forEach((user) => {
            if(!this.privateState.answers.find((answer) => answer.user_id == user.id)) {
                this.addKnockedOutUser(user.id);
            }
        });
    }

    calculatePlayersLeft() {
        const players_left = this.participants.length - this.currentState.knockoutsPerRound;
        this.currentState.players_left = players_left;
        return players_left;
    }

    lockKnockout() {
        this.currentState.locked = true;
    }

    addKnockedOutUser(userId) {
        this.privateState.knockouts.push(userId);
        this.currentState.knockoutsPerRound += 1;
    }

    checkAnswer(answer, currentQuestion) {
        if(answer.choice != currentQuestion.correct_answer) {
           this.addKnockedOutUser(answer.user_id);
        }
    }

    checkAnswers(action, data) {
        if(this.currentState.phase == "deciding_question") {
            return this.checkDecidingAnswers(action, data);
        }

        const currentKnockout = this.Admin.activeAction.data.user.knockout;
        const currentQuestion = currentKnockout.questions[this.currentState.questionIdx];
        this.privateState.answers.forEach((answer) => this.checkAnswer(answer, currentQuestion));

        this.lockKnockout();
        this.knockoutInactiveUsers();
        const players_left = this.calculatePlayersLeft();

        if(players_left === 1) {
            this.showWinner();
        } else {
            this.knockoutPlayers(action, players_left);
        }
    }

    calcCandidateOffsets(correctAnswer) {
        const candidates = [];
        let winningOffset = 99999999;
        this.privateState.deciding_answers.forEach((a) => {
            const offset = Math.abs(parseInt(a.answer) - parseInt(correctAnswer));
            if(offset <= winningOffset) {
                candidates.push({ user_id: a.user_id, answer: a.answer, offset });
                winningOffset = offset;
            }
        });
        return { candidates, winningOffset };
    }

    getWinnersData(candidates, winningOffset) {
        const winners = candidates.filter((candidate) => candidate.offset <= winningOffset);
        return this.participants.filter((user) => winners.find((answer) => user.id == answer.user_id));
    }

    checkDecidingAnswers(action, data) {
        const correctAnswer = this.Admin.activeAction.data.display.knockout.deciding_answer;
        const { candidates, winningOffset } = this.calcCandidateOffsets(correctAnswer);
        const winnersData = this.getWinnersData(candidates, winningOffset);

        this.broadcastToUsers("knockout_set_winner", { winner: winnersData });
        this.Admin.broadcastToChannel("display", "knockout_set_winner", { winner: winnersData });
        this.Admin.setActiveAction({
            action: "knockout_set_winner",
            data: {
                user: {
                    winner: winnersData,
                    knockout: {id: this.Admin.activeAction.data.user.knockout.id }
                },
                display: {
                    winner: winnersData,
                    knockout: {id: this.Admin.activeAction.data.user.knockout.id }
                }
            }
        });
    }

    calcFinal() {
        const final = { winners: [], losers: [] };
        const users = this.calcParticipatingUsers();
        users.forEach((user) => {
            if(this.privateState.knockouts.indexOf(user.id) === -1) {
                final.winners.push(user);
            } else {
                final.losers.push(user);
            }
        });
        return final;
    }

    calcParticipatingUsers() {
        const users = this.Admin.managers.user.getUsers();
        return users.filter((user) => {
            const userGroupIds = (user.groups) ? user.groups.map((group) => group.id) : [];
            return this.Admin.managers.user.hasMatchingGroup(this.group_ids, userGroupIds)
                && this.Admin.managers.user.isAttending(user);
        });
    }

    cleanUpRound() {
        // set remaining users so next calculation is less heavy
        this.participants = this.participants.filter((user) =>
            this.privateState.knockouts.indexOf(user.id) === -1
        );
        this.privateState.answers = [];
        this.currentState.knockoutsPerRound = 0;
    }

    getKnockOutMap() {
        return this.privateState.knockouts;
    }

    getTotalKnockouts() {
        return this.privateState.knockouts.length;
    }

    handle(action, data) {
        switch(action) {
            case 'knockout_start':
                this.start(action, data);
                break;
            case 'knockout_stop':
                this.stop(action, data);
                break;
            case 'knockout_check_answer':
                this.checkAnswers(action, data);
                break;
            case 'knockout_show_question':
                this.showQuestion(action, data);
                break;
            case 'knockout_show_final':
                this.showFinal(action, data);
                break;
            case 'knockout_deciding_question':
                this.setDecidingQuestion(action, data);
                break;
        }
    }

    setDecidingQuestion(action, data) {
        this.currentState.phase = "deciding_question";
        this.broadcastToUsers(action, data, this.privateState.knockouts);
        this.Admin.broadcastToChannel("display", action, data);
        this.Admin.setActiveAction({
            action: "knockout_deciding_question",
            data: {
                user: {
                    phase: this.currentState.phase,
                    knockout: this.Admin.activeAction.data.user.knockout
                },
                display: {
                    phase: this.currentState.phase,
                    knockout: this.Admin.activeAction.data.display.knockout
                }
            }
        });
    }

    status() {
        if(!this.Admin.activeAction || this.Admin.getManager(this.Admin.activeAction.action) !== this.namespace) {
            return { knockout_id: false };
        }
        return {
            knockout_id: this.Admin.activeAction.data.user.knockout.id,
            question_index: this.currentState.questionIdx,
            locked: this.currentState.locked,
            players_left: this.currentState.players_left
        };
    }

    reset() {
        this.currentState = Object.assign({}, initialCurrentState);
        this.privateState = {
            answers: [],
            knockouts: [],
            deciding_answers: []
        };
    }

    showFinal(action, data) {
        const final = this.calcFinal();
        this.Admin.broadcastToChannel("display", action, final);
    }

    showQuestion(action, data) {
        this.cleanUpRound();
        this.currentState.questionIdx = data.question_index;
        this.currentState.locked = false;
        this.broadcastToUsers(action, data, this.privateState.knockouts);
        this.Admin.broadcastToChannel("display", action, data);
    }

    getExtractedData(data) {
        return {
            user: this.extract(data, [
                {key: 'knockout', keys: ["questions", "deciding_question", "id"]},
                {key: 'question_index', keys: null}
            ]),
            display: this.extract(data, [
                {
                    key: 'knockout',
                    keys: ["questions", "id", "deciding_question", "deciding_answer"]
                },
                {key: 'question_index', keys: null}
            ])
        };
    }

    saveTotalParticipants(extraction) {
        const totalParticipants = this.participants.length;
        extraction.display.players_left = totalParticipants;
        this.currentState.players_left = totalParticipants;
    }

    start(action, data) {
        this.reset();
        this.setBroadcastLevel(data.knockout);
        this.participants = this.calcParticipatingUsers();

        const extraction = this.getExtractedData(data);
        this.saveTotalParticipants(extraction);

        this.broadcastToUsers(action, extraction.user);
        this.Admin.broadcastToChannel("display", action, extraction.display);
        this.Admin.setActiveAction({ action, data: extraction });
    }

    stop(action, data) {
        if(this.isActive()) {
            this.broadcastToUsers(action);
            this.Admin.broadcastToChannel("display", action);
            this.Admin.setActiveAction(null);
        }
    }

    setSubmitError(userSocket, message) {
        this.Admin.sendError(
            userSocket,
            message
        );
        const userId = this.Admin.getConnection("user", userSocket.id).id;
        this.Admin.broadcastToUsers([userId], 'knockout_reset_answer');
    }

    submitAnswer(answer, userSocket) {
        this.Admin.save('/knockout/answer', answer, userSocket.id)
        .catch(() => this.setSubmitError(userSocket, "The backend has some problems, contact someone at FX.."))
        .then((result) => {
            if(result.data.success) {
                this.addAnswer({
                    choice: result.data.answer.choice,
                    user_id: result.data.answer.user_id
                });

                // update all of the user connections
                this.Admin.broadcastToUsers(
                    [result.data.answer.user_id],
                    'knockout_set_answer',
                    answer.choice
                );
            } else {
                this.setSubmitError(userSocket, "The Answer could not be submitted, please try again..");
            }
        });
    }

    submitDecidingAnswer(data, user) {
        const userData = this.Admin.getConnection("user", user.id);
        this.addDecidingAnswer({answer: parseInt(data.answer), user_id: userData.id});
    }
}

module.exports = KnockoutManager;
