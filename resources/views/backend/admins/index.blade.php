@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Admins</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Admins</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.admins._partials.aside')
    </div>
    <div class="col-xs-9">

        @include('backend._partials.errors')

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($admins as $admin)
                    <tr>
                        <td>{{ $admin->name }}</td>
                        <td>{{ $admin->email }}</td>
                        <td>
                            <a href="{{ route('backend.admins.edit', $admin->id) }}" class="btn btn-xs btn-success">
                                <span class="glyphicon glyphicon-edit"></span> Edit
                            </a>
                            {!! Form::open(['route'=>['backend.admins.remove',$admin->id], 'class'=>'inline-form','method'=>'POST']) !!}
                            <button onclick="return confirm('Are you sure?');" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span> Remove</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection