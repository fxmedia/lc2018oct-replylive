<?php

namespace App\Listeners;

use App\Events\UserSetDeclinedByAdmin;
use App\Senders\EmailSender;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailUserDeclined
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserSetDeclinedByAdmin $event
     * @return void
     */
    public function handle(UserSetDeclinedByAdmin $event)
    {
        //
        $user = $event->getUser();
        EmailSender::sendAction($user, 'register_declined');
    }
}
