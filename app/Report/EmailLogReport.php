<?php

namespace App\Report;

use App\EmailLog;
use Maatwebsite\Excel\Facades\Excel;

class EmailLogReport extends BaseReport
{
    private $emails;

    public function __construct($emails)
    {
        $this->emails = $emails;
    }

    public function getFilename()
    {
        return 'email_log_report_' . date('Y_m_d_h_m_s');
    }

    private function getLogsData($email){
        $data = [];
        $ls = EmailLog::with('user')->where('email_id',$email->id)->get();

        foreach ( $ls as $log ) {
            if ( ! isset( $data[ $log->batch ] ) ) {
                $data[ $log->batch ] = [
                    'created_at' => $log->created_at,
                    'users'      => 0,
                    'processing' => 0,
                    'failures'   => 0,
                    'rows'       => []
                ];
            }
            if ( $log->error ) {
                $data[ $log->batch ]['failures'] ++;
            }
            if ( $log->status !== 'done') {
                $data[ $log->batch ]['processing'] ++;
            }
            $data[ $log->batch ]['users'] ++;
            $data[ $log->batch ]['rows'][] = $log;
        }

        return array_reverse($data, true);
    }

    public function report(){
        Excel::create($this->getFilename(), function($excel){
            foreach($this->emails as $email){
                $name = substr(trim(preg_replace('/[\*|\:|\\|\/|\?|\[|\]]/', '', $email->name)), 0, 31);

                $excel->sheet($name, function($sheet) use ($email){
                    $index = 1;

                    $sheet->row($index, ['Email:', $email->name]);

                    $sheet->row($index, function($row){
                        $row->setFontSize(14);
                        $row->setFontWeight('bold');
                    });
                    $index+= 2;

                    $logs = $this->getLogsData($email);

                    if(count($logs) < 1){
                        $sheet->row($index, ['No Logs for this email']);
                        $index++;
                    }

                    foreach($logs as $batch => $log){
                        $sheet->row($index, ['Batch', 'Created At', 'Users', 'Still Processing', 'Failures']);
                        $sheet->row($index, function($row){
                            $row->setFontWeight('bold');
                        });
                        $index++;

                        $sheet->row($index, [$batch, $log['created_at'], $log['users'], $log['processing'], $log['failures'] ]);
                        $sheet->row($index, function($row){
                            $row->setFontWeight('bold');
                        });

                        $index++;

                        $sheet->row($index, ['Failures:']);
                        $index++;

                        foreach($log['rows'] as $row){
                            if(isset($row->error)){
                                $sheet->row($index, [$row->user->name, $row->user->email, $row->error]);

                                $index++;
                            }
                        }
                        $index++;
                    }


                    // group reports by batch, report created_at, amount of users, still processing, failed, and from failed on who and the messages
                });
            }
        })->export('xls');
    }
}