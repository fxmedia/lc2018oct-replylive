<?php

namespace App\Report;

use App\SmsLog;
use Maatwebsite\Excel\Facades\Excel;

class SmsLogReport extends BaseReport
{
    private $sms;

    public function __construct($sms)
    {
        $this->sms = $sms;
    }

    public function getFilename()
    {
        return 'sms_log_report_' . date('Y_m_d_h_m_s');
    }

    private function getLogsData($sms){
        $data = [];
        $lg = SmsLog::with('user')->where('sms_id', $sms->id)->get();
        foreach ( $lg as $log ) {
            if ( ! isset( $data[ $log->batch ] ) ) {
                $data[ $log->batch ] = [
                    'created_at' => $log->created_at,
                    'users'      => 0,
                    'processing' => 0,
                    'failures'   => 0,
                    'rows'       => []
                ];
            }
            if ( $log->error ) {
                $data[ $log->batch ]['failures'] ++;
            }
            if ( $log->status !== 'done') {
                $data[ $log->batch ]['processing'] ++;
            }
            $data[ $log->batch ]['users'] ++;
            $data[ $log->batch ]['rows'][] = $log;
        }

        return array_reverse($data, true);
    }

    public function report(){
        Excel::create($this->getFilename(), function($excel){
            foreach($this->sms as $sms){
                $name = substr(trim(preg_replace('/[\*|\:|\\|\/|\?|\[|\]]/', '', $sms->name)), 0, 31);

                $excel->sheet($name, function($sheet) use ($sms){
                    $index = 1;

                    $sheet->row($index, ['Sms:', $sms->name]);

                    $sheet->row($index, function($row){
                        $row->setFontSize(14);
                        $row->setFontWeight('bold');
                    });
                    $index+= 2;

                    $logs = $this->getLogsData($sms);

                    if(count($logs) < 1){
                        $sheet->row($index, ['No Logs for this email']);
                        $index++;
                    }

                    foreach($logs as $batch => $log){
                        $sheet->row($index, ['Batch', 'Created At', 'Users', 'Still Processing', 'Failures']);
                        $sheet->row($index, function($row){
                            $row->setFontWeight('bold');
                        });
                        $index++;

                        $sheet->row($index, [$batch, $log['created_at'], $log['users'], $log['processing'], $log['failures'] ]);
                        $sheet->row($index, function($row){
                            $row->setFontWeight('bold');
                        });

                        $index++;

                        $sheet->row($index, ['Failures:']);
                        $index++;

                        foreach($log['rows'] as $row){
                            if(isset($row->error)){
                                $sheet->row($index, [$row->user->name, $row->user->email, $row->error, $row->result]);

                                $index++;
                            }
                        }
                        $index++;
                    }
                });
            }
        })->export('xls');
    }
}