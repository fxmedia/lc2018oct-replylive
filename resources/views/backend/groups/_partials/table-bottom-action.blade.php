<tfoot>
<tr>
    <td colspan="2">
        {!! Form::select('perform', [
            'export'=>'Export users',
            'delete'=>'Delete groups',
            'empty' => 'Empty group'
        ], 'send_text', ['class'=>'form-control pull-left perform-selector']) !!}
    </td>
    <td>
        {!! Form::submit( 'Submit', ['class' => 'btn btn-md pull-left btn-primary'] ) !!}
    </td>
    <td colspan="3">&nbsp;</td>
</tr>
</tfoot>