<?php
return [
    'settingGroups' => [
        ['name' => 'Default Settings', 'order' => 0],
        ['name' => 'Advanced Settings', 'order' => 1],
        ['name' => 'Email/Sms Settings', 'order' => 2]
    ],
    'settings' => [
        // Default Settings
        [
            'group_id' => 1,
            'name' => 'Name',
            'key' => 'event_name',
            'value' => '',
            'type' => 'text',
            'placeholder' => 'Name of the event',
            'validation' => 'string|max:255',
            'description' => 'Name of the event this Reply.live is being used for.'
        ],
        [
            'group_id' => 1,
            'name' => 'Event site',
            'key' => 'event_domain',
            'value' => '',
            'type' => 'text',
            'placeholder' => 'domain.event.live (without http/https)',
            'validation' => 'required|string|max:255',
            'description' => 'Domain name of the event website. This is necessary for authentication for the takeover. Give the url without "http(s)://" and make the url as detailed as necessary.'
        ],
        [
            'group_id' => 1,
            'name' => 'Reply.Live',
            'key' => 'reply_live_domain',
            'value' => '',
            'type' => 'text',
            'placeholder' => '(http|https)://domain.reply.live',
            'validation' => 'required|string|max:255',
            'description' => 'Domain name for the current Reply.live site. We can skip this by checking the the current url, but might want to keep it for security. Fill this field with "http(s)://".'
        ],
        [
            'group_id' => 1,
            'name' => 'NodeJS Server',
            'key' => 'nodejs_domain',
            'value' => '',
            'type' => 'text',
            'placeholder' => 'ask your sysadmin',
            'validation' => 'required|string|max:255',
            'description' => 'Domain name of the NodeJS Server. Give the url without "http(s)://" in front of it.'
        ],
        [
            'group_id' => 1,
            'name' => 'Port NodeJS',
            'key' => 'nodejs_port',
            'value' => '443',
            'type' => 'tel',
            'placeholder' => 'port',
            'validation' => 'required|numeric',
            'description' => 'Port of the NodeJS Server. You can keep the default 443 value for live environment, but change it (most likely to 5768) for local environment.'
        ],
        [
            'group_id' => 1,
            'name' => 'Is online',
            'key' => 'online',
            'value' => '1',
            'type' => 'boolean',
            'placeholder' => '',
            'validation' => 'max:255',
            'description' => 'Switch Reply.live on or off for the event website.'
        ],
        [
            'group_id' => 1,
            'name' => 'Security Key',
            'key' => 'security_token',
            'value' => '',
            'type' => 'text',
            'placeholder' => 'Leave empty if you don\'t want to secure the display',
            'validation' => 'max:255',
            'description' => 'This key will be used for authentication of pages like the Display.',
            'auto_generate' => true
        ],
        [
            'group_id' => 1,
            'name' => 'API key',
            'key' => 'api_token',
            'value' => '',
            'type' => 'text',
            'placeholder' => '',
            'validation' => 'required|string|max:255',
            'description' => 'This key will be used for authentication with for example the NodeJS server.'
        ],
        [
            'group_id' => 1,
            'name' => 'Stand alone Register',
            'key' => 'stand_alone_register',
            'value' => '0',
            'type' => 'boolean',
            'placeholder' => '',
            'validation' => 'max:255',
            'description' => 'Some events don\'t require an event site, but only a simple login/register page with the interactive elements. Turn this value to on to enable it.'
        ],
        [
            'group_id' => 1,
            'name' => 'Event attending limit',
            'key' => 'event_attending_limit',
            'value' => '0',
            'type' => 'number',
            'placeholder' => '',
            'validation' => 'numeric',
            'description' => 'Determine the eventwide attending limit. Set 0 for unlimited.'
        ],
        [
            'group_id' => 1,
            'name' => 'Register Password',
            'key' => 'register_password',
            'value' => '',
            'type' => 'text',
            'placeholder' => 'Leave empty if you don\'t want to secure the register page',
            'validation' => 'max:255',
            'description' => 'For some events there is no event website and users will log in on a register page. For this we need a global password which is being shared on the event, to prevent others outside the event to join in on Reply.Live'
        ],
        [
            'group_id' => 1,
            'name' => 'Timezone',
            'key' => 'timezone',
            'placeholder' => 'Europe/Amsterdam',
            'validation' => 'required|max:255',
            'value' => 'Europe/Amsterdam',
            'type' => 'text',
            'description' => 'Sets the timezone for this event. Used in DateTime formats in multiple places. See http://php.net/manual/en/timezones.php for valid timezones.'
        ],
        // Advanced Settings
        [
            'group_id' => 2,
            'name' => 'JS Cache Version',
            'key' => 'js_cache_version',
            'value' => '1',
            'type' => 'tel',
            'placeholder' => '',
            'validation' => 'max:255',
            'description' => 'Cache Version for JavaScript files. Change this value if updates has been pushed to the live environment.'
        ],
        [
            'group_id' => 2,
            'name' => 'CSS Cache Version',
            'key' => 'css_cache_version',
            'value' => '1',
            'type' => 'tel',
            'placeholder' => '',
            'validation' => 'max:255',
            'description' => 'Cache Version for CSS files. Change this value if updates has been pushed to the live environment.'
        ],
        [
            'group_id' => 2,
            'name' => 'Media Request Token',
            'key' => 'media_token',
            'value' => '',
            'type' => 'text',
            'placeholder' => '',
            'validation' => 'max:255|required',
            'description' => 'For media like user images, we generate a token to allow users to watch them. Change this token to block users/intruders to access media.',
            'auto_generate' => true
        ],
        [
            'group_id' => 2,
            'name' => 'Max Upload Image size (px)',
            'key' => 'upload_max_size',
            'value' => '640',
            'type' => 'tel',
            'placeholder' => '',
            'validation' => 'required|numeric',
            'description' => 'Max width and height of images uploaded by users. Defined in px. Images largen then this size, will automatically be resized.'
        ],
        [
            'group_id' => 2,
            'name' => 'Max Upload Video length (sec)',
            'key' => 'upload_max_length',
            'value' => '10',
            'type' => 'tel',
            'placeholder' => '',
            'validation' => 'required|numeric',
            'description' => 'Max length of videos being uploaded by users. Videos longer then this length, will automatically be cropped.'
        ],
        [
            'group_id' => 2,
            'name' => 'Posts Question Column',
            'key' => 'wall_question_column_enabled',
            'value' => '1',
            'type' => 'boolean',
            'placeholder' => '',
            'validation' => 'max:255',
            'description' => 'Re:Wall has two columns, messages and questions. Questions can be disabled.'
        ],
        [
            'group_id' => 2,
            'name' => 'Posts Visible by Default',
            'key' => 'posts_visible_default',
            'value' => '1',
            'type' => 'boolean',
            'placeholder' => '',
            'validation' => 'max:255',
            'description' => 'Posts will be shown immediately by default, can be turned off to manually validate.'
        ],
        [
            'group_id' => 2,
            'name' => 'Redirect url',
            'key' => 'redirect_url',
            'value' => 'https://reply.live',
            'type' => 'text',
            'placeholder' => 'https://reply.live',
            'validation' => 'max:255|string|required',
            'description' => 'If the Event is offline or a user is not allowed, the website will be redirected to this url.'
        ],
        [
            'group_id' => 2,
            'name' => 'Attach Groups',
            'key' => 'attach_groups',
            'value' => '1',
            'type' => 'boolean',
            'placeholder' => '',
            'validation' => 'max:255',
            'description' => 'When importing a user list and this is on "on", for existing users groups will be merged. When this is on "off", already lisnked groups will be cleared.'
        ],
        [
            'group_id' => 2,
            'name' => 'Overwrite token',
            'key' => 'overwrite_token',
            'value' => '0',
            'type' => 'boolean',
            'placeholder' => '',
            'validation' => 'max:255',
            'description' => 'Overwrite existing tokens of existing users on import. '
        ],
        [
            'group_id' => 2,
            'name' => 'Attach Extras',
            'key' => 'attach_extras',
            'value' => '1',
            'type' => 'boolean',
            'placeholder' => '',
            'validation' => 'max:255',
            'description' => 'When this is on, extra fields of users will be merged on import. When off, the extra fields will first be cleared. '
        ],
        // Email/Sms Settings
        [
            'group_id' => 3,
            'name' => 'Email Image cropping size',
            'key' => 'email_image_size',
            'value' => '640',
            'type' => 'number',
            'placeholder' => 'in px',
            'validation' => 'required|numeric',
            'description' => 'When the resize checkbox on header and footer image is checked, images will automatically be resize to this value'
        ],
        [
            'group_id' => 3,
            'name' => 'Email Default Reply Address',
            'key' => 'email_default_reply',
            'value' => '',
            'type' => 'email',
            'placeholder' => 'info@fxmedia.com',
            'validation' => 'max:255|nullable|email',
            'description' => 'This email address will be used as default for reply-to when you create a new email'
        ],
        [
            'group_id' => 3,
            'name' => 'Email Default From Address',
            'key' => 'email_default_from_email',
            'value' => '',
            'type' => 'email',
            'placeholder' => 'info@fxmedia.com',
            'validation' => 'max:255|nullable|email',
            'description' => 'This email address will be used as default as from email when you create a new email'
        ],
        [
            'group_id' => 3,
            'name' => 'Email Default From Name',
            'key' => 'email_default_from_name',
            'value' => '',
            'type' => 'text',
            'placeholder' => 'FXmedia',
            'validation' => 'max:255|nullable|string',
            'description' => 'This name will be used as default as from name when you create a new email'
        ],
        [
            'group_id' => 3,
            'name' => 'Notification Adresses',
            'key' => 'email_notification',
            'value' => '',
            'type' => 'text',
            'placeholder' => 'Email Addresses, comma separated',
            'validation' => 'max:255|nullable|string',
            'description' => 'These email addresses will get notified when e.g. an anonymous user wants register, or when the attending limit is reached.'
        ],
        [
            'group_id' => 3,
            'name' => 'Sms Default Sms name',
            'key' => 'sms_default_subject',
            'value' => '',
            'type' => 'text',
            'placeholder' => 'FXmedia',
            'validation' => 'max:11|nullable|string',
            'description' => 'This value will be used as default sender name for sms messages when you create a new sms'
        ]

    ]
];
