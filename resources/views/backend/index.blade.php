@extends('backend.layout.app')

@section('header')
    <h1>Reply.live</h1>
    {{--<ol class="breadcrumb">--}}
        {{--<li>Backend</li>--}}
    {{--</ol>--}}
@endsection

@section('content')
    <h2>Backend</h2>
    <ol>
        <li><a href="{{ route('backend.admins.index') }}">Admins</a></li>
        <li><a href="{{ route('backend.settings') }}">Settings</a></li>
        <hr>
        <li><a href="{{ route('backend.users.index') }}">Users</a></li>
        <li><a href="{{ route('backend.groups.index') }}">Groups</a></li>
        <hr>
        <li><a href="{{ route('backend.polls.index') }}">Re:Poll</a></li>
        <li><a href="{{ route('backend.quizzes.index') }}">Re;Quiz</a></li>
        <li><a href="{{ route('backend.open_questions.index') }}">Re:Question</a></li>
        <li><a href="{{ route('backend.knockouts.index') }}">Re:Knockout</a></li>
        <li><a href="{{ route('backend.surveys.index') }}">Re:Survey</a></li>
        <li><a href="{{ route('backend.posts.index') }}">Re:Wall</a></li>
        <li><a href="{{ route('backend.learning_curves.index') }}">Re:Assessment</a></li>
        <li><a href="{{ route('backend.timer.index') }}">Re:Countdown</a></li>
        <li><a href="{{ route('backend.pushes.index') }}">Re:Push</a></li>
        <hr>
        <li><a href="{{ route('backend.emails.index') }}">Emails</a></li>
        <li><a href="{{ route('backend.sms.index') }}">Sms</a></li>
    </ol>
    <h2>Displays</h2>
    <ol>
        <li><a href="{{ route('frontend.display', [$settings->security_token ]) }}" target="_blank">Default display</a></li>
        <hr>
        <li><a href="{{ route('frontend.scanner.qrscanner', [$settings->media_token]) }}" target="_blank">QR Ticket Scanner</a></li>
        <li><a href="{{ route('frontend.scanner.index', [$settings->media_token]) }}" target="_blank">Old Barcode Scanner</a></li>
        <hr>
        <li><a href="{{ route('frontend.helpdesk.index', [$settings->media_token]) }}" target="_blank">Helpdesk</a></li>
    </ol>
@endsection
