import { observable, action } from 'mobx';

export default class OpenQuestionStore {

    @observable data;
    @observable highlight;
    @observable totalAnswers;
    @observable showThanks = false;
    @observable isLoading = false;
    @observable activeAnswer = "";

    constructor(socket, mainStore) {
        this.data = {};
        this.highlight = null;
        this.totalAnswers = 0;
        this.mainStore = mainStore;
        this.socket = socket;
        this.listen();
    }

    @action updateAnswer(answer) {
        this.activeAnswer = answer;
    }

    @action addAnswer(answer) {
        this.data.result.push(answer);
        this.totalAnswers++;
    }

    @action deleteAnswer(data) {
        this.data.result = this.data.result.filter(answer => data.open_question_answer_ids.indexOf(answer.id.toString()) === -1);
    }

    @action highlightAnswer(data) {
        this.highlight = data.open_question_answer_id;
    }

    @action reset() {
        this.data.result = [];
        this.totalAnswers = 0;
        this.highlight = null;
        this.isLoading = false;
        this.showThanks = false;
    }

    @action resetAnswer() {
        this.isLoading = false;
    }

    @action setShowThanks() {
        this.isLoading = false;
        this.showThanks = true;

        setTimeout(() => {
            if(!this.data.multiple_answers) {
                this.mainStore.closeTakeover();
                this.mainStore.setActivePage("home");
                this.updateAnswer("");
            } else {
                this.showThanks = false;
                this.updateAnswer("");
            }
        }, 2000);
    }

    @action start(data) {
        this.reset();
        this.data = data;
        if(data.result) {
            this.totalAnswers = data.result.length;
        }
        this.mainStore.setActivePage("oq", true);
    }

    @action stop() {
        this.mainStore.closeTakeover();
        this.mainStore.setActivePage("home");
        this.data = {};
    }

    @action submitAnswer() {
        this.isLoading = true;
        this.socket.connection.emit('oq_submit_answer', {answer: this.activeAnswer, open_question_id: this.data.id});
    }

    @action listen() {
        this.socket.connection.on('oq_start', data => this.start(data));
        this.socket.connection.on('oq_stop', () => this.stop());
        this.socket.connection.on('oq_add_answer', data => this.addAnswer(data));
        this.socket.connection.on('oq_delete_answer', data => this.deleteAnswer(data));
        this.socket.connection.on('oq_highlight_answer', data => this.highlightAnswer(data));
        this.socket.connection.on('oq_show_thanks', () => this.setShowThanks());
        this.socket.connection.on('oq_reset_answer', () => this.resetAnswer());
    }
}
