import React from 'react';
import { inject, observer } from 'mobx-react';

@inject("stores") @observer
export default class TimerPage extends React.Component {

    componentWillUnmount() {
        this.props.stores.timer.stopAudio();
        this.props.stores.timer.stopTimer();
    }

    parseTimeLeft(time) {
        const seconds = time % 60;
        const minutes = Math.floor((time / 60) % 60);
        return (
            <span>
                {(minutes.toString().length > 1) ? minutes : `0${minutes}` }:
                {(seconds.toString().length > 1) ? seconds : `0${seconds}`}
            </span>
        );
    }

    render() {
        const { remainingSeconds, totalSeconds } = this.props.stores.timer;
        if(!remainingSeconds && !totalSeconds) return null;

        return (
            <div className="TimerPage">
                <div className="timer-wrapper">
                    <span className="timer">{this.parseTimeLeft(remainingSeconds)}</span>
                </div>
            </div>
        );
    }
}
