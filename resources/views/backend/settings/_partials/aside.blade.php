<div class="panel panel-default">
    {{--<div class="panel-heading"><i class="fa fa-plus"></i> Create</div>--}}
    {{--<ul class="list-group">--}}
        {{--<li class="list-group-item"><a href="">Add Setting</a></li>--}}
        {{--<li class="list-group-item"><a href="">Add Setting Group</a></li>--}}
    {{--</ul>--}}
    <div class="panel-heading"><i class="fa fa-upload"></i> Upload</div>
    <div class="panel-body panel-uploader">
        {!! Form::open(['route' => 'backend.settings.import', 'files' => true]) !!}
        {!! Form::file( 'setting_upload', ['class' => 'importfile'] ) !!}
        <br>
        {!! Form::submit( 'Upload', ['class' => 'btn btn-sm btn-primary'] ) !!}
        {!! Form::close() !!}
    </div>

    <div class="panel-heading"><i class="fa fa-download"></i> Download</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.settings.export') }}">Download all settings</a></li>
    </ul>

</div>