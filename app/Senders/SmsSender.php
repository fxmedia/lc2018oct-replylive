<?php namespace App\Senders;

use App\Jobs\SendSms;
use App\Sms;
use App\SmsLog;
use App\User;
use Exception;
use MessageBird\Client as MessageBirdClient;
use MessageBird\Objects\Message as M;

class SmsSender extends BaseSender
{
    /**
     * Send sms by action
     *
     * @param User $user
     * @param string $action
     *
     * @return bool
     */
    public static function sendAction(User $user, string $action)
    {
        if( $sms = Sms::getByAction($action)){
            self::send($sms, $user);
            return true;
        }
        return false;
    }

    /**
     * Add the sms to send to one user to the queue
     *
     * @param Sms $sms
     * @param User $user
     *
     * @return void
     */
    public static function send(Sms $sms, User $user)
    {
        /**
         * TODO: could add priorities here
         */
        $log = new SmsLog([
            'sms_id' => $sms->id,
            'user_id' => $user->id,
            'batch' => null
        ]);
        $log->save();
        SendSms::dispatch($sms, $user);
    }

    /**
     * Add sms to send to multiple users to the queue
     *
     * @param Sms $sms
     * @param array <User> $users
     *
     * @return integer
     */
    public static function batch(Sms $sms, $users){
        $max = SmsLog::whereSmsId($sms->id)->max('batch');
        $batch = ($max) ? $max+1 : 1;

        foreach($users as $user){
            $log = new SmsLog([
                'sms_id' => $sms->id,
                'user_id' => $user->id,
                'batch' => $batch
            ]);
            $log->save();
            SendSms::dispatch($sms, $user, $log);
        }

        return $batch;
    }

    /**
     * Being called by the queue-worker
     *
     * @param Sms $sms
     * @param User $user
     * @return mixed $result
     */
    public static function _send(Sms $sms, User $user){
        $data = self::createSendDataObject($user);

        $body = self::parseTextVariables($data, $sms->body_text);

        // send sms with this data

        $messageBird = new MessageBirdClient( config('messagebird.token') );

        $message = new M();
        $message->originator = $sms->subject;
        $message->recipients = [$user->phonenumber];
        $message->body = $body;

        try{
            $result = $messageBird->messages->create($message);
        } catch(Exception $e){
            $result = (object)[
                'failed' => true,
                'error' => $e->getMessage()
            ];
        }

        return $result;
    }
}