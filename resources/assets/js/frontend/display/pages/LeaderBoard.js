import React from 'react';
import { inject, observer } from 'mobx-react';

@inject("stores", "t") @observer
export default class LeaderBoardPage extends React.Component {
    render() {
        const { results } = this.props.stores.lb;
        return (
            <div className="LeaderBoardPage game-wrapper">
                <h2>{this.props.t.lb.header}</h2>
                <div className="user-container">
                    {results.map(user =>
                        <div className="user" key={user.id}>
                            <div className="details-wrapper">
                                <div
                                    className="avatar-wrapper"
                                    style={{ backgroundImage: `url(${user.image_path || "/images/avatar.png"})` }}
                                />
                                <h3>{user.name}</h3>
                            </div>
                            <span className="points">{user.points} {this.props.t.general.points}</span>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}
