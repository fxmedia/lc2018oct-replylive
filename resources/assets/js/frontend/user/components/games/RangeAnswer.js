import React from 'react';
import { inject } from 'mobx-react';

import SubmitButton from './SubmitButton';
import InputRange from './InputRange';

@inject("t")
export default class RangeAnswer extends React.Component {

    constructor(props) {
        super(props);
        this.state = { input: Math.round((props.data.max_range - props.data.min_range) / 2) };
    }

    handleInput(input) {
        this.setState({ input });
    }

    handleAnswer() {
        this.props.handleAnswer(this.state.input);
    }

    render() {
        const {min_range, min_label, max_range, max_label} = this.props.data;
        return (
            <div className="RangeAnswer">
                <div className="slider-wrapper">
                    <InputRange
                        min={min_range}
                        max={max_range}
                        roundValues={true}
                        handleInput={this.handleInput.bind(this)}
                        value={this.state.input}
                    />
                    <div className="label first">
                        <span>{min_label != null ? min_label : min_range}</span>
                        <span>min</span>
                    </div>
                    <div className="label last">
                        <span>{max_label != null ? max_label : max_range}</span>
                        <span>max</span>
                    </div>
                    <div className="clear">&nbsp;</div>
                </div>
                <SubmitButton
                    text={(this.props.isLast) ? this.props.t.survey.submit : this.props.t.survey.next}
                    handleSubmit={this.handleAnswer.bind(this)}
                />
            </div>
        )
    }
}
