<div class="modal fade" id="statusModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Progress</h4>
            </div>
            <div class="modal-body">
                <div id="waiting-for-response" class="waiting-for-response">
                    <div class="boxLoadingContainer"><div class="boxLoading"></div></div>
                </div>
                <div id="ajax-success-container">
                    Done <span id="result-done">0</span> of <span id="result-total">0</span>
                    <table class="table table-hover">
                        <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Error</th>
                        </thead>
                        <tbody id="batchTableBody">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="batchStatusCancel" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>