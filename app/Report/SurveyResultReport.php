<?php

namespace App\Report;

use App\User;
use App\SurveyAnswer;
use Maatwebsite\Excel\Facades\Excel;

class SurveyResultReport extends BaseReport{

    private $surveys;

    public function __construct($surveys)
    {
        $this->surveys = $surveys;
    }

    public function getFilename()
    {
        return 'survey_report_' . date('Y_m_d_h_m_s');
    }

    private function findUserAnswer($answers, $userId) {
        $item = null;
        foreach($answers as $answer) {
            if ($userId == $answer->user_id) {
                $item = $answer;
                break;
            }
        }
        return $item;
    }

    public function report(){
        Excel::create($this->getFilename(), function($excel){

            foreach($this->surveys as $survey){

                $name = substr(trim(preg_replace('/[\*|\:|\\|\/|\?|\[|\]]/', '', $survey->name)), 0, 31);
                $excel->sheet($name, function($sheet) use ($survey){
                    $index = 1;

                    $sheet->row($index, ['Survey Name: ' . $survey->name ]);
                    $sheet->row($index, function ($row) {
                        $row->setFontSize(18);
                        $row->setFontWeight('bold');
                    });
                    $index++;

                    $headers = ['User'];
                    foreach($survey->questions as $question) {
                        array_push($headers, $question->title);
                    }
                    $sheet->row($index, $headers);
                    $index++;

                    $users = User::all();

                    foreach($users as $user) {
                        $row = [$user->firstname . " " . $user->lastname];
                        foreach($survey->questions as $question) {
                            $answer = $this->findUserAnswer($question->answers, $user->id);
                            $answerInput = ($answer) ? $answer->input : "";
                            array_push($row, $this->remove_emoji($answerInput));
                        }
                        $sheet->row($index, $row);
                        $index++;
                    }
                });
            }

        })->export('xls');
    }
}
