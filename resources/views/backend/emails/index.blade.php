@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Emails</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Emails</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.emails._partials.aside')
    </div>
    <div class="col-xs-9">
        @include('backend._partials.errors')

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Send on</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($emails as $email)
                    <tr id="email_{{ $email->id }}">
                        <td><strong>{{ $email->name }}</strong></td>
                        <td>{{ ($email->action !== 'noaction')? $email->action : 'manually' }}</td>
                        <td>
                            <a class="btn btn-xs btn-success pull-left" href="{{ route('backend.emails.setup', [$email->id]) }}">
                                <span class="glyphicon glyphicon-send"></span>Send
                            </a>
                            <a class="btn btn-xs btn-warning pull-left" href="{{ route('backend.emails.edit', [$email->id]) }}">
                                <span class="glyphicon glyphicon-edit"></span>Edit
                            </a>
                            <a class="btn btn-xs btn-info pull-left" href="{{ route('backend.emails.log', [$email->id]) }}">
                                <span class="glyphicon glyphicon-stats"></span>Log
                            </a>

                            {!! Form::open(['method'=>'DELETE', 'route' => ['backend.emails.remove', $email->id], 'onsubmit' => 'return confirm("Please confirm to remove the selected email?")']) !!}
                            <button class="btn btn-xs btn-danger btn-remove pull-left" type="submit">
                                <span class="glyphicon glyphicon-trash"></span> Delete
                            </button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
@endsection