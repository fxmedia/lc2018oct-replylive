<?php

use Illuminate\Database\Seeder;

class SurveyTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->command->info('Seeding the database with random surveys');
        factory('App\Survey', 2)->create()->each(function($s){
            $s->questions()->save(factory('App\SurveyQuestion')->make(['type'=>'abcd']));
            $s->questions()->save(factory('App\SurveyQuestion')->make(['type'=>'abcd-multiple']));
            $s->questions()->save(factory('App\SurveyQuestion')->make(['type'=>'range']));
            $s->questions()->save(factory('App\SurveyQuestion')->make(['type'=>'open']));
        });
    }
}
