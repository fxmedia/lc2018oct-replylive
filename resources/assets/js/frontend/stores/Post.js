import React from 'react';
import { observable, action } from 'mobx';
import axios from 'axios';
require('smoothscroll-polyfill').polyfill();

export default class PostStore {

    @observable feed;
    @observable highlight;
    @observable isLoading = false;

    constructor(socket, mainStore, api) {
        this.feed = [];
        this.highlight = null;
        this.socket = socket;
        this.api = api;
        this.mainStore = mainStore;
        this.listen();
    }

    @action setContainer() {
        this.container = document.querySelector(".App");
    }

    @action setIsLoading(isLoading) {
        this.isLoading = isLoading;
    }

    @action addPost(data) {
        this.isLoading = false;
        let feed = [...this.feed];
        feed.push(data);
        this.feed = feed;
    }

    @action scrollToLast() {
        /**
         * TODO check if user scrolled by himself. if so, block this function until scrolled has been set to bottom
         */
        if(this.container) {
            this.container.scroll({
                top: this.container.scrollHeight,
                left: 0,
                // behavior: 'smooth'
            });
        }
    }

    @action addPosts(data) {
        this.feed = [];
        this.sort(data);
        data.forEach((post) => this.feed.push(post));
    }

    @action toggleVisibility(data) {
        data.post_ids.forEach(post_id => {
            const changedPostIdx = this.feed.findIndex(post => post.id == post_id);
            let changedPost = {...this.feed[changedPostIdx]};
            changedPost.visible = data.visible;
            this.feed[changedPostIdx] = changedPost;
        })
    }

    @action highlightPost(data) {
        const post = this.feed.find(_post => _post.id == data.post_id);
        if(post) {
            this.highlight = (this.highlight != data.post_id) ? data.post_id : null;
        } else {
            // post does not exist, but remove previous highlight
            this.highlight = null;
        }
    }

    @action deletePost(data) {
        this.feed = this.feed.filter(post => post.id != data.post_id)
    }

    @action fetchAll() {
        this.socket.connection.emit('post_fetch_all');
    }

    @action removePost(data) {
        let feed = [...this.feed];
        data.post_ids.forEach(post_id => {
            const removedPostIdx = this.feed.findIndex(post => post.id == post_id);
            feed.splice(removedPostIdx, 1);
        });
        this.feed = feed;
    }

    @action setPosts(posts) {
        this.feed = posts;
        this.setIsLoading(false);
    }

    @action submitFile(route, file, cb) {
        const formData = new FormData();
        formData.append('file', file);

        // set loader
        this.api.defaults.onUploadProgress = cb;

        return this.api.post(route, formData)
    }

    @action resetPost() {
        this.isLoading = false;
    }

    @action submitPost(post) {
        this.isLoading = true;
        this.socket.connection.emit('post_submit', post);
    }

    @action sort() {
        this.feed = _.sortBy(this.feed, 'timestamp', false);
    }

    @action listen() {
        this.socket.connection.on('post_add', post => this.addPost(post));
        this.socket.connection.on('post_set_all', posts => this.setPosts(posts));
        this.socket.connection.on('post_toggle_visibility', data => this.toggleVisibility(data));
        this.socket.connection.on('post_highlight', data => this.highlightPost(data));
        this.socket.connection.on('post_remove', data => this.removePost(data));
        this.socket.connection.on('post_reset', () => this.resetPost());
    }
}
