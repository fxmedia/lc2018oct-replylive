<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-plus"></i> Create</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.sms.create') }}">Add Sms</a></li>
    </ul>
    <div class="panel-heading"><i class="fa fa-upload"></i> Upload</div>
    <div class="panel-body panel-uploader">
        {!! Form::open(['route' => 'backend.sms.import', 'files' => true]) !!}
        {!! Form::file( 'sms_upload', ['class' => 'importfile'] ) !!}
        <br>
        {!! Form::submit( 'Upload', ['class' => 'btn btn-sm btn-primary'] ) !!}
        {!! Form::close() !!}
    </div>
    <div class="panel-heading"><i class="fa fa-download"></i> Download</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.sms.export') }}">Download all Sms</a></li>
        <li class="list-group-item"><a href="{{ route('backend.sms.report') }}">Download Logs</a></li>
    </ul>

    <div class="panel-heading"><i class="glyphicon glyphicon-wrench"></i> Actions</div>
    <ul class="list-group">
        {{--<li class="list-group-item">--}}
        {{--<form action="{{ URL::route('backend.sms.reset') }}" method="post" onsubmit="return confirm('Are you sure you wish to reset all scores?')">--}}
        {{--{!! csrf_field() !!}--}}
        {{--<button type="submit" class="btn-link">Reset all quiz scores</button>--}}
        {{--</form>--}}
        {{--</li>--}}
    </ul>

    <div class="panel-heading"><i class="glyphicon glyphicon-euro"></i> Credits</div>
    <ul class="list-group">
        <li class="list-group-item">Credits:<span class="pull-right badge">{{ $balance->amount }}</span></li>
    </ul>

    <div class="panel-heading"><i class="glyphicon glyphicon-stats"></i> Statistics</div>
    <ul class="list-group">
        <li class="list-group-item"><span class="pull-right badge">{{ count($sms) }}</span>Total email templates: </li>
        <li class="list-group-item"><span class="pull-right badge">{{ $sent }}</span>Sms sent: </li>
        <li class="list-group-item"><span class="pull-right badge">{{ $processing }}</span>Sms processing: </li>
        <li class="list-group-item"><span class="pull-right badge">{{ $failed }}</span>Sms failed: </li>

        {{--@if(isset($amountQuestions))--}}
        {{--<li class="list-group-item"><span class="pull-right badge">{{ $amountQuestions }}</span>Total Questions:</li>--}}
        {{--@endif--}}
        {{--@if(isset($totalAnswers))--}}
        {{--<li class="list-group-item"><span class="pull-right badge">{{ $totalAnswers }}</span>Total Answers:</li>--}}
        {{--@endif--}}
    </ul>
</div>