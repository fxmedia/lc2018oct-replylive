<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOpenQuestionMultipleAnswersSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('open_questions', function(Blueprint $table){
            $table->boolean('multiple_answers')->after('tags')->default(false);
            $table->dropColumn('settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('open_questions', function(Blueprint $table){
            $table->text('settings')->after('question')->nullable();
            $table->dropColumn('multiple_answers');
        });
    }
}
