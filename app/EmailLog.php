<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\EmailLog
 *
 * @property int $id
 * @property int $email_id
 * @property int $user_id
 * @property int|null $batch
 * @property string|null $error
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Email $email
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailLog whereBatch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailLog whereEmailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailLog whereError($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailLog whereUserId($value)
 * @mixin \Eloquent
 * @property string $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailLog whereStatus($value)
 */
class EmailLog extends Model
{
    //
    protected $table = 'email_log';
    protected $fillable = ['email_id','user_id','batch', 'status','error'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function email()
    {
        return $this->belongsTo('App\Email');
    }
}
