@if( isset($user) )
    {!! Form::model( $user, ['class'=>'form-horizontal user-admin', 'method'=>'patch', 'files'=>'true', 'route' => ['backend.users.update', $user->id]] ) !!}
@else
    {!! Form::open(['class'=>'form-horizontal user-admin', 'route' => 'backend.users.store', 'files'=>'true']) !!}
@endif
{!! Form::hidden('redirect', (isset($redirect) ? $redirect : null) ) !!}
<div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
    {!! Form::label('firstname','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('firstname', Input::old('firstname'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('firstname') !!}</span>
</div>

<div class="form-group {{ $errors->has('lastname') ? 'has-error' : '' }}">
    {!! Form::label('lastname','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('lastname', Input::old('lastname'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('firstname') !!}</span>
</div>

<div class="form-group">
    {!! Form::label('group','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        @forelse ($groups as $group )
            <label>
                @if( isset($user) )
                    {!! Form::checkbox('groups[' . $group->id . ']', $group->id, $user->isInGroup($group) ) !!}
                @else
                    {!! Form::checkbox('groups[' . $group->id . ']', $group->id ) !!}
                @endif
                {{ $group->name }}
            </label><br>
        @empty
            No user groups
        @endforelse
    </div>
</div>

<div class="form-group">
    {!! Form::label('phonenumber','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('phonenumber', Input::old('phonenumber'),['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('email','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('email', Input::old('email'),['class'=>'form-control']) !!}
    </div>
</div>

{{--<div class="form-group">--}}
    {{--{!! Form::label('Bonus Points','', ['class' => 'col-sm-3 control-label']) !!}--}}
    {{--<div class="col-sm-9">--}}
        {{--{!! Form::text('bonus_points', Input::old('bonus_points'),['class'=>'form-control']) !!}--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group">
    {!! Form::label('image','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        @if( isset($user) && isset($user->image_path) )
            <img src="{{ $user->image_path }}" class="img-responsive">
        @endif
        {!! Form::file( 'image' ) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('Test User', '', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <div class="form-switch">
            <input class="on" id="test_userOn" type="radio" name="test_user" value="1" @if(isset($user) && $user->test_user == '1') checked @endif>
            <input class="off" id="test_userOff" type="radio" name="test_user" value="0" @if((isset($user) && !$user->test_user == '1') || !isset($user)) checked @endif>
            <div class="switch">
                <label class="on" for="test_userOff">On</label>
                <label class="off" for="test_userOn">Off</label>
            </div>
        </div>
    </div>
</div>
{{--<div class="col-sm-8">--}}
{{--</div>--}}


<hr>
<div class="well form-group">
    <div class="form-group">
        <div class="col-sm-3"></div>
        <div class="col-sm-3">
            Label (Prefix with _ for event website)
        </div>
        <div class="col-sm-5">
            Value
        </div>
    </div>
    {!! Form::label('extra','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">

        <?php $c = 0; ?>
        @if(isset($user) && is_array($user->extra))
            @foreach($user->extra as $key => $value)
                <div class="form-group array-row" data-row="{{ $c }}">
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="extra[{{ $c }}][key]" value="{{ $key }}" />
                    </div>
                    @if (is_array($value))
                        @foreach($value as $v)
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="extra[{{ $c }}][value][]" value="{!! $v !!}" />
                            </div>
                        @endforeach
                    @else
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="extra[{{ $c }}][value]" value="{!! $value !!}" />
                        </div>
                    @endif
                    <div class="col-sm-1">
                        <div class="btn btn-danger remove-row" data-type="user" data-row="{{ $c }}">Delete</div>
                    </div>
                </div>
                <?php $c++; ?>
            @endforeach
        @endif
            <div class="form-group extra-empty">
                <div class="col-sm-4">
                    <input type="text" class="key form-control" name="extra[{{ $c }}][key]" data-replace="key" />
                </div>
                <div class="col-sm-7">
                    <input type="text" class="value form-control" name="extra[{{ $c }}][value]" data-replace="value" />
                </div>
                <div class="col-sm-1">
                    <div class="btn btn-info add-row" data-type="user" data-key="{{ $c }}">Add</div>
                </div>
            </div>
    </div>


</div>

<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
        {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
    </div>
</div>


{!! Form::close() !!}

