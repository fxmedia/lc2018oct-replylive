<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Sms
 *
 * @property int $id
 * @property string $name
 * @property string $subject
 * @property string $body_text
 * @property string|null $action
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SmsLog[] $logs
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereBodyText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sms whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Sms extends Model
{
    //
    protected $table = 'sms';
    protected $fillable = ['name', 'subject', 'body_text', 'action'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany('App\SmsLog');
    }

    /**
     * Returns list of implemented automated actions
     * @return array
     */
    public static function getActions()
    {
        return [
            'noaction' => 'No action'
        ];
    }


    /**
     * @param string $action
     * @return Model|null|static
     */
    public static function getByAction($action)
    {
        return Sms::whereAction($action)->first();
    }
}
