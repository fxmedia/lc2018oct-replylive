import React from 'react';
import moment from 'moment';

import Image from '../utils/Image';

const TIMEZONE_CORRECTION = (new Date(0).getTimezoneOffset()) * 60 * 1000; // timezone offset is in minutes, and we need it in 1/1000 of a second

export default function Post(props) {
    if(!props.data || !props.data.user) return null;


    const getStyling = () => {
        let base = "Post";
        if(props.data.image_path) {
            base += " with-image"
        }
        if(props.highlight) {
            base += " highlight"
        }
        return base;
    }

    const getTimeAgo = (dateTime) => {
        if(!dateTime) return { str: "unknown time", total: 1 };

        const placed = moment(dateTime).valueOf();
        const now = Date.now();
        const elapsed = Math.max(now - placed + TIMEZONE_CORRECTION, TIMEZONE_CORRECTION); // make its not below utc timestamp

        const diff = new Date(elapsed);
        const hours = diff.getHours();
        const minutes = diff.getMinutes();
        const seconds = diff.getSeconds();

        if(hours > 0) return { str: `${hours} hour`, total: hours }
        if(minutes > 0) return { str: `${minutes} minute`, total: minutes };
        if(seconds >= 0) return { str: `${seconds} second`, total: seconds };

        return { str: "unknown time", total: 1 }
    }

    const timeAgo = getTimeAgo(props.data.created_at);
    const avatar = props.data.user.image_path || "/images/avatar.png";
    console.log(avatar)

    return (
        <div
            className={getStyling()}
            onClick={props.handleClick}
        >
            <div className="message-wrapper">
                {props.data.image_path ? <img src={props.data.image_path} /> : null}
                {props.data.message &&
                    <p className="message">
                        {props.data.message}
                    </p>
                }
            </div>
            <div className="user-wrapper">
                <div className="avatar-wrapper">
                    <div className="avatar" style={{ backgroundImage: `url(${avatar})` }} />
                </div>
                <div className="details">
                    <h4 className="name">{props.data.user.firstname} {props.data.user.lastname}</h4>
                    <span className="timestamp">{timeAgo.str}{timeAgo.total !== 1 ? "s" : ""} ago</span>
                </div>
                <span className="identifier">#{props.data.id}</span>
            </div>
        </div>
    )
}
