<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDecidingQuestionKnockout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('knockouts', function(Blueprint $table){
            $table->string('deciding_question')->after('tags')->nullable();
            $table->integer('deciding_answer')->after('deciding_question')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('knockouts', function(Blueprint $table){
            $table->dropColumn('deciding_answer');
            $table->dropColumn('deciding_question');
        });
    }
}
