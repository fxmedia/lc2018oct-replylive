<?php

namespace App\Http\Controllers\Frontend;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class HelpdeskController extends Controller
{
    public function index(){
        $eventName = Setting::getValue('event_name');
        $timezone = Setting::getValue('timezone');
        $users = User::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

        return view('frontend.helpdesk.index', compact('eventName', 'timezone', 'users'));
    }
}
