import React from 'react';
import { inject, observer } from 'mobx-react';
import Question from '../components/games/Question';
import GameLabel from '../components/games/Label';

import AnswerUserTotal from '../components/games/AnswerUserTotal';

const LearningCurvePage = inject("stores", "t")(observer((props) => {
    if(!props.stores.curve.data) return null;

    const { totalUsers, totalAnswers } = props.stores.curve.display;
    const { name } = props.stores.curve.data;
    console.log(totalUsers, totalAnswers);

    return (
        <div className="CurvePage game-wrapper">
             <GameLabel text={props.t.curve.name} />
             <Question text={name} />
            <div className="counter-container">
                {totalAnswers.map((total, idx) =>
                    <AnswerUserTotal key={idx} totalAnswers={total} totalUsers={totalUsers} questionNumber={idx + 1} />
                )}
            </div>
        </div>
    );
}));

export default LearningCurvePage;