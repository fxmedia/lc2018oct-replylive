import UserStore from './stores/User';
import DisplayStore from './stores/Display';
import KnockoutStore from './stores/Knockout';
import LeaderBoardStore from './stores/LeaderBoard';
import OpenQuestionStore from './stores/OpenQuestion';
import PollStore from './stores/Poll';
import PushStore from './stores/Push';
import PledgeStore from './stores/Pledge';
import QuizStore from './stores/Quiz';
import PostStore from './stores/Post';
import SurveyStore from './stores/Survey';
import LearningCurveStore from './stores/LearningCurve';
import TimerStore from './stores/Timer';
import UserSocketIO from './user/backend/Socket';
import DisplaySocketIO from './display/backend/Socket';
import Api from './api';

export default function createStores(namespace) {

    const socket = (namespace == "users") ? new UserSocketIO(namespace) : new DisplaySocketIO(namespace);
    const mainStore = (namespace == "users") ? new UserStore(socket, Api) : new DisplayStore(socket);

    return {
        main: mainStore,
        knockout: new KnockoutStore(socket, mainStore),
        oq: new OpenQuestionStore(socket, mainStore),
        post: new PostStore(socket, mainStore, Api),
        poll: new PollStore(socket, mainStore),
        push: new PushStore(socket, mainStore),
        pledge: new PledgeStore(socket, mainStore),
        quiz: new QuizStore(socket, mainStore),
        survey: new SurveyStore(socket, mainStore),
        lb: new LeaderBoardStore(socket, mainStore),
        timer: new TimerStore(socket, mainStore),
        curve: new LearningCurveStore(socket, mainStore)
    }
}
