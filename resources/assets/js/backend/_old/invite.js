$(document).ready(function() {
    
    var mailsSent = 0,
        mailSentUrl = sentUrl,
        $modal = $('#users'),
        sending = false,
        userIds = [];

    $('.select-all').on('click', function ( e ) {
        e.preventDefault();

        var checking = ($(this).html() == 'Select all');

        $('.user-row:visible input[type="checkbox"]').prop('checked', checking);

        if(checking) {
            $( this ).html( 'De-select all' )
        } else {
            $( this ).html( 'Select all' )
        }
    });

    $('.select-waiting').on('click', function ( e ) {
        e.preventDefault();

        var checking = ($(this).html() == 'Select waiting');

        $('.user-row:visible.waiting input[type="checkbox"]').prop('checked', checking);

        if(checking) {
            $( this ).html( 'De-select waiting' )
        } else {
            $( this ).html( 'Select waiting' )
        }
    });

    $('.select-attending').on('click', function ( e ) {
        e.preventDefault();

        var checking = ($(this).html() == 'Select attending');

        $('.user-row:visible.attending input[type="checkbox"]').prop('checked', checking);

        if(checking) {
            $( this ).html( 'De-select attending' )
        } else {
            $( this ).html( 'Select attending' )
        }
    });

    $('.select-other').on('click', function ( e ) {
        e.preventDefault();

        var checking = ($(this).html() == 'Select other');

        $('.user-row:visible:not(.attending):not(.waiting) input[type="checkbox"]').prop('checked', checking);

        if(checking) {
            $( this ).html( 'De-select other' )
        } else {
            $( this ).html( 'Select other' )
        }
    });

    $('.user-row input[type="checkbox"]').on('click', function(e) {
        e.stopPropagation();
    });

    $('.user-row').on('click', function() {
        $(this).find('input[type="checkbox"]').prop('checked', !$(this).find('input[type="checkbox"]').prop('checked'));
    });
    
    $('.create-attending').on('click', setAttending.bind(this, true, updateUrl));
    $('.stop-attending').on('click', setAttending.bind(this, false, updateUrl));
    $('.force-attending').on('click', setAttending.bind(this, true, waitingUrl));
    $('.remove-waiting').on('click', setAttending.bind(this, false, waitingUrl));

    function setAttending(attending, url, event) {
        event.stopPropagation();
        var id = $(event.currentTarget).parent().parent().attr('id').replace('user_', '');
        
        $.ajax({
            url: url,
            method: 'post',
            data: {user_id: id, attending: attending},
            success: function( response ) {
                console.log('response', response);
                document.location.reload();
            }
        })
    }
    
    $('#filter-group').change(function(e) {
        e.preventDefault();

        var val = $(this).val();

        if(val == 'all') {
            $('.user-row').show();
        } else if(val == 'no_group') {
            $('.user-row.group_id__').show();
            $('.user-row:not(.group_id__)').hide();
        } else {
            $('.user-row.group_id_'+ val).show();
            $('.user-row:not(.group_id_'+ val +')').hide();
        }
    });

    $('.send-action').on('click', function ( e ) {
        e.preventDefault();

        var type = $(this).data('type'),
            template = $(this).data('template'),
            ids = getCheckedUserIds();

        if(!confirm('Are you sure you want to ['+type+'] with template ['+template+'] to ['+ids.length+'] select users?')) return false;

        sendMultiple(ids, type, template);
    });


    function getCheckedUserIds() {
        var ids = [];
        $('.user-row').each(function(key, row) {
            if(!$(row).find('input[type="checkbox"]').prop('checked')) return true;
            ids.push(parseInt($(row).attr('id').replace('user_', '')));
        });
        return ids;
    }
    
    function sendMultiple(ids, type, template) {
        sending = ids.length;
        mailsSent = 0;
        userIds = ids;

        showProgressBar('Sending [' + type + '] template [' + template + ']', ids.length, function(){ sending = false; });

        send(userIds[0], type, template);
    }
    
    function send( user_id, type, template )
    {
        console.log('sendmail : ', user_id, type, template);

        if(!sending) return false;

        $.ajax({
            url: mailSentUrl,
            method: 'post',
            data: { user_id: user_id, type: type, template: template },
            success: function( response ) {

                if( !response.success ) {
                    $('.progress-bar').removeClass('progress-bar-striped active');
                    $('.progress-stats').html('Failure in message');
                    $('#cancel').html('Close');
                    return false;
                }

                mailsSent++;

                var progress = (100 / sending) * mailsSent;
                updateProgress(progress);

                var index = userIds.indexOf(user_id);
                userIds.splice( index, 1 );

                if( userIds.length > 0 ) {
                    send( userIds[0], type, template );
                } else {
                    $('.progress-bar').removeClass('progress-bar-striped active');
                    $('.progress-stats').html('Completed!');
                    $('#cancel').html('Close');
                }

            }
        });
    }


    function showProgressBar(title, max, cancelCb) {
        title = title || 'Progress';
        max = max || 0;

        var $progress = {
            valuemin: 0,
            valuemax: 100,
            valuenow: 0,
            with:     '0%'
        };

        var progress = $( '<div>' ).addClass( 'progress' ),
            bar      = $( '<div>' ).addClass( 'progress-bar progress-bar-striped active' ).attr( 'role', 'progressbar' ).attr( 'aria-valuenow', $progress.valuenow ).attr( 'aria-valuemin', $progress.valuemin ).attr( 'aria-valuemax', $progress.valuemax ).css( 'width', $progress.with ),
            send     = $( '<span>' ).addClass( 'send' ).html( 1 ),
            stats    = $( '<div>' ).addClass( 'progress-stats' ).html( 'Send <span id="progress-min">0</span> of <span id="progress-max">' + max + '</span>' );
        progress.append( bar );

        $modal.find( '.modal-title' ).html( title );
        $modal.find( '.modal-body' ).empty().append( progress ).append( stats );
        $modal.find( '#cancel' ).on('click', cancelCb);
        $modal.modal({show: true});
    }

    function updateProgress(progress) {
        var width = progress + '%';
        $('.progress-bar').attr('role','progressbar').attr('aria-valuenow', progress).css('width', width);
        $('#progress-min').html( mailsSent );
    }

    function hideProgress() {
        $modal.modal({show: false});
    }

    window.spb = showProgressBar;

});