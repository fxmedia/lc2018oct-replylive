(function(scope, $){
    console.log('Quizzes.js');

    class _Quiz {
        constructor() {
            this.routes = scope.api_routes;
            this.$modal = $('#quizModal');
            this.quiz = null;
            this.currentQuestion = 0;
            this.stateCheck = false;

            this.$btnNext = this.$modal.find('.next');
            this.$btnCheckAnswer = this.$modal.find('.check-answer');

            $('.toggle-quiz-start-stop').on('click', this.toggleQuizStart.bind(this));

            this.$btnNext.on('click', this.showQuestion.bind(this));
            this.$btnCheckAnswer.on('click', this.checkAnswer.bind(this));

            this.$modal.find('.close').on('click', this.stopQuiz.bind(this));
            this.$modal.find('.end').on('click', this.stopQuiz.bind(this));
            this.$modal.find('.results').on('click', this.toggleResults.bind(this));

            let active = $('.toggle-quiz-start-stop.btn-danger');
            if(active.length > 0){
                // oh noes ! dont know what to do...
                var quizId = active.parent().data('quiz-id');
                var questionIndex = parseInt(active.data('question-index'));
                this.stateCheck = (active.data('check-answer')) || false;

                if(isNaN(questionIndex) || !quizId){
                    // something is wrong with this.
                    this.error({error: 'Quiz '+quizId+' appears to be active, but data is set incorrectly on this page'});
                } else {
                    this.getCurrentQuizInfo(quizId, questionIndex);
                }
            }
        }

        getCurrentQuizInfo(quizId, questionIndex){
            var route = this.getRoute(quizId, 'getData');

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                console.log('current quiz info:', data);
                this.quiz = data.quiz;
                this.currentQuestion = questionIndex;
                //this.stateCheck = false;

                this.setModal();
                this.$modal.modal('show');
            }).error(this.error);
        }

        getRoute(quiz_id, key) {
            return this.routes[quiz_id][key];
        }

        toggleQuizStart(event){
            var el = $(event.currentTarget),
                quizId = el.parent().data('quiz-id'),
                isActive = el.hasClass('btn-danger');

            isActive
                ? this.stopQuiz()
                : this.startQuiz(el, quizId);
        }

        stopQuiz(){
            if(confirm('are you sure you want to stop this quiz?')){
                var route = this.getRoute('other', 'stop');

                $.ajax(route, {
                    method: 'Post'
                }).success(data => {
                    if(!data || !data['success']) return this.error(data);

                    this.quiz = null;
                    this.$modal.modal('hide');
                    document.location.reload(); // lol nice wallhack this is
                }).error(this.error);
            }
        }

        startQuiz(el, quizId){
            var route = this.getRoute(quizId, 'start'),
                questionIndex = el.data('question-index') || 0;

            if(!route){
                // something went wrong
                return this.error({success: false, error: 'No route found to start this quiz.'})
            }

            // this part should probably never happen
            var currentActive = $('.toggle-quiz-start-stop.btn-danger').length > 0;

            if(currentActive && !confirm('There is already a poll active, do you wish to proceed to this question?')) {
                return;
            }
            // end of part which probably never should happen

            $.ajax(route, {
                method: 'POST',
                data: {
                    question_index: questionIndex
                }
            }).success(data => {
                if(!data || !data['success']) return this.error(data);
                console.log('start click success', data);

                this.quiz = data.data.quiz;
                this.currentQuestion = data.data.question_index;
                this.stateCheck = false;

                this.setModal();
                this.$modal.modal('show');

            }).error(this.error);
        }

        checkAnswer(event){
            event.preventDefault();
            var route = this.getRoute('other', 'checkAnswer');

            if(!route){
                // something went wrong
                return this.error({success: false, error: 'Url to set the answer on check was set improperly.'})
            }

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                this.stateCheck = true;
                this.setModal();
            }).error(this.error)
        }

        showQuestion(event){
            event.preventDefault();

            var el = $(event.currentTarget),
                questionIndex = parseInt(el.data('question-index')),
                route = this.getRoute(this.quiz.id,'showQuestion');

            if(el.attr('disabled')){
                // not allowed to click this thing.
                return;
            }

            if(!route){
                // something went wrong
                return this.error({success: false, error: 'Url to switch question was set improperly.'})
            }
            if(isNaN(questionIndex) ){
                return this.error({success: false, error: 'Question index was set improperly'});
            }

            $.ajax(route, {
                method: 'POST',
                data: {
                    question_index: questionIndex
                }
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                this.stateCheck = false;
                this.currentQuestion = questionIndex;

                this.setModal();

            }).error(this.error);
        }

        toggleResults(event){
            event.preventDefault();
            console.log('toggle results');
            var route = this.getRoute('other','toggleResults');
            $.ajax(route, {
                method: 'POST',
                data: {}
            }).success(data => {
                if(!data || !data['success']) return this.error(data);
            }).error(this.error);
        }


        setModal() {
            if(!this.quiz){
                // oh noes!
                return Notifications.error('Unable to set modal without current quiz');
            }

            this.$modal.find('.quiz-name').html( this.quiz.name );
            var question = this.quiz.questions[ this.currentQuestion ];

            if(!question){
                // oh noes!
                return Notifications.error('Unable to set modal for non existing question ' + this.currentQuestion);
            }

            this.$modal.find('.question-number').html(this.currentQuestion+1);
            this.$modal.find('.question-numbers').html(this.quiz.questions.length);
            this.$modal.find('.question-name').html(question.title);
            this.$modal.find('.question-answer').html(question.correct_answer);


            var listAnswers = $('<ol type="a">');
            for(var i=0; i< question.choices.length; i++){
                if(this.stateCheck && question.choices[i].choice === question.correct_answer){
                    listAnswers.append( $('<li class="correct">').html(question.choices[i].value) );
                } else {
                    listAnswers.append( $('<li>').html(question.choices[i].value) );
                }
            }
            this.$modal.find('.question-answers').html( listAnswers );

            console.log('set modal with currentQuestion', this.currentQuestion);

            this.$btnNext.data('question-index',this.currentQuestion+1).removeClass('hide');


            if(this.stateCheck){
                // quiz is in state of showing correct answer
                this.$btnCheckAnswer.addClass('hide');

                if((this.currentQuestion+1) === this.quiz['questions'].length){
                    console.log('locking last question!');

                    this.$modal.find('.results').removeClass('hide');
                    this.$modal.find('.end').removeClass('hide');
                    this.$btnNext.addClass('hide');
                } else {

                    this.$btnNext.removeAttr('disabled');
                }
            } else {
                // quiz is in state of ppl can do answer

                this.$btnCheckAnswer.removeClass('hide');
                this.$btnNext.attr('disabled','disabled');

                //this.$modal.find('.results').addClass('hide');
                this.$modal.find('.end').addClass('hide');
            }
        }

        error(xhr, status, errorThrown) {
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }
    }

    scope.quiz = new _Quiz();


})(this, $);
