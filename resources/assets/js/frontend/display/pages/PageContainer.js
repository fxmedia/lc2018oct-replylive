import Home from './Home';
import Knockout from './Knockout';
import LeaderBoard from './LeaderBoard';
import OpenQuestion from './OpenQuestion';
import Poll from './Poll';
import Pledge from './Pledge';
import Quiz from './Quiz';
import Timer from './Timer';
import Post from './Post';
import Waiting from './Waiting';
import LearningCurve from './LearningCurve';

module.exports = {
    home: Home,
    post: Post,
    poll: Poll,
    pledge: Pledge,
    quiz: Quiz,
    waiting: Waiting,
    oq: OpenQuestion,
    knockout: Knockout,
    lb: LeaderBoard,
    timer: Timer,
    curve: LearningCurve
}
