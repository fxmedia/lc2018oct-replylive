<tfoot>
    <tr>
        <td colspan="2">
            {!! Form::select('perform', [
                'delete' => 'Delete Answers'
            ], 'send_text', ['class'=>'form-control pull-left perform-selector']) !!}
        </td>
        <td>
            {!! Form::submit('Submit', ['class' => 'btn btn-md pull-left btn-primary']) !!}
        </td>
    </tr>
</tfoot>
