<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInBetweenScreenToSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // this doesn't work unfortunately
        // Schema::table('quiz_questions', function(Blueprint $table){
        //     $table->enum('type', config('surveys.question_types'))->change();
        // });
        $baseQuery = "ALTER TABLE survey_questions CHANGE COLUMN type type ENUM(";
        foreach(config('surveys.question_types') as $type) {
            $baseQuery .= "'" . $type . "',";
        }
        $query = rtrim($baseQuery, ",") . ")";
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
