const GameManager = require('./Game');

class SurveyManager extends GameManager {
    constructor(Admin) {
        super(Admin, "survey");
        this.currentState = { filter: ['users'], users: {} };
    }

    handle(action, data) {
        switch(action) {
            case 'survey_start':
                this.start(action, data);
                break;
            case 'survey_stop':
                this.stop(action, data);
                break;
            case 'survey_start_background':
                this.startBackground(action, data);
                break;
            case 'survey_stop_background':
                this.stopBackground(action, data);
                break;
        }
    }

    saveNextQuestion(data, userSocket) {
        const userId = this.Admin.getConnection("user", userSocket.id).id;
        if(!this.currentState.users[userId]) this.currentState.users[userId] = {};
        if(!this.currentState.users[userId][data.survey_id])
            this.currentState.users[userId][data.survey_id] = { question_index: 0 };

        this.currentState.users[userId][data.survey_id].question_index++;
        console.log("current question idx is: ", this.currentState.users[userId][data.survey_id].question_index);
    }

    start(action, data) {
        this.setBroadcastLevel(data);
        const extraction = {
            user: this.extract(data, ["questions", "takeover", "intro", "outro", "id", "name"]),
            display: {}
        };
        this.broadcastToUsers(action, extraction.user);
        this.Admin.setActiveAction({ action, data: extraction });
    }

    stop(action, data) {
        if(this.isActive()) {
            this.currentState.users = {};
            this.broadcastToUsers(action, this.Admin.activeAction.data.user);
            this.Admin.setActiveAction(null);
        }
    }

    startBackground(action, data) {
        this.setBroadcastLevel(data);
        const extraction = {
            user: this.extract(data, ["questions", "takeover", "intro", "outro", "id", "name"]),
            display: {}
        };
        this.Admin.addToBackground("survey", extraction.user);
        this.broadcastToUsers(action, extraction.user);
    }

    stopBackground(action, data) {
        this.Admin.removeFromBackground("survey", data.survey_id);
        this.broadcastToUsers(action, data);
    }

    status() {
        let survey_ids = [];
        if(this.Admin.backgroundState.survey) {
            this.Admin.backgroundState.survey.forEach(survey => survey_ids.push(survey.id));
        }
        if(!this.Admin.activeAction || this.Admin.getManager(this.Admin.activeAction.action) !== this.namespace) {
            return { survey_ids };
        }

        return {
            survey_ids: survey_ids.concat([this.Admin.activeAction.data.user.id])
        };
    }

    submitAnswer(answer, userSocket) {
        this.Admin.save('/survey/answer', answer, userSocket.id)
        .catch(err => {
            this.Admin.sendError(userSocket, err)
            const userId = this.Admin.getConnection("user", userSocket.id).id;
            this.Admin.broadcastToUsers([userId], 'survey_reset_answer');
        })
        .then(result => {
            if(result.data.success) {
                this.Admin.broadcastToUsers([result.data.answer.user_id], 'survey_add_answer', result.data.answer);
                const userId = result.data.answer.user_id;
                const surveyId = result.data.survey_id;

                if(!this.currentState.users[userId]) this.currentState.users[userId] = {};
                if(!this.currentState.users[userId][surveyId]) this.currentState.users[userId][surveyId] = {question_index: 0};

                this.currentState.users[userId][surveyId].question_index++;
            } else {
                this.Admin.sendError(userSocket, "The answer could not be saved..");
                const userId = this.Admin.getConnection("user", userSocket.id).id;
                this.Admin.broadcastToUsers([userId], 'survey_reset_answer');
            }
        })
    }
}

module.exports = SurveyManager;
