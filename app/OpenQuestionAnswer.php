<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\OpenQuestionAnswer
 *
 * @property int $id
 * @property int $open_question_id
 * @property int $user_id
 * @property string $answer
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\OpenQuestion $openQuestion
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestionAnswer whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestionAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestionAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestionAnswer whereOpenQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestionAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OpenQuestionAnswer whereUserId($value)
 * @mixin \Eloquent
 */
class OpenQuestionAnswer extends Model
{
    protected $fillable = ["open_question_id", "user_id", "answer"];
    protected $table = "open_question_answers";

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function openQuestion() {
        return $this->belongsTo('App\OpenQuestion');
    }
}
