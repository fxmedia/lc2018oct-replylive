<?php

namespace App\Import;

use App\Group;
use App\LearningCurve;
use App\LearningCurveQuestion;
use Validator;

class LearningCurveImport extends BaseImport
{

    public function getHeaders(){
        return ['id', 'learning_curve', 'tags', 'groups', 'question' , 'question_id', 'correct_answer' ,'choice', 'value'];
    }

    private function getGroupsIds($groups = null){
        if(isset($groups) && strlen($groups) > 0){
            if(count($names = explode(',',$groups)) > 0){
                $groupIds = [];

                foreach($names as $name){
                    // if given group doesn't exist, create it!
                    if(!$dbGroup = Group::where('name', trim($name))->first()){
                        $dbGroup = new Group;
                        $dbGroup->name = trim($name);
                        $dbGroup->save();
                    }
                    // store the id in the array
                    $groupIds[] = $dbGroup->id;
                }

                return $groupIds;
            }
        }

        return [];
    }

    public function import(){
        $skipCount = 0;
        $addCount = 0;
        $addQuestionCount = 0;
        $addChoiceCount = 0;
        $warnings = [];
        $errors = [];

        $idLink = []; // for learning_curves
        $idLinkQ = [];// for questions

        foreach($this->data as $key => $obj){
            if(
                ($obj->id !== null && $obj->learning_curve !== null ) &&
                ($obj->id !== null && $obj->question_id !== null) &&
                ($obj->question_id !== null && $obj->choice !== null)
            ) {
                // skip, cause it misses identifier fields
                $warnings[] = 'Skipped row '. ($key+1) . ': neither learning_curve, nor question nor choice';
                $skipCount++;
                continue;
            }

            $data = [];
            foreach($obj as $k => $v){
                if($v !== null && $k !== 'id')$data[$k] = $v;
            }


            if($obj->learning_curve != null){
                $data['name'] = $obj->learning_curve;

                $validation = Validator::make($data,config('validation.learning_curves.import.learning_curve'));

                if($validation->fails()){
                    $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                    $skipCount++;
                    continue;
                }
                $learning_curve = new LearningCurve($validation->getData());

                if($learning_curve->save()){
                    $idLink[$obj->id] = $learning_curve->id;

                    $groupIds = $this->getGroupsIds($obj['groups']);
                    $learning_curve->groups()->sync($groupIds);

                    $addCount++;
                } else {
                    $errors[] = 'Unable to save LearningCurve on row '. ($key+1);
                    $skipCount++;
                }

            } else if($obj->question != null){
                $data['title'] = $obj->question;

                $validation = Validator::make($data, config('validation.learning_curves.import.question'));

                if($validation->fails()){
                    $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                    $skipCount++;
                    continue;
                }

                if(!isset($idLink[$obj->id])){
                    $errors[] = "Question link with learning_curve id on row ". ($key+1) . ' is not set correctly';
                    $skipCount++;
                    continue;
                }

                $question = new LearningCurveQuestion($validation->getData());
                $question->learning_curve_id = $idLink[$obj->id];
                $question->choices = [];

                if($question->save()){
                    $idLinkQ[$obj->question_id] = $question->id;
                    $addQuestionCount++;
                } else {
                    $errors[] = 'Unable to save Question on row '. ($key+1);
                    $skipCount++;
                }


            } else {
                $validation = Validator::make($data, config('validation.learning_curves.import.choices'));

                if($validation->fails()){
                    $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                    $skipCount++;
                    continue;
                }

                if(!isset($idLinkQ[$obj->question_id])){
                    $errors[] = "Choice link with question id on row ". ($key+1) . ' is not set correctly';
                    $skipCount++;
                    continue;
                }

                $question = LearningCurveQuestion::find($idLinkQ[$obj->question_id]);
                $question->choices = array_merge( $question->choices, [$validation->getData()]);

                if($question->save()){
                    $addChoiceCount++;
                } else {
                    $errors[] = "Failed saving choice on row ". ($key+1) ." on question ".$obj->question_id;
                    $skipCount++;
                }

            }
        }

        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $addCount . ' LearningCurve'. ($addCount == 1? '' : 'zes') .' added',
                'With '. $addQuestionCount . ' Question'. ($addCount == 1? '' : 's'),
                'With '. $addChoiceCount . ' Choice'. ($addCount == 1? '' : 's')
            ]
        ];
    }
}
