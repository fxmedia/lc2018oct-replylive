<?php namespace App\Http\Controllers\Api;

use App\Knockout;
use App\KnockoutAnswer;
//use App\KnockoutQuestion;
use App\NodeJS\Command;
//use App\User;
use Illuminate\Http\Request;
use \Exception;

class KnockoutController extends ApiBaseController {

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeAnswer(Request $request) {
        $answer = KnockoutAnswer::firstOrNew([
            'knockout_question_id' => $request->data['knockout_question_id'],
            'user_id' => $request->input('user_id')
        ]);
        $answer->choice = $request->data['choice'];

        $success = $answer->save();

        return $this->respond(["success" => $success, "answer" => $answer]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData($id){
        $knockout = Knockout::find($id);

        if($knockout == null){
            return $this->respondWithError('No knockout found');
        }

        return $this->respond(['success' => true, 'knockout' => $knockout->toArray()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function start(Request $request, $id){
        // if request->question_index exists, start at that index, otherwise at 0
        $knockout = Knockout::with('groups:id')->find($id);

        if($knockout == null){
            return $this->respondWithError('Knockout with id '. $id .' was not found.');
        }
        $data = ['knockout' =>$knockout->toArray()];

        $index = $request->input('question_index') ?? 0;
        if($index < 0 || $index >= count($knockout->questions)) $index = 0;

        $data['question_index'] = $index;


        return $this->sendToNodeServer('knockout_start', $data, $data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkAnswer(){
        return $this->sendToNodeServer('knockout_check_answer', []);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showQuestion(Request $request, $id){
        // only care about question_index, but we can check if knockout for it exists and there is a question at that index
        $knockout = Knockout::with('groups:id')->find($id);

        if($knockout == null){
            return $this->respondWithError('Knockout with id '. $id .' was not found.');
        }
        $index = $request->input('question_index');
        if($index === null){
            return $this->respondWithError('No question index given');
        }

        if($index >= count($knockout->questions) ){
            return $this->respondWithError('Index is larger then amount of questions');
        }

        return $this->sendToNodeServer('knockout_show_question',['question_index' => $index]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function stop(){
        return $this->sendToNodeServer('knockout_stop');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function setDecidingQuestion(){
        return $this->sendToNodeServer('knockout_deciding_question');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatus(){
        $data = [
            'success' => true

        ];
        try {
            $status = json_decode( Command::status('knockout_status') ,true);

            if(!$status['knockout_id']) {
                return $this->respondWithError('Knockout currently not active');
            }

            $data['knockout_id'] = $status['knockout_id'];
            $data['players_left'] = $status['players_left'];

        } catch(Exception $e){
            return $this->respondWithError('Unable to get data from NodeJS server.');
        }

        return $this->respond($data);
    }

    public function showScore(){

    }
}
