class GameManager {
    constructor(Admin, namespace) {
        this.Admin = Admin;
        this.broadcastLevel = "all";
        this.group_ids = [];
        this.user_answers = null;
        this.userData = null;
        this.namespace = namespace;
    }

    broadcastToUsers(action, data, result_users) {
        switch(this.broadcastLevel) {
            case 'filter':
                this.Admin.broadcastToFilter(action, data, result_users, this.group_ids);
                break;
            case 'individual':
                this.Admin.broadcastToIndividuals(action, data, this.user_answers, this.group_ids);
                break;
            default:
                this.Admin.broadcastToChannel("user", action, data);
        }
    }


    extract(data, keys) {
        let extraction = {};
        if(Array.isArray(keys) && keys[0].hasOwnProperty("key")) {
            keys.forEach(obj => {
                if(obj.keys) {
                    extraction[obj.key] = {};
                    obj.keys.forEach(key => extraction[obj.key][key] = data[obj.key][key]);
                } else {
                    extraction[obj.key] = data[obj.key];
                }
            });
        } else {
            keys.forEach(key => extraction[key] = data[key]);
        }
        return extraction;
    }

    getGroupIds(data) {
        if(data.group_ids && data.group_ids.length > 0) {
            return data.group_ids;
        } else if (data[this.namespace] && data[this.namespace].group_ids && data[this.namespace].group_ids.length > 0) {
            return data[this.namespace].group_ids;
        } else if (data.groups && data.groups.length > 0) {
            return data.groups.map(group => group.id);
        }
        return null;
    }

    isActive() {
        return this.Admin.activeAction
               && this.Admin.getManager(this.Admin.activeAction.action) === this.namespace;
    }

    resetBroadcast() {
        this.group_ids = [];
        this.broadcastLevel = "all";
    }

    setBroadcastLevel(data) {
        this.resetBroadcast();

        const group_ids = this.getGroupIds(data);

        if(group_ids) {
            this.group_ids = group_ids;
        }

        if(data.user_answers) {
            this.broadcastLevel = "individual";
            this.user_answers = data.user_answers;
        } else {
            this.broadcastLevel = "filter";
        }
    }
}

module.exports = GameManager;
