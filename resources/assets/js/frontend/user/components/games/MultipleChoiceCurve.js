import React from 'react'
import ConfirmBox from './ConfirmBox'

export default function MultipleChoiceCurve(props) {

    const getStyling = () => {
        let base = "MultipleChoiceAnswer";

        if(props.isSelected && !props.isConfirmed) {
            base += " active";
        } else if(props.showRightWrong && props.choice == props.correctAnswer) {
            base += " correct";
        } else if(props.showRightWrong && !props.isCorrect && props.activeAnswer.choice == props.choice) {
            base += " wrong";
        } else if(props.isConfirmed) {
            base += " confirmed";
        } else if (props.isInActive) {
            base += " inactive";
        }
        return base;
    }

    const renderChoice = () => {
        if(props.showRightWrong && props.choice == props.correctAnswer)
            return <i className="fa fa-check" />
        else if(props.showRightWrong && props.activeAnswer.choice == props.choice && props.choice != props.correctAnswer)
            return <i className="fa fa-times" />
        else
            return props.choice
    }

    return (
      <li className={getStyling()} onClick={(props.isSelected || props.activeAnswer.checked) ? false : props.handleSelect}>
          <div className="choice">
              <span>{renderChoice()}</span>
          </div>
          <a className={"answer"}>
              <p className="text">{props.text}</p>
              <ConfirmBox
                  active={props.isSelected && !props.isConfirmed}
                  confirmAnswer={props.handleConfirm}
              />
          </a>
      </li>
    )
}
