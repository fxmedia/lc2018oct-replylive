import React from 'react';
import moment from 'moment';

import Image from '../utils/Image';

export default function PostHighlight(props) {
    if(!props.data || !props.data.user) return null;

    const getStyling = () => {
        let base = "Post highlight";
        if(props.data.image_path) {
            base += " with-image"
        }
        else {
            base += " no-image";
        }
        return base;
    }

    const getWrapperStyling = () => {
        let base = "message-wrapper";
        if(!props.data.message) {
            base += " no-text";
        }
        else {
            base += " with-text";
        }

        return base;
    }

    const getTimeAgo = (dateTime) => {
        if(!dateTime) return { str: "unknown time", total: 1 };

        const placed = moment(dateTime).valueOf();
        const now = Date.now();
        const elapsed = now - placed - 3600000; //// correction cause it always default to 1;

        const diff = new Date(elapsed);
        const hours = diff.getHours();
        const minutes = diff.getMinutes();
        const seconds = diff.getSeconds();

        if(hours > 0) return { str: `${hours} hour`, total: hours }
        if(minutes > 0) return { str: `${minutes} minute`, total: minutes };
        if(seconds >= 0) return { str: `${seconds} second`, total: seconds };

        return { str: "unknown time", total: 1 }
    }

    const timeAgo = getTimeAgo(props.data.created_at);

    return (
        <div
            className={getStyling()}
            onClick={props.handleClick}
        >
            <div className={getWrapperStyling()}>
            {props.data.image_path &&
                <img src={props.data.image_path} />
            }
            {props.data.message &&
                <p className="message">
                    {props.data.message}
                </p>
            }
            </div>
            <div className="user-wrapper">
                <div className="avatar-wrapper">
                    <img src={props.data.user.image_path} />
                </div>
                <div className="details">
                    <h4 className="name">{props.data.user.firstname} {props.data.user.lastname}</h4>
                    <span className="timestamp">{timeAgo.str}{timeAgo.total !== 1 ? "s" : ""} ago</span>
                </div>
                <span className="identifier">#{props.data.id}</span>
            </div>
        </div>
    )
}
