import React from 'react';
// import Image from '../utils/Image';
import { inject } from 'mobx-react';
import moment from 'moment';

const TIMEZONE_CORRECTION = (new Date(0).getTimezoneOffset()) * 60 * 1000; // timezone offset is in minutes, and we need it in 1/1000 of a second

const Post = inject("stores","t")((props) => {
    if(!props.data || !props.data.user) return null;

    const renderLayout = () => {
        let base = "Post";
        if(props.mine) {
            base += " mine";
        }
        if(!props.data.visible) {
            base += " unapproved";
        }
        return base;
    };

    const getTimeAgo = (dateTime) => {
        if(!dateTime) return { str: "unknown time", total: 1 };

        const placed = moment(dateTime).valueOf();
        const now = Date.now();
        const elapsed = Math.max(now - placed + TIMEZONE_CORRECTION, TIMEZONE_CORRECTION); // make its not below utc timestamp

        const diff = new Date(elapsed);
        const hours = diff.getHours();
        const minutes = diff.getMinutes();
        const seconds = diff.getSeconds();

        if(hours > 0) return { str: `${hours} hour`, total: hours };
        if(minutes > 0) return { str: `${minutes} minute`, total: minutes };
        if(seconds >= 0) return { str: `${seconds} second`, total: seconds };

        return { str: props.t.errors.unknown_time, total: 1 }
    };

    const timeAgo = getTimeAgo(props.data.created_at);
    const avatar = props.data.user.image_path || "/images/avatar.png";

    const onImageLoad = () => {
        // weird, this needs a small delay after the onload
        // and why do i have to add call?
        setTimeout(() => props.stores.post.scrollToLast.call(props.stores.post), 1);
    };

    return (
        <div className={renderLayout()}>
            <div className="header">
                <div className="image-wrapper">
                    <div className="bg-image avatar" style={{ backgroundImage: `url(${avatar})`}} />
                </div>
                <div className="details">
                    <h4 className="name">{props.data.user.name}</h4>
                    <span className="timestamp">{timeAgo.str}{timeAgo.total !== 1 ? "s" : ""} {props.t.post.ago}</span>
                </div>
            </div>
            <div className="message">
                {props.data.image_path ? <img src={props.data.image_path} onLoad={onImageLoad} /> : null}
                <p className="body">
                    {props.data.message}
                </p>
                {(!props.data.visible) ? <p className="unapproved-message">{props.t.post.awaiting_approval}</p> : null}
            </div>

        </div>
    )
});

export default Post;
