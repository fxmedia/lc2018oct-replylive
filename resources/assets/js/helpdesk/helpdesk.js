import $ from 'jquery';
window.jQuery = window.$ = $;

import Popper       from 'popper.js';
import bootstrap    from 'bootstrap';
import Swal         from 'sweetalert2';

import 'datatables.net';
import 'datatables.net-responsive';
import 'datatables.net-bs4';
import 'datatables.net-responsive-bs4';

jQuery(document).ready(function() {

    jQuery('#user-table').DataTable({
        responsive: true
    });

    jQuery('#user-table_wrapper').on('click', '.set-present', function(e){
        Swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#10a184',
            confirmButtonText: 'Yes, set user present',
            reverseButtons: true,
            showLoaderOnConfirm: true,
            allowOutsideClick: () => !Swal.isLoading(),
            preConfirm: () => {
                return new Promise( (resolve, reject) => {
                    jQuery.ajax('/api/scan', {
                        method:'POST',
                        data: {
                            barcode: jQuery(this).attr('data-token')
                        },
                        success: (data) => {
                            if (!data.success) {
                                console.error(data);
                            }
                            else {
                                resolve(data);
                            }
                        },
                        error: (data) => {
                            console.error(data);
                        }
                    });
                });
            },
        })
        .then((result) => {
            if (!result.dismiss) {
                Swal({
                    title: 'Success',
                    type: 'success',
                    text: "User is succesfully set to 'Present'"
                });

                jQuery( 'tr[data-token="'+jQuery(this).attr('data-token')+'"]').find('td.status').find('span').removeClass().addClass('badge badge-success').text('Present');
                jQuery( 'tr[data-token="'+jQuery(this).attr('data-token')+'"]').find('td.status').next().text('Helpdesk');
            }

        })
        .catch((error) => {
            Swal({
                title: 'Error',
                type: 'error',
                text: error
            });
        })
    });

});