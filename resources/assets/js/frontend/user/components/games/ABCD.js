import React from 'react';
import MultipleChoiceAnswer from './MultipleChoiceAnswer';
import SubmitButton from './SubmitButton';
import { inject, observer } from 'mobx-react';

@inject("t", "stores") @observer
export default class ABCD extends React.Component {

    handleConfirm(isConfirmed) {
        const { activeAnswer } = this.props.stores.survey;

        if(!isConfirmed) {
            this.props.stores.survey.selectAnswer(null);
        } else {
            this.props.stores.survey.submitAnswer(activeAnswer.selected);
        }
    }

    handleSelect(answer) {
        this.props.stores.survey.selectAnswer(answer);
    }

    render() {
        const { activeAnswer } = this.props.stores.survey;

        return (
            <div className="ABCD">
                {this.props.data.map((answer, idx) =>
                    <MultipleChoiceAnswer
                        isSelected={activeAnswer.selected == answer.choice}
                        isConfirmed={activeAnswer.choice == answer.choice}
                        handleSelect={this.handleSelect.bind(this, answer.choice)}
                        choice={answer.choice}
                        text={answer.value}
                        key={answer.choice}
                        block={activeAnswer.choice != null}
                        handleConfirm={this.handleConfirm.bind(this)}
                        isInActive={activeAnswer.choice && activeAnswer.choice != answer.choice}
                    />
                )}
            </div>
        )
    }
}
