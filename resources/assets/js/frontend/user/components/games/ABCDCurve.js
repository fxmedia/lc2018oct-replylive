import React from 'react';
import MultipleChoiceCurve from './MultipleChoiceCurve';
import { inject } from 'mobx-react';

@inject("t")
export default class ABCDCurve extends React.Component {

    handleSelect(answer) {
        this.props.handleSelect(answer);
    }

    render() {
        return (
            <ol className="ABCD ABCDCurve">
                {this.props.question.choices.map((answer) =>
                    <MultipleChoiceCurve
                        isSelected={this.props.activeAnswer.selected == answer.choice}
                        handleSelect={this.handleSelect.bind(this, answer.choice)}
                        handleConfirm={this.props.handleAnswer}
                        activeAnswer={this.props.activeAnswer}
                        correctAnswer={this.props.question.correct_answer}
                        choice={answer.choice}
                        isCorrect={this.props.isCorrect}
                        text={answer.value}
                        key={answer.choice}
                        showRightWrong={!!this.props.activeAnswer.checked}
                    />
                )}
            </ol>
        )
    }
}
