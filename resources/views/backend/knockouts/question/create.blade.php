@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Knockout Questions</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('backend.index') }}">Backend</a></li>
        <li><a href="{{ route('backend.knockouts.index') }}">Knockouts</a></li>
        <li><a href="{{ route('backend.knockouts.edit', [$knockout->id]) }}" >{{ $knockout->name }}</a></li>
        <li>New Question</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.knockouts.question._partials.knockout-question-form')

    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/forms.js') }}"></script>
@endsection
