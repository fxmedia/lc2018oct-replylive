import axios from 'axios';
import config from './config';

export default axios.create({
    baseURL: config.api_base_url,
    headers: {
        'content-type': 'multipart/form-data',
        'x-header-token': config.api_token
    },
    onUploadProgress: (progressEvent) => {
        console.log(progressEvent);
    },
});
