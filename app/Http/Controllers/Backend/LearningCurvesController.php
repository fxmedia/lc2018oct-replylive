<?php

namespace App\Http\Controllers\Backend;

use App\Export\LearningCurveExport;
use App\Import\LearningCurveImport;
use App\NodeJS\Command;
use App\Report\LearningCurveResultReport;
use App\LearningCurve;
use App\Group;
use App\LearningCurveAnswer;
use App\LearningCurveQuestion;
use Exception;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LearningCurvesController extends Controller
{
    //
    public function index(){
        $learning_curves = LearningCurve::all();
        $totalAnswers = LearningCurveAnswer::count();
        $amountQuestions = LearningCurveQuestion::count();

        try {
            $status = json_decode( Command::status('curve_status') ,true);

            $phase = (isset($status['phase'])) ? $status['phase'] : null;
            
            foreach($learning_curves as $learning_curve){
                if($status['curve_id'] && $status['curve_id'] == $learning_curve->id){
                    $learning_curve->isActive = true;
                }
            }

        } catch(Exception $e){}


        return view('backend.learning_curves.index', compact('learning_curves', 'phase', 'totalAnswers', 'amountQuestions'));
    }


    public function show($id){
        $learning_curve = LearningCurve::find($id);

        if($learning_curve === null){
            return redirect()->back()->withErrors(['message' => 'Unable to show non existing LearningCurve']);
        }

        return view('backend.learning_curves.show', compact('learning_curve'));
    }

    public function create(){
        $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

        return view('backend.learning_curves.create', compact('groups'));
    }

    public function store(Request $request){
        $validators = config('validation.learning_curves.store');

        $data = $request->validate($validators);

        $learning_curve = new LearningCurve;
        $learning_curve->fill($data);


        if(!$learning_curve->save()){
            return redirect()->back()->withErrors(['message'=>'Error saving new learning_curve']);
        }

        $groups = [];
        if($request->input('groups')){
            $groups = (array)$request->input('groups');
        }
        $learning_curve->groups()->sync($groups);

        return redirect()->route('backend.learning_curves.edit', [$learning_curve->id])->with(['message' => 'Successfully added new learning_curve. Go edit learning_curve to add questions']);
    }

    public function edit($id){
        $learning_curve = LearningCurve::find($id);

        if($learning_curve !== null){
            $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

            return view('backend.learning_curves.edit', compact('learning_curve','groups'));
        }

        return redirect()->back()->withErrors(['message' => 'Unable to find selected LearningCurve']);
    }

    public function update(Request $request, $id){
        $learning_curve = LearningCurve::find($id);

        if($learning_curve === null){
            return redirect()->back()->withErrors(['message' => 'Unable to update non existing LearningCurve']);
        }

        $validators = config('validation.learning_curves.store');

        $data = $request->validate($validators);

        if($learning_curve->update($data)){
            $groups = [];
            if($request->input('groups')){
                $groups = (array)$request->input('groups');
            }
            $learning_curve->groups()->sync($groups);

            return redirect()->route('backend.learning_curves.index')->with(['message' => 'Sucessfully updated learning_curve']);
        }

        return redirect()->back()->withErrors(['message' => 'Error saving updates to learning_curve']);
    }

    public function removeAll() {
        try {
            $learning_curves = LearningCurve::all();
            foreach($learning_curves as $learning_curve) {
                $learning_curve->delete();
            }
        } catch(Exception $e) {
            return redirect()->route('backend.learning_curves.index')->withErrors(['message' => 'Deleting all of the learning_curves failed..']);
        }
        return redirect()->back()->with('message', 'All learning_curves have been deleted');
    }

    public function remove($id){
        $learning_curve = LearningCurve::find($id);

        if($learning_curve === null){
            return redirect()->route('backend.learning_curves.index')->withErrors(['message' => 'Unable to remove non existing learning_curve']);
        }

        /**
         * check first maybe if current poll is active? and deactivate it
         */

        try{
            $removed = $learning_curve->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'LearningCurve has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing learning_curve.']);
    }

    public function reset(){
        $errors = [];
        try{
            LearningCurveAnswer::truncate();
            $success = true;
        } catch(Exception $err) {
            $success = false;
            $errors = $err->getMessage();
        }

        if($success) return redirect()->back()->with('message', 'All LearningCurves have been reset');

        return redirect()->back()->withErrors($errors);
    }

    public function resetSingle($id){
        $learning_curve = LearningCurve::find($id);

        if($learning_curve === null){
            return redirect()->route('backend.learning_curves.index')->withErrors(['message' => 'Unable to reset non existing learning_curve']);
        }
        $totalRemoved = 0;
        $totalFailed = 0;

        foreach($learning_curve->questions as $question) {
            foreach ($question->answers as $answer) {
                try {
                    $s = $answer->delete();
                    if ($s == true) $totalRemoved++;
                } catch (Exception $e) {
                    $totalFailed++;
                }
            }
        }

        return redirect()->back()->with('message', 'Removed '.$totalRemoved. ' answers and skipped '. $totalFailed .' from LearningCurve '. $learning_curve->id);
    }

    public function perform(Request $request){
        $message = '';
        $count = 0;
        $learning_curveIds = $request->input('learning_curve');

        if($learning_curveIds && is_array($learning_curveIds)){
            $learning_curves = LearningCurve::find($learning_curveIds);

            /**
             * TODO: build stuff here
             */

            switch($request->input('perform')){
                case 'export':
                    $message = 'Exported ';
                    break;
                case 'clear_score':
                    $message = 'Cleared scores of ';
                    break;
                case 'report':
                    $message = 'Build Report of ';
                    break;
                case 'delete':
                    $message = 'Deleted ';
                    break;
            }
        }

        return redirect()->back()->with('message', $message . $count . ' learning_curves');
    }

    public function export(){
        $learning_curves = LearningCurve::all();
        $count = count($learning_curves);

        if($count > 0){
            $export = new LearningCurveExport($learning_curves,'all');
            $export->export();

        }

        return redirect()->back()->with('message','Exporting '.$count.' learning_curves');
    }

    public function import(Request $request){
        if ($request->hasFile('learning_curve_upload') && $request->file('learning_curve_upload')->isValid()) {

            $import = new LearningCurveImport($request->file('learning_curve_upload'));

            if ($import->isValid()) {

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors(['no_file' => 'No or wrong file uploaded']);
    }

    public function report(){
        $learning_curves = LearningCurve::all();

        if(count($learning_curves) > 0){
            $report = new LearningCurveResultReport($learning_curves);

            $report->report();
        }

        return redirect()->back()->with(['message', 'No learning_curves to report results of']);
    }

    public function reportSingle($id){
        return redirect()->back()->with('message','Download Result does not yet exist for LearningCurve');
    }
}
