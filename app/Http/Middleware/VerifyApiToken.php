<?php

namespace App\Http\Middleware;

use App\Setting;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VerifyApiToken
{
    /**
     * Check if the api token is correct
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //return response()->json([$request->headers->get('x-header-token')]);
        if (Setting::verifyApiToken($request->headers->get('x-header-token'))) {
            return $next($request);
        }
        throw new NotFoundHttpException('Please add the api token to the x-header-token request header');
    }
}
