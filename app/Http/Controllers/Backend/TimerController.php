<?php namespace App\Http\Controllers\Backend;

use App\NodeJS\Command;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;

class TimerController extends Controller {
    public function index() {
        return view( 'backend.timer.index' );
    }

    public function submit(Request $request) {
        $validators = config('validation.timer');
        $data = $request->validate($validators);

        Command::make('timer_start', [
            'total_seconds' => $request->total_seconds,
            'audio_url' => $request->audio_url
        ]);
        return Redirect::route( 'backend.timer.index' );
    }

    public function stop(Request $request) {

        Command::make('timer_stop');
        return Redirect::route( 'backend.timer.index' );
    }
}
