import React from 'react';
import { inject, observer } from 'mobx-react';
import config from "../../config";

import OpenAnswer from '../components/games/OpenAnswer';
import Question from '../components/games/Question';
import Thanks from '../components/games/Thanks';
import Loader from '../components/Loader';

@inject('stores', 't') @observer
export default class OpenQuestionPage extends React.Component {

    handleAnswer() {
        this.props.stores.oq.submitAnswer();
    }

    handleChange(answer) {
        this.props.stores.oq.updateAnswer(answer);
    }

    render() {
        const { question } = this.props.stores.oq.data;
        const { showThanks, isLoading, activeAnswer } = this.props.stores.oq;

        if(!question) return <p>OpenQuestion not found</p>;

        return (
            <div className="OpenQuestionPage game-wrapper">
                <Question text={question} />
                <OpenAnswer
                    isLast={true}
                    handleAnswer={this.handleAnswer.bind(this)}
                    handleChange={this.handleChange.bind(this)}
                    value={activeAnswer}
                    limit={config.maxMessageSize}
                />
                <Thanks show={showThanks} message={this.props.t.oq.thanks} />
                {(isLoading) ? <Loader /> : null}
            </div>
        )
    }
}
