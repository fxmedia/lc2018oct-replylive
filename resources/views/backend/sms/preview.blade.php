@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Emails</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.sms.index') }}">Emails</a></li>
        <li><a href="{{ URL::route('backend.sms.setup', [$sms->id]) }}">Setup</a></li>
        <li>Preview {{ $sms->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        <h3>You're ready to send the sms! Review the details below before sending your sms</h3>
    </div>

    <div class="col-xs-12">
        <table class="table email-preview-table">
            <tbody>
            <tr class="preview-row">
                <td>
                    <h4>List</h4>
                    <p>This sms will be deliverd to <a href="#" data-toggle="modal" data-target="#modal-users">{{ count($sendUsers) }} users</a> and <a href="#" data-toggle="modal" data-target="#modal-skip">{{ count($skipUsers) }} users</a> without phonenumber will be skipped.</p>
                </td>
            </tr>
            <tr class="preview-row">
                <td>
                    <h4>Subject</h4>
                    <p>The subject of this sms will be {{ $sms->subject }}</p>
                </td>
            </tr>
            <tr class="preview-row">
                <td>
                    <h4>Content</h4>
                    <p>{{ $sms->body_text }}</p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="form-group">
        <div class="col-sm-6 breadcrumb breadcrumb-footer">
            <a href="{{ URL::route('backend.sms.index') }}" class="btn btn-default">Back</a>
        </div>
        <div class="col-sm-6 breadcrumb breadcrumb-footer text-right">
            <a href="#" data-url="{{ route('backend.sms.ajax-send', [$sms->id]) }}" class="btn btn-success" id="previewSend">Send!</a>
        </div>
    </div>

    <div class="modal fade" id="modal-users">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">This sms will be delivered to:</h4>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                        <th>Name</th>
                        <th>Phonenumber</th>
                        <tbody>
                        @foreach( $sendUsers as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->phonenumber }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-skip">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">This sms will <strong>not</strong> be delivered to:</h4>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                        <th>Name</th>
                        <th>Phonenumber</th>
                        <tbody>
                        @foreach( $skipUsers as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->phonenumber }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    @include('backend.sms._partials.batch-progress-modal')
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/message-batch-status.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/message-preview.js') }}"></script>
@endsection