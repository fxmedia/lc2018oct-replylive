import React from 'react';

export default function CloseButton(props) {
    return (
        <div className="CloseButton" onClick={props.handleClose}>
            <span>X</span>
        </div>
    )
}
