import React from 'react';

export default function ProgressBar(props) {
    if(props.hide) return null;
    
    const questionNr = props.currentNum + 1; // correction for index starting at 0
    const progress = (questionNr / props.totalNum) * 100;

    return (
        <div className="ProgressBar">
            <span>Question {questionNr} of {props.totalNum}</span>
        </div>
    )
}
