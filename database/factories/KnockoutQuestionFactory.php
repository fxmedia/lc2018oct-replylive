<?php

use Faker\Generator as Faker;

$factory->define(App\KnockoutQuestion::class, function (Faker $faker) {
    return [
        'title' => $faker->text(100) . '?',
        'correct_answer' => $faker->randomElement(['a','b']),
        'choices' => [
            [
                'choice' => 'a',
                'value' => $faker->text(20)
            ],
            [
                'choice' => 'b',
                'value' => $faker->text(20)
            ]
        ]
    ];
});
