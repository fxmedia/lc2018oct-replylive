<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModulesLinkTablesAndTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Polls
        Schema::table('polls', function(Blueprint $table){
            $table->string('tags')->nullable()->after('choices');
        });

        Schema::create('group_poll', function(Blueprint $table){
            $table->integer('poll_id')->unsigned();
            $table->foreign('poll_id')->references('id')->on('polls')->onUpdate('cascade')->onDelete('cascade');


            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onUpdate('cascade')->onDelete('cascade');

            $table->index( ['poll_id', 'group_id'] );
        });

        // Quizzes
        Schema::table('quizzes', function(Blueprint $table){
            $table->string('tags')->nullable()->after('name');
        });

        Schema::create('group_quiz', function(Blueprint $table){
            $table->integer('quiz_id')->unsigned();
            $table->foreign('quiz_id')->references('id')->on('quizzes')->onUpdate('cascade')->onDelete('cascade');


            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onUpdate('cascade')->onDelete('cascade');

            $table->index( ['quiz_id', 'group_id'] );
        });

        // Surveys
        Schema::table('surveys', function(Blueprint $table){
            $table->string('tags')->nullable()->after('name');
        });

        Schema::create('group_survey', function(Blueprint $table){
            $table->integer('survey_id')->unsigned();
            $table->foreign('survey_id')->references('id')->on('surveys')->onUpdate('cascade')->onDelete('cascade');


            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onUpdate('cascade')->onDelete('cascade');

            $table->index( ['survey_id', 'group_id'] );
        });


        // Knockouts
        Schema::table('knockouts', function(Blueprint $table){
            $table->string('tags')->nullable()->after('name');
        });

        Schema::create('group_knockout', function(Blueprint $table){
            $table->integer('knockout_id')->unsigned();
            $table->foreign('knockout_id')->references('id')->on('knockouts')->onUpdate('cascade')->onDelete('cascade');


            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onUpdate('cascade')->onDelete('cascade');

            $table->index( ['knockout_id', 'group_id'] );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Knockouts
        Schema::table('knockouts', function(Blueprint $table){
            $table->dropColumn('tags');
        });

        Schema::dropIfExists('group_knockout');

        // Surveys
        Schema::table('surveys', function(Blueprint $table){
            $table->dropColumn('tags');
        });

        Schema::dropIfExists('group_survey');

        // Quizes
        Schema::table('quizzes', function(Blueprint $table){
            $table->dropColumn('tags');
        });

        Schema::dropIfExists('group_quiz');

        // Poll
        Schema::table('polls', function(Blueprint $table){
            $table->dropColumn('tags');
        });

        Schema::dropIfExists('group_poll');
    }
}
