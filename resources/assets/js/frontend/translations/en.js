module.exports = {

    general: {
        dear: "Dear",
        go_home: "Close",
        points: "Points",
        chars_left_plural: "characters left",
        chars_left_single: "character left"
    },

    errors: {
        wrong_file_type: "This file type is not supported",
        no_video_support: "Your browser does not support HTML5 videos",
        unknown_time: "Unknown time",
        file_too_big: "There was a problem uploading your file, probably too big",
        generic: "Something went wrong.. please refresh and try again"
    },

    post: {
        open_posts: "Show me the messages!",
        chars_left_plural: "characters left",
        chars_left_single: "character left",
        form_placeholder: "Your message..",
        awaiting_approval: "Awaiting approval",
        ago: "ago"
    },

    lb: {
        header: "Leaderboard"
    },

    oq: {
        thanks: "Thanks for your answer!"
    },

    survey: {
        start: "Start Survey",
        end: "End Survey",
        submit: "Submit",
        next: "Next question",
        open_answer_placeholder: "Your answer..."
    },

    curve: {
        submit: "Submit answer",
        next: "Next question",
        name: "Assessment",
        confirm_decline: "Cancel",
        confirm_confirm: "Confirm",
        current_score: "Post-Assessment",
        previous_score: "Pre-Assessment",
        results: "Results",
        results_intro: "Check how you did in the Post-Assessment vs the Pre-Assessment"
    },

    quiz: {
        of: "of",
        question: "question",
        end: "End quiz",
        next: "Next question",
        thanks: "Thanks, your answer will be checked soon",
        right: {
            header: "Right answer",
            description: "You answered the question correctly. Congratulations!"
        },
        wrong: {
            header: "Wrong answer",
            description: "You answered the question incorrectly. Better luck next question!"
        },
        submit: "Submit answer"
    },

    knockout: {
        answer: {
            tap: "Tap to choose",
            option: "option"
        },
        players_left: "players left",
        win_challenge: "You won the knockout challenge!",
        congratulations: "Congratulations!",
        lost: "Uh-oh. You lost.",
        lost_challenge: "You lost the knockout challenge.",
        submit: "Submit answer",
        we_have_winner: "We have a winner. Congratulations!",
        multiple_winners: "We have multiple winners. Congratulations!",
        answer_mobile: "Please fill in an answer on your mobile phone",
        right: {
            header: "Right answer",
            description: "You answered the question correctly. Congratulations!"
        },
        wrong: {
            header: "Wrong answer",
            description: "You answered the question incorrectly. Game over."
        }
    },

    poll: {
        submit: "submit answer",
        name: "Poll",
        thanks: "Thanks for your answer!"
    },

    waiting: {
        please_wait: "Please wait"
    },
};
