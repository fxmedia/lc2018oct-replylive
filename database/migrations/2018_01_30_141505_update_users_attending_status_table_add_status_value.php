<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersAttendingStatusTableAddStatusValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("ALTER TABLE users_attending_status CHANGE status status ENUM('attending','waiting','pending','cancelled','declined')");
//        Schema::table('users_attending_status', function(Blueprint $table){
//            $table->enum('status', ['attending','waiting','pending','cancelled','denied'])->change();
//
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
//        Schema::table('users_attending_status', function(Blueprint $table){
//            $table->dropColumn('status');
//        });
        DB::statement("ALTER TABLE users_attending_status CHANGE status status ENUM('attending','waiting','pending','cancelled')");
    }
}
