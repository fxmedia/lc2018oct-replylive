<?php

namespace App\Import;

use App\Group;
use App\Poll;
use Validator;

class PollImport extends BaseImport
{

    public function getHeaders(){
        return ['id', 'question', 'tags', 'groups', 'choice', 'value'];
    }

    private function getGroupsIds($groups = null){
        if(isset($groups) && strlen($groups) > 0){
            if(count($names = explode(',',$groups)) > 0){
                $groupIds = [];

                foreach($names as $name){
                    // if given group doesn't exist, create it!
                    if(!$dbGroup = Group::where('name', trim($name))->first()){
                        $dbGroup = new Group;
                        $dbGroup->name = trim($name);
                        $dbGroup->save();
                    }
                    // store the id in the array
                    $groupIds[] = $dbGroup->id;
                }

                return $groupIds;
            }
        }

        return [];
    }

    public function import(){
        $skipCount = 0;
        $addCount = 0;
        $addChoiceCount = 0;
        $warnings = [];
        $errors = [];

        $idLink = [];

        foreach($this->data as $key => $obj){
            if(
                $obj->id == null &&
                ($obj->question == null || $obj->choice == null)
            ) {
                // skip, cause it misses identifier fields
                $warnings[] = 'Skipped row '. ($key+1) . ': neither question nor choice';
                $skipCount++;
                continue;
            }

            $data = [];
            foreach($obj as $k => $v){
                if($v != null && $k !== 'id')$data[$k] = $v;
            }

            if($obj->question != null){
                $validation = Validator::make($data,config('validation.polls.import.poll'));

                if($validation->fails()){
                    $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                    $skipCount++;
                    continue;
                }


                $poll = new Poll;


                $poll->fill($validation->getData());
                $poll->choices = [];

                if($poll->save()){
                    $idLink[$obj->id] = $poll->id;

                    $groupIds = $this->getGroupsIds($obj['groups']);
                    $poll->groups()->sync($groupIds);

                    $addCount++;
                }

            } else {
                $validation = Validator::make($data,config('validation.polls.import.choice'));

                if($validation->fails()){
                    $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                    $skipCount++;
                    continue;
                }

                if(!isset($idLink[$obj->id])){
                    $errors[] = "Choice id on row ". ($key+1) . ' is not set correctly';
                    $skipCount++;
                    continue;
                }

                $poll = Poll::find($idLink[$obj->id]);

                //dd($validation->getData());
                $poll->choices = array_merge( $poll->choices, [$validation->getData()] );

                if($poll->save()){
                    $addChoiceCount++;
                } else {
                    $skipCount++;
                    $errors[] = 'Failed saving choice on row '. ($key+1) . ' on Poll '. $obj->id;
                }
            }
        }

        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $addCount . ' Poll'. ($addCount == 1? '' : 's') .' added',
                'With '. $addChoiceCount . ' Choice'. ($addCount == 1? '' : 's')
            ]
        ];
    }
}