import React from 'react';
import Points from '../user/Points';
import config from '../../../config';

export default function Header(props) {

    const renderSecondaryLogo = () => {
        return (
            <div className="logo-wrapper secondary">
                <img src={`${config.base_url}/images/logo-secondary.png`} />
            </div>
        )
    }

    if(props.closable) {
        return (
            <div className="ClosableHeader">
                <div className="close-takeover" onClick={props.handleClose}>
                    <i className="fa fa-chevron-left"></i>
                </div>
                <div className="title-wrapper">
                    <h2>{props.title}</h2>
                </div>
            </div>
        )
    }

    return (
        <div className="Header">
            <div className="logo-wrapper">
                <img src={`${config.base_url}/images/logo.png`} />
            </div>
        </div>
    )
}
