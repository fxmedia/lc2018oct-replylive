<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VerifyUserToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(User::verifyUserToken($request->headers->get('x-header-token'))){
            return $next($request);
        }
        throw new NotFoundHttpException('Please add the user token to the x-header-token request header');
    }
}
