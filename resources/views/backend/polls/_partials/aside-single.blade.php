<div class="panel panel-default">

    <div class="panel-heading"><i class="fa fa-download"></i> Download</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.polls.report.single',[$poll->id]) }}">Download Results</a></li>
    </ul>

    <div class="panel-heading"><i class="glyphicon glyphicon-wrench"></i> Actions</div>
    <ul class="list-group">
        <li class="list-group-item">
            <form action="{{ URL::route('backend.polls.reset.single', [$poll->id]) }}" method="post" onsubmit="return confirm('Are you sure you wish to reset the score?')">
                {!! csrf_field() !!}
                <button type="submit" class="btn-link">Reset Score</button>
            </form>
        </li>
    </ul>

    <div class="panel-heading"><i class="glyphicon glyphicon-stats"></i> Statistics</div>
    <ul class="list-group">
        <li class="list-group-item">Answers: <span class="pull-right badge">{{ count($poll->answers) }}</span></li>
    </ul>
</div>