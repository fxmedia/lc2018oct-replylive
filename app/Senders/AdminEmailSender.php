<?php namespace App\Senders;


use App\Admin;
use App\Email;
use App\Jobs\SendAdminEmail;
use App\Setting;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

class AdminEmailSender extends BaseSender
{
    /**
     * @param \stdClass|Email $email
     * @param array $data
     */
    public static function sendSubscribers($email, array $data){
        $addresses = [];

        // get subscribed admins
        $admins = Admin::whereNotificationSubscription(1)->get();
        foreach($admins as $admin){
            $addresses[] = $admin->email;
        }

        // get subscribed emails from settings
        $settingAddresses = explode(',', Setting::getValue('email_notification') );
        foreach($settingAddresses as $settingAddress){
            $settingAddress = trim($settingAddress);

            if($settingAddress !== '') $addresses[] = $settingAddress;
        }

        // send the mail to the emails via the queue worker
        foreach($addresses as $address){
            SendAdminEmail::dispatch($email, $address, $data);
        }
    }

    /**
     * @param \stdClass|Email $email
     * @param string $address
     * @param array $data
     */
    public static function _send($email, string $address, array $data){

        Mail::send($email->view, $data, function(Message $message) use ($email, $address){
            $message->subject($email->subject);
            $message->from($email->from_email, $email->from_name);

            if( $email->reply_to ){
                $message->replyTo($email->reply_to);
            }
            /**
             * TODO Add attachment correctly
             */
//            if( $email->attachment ){
//                $message->attach();
//            }

            $message->to($address);
        });

    }
}