import React from 'react';
import { inject, observer } from 'mobx-react';

@inject("stores", "t") @observer
export default class PushPage extends React.Component {

    renderAsset() {
        const { image_path } = this.props.stores.push.data;
        if(!image_path) return null;

        return <img src={image_path} />
    }

    render() {
        const { data } = this.props.stores.push;
        if(!data) return null;

        return (
            <div className="PushPage game-wrapper">
                <h2>{data.title}</h2>
                <div dangerouslySetInnerHTML={{__html: data.copy }} />
                {this.renderAsset()}
            </div>
        )
    }
}
