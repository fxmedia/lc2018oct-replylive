@if( isset($question) )
    {!! Form::model( $question, ['class'=>'form-horizontal user-admin', 'files'=>'true', 'method'=>'patch', 'route' => ['backend.quizzes.questions.update', $question->id]] ) !!}
    {!! Form::hidden('order', $question->order) !!}
@else
    {!! Form::open(['class'=>'form-horizontal user-admin', 'files'=>'true', 'route' => ['backend.quizzes.questions.store', $quiz->id]]) !!}
    {!! Form::hidden('order', count($quiz->questions)) !!}
@endif


<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    {!! Form::label('Question','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('title', Input::old('title'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('title') !!}</span>
</div>

<div class="form-group {{ $errors->has('points') ? 'has-error' : '' }}">
    {!! Form::label('Points','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::number('points', Input::old('points', 1), ['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('points') !!}</span>
</div>

<div class="form-group {{ ($errors->has('image') || $errors->has('image_upload')) ? 'has-error' : ''}}">
    {!! Form::label('image','', ['class' => 'col-sm-3 control-label']) !!}
    @if( isset($question) && isset($question->image_path) )
        <div class="col-sm-9">
            <img src="{{ $question->image_path }}" class="img-responsive">
        </div>
        <div class="col-sm-3"></div>
    @endif
    <div class="col-sm-4">
        {!! Form::select('image', $images, Input::old('image'), ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-4">
        {!! Form::file('image_upload', ['class'=>'form-control']) !!}
    </div>
    <div class="col-sm-1" style="margin-top: 6px;">
        Resize {!! Form::checkbox('resize_image', null, ['class' => 'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('image') !!}</span>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('image_upload') !!}</span>

</div>

<div class="form-group {{ $errors->has('correct_answer') ? 'has-error' : '' }}">
    {!! Form::label('Correct Answer','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-2">
        <select name="correct_answer" class="correct-answer-select form-control">
            @if( isset($question) && is_array($question->choices) )
                @foreach($question->choices as $ch)
                    <option value="{{ $ch['choice'] }}" @if($question->correct_answer == $ch['choice']) selected @endif>{{ $ch['choice'] }}</option>
                @endforeach
            @else
                <option value="a">a</option>
            @endif
        </select>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('answers','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <?php $c =0; $r = range('a', 'z') ?>

        @if(isset($question) && is_array($question->choices))
            @foreach($question->choices as $ch)
                    <div class="form-group array-row" data-row="{{ $c }}">
                        <div class="col-sm-1">
                            <input type="text" class="key correct-key form-control" name="choices[{{ $c }}][choice]" value="{{ $ch['choice'] }}" readonly />
                        </div>
                        <div class="col-sm-10">
                            <input type="text" class="value form-control" name="choices[{{ $c }}][value]" value="{{ $ch['value'] }}" />
                        </div>
                        <div class="col-sm-1">
                            <div class="btn btn-danger remove-row" data-type="quiz_question" data-row="{{ $c }}">Delete</div>
                        </div>
                    </div>
                <?php $c++; ?>
            @endforeach
        @endif

            <div class="form-group extra-empty">
                <div class="col-sm-1">
                    <input type="text" class="key correct-key form-control" name="choices[{{ $c }}][choice]" value="{{ $r[$c] }}" readonly data-replace="choice"/>
                </div>
                <div class="col-sm-10">
                    <input type="text" class="value form-control" name="choices[{{ $c }}][value]" data-replace="value"/>
                </div>
                <div class="col-sm-1">
                    <div class="btn btn-info add-row" data-type="quiz_question" data-key="{{ $c }}">Add</div>
                </div>
            </div>

    </div>
</div>

<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
        {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
    </div>
</div>

{!! Form::close() !!}
