<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushModuleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pushes', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('title')->nullable();
            $table->text('copy')->nullable();
            $table->string('image')->nullable();
            $table->string('module')->nullable();
            $table->timestamps();
        });

        Schema::create('group_push', function(Blueprint $table){
            $table->integer('push_id')->unsigned();
            $table->foreign('push_id')->references('id')->on('pushes')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onUpdate('cascade')->onDelete('cascade');

            $table->index( ['push_id', 'group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('group_push');

        Schema::dropIfExists('pushes');
    }
}
