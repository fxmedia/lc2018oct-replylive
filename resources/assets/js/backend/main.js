// import KonamiCode from 'konami-code';

(function(){

    var statusModal = document.getElementById("nodeServerStatusModal");
    var connectedUsers = statusModal.querySelector(".connected-users");
    var activeModule = statusModal.querySelector(".active-module");
    var online = statusModal.querySelector(".online");

    // trigger immediately
    fetchStatus();

    // and then each 3000ms
    setInterval(fetchStatus, 3000);

    function updateStatus(data) {
        if(data.status){
            connectedUsers.innerHTML = data.status.connected_users;
            activeModule.innerHTML = data.status.active_module;
        }
        online.innerHTML = (data.success) ? "online" : "offline";
    }

    function fetchStatus() {

        $.ajax(statusRoute, {
             method: 'GET'
         }).success( function(data) {
             //console.log("data fetched: ", data);
             updateStatus(data);
        }).error(function(data) {
            console.log( 'Error:: ', data);
        });
    }

    // var konami = new KonamiCode();
    // konami.listen(function() {
    //      alert( '<INSERT EASTEREGG HERE>');
    // });
})();
