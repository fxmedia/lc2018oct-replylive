import React from 'react';
import config from "../../../config";

export default function RightAnswer(props) {
  return (
      <div className="AnswerOutcome right">
          <img src={`${config.base_url}/images/right.png`} />
          <h2>{props.text.header}</h2>
          <p>{props.text.description}</p>
      </div>
  )
}
