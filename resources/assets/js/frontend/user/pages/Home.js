import React from 'react';
import { inject, observer } from 'mobx-react';

import UploadPicture from '../components/user/UploadPicture';
import UserName from '../components/user/Name';
import OpenButton from '../components/post/OpenButton';

@inject("stores", "t") @observer
export default class HomePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = { loading: false, progress: 0 }
    }

    openPosts() {
        this.props.stores.main.setActivePage("post", true, true);
    }

    handleProgress(e) {
        if(e && e.lengthComputable) {
            const progress = (e.loaded / e.total) * 100;
            this.setState({ progress })
        }
    }

    handleUpload(image) {
        this.setState({ loading: true });
        this.props.stores.main.submitPicture(image, this.handleProgress.bind(this))
        .then(() => this.setState({ loading: false, progress: 0 }))
    }

    openBackgroundSurveys() {
        this.props.stores.main.setActivePage("survey_overview");
    }

    render() {
        return false;
    }
}
