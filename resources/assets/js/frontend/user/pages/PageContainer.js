import Home from './Home';
import Knockout from './Knockout';
import LeaderBoard from './LeaderBoard';
import OpenQuestion from './OpenQuestion';
import Poll from './Poll';
import Push from './Push';
import Quiz from './Quiz';
import Post from './Post';
import Pledge from './Pledge';
import Survey from './Survey';
import SurveyOverview from './SurveyOverview';
import LearningCurve from './LearningCurve';
import Waiting from './Waiting';

module.exports = {
    home: {module: Home, title: ""},
    post: {module: Post, title: "Wall"},
    poll: {module: Poll, title: ""},
    push: {module: Push, title: ""},
    pledge: {module: Pledge, title: ""},
    quiz: {module: Quiz, title: ""},
    survey: {module: Survey, title: ""},
    survey_overview: {module: SurveyOverview, title: ""},
    curve: {module: LearningCurve, title: ""},
    waiting: {module: Waiting, title: ""},
    oq: {module: OpenQuestion, title: ""},
    knockout: {module: Knockout, title: ""},
    lb: {module: LeaderBoard, title: ""}
};
