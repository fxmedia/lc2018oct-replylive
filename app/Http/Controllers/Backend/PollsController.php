<?php

namespace App\Http\Controllers\Backend;

use App\Export\PollExport;
use App\Group;
use App\Import\PollImport;
use App\NodeJS\Command;
use App\Poll;
use App\PollAnswer;
use App\Report\PollResultReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Session;

class PollsController extends Controller
{
    //
    public function index(){
        $polls = Poll::with('answers','groups')->get();
        $totalAnswers = PollAnswer::count();

        try{
            $status = json_decode( Command::status('poll_status') ,true);
            
            foreach($polls as $poll){
                if($status['poll_id'] && $status['poll_id'] == $poll->id){
                    $poll->isActive = true;
                }
            }

        } catch(Exception $e){}

        return view('backend.polls.index', compact('polls', 'totalAnswers'));
    }

    public function show($id){
        $poll = Poll::find($id);

        if($poll === null){
            return redirect()->back()->withErrors(['message' => 'Unable to show non existing Poll']);
        }

        return view('backend.polls.show', compact('poll'));
    }

    public function create(){
        $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

        return view('backend.polls.create', compact('groups'));
    }

    public function store(Request $request){
        $validators = config('validation.polls.store');

        $data = $request->validate($validators);

        // cleaning empty choices
        $choices = [];
        foreach($data['choices'] as $c){
            if($c['value'] !== null) {
                $choices[] = [
                    'choice' => $c['choice'],
                    'value' => $c['value']
                ];
            }
        }
        $data['choices'] = $choices;

        $poll = new Poll;
        $poll->fill($data);

        // if(isset($data['choices']) && is_array())

        if(!$poll->save()){
            return redirect()->back()->withErrors(['message'=>'Error saving new poll']);
        }

        $groups = [];
        if($request->input('groups')){
            $groups = (array)$request->input('groups');
        }
        $poll->groups()->sync($groups);

        return redirect()->route('backend.polls.index')->with(['message' => "Successfully added a new poll"]);
    }

    public function edit($id){
       $poll = Poll::find($id);

       if($poll !== null){
           $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

           return view('backend.polls.edit', compact('poll','groups'));
       }

       return redirect()->back()->withErrors(['message' => 'Unable to find selected Poll']);
    }

    public function update(Request $request, $id){
        $poll = Poll::find($id);

        if ($poll === null) {
            return redirect()->route('backend.polls.index')->withErrors(['message' => 'Error saving updates to poll']);
        }

        $validators = config('validation.polls.store');

        $data = $request->validate($validators);

        // cleaning empty choices
        $choices = [];
        foreach($data['choices'] as $c){
            if($c['value'] !== null) {
                $choices[] = [
                    'choice' => $c['choice'],
                    'value' => $c['value']
                ];
            }
        }
        $data['choices'] = $choices;

        if($poll->update($data)){
            $groups = [];
            if($request->input('groups')){
                $groups = (array)$request->input('groups');
            }
            $poll->groups()->sync($groups);

            return redirect()->route('backend.polls.index')->with(['message' => "Successfully edited a Poll"]);
        }

        return redirect()->back()->withErrors(['message' => 'Error saving updates to poll']);
    }

    public function remove($id){
        $poll = Poll::find($id);

        if($poll === null){
            return redirect()->route('backend.polls.index')->withErrors(['message' => 'Unable to remove non existing poll']);
        }

        /**
         * check first maybe if current poll is active? and deactivate it
         */

        try{
            $removed = $poll->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Poll has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing poll.']);
    }

    public function reset(){
        $errors = [];
        try{
            PollAnswer::truncate();
            $success = true;
        } catch(Exception $err) {
            $success = false;
            $errors = $err->getMessage();
        }

        if($success) return redirect()->back()->with('message', 'All Polls have been reset');

        return redirect()->back()->withErrors($errors);
    }

    public function resetSingle($id){
        $poll = Poll::find($id);

        if($poll === null){
            return redirect()->route('backend.polls.index')->withErrors(['message' => 'Unable to reset non existing poll']);
        }
        $totalRemoved = 0;
        $totalFailed = 0;

        foreach($poll->answers as $answer){
            try{
                $s = $answer->delete();
                if($s == true)$totalRemoved++;
            } catch (Exception $e){
                $totalFailed++;
            }
        }

        return redirect()->back()->with('message', 'Removed '.$totalRemoved. ' answers and skipped '. $totalFailed .' from poll '. $poll->id);
    }

    public function perform(Request $request){
        $message = '';
        $count = 0;
        $pollIds = $request->input('poll');

        if($pollIds && is_array($pollIds)){
            $polls = Poll::find($pollIds);

            /**
             * TODO: build stuff here
             */

            switch($request->input('perform')){
                case 'export':
                    $message = 'Exported ';
                    break;
                case 'clear_score':
                    $message = 'Cleared scores of ';
                    break;
                case 'report':
                    $message = 'Build Report of ';
                    break;
                case 'delete':
                    $message = 'Deleted ';
                    break;
            }
        }

        return redirect()->back()->with('message', $message . $count . ' polls');
    }


    public function export(){
        $polls = Poll::all();
        $count = count($polls);

        if($count > 0){
            $export = new PollExport($polls, 'all');
            $export->export();

            return redirect()->back()->with('message', 'Exporting ' . $count . ' polls');
        }

        return redirect()->back()->with('message', 'No polls to export');
    }

    public function import(Request $request){
        if ($request->hasFile('poll_upload') && $request->file('poll_upload')->isValid()) {

            $import = new PollImport($request->file('poll_upload'));

            if ($import->isValid()) {

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors(['no_file' => 'No or wrong file uploaded']);
    }

    public function report(){
        $polls = Poll::all();

        if(count($polls) > 0){
            $report = new PollResultReport($polls);

            $report->report();
        }

        return redirect()->back()->with(['message', 'No polls to report results of']);
    }

    public function reportSingle($id){
        $poll = Poll::find($id);

        if($poll !== null){
            $report = new PollResultReport([$poll]);

            $report->report();

        }
        return redirect()->back()->with(['message', 'No polls to report results of']);
    }
}
