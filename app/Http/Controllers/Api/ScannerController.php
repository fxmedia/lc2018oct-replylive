<?php

namespace App\Http\Controllers\Api;

use App\NodeJS\Command;
use App\User;
use App\Attending;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ScannerController extends ApiBaseController
{
    //
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function scan(Request $request){
//        dd($request->all());
        $token = $request->input('barcode');
        $period = $request->input('period');

        if($token === null){
            return $this->respondWithError('No input given');
        }

        $user = User::with('attending')->whereToken($token)->first();
        if($user === null){
            return $this->respondWithError('This person is not registered for<br> this event.');
        }

        $extra = $user->extra;

        $storeName = ($period !== null)? 'scanned_'.$period : 'scanned';

        if($period !== null){
            if(!isset($extra[$period])){
                // user not allowed for this period
                return $this->respondWithError( $user->name.' does not have access to '. $period);
            }

            if(isset($extra[$storeName])){
                // already scanned
                return $this->respond(['success' => true, 'warning' => true, 'message' => $user->name .' has already been scanned for '. $period]);
            }

        } else {
            if(isset($extra['scanned'])) {
                $scanned = Carbon::createFromFormat('U', $extra['scanned']);
                $scanned->setTimezone(Setting::getValue('timezone'));
                return $this->respond(['success' => true, 'warning' => true, 'name' => $user->name, 'scanned' => $scanned->format('\o\n F jS \a\t H:i\h')] );
            }
        }

        $extra[$storeName] = time();
        $user->extra = $extra;

        if ( $user->save() ) {
            $attending = Attending::whereUserId( $user->id )->first();
            $attending->status = 'present';
            if ( !$attending->save() ) {
                return $this->respondWithError('Unable to scan the user');
            }

            $user = $user->fresh();

            Command::make('user_update', $user->toArray());
            $command = Command::make('user_scan', ['token' => $token]);

            return $this->respond(['success' => true, 'name' => $user->name, 'command' => $command]);
        }
        else {
            return $this->respondWithError('Unable to scan the user');
        }
    }
}
