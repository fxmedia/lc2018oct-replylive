<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Display
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Display whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Display whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Display whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Display whereUpdatedAt($value)
 */
class Display extends Model
{

}
