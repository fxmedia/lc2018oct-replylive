class _Notifications {

    constructor() {
        this.wrapper = $('#notifications');
    }

    add(type, text) {
        var card = $('<div>')
            .addClass('card card--' + type)
            .text(text)
            .on('click', this.remove.bind(this));

        this.wrapper.append(card);

        setTimeout(() => card.addClass('show'), 0);
    }

    error(text) {
        this.add('error', text);
    }

    remove(event) {
        $(event.currentTarget).removeClass('show');
        setTimeout(() => $(event.currentTarget).remove(), 1000);
    }

}

window.Notifications = new _Notifications();