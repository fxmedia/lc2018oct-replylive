import { compose } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";

const MapWithAMarker = compose(withScriptjs, withGoogleMap)(props =>
  <GoogleMap defaultZoom={15} defaultCenter={{ lat: 52.093134, lng: 5.135471 }}>
    <Marker position={{ lat: 52.093134, lng: 5.135471 }} />
  </GoogleMap>
);

module.exports = MapWithAMarker
