import { observable, action, computed } from 'mobx';

export default class QuizStore {

    @observable answerCount = 0;
    @observable data;
    @observable currentQuestionIdx;
    @observable activeAnswer;
    @observable isLoading = false;
    @observable showThanks = false;

    constructor(socket, mainStore) {
        this.socket = socket;
        this.mainStore = mainStore;
        this.data = null;
        this.currentQuestionIdx = 0;
        this.activeAnswer = {choice: null, selected: null, checked: false};
        this.listen();
    }

    @computed get activeQuestion() {
        if(!this.data) return null;

        return this.data.questions[this.currentQuestionIdx];
    }

    @computed get isLastQuestion() {
        if(!this.data) return null;

        // (-1) correction for updating total answers after clicking next question
        return this.currentQuestionIdx == this.data.questions.length - 1 ;
    }

    @computed get isCorrect() {
        if(!this.activeAnswer.checked) return false;

        return this.activeQuestion.correct_answer == this.activeAnswer.choice;
    }

    @action addAnswer() {
        this.answerCount += 1;
    }

    @action checkAnswer() {
        if(this.activeAnswer.choice == this.activeQuestion.correct_answer) {
            this.mainStore.addPoints(this.activeQuestion.points);
            this.setBackground("right");
        } else {
            this.setBackground("wrong");
        }
        this.setAnswerToChecked();
    }

    @action reboot(data) {
        this.currentQuestionIdx = data.questionIdx;
        if(data.currentAnswer) {
            this.setAnswer(data.currentAnswer.choice);
        }
        if(data.locked) {
            this.checkAnswer();
        }
    }

    @action reset() {
        this.resetAnswer();
        this.resetAnswerCount();
        this.currentQuestionIdx = 0;
    }

    @action resetAnswer() {
        this.activeAnswer = { choice: null, selected: null, checked: false };
        this.showThanks = false;
        this.isLoading = false;
    }

    @action resetAnswerCount() {
        this.answerCount = 0;
    }

    @action selectAnswer(answer) {
        let activeAnswer = {...this.activeAnswer};
        activeAnswer.selected = answer;
        this.activeAnswer = activeAnswer;
    }

    @action setAnswer(answer) {
        let activeAnswer = {...this.activeAnswer};
        activeAnswer.choice = answer;
        activeAnswer.selected = answer;
        this.activeAnswer = activeAnswer;
        this.showThanks = true;
        this.isLoading = false;
    }

    @action showQuestion(data) {
        this.resetAnswer();
        this.resetAnswerCount();
        this.currentQuestionIdx = parseInt(data.question_index);
        this.setBackground(null);
    }

    @action setBackground(bg) {
        if(this.mainStore.namespace === "user") {
            this.mainStore.setBackground(bg);
        }
    }

    @action submitAnswer(answer) {
        this.isLoading = true;
        this.socket.connection.emit(
            'quiz_submit_answer',
            {choice: this.activeAnswer.selected, quiz_question_id: this.activeQuestion.id }
        );
        //this.setAnswer(this.activeAnswer.selected);
    }

    @action setAnswerToChecked() {
        let activeAnswer = {...this.activeAnswer};
        activeAnswer.checked = true;
        this.activeAnswer = activeAnswer;
    }

    @action setPrivateState(data) {
        this.answerCount = data.answers.length;
    }

    @action start(data) {
        this.reset();
        this.data = data.quiz;
        if(data.private) {
            this.setPrivateState(data.private);
        }
        if(data.reboot) {
            this.reboot(data.reboot);
        } else if(data.question_index) {
            this.currentQuestionIdx = parseInt(data.question_index);
        }
        this.mainStore.setActivePage("quiz", true);
    }

    @action stop() {
        this.mainStore.closeTakeover();
        this.setBackground(null);
        this.mainStore.setActivePage("home");
        this.data = null;
    }

    @action listen() {
        this.socket.connection.on('quiz_start', data => this.start(data));
        this.socket.connection.on('quiz_stop', () => this.stop());
        this.socket.connection.on('quiz_add_answer', () => this.addAnswer());
        this.socket.connection.on('quiz_set_answer', answer => this.setAnswer(answer));
        this.socket.connection.on('quiz_show_question', data => this.showQuestion(data));
        this.socket.connection.on('quiz_check_answer', () => this.checkAnswer());
        this.socket.connection.on('quiz_reset_answer', () => this.resetAnswer());
    }
}
