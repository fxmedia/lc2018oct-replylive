<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Setting
 *
 * @property-read \App\SettingGroup $settingGroup
 * @mixin \Eloquent
 * @property int $id
 * @property int $setting_group_id
 * @property string $name
 * @property string $key
 * @property string|null $value
 * @property string $type
 * @property string $placeholder
 * @property string $validation
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting wherePlaceholder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereSettingGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereValidation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereValue($value)
 */
class Setting extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    protected $fillable = ['value'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function settingGroup(){
        return $this->belongsTo(SettingGroup::class);
    }

    /**
     * Get the Value of certain setting by key
     *
     * @param String $key String
     *
     * @return String
     */
    public static function getValue(String $key){
        return Setting::where('key',$key)->pluck('value')->first();
    }

    /**
     * Gives back all settings as $obj[$setting->key] = $setting->value
     *
     * @return object
     */
    public static function getSettingsAsObject(){
        return (object) Setting::pluck('value','key')->all();
    }

    /**
     * Gives back the js cache version
     *
     * @return int|string
     */
    public static function getJSCacheVersion(){
        $v = Setting::where('key', 'js_cache_version')->pluck('value')->first();

        if($v) return $v->value;

        return 1;
    }

    /**
     * Gives back the css cache version
     *
     * @return int|string
     */
    public static function getCSSCacheVersion(){

        $v = Setting::where('key', 'css_cache_version')->pluck('value')->first();

        if($v) return $v;

        return 1;
    }

    /**
     * Check if reply.live for this is online
     *
     * @return bool
     */
    public static function getIsOnline(){
        $v = Setting::where('key', 'online')->pluck('value')->first();
//        if($v === null) return false;
        return (bool)($v);
    }

    /**
     * Check if given media token is correct
     *
     * @param string $token
     * @return bool
     */
    public static function verifyMediaToken($token){
        $v = Setting::where('key', 'media_token')->pluck('value')->first();

        if($v == null) return true;

        return ($token === $v);
    }

    /**
     * Check if given api token is correct
     *
     * @param string $token
     *
     * @return bool
     */
    public static function verifyApiToken($token){
        $v = Setting::where('key', 'api_token')->pluck('value')->first();

        if($v == null) return true;

        return ($token === $v);
    }
}
