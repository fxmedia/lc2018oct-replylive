<?php

use Faker\Generator as Faker;

$factory->define(App\SurveyQuestion::class, function (Faker $faker, $args=['type'=>'open']) {

    $data = [
        'type' => $args['type'],
        'title' => $faker->text(100) . '?',
        'order' => 0
    ];

    switch($args['type']){
        case 'abcd':
        case 'abcd-multiple':
            $data['data'] = [
                [
                    'choice'=> 'a',
                    'value' => $faker->text(50)
                ],
                [
                    'choice'=> 'b',
                    'value' => $faker->text(50)
                ],
                [
                    'choice'=> 'c',
                    'value' => $faker->text(50)
                ],
                [
                    'choice'=> 'd',
                    'value' => $faker->text(50)
                ]
            ];
            break;
        case 'range':
            $data['data'] = [
                'min_range' => 1,
                'max_range' => $faker->numberBetween(5,10)
            ];
            break;
        case 'open':
        default:
            $data['data'] = [];
            break;
    }

    return $data;
});
