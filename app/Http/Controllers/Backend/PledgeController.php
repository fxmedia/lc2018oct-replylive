<?php

namespace App\Http\Controllers\Backend;

use App\Export\PledgeExport;
use App\Http\Controllers\Controller;
use App\NodeJS\Command;
use Illuminate\Http\Request;
use App\Pledge;
use Exception;

class PledgeController extends Controller
{
    public function index(){
        $pledges = Pledge::orderBy('id','desc')->get();
        $showDisplayPledges = 0;

        try{
            $showDisplayStatus = json_decode( Command::status('display_page_status'), true);
            $showDisplayPledges = ($showDisplayStatus['page'] == "pledge") ? 1 : 0;
        } catch(Exception $e){}

        return view('backend.pledges.index', compact('pledges', 'showDisplayPledges'));
    }

    public function remove($id){
        $pledge = Pledge::find($id);

        if($pledge === null){
            return redirect()->back()->withErrors(['message' => 'Unable to find selected pledge']);
        }
        if($pledge->delete()){

            // Command::make([
            //     'action' => 'pledge_remove',
            //     'module' => 'pledge',
            //     'data' => [
            //         'pledge_id' => $pledge->id
            //     ]
            // ]);

            return redirect()->back()->with('message', 'Pledge has been removed');
        }
        return redirect()->back()->withErrors(['message' => 'Error removing pledge']);
    }

    public function perform(Request $request){
        $message = '';
        $count = 0;
        $pledgeIds = $request->input('pledge');
        if($pledgeIds && is_array($pledgeIds)){
            $pledges = Pledge::find($pledgeIds);

            switch($request->input('perform')){
                case 'export':
                    $message = 'Perform export ';

                    /**
                    * TODO: like below, shouldn't this be a "Report" ?
                    */
                    $export = new PledgeExport($pledges, 'selection');
                    $export->export();
                    break;
                case 'delete':
                    $message = 'Deleted ';
                    foreach($pledges as $pledge){
                        if($pledge->delete()){
                            $count++;
                        }
                    }
                    if($count > 0){
                        Command::make('pledge_remove', ['pledge_ids' => $pledgeIds]);
                    }

                    break;
            }
        }

        return redirect()->back()->with('message', $message . $count . ' pledges');
    }

    public function exportAll(){
        $pledges = Pledge::all();
        $count = count($pledges);

        if($count === 0){
            return redirect()->back()->with('message', 'No pledges to export');
        }

        $export = new PledgeExport($pledges, 'all');
        $export->export();

        return redirect()->back()->with('message', 'Exporting '.$count.' pledges');
    }
}
