<?php

namespace App\Http\Controllers\Backend;

use App\Export\SurveyExport;
use App\Group;
use App\Import\SurveyImport;
use App\NodeJS\Command;
use App\Report\SurveyResultReport;
use App\Survey;
use App\SurveyAnswer;
use App\SurveyQuestion;
use Exception;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurveysController extends Controller
{
    //
    public function index(){
        $surveys = Survey::with('groups')->get();
        $totalAnswers = SurveyAnswer::count();
        $amountQuestions = SurveyQuestion::count();

        try {
            $status = json_decode( Command::status('survey_status'), true);

            foreach($surveys as $survey){
                if($status['survey_ids'] && is_array($status['survey_ids'])){
                    if(in_array($survey->id, $status['survey_ids'])){
                        $survey->isActive = true;
                    }
                }
            }
        } catch(Exception $e){}

        return view('backend.surveys.index', compact('surveys', 'totalAnswers', 'amountQuestions'));
    }

    public function create(){
        $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

        return view('backend.surveys.create', compact('groups'));
    }

    public function store(Request $request){

        $validators = config('validation.surveys.store');

        $data = $request->validate($validators);


        $survey = new Survey($data);

        if(!$survey->save()){
            return redirect()->back()->withErrors(['message' => 'Error saving new survey']);
        }


        $groups = [];
        if($request->input('groups')){
            $groups = (array)$request->input('groups');
        }
        $survey->groups()->sync($groups);

        return redirect()->route('backend.surveys.edit', [$survey->id])->with(['message' => 'Successfully saved new survey.']);
    }

//    public function show($id){
//        /**
//         * TODO
//         */
//    }

    public function edit($id){
        $survey = Survey::find($id);

        if($survey !== null){
            $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

            return view('backend.surveys.edit', compact('survey','groups'));
        }

        return redirect()->back()->withErrors(['message' => 'Unable to find selected Survey']);
    }

    public function update(Request $request, $id){
        $survey = Survey::find($id);

        if($survey === null){
            return redirect()->back()->withErrors(['message' => 'Unable to update non existing Quiz']);
        }

        $validators = config('validation.surveys.store');

        $data = $request->validate($validators);

        if($survey->update($data)){
            $groups = [];
            if($request->input('groups')){
                $groups = (array)$request->input('groups');
            }
            $survey->groups()->sync($groups);

            return redirect()->route('backend.surveys.index')->with(['message' => 'Sucessfully updated survey']);
        }

        return redirect()->back->withErrors(['message' => 'Error saving updates to survey']);
    }

    public function remove($id){
        $survey = Survey::find($id);


        if($survey === null){
            return redirect()->route('backend.surveys.index')->withErrors(['message' => 'Unable to remove non existing survey']);
        }

        try{
            $removed = $survey->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Survey has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing survey']);
    }

    public function reset(){
        $errors = [];
        try{
            SurveyAnswer::truncate();
            $success = true;
        } catch(Exception $err) {
            $success = false;
            $errors = $err->getMessage();
        }

        if($success) return redirect()->back()->with('message', 'All Surveys have been reset');

        return redirect()->back()->withErrors($errors);
    }

    public function resetSingle($id){
        $survey = Survey::find($id);

        if($survey === null){
            return redirect()->route('backend.surveys.index')->withErrors(['message' => 'Unable to reset non existing survey']);
        }
        $totalRemoved = 0;
        $totalFailed = 0;

        foreach($survey->questions as $question) {
            foreach ($question->answers as $answer) {
                try {
                    $s = $answer->delete();
                    if ($s == true) $totalRemoved++;
                } catch (Exception $e) {
                    $totalFailed++;
                }
            }
        }

        return redirect()->back()->with('message', 'Removed '.$totalRemoved. ' answers and skipped '. $totalFailed .' from survey '. $survey->id);
    }

    public function perform(Request $request){
        /**
         * TODO or not TODO
         */
    }

    public function export(){
        $surveys = Survey::all();
        $count = count($surveys);

        if($count >0){
            $export = new SurveyExport($surveys, 'all');
            $export->export();
        }

        return redirect()->back()->with('message','Exporting '.$count.' surveys');
    }

    public function import(Request $request){
        if ($request->hasFile('survey_upload') && $request->file('survey_upload')->isValid()) {

            $import = new SurveyImport($request->file('survey_upload'));

            if($import->isValid()){

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }
    }

    public function report(){
        $surveys = Survey::all();

        if(count($surveys) > 0){
            $report = new SurveyResultReport($surveys);

            $report->report();
        }

        return redirect()->back()->with(['message', 'No surveys to report results of']);
    }

    public function reportSingle(){
        /**
         * TODO
         */

    }
}
