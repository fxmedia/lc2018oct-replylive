@extends('backend.layout.app')

@section('header')
    <h1>Open Question Answers</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.open_questions.index') }}">Open Questions</a></li>
        <li>Open Question Answers</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-9">

        @include('backend._partials.errors')
        {!! Form::open(['route'=>['backend.open_questions.answers.perform'], 'id'=>'open_question_answersTable', 'method'=>'POST']) !!}
        <table class="table table-hover">
            <thead>
                @include('backend._partials.table-top-actions')
                <tr>
                    <th>&nbsp;</th>
                    <th>Highlight</th>
                    <th>Answer</th>
                    <th>User</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($open_question_answers as $open_question_answer)
                    <tr id="open_question_{{ $open_question_answer->id }}" class="{{ (!$open_question_answer->visible ? 'greyed-out' : '') }}">
                        <td>{!! Form::checkbox('open_question_answer[]', $open_question_answer->id, false, ['class' => 'selectable', 'id' => 'checkbox'.$open_question_answer->id]) !!}</td>
                        <td><a class="toggle-open_question_answer-highlight btn @if($open_question_answer->isHighlight) btn-success @else btn-primary @endif btn-xs" data-open_question_answer-id="{{ $open_question_answer->id }}"><i class="fa @if($open_question_answer->isHighlight) fa-heart @else fa-heart @endif"></i></a></td>
                        <td>{{ $open_question_answer->answer }}</td>
                        <td>{{ $open_question_answer->user->name }}</td>
                        <td>
                            {{--<a class="btn btn-xs btn-success" href="{{ route('backend.open_question_answers.edit', [$open_question_answer->id] ) }}">--}}
                                {{--<span class="glyphicon glyphicon-edit"></span>--}}
                            {{--</a>--}}
                            <a class="btn btn-xs btn-danger form-action" href="#"
                               data-action="delete"
                               data-username="{{ $open_question_answer->user_id }}"
                               data-id="{{ $open_question_answer->id }}"
                               data-form="open_question_answersTable">
                                <span class="glyphicon glyphicon-trash"></span>
                                Remove
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            @include('backend.open_questions._partials.answers-table-bottom-action')
        </table>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        var api_routes = {
            'other': {
                lowlight: '{{ route('api.open_question.highlight') }}'
            }
        };
        @foreach($open_question_answers as $open_question_answer)
            api_routes['{{ $open_question_answer->id }}'] = {
                highlight: '{{ route('api.open_question.highlight', [$open_question_answer->id]) }}'
        };
        @endforeach
        console.log("api routes: ", api_routes);
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/open_question_answers.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>
@endsection
