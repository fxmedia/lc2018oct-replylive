import React from 'react';
import { inject, observer } from 'mobx-react';
import mapIndexToChar from '../../utils/mapIndexToChar';

import MultipleChoiceAnswer from '../components/games/MultipleChoiceAnswer';
import Question from '../components/games/Question';
import Thanks from '../components/games/Thanks';
import Loader from '../components/Loader';
import SubmitButton from '../components/games/SubmitButton';

@inject('stores', 't') @observer
export default class PollPage extends React.Component {

    handleConfirm(isConfirmed) {
        if(!isConfirmed) {
            this.props.stores.poll.selectAnswer(null);
        } else {
            this.props.stores.poll.submitAnswer();
        }
    }

    handleSelect(answer) {
        this.props.stores.poll.selectAnswer(answer);
    }

    render() {
        const { question, choices } = this.props.stores.poll.data;
        const { activeAnswer, showThanks, isLoading } = this.props.stores.poll;

        if(!question || !choices) return <p>Poll not found</p>;
        console.log( 'Activeanswer::', activeAnswer.choice );
        return (
            <div className="PollPage game-wrapper">
                <Question text={question} />
                <ol className={"ABCD"}>
                    {choices.map((answer, idx) =>
                        <MultipleChoiceAnswer
                            key={answer.choice}
                            text={answer.value}
                            choice={answer.choice}
                            handleSelect={this.handleSelect.bind(this, answer.choice)}
                            isSelected={activeAnswer.selected == answer.choice}
                            handleConfirm={this.handleConfirm.bind(this)}
                            block={activeAnswer.choice != null}
                            isConfirmed={activeAnswer.choice == answer.choice}
                            isInActive={activeAnswer.choice && activeAnswer.choice != answer.choice}
                        />
                    )}
                </ol>
                <Thanks show={showThanks} message={this.props.t.poll.thanks} />
                {(isLoading) ? <Loader /> : null}
            </div>
        )
    }
}
