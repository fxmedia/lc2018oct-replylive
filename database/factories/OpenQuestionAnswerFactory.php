<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\OpenQuestionAnswer::class, function (Faker $faker) {
    $user = User::inRandomOrder()->first();

    return [
        'user_id' => $user->id,
        'answer' => $faker->text('120')
    ];
});
