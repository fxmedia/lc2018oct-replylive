<?php

namespace App\Listeners;

use App\Events\NewUserRegistered;
use App\Senders\EmailSender;
//use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailUserRegistered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NewUserRegistered $event
     * @return void
     */
    public function handle(NewUserRegistered $event)
    {
        //
        $user = $event->getUser();

        EmailSender::sendAction($user, 'register');
    }
}
