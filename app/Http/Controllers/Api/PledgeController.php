<?php namespace App\Http\Controllers\Api;

use App\NodeJS\Command;
use App\Pledge;
use App\User;
use Illuminate\Http\Request;

class PledgeController extends ApiBaseController {

	public function storePledge( Request $request) {
		$user = User::find($request->input('user_id'));

		$pledge = new Pledge;
		$pledge->amount = $request->data['amount'];
		$pledge->anonymous = $request->data['anonymous'];
		$pledge->name = $request->data['name'];

		$user->pledges()->save($pledge);
		$np = Pledge::find($pledge->id);

		return $this->respond(["pledge" => $np->toArray()]);
	}

	public function fetchPledges() {
		$pledges = Pledge::with('user')->get();

		return $this->respond(["pledges" => $pledges->toArray()]);
	}

	public function toggleDisplay(Request $request) {

		try {
			Command::make('display_set_page', [
				'name' => ($request->input('showDisplay') === "1") ? "pledge" : "idle"
			]);
		} catch(Exception $e) {
			return $this->respondWithError("Node server could not be reached");
		}

		return $this->respond(["success" => true]);
	}
}
