<?php

namespace App\Report;

use Maatwebsite\Excel\Facades\Excel;
use App\User;

class OpenQuestionResultReport extends BaseReport{

    private $open_questions;

    public function __construct($open_questions)
    {
        $this->open_questions = $open_questions;
    }

    public function getFilename()
    {
        return 'open_question_report_' . date('Y_m_d_h_m_s');
    }

    public function report(){
        Excel::create($this->getFilename(), function($excel){

            foreach($this->open_questions as $open_question){

                $name = substr(trim(preg_replace('/[\*|\:|\\|\/|\?|\[|\]]/', '', $open_question->question)), 0, 31);

                $excel->sheet($name, function($sheet) use ($open_question){
                    $index = 1;

                    $sheet->row($index, ['Question:', $open_question->question, ]);

                    $sheet->row($index, function ($row) {
                        $row->setFontSize(14);
                        $row->setFontWeight('bold');
                    });
                    $index++;

                    $sheet->row($index, ['Total Answers', count($open_question->answers)]);

                    $index+=2;

                    $sheet->row($index, ['Name', 'Answer']);

                    $index++;
                    foreach($open_question->answers as $a){
                        $user = User::find($a->user_id);
                        $sheet->row($index, [$user->name, $this->remove_emoji($a->answer) ]);
                        $index++;
                    }
                });
            }

        })->export('xls');
    }


}
