import { observable, action, computed } from 'mobx';

export default class KnockoutStore {

    @observable activeAnswer = { choice: null, checked: false };
    @observable data = null;
    @observable currentQuestionIdx = 0;
    @observable knockedOut = false;
    @observable players_left = null;
    @observable final = null;
    @observable phase = "multiple_choice";

    constructor(socket, mainStore) {
        this.socket = socket;
        this.mainStore = mainStore;
        this.listen();
    }

    @computed get activeQuestion() {
        if(!this.data) return null;

        return this.data.questions[this.currentQuestionIdx];
    }

    @computed get isLastQuestion() {
        if(!this.data) return null;

        // (-1) correction for updating total answers after clicking next question
        return this.currentQuestionIdx == this.data.questions.length - 1 ;
    }

    @action setDecidingQuestion(data) {
        if(data) {
            this.reboot(data);
        }
        this.setBackground(null);
        this.resetAnswer();
        this.phase = "deciding_question";
        this.mainStore.setActivePage("knockout", true);
        console.log("changing phase to deciding question");
    }

    @action setWinner(data) {
        this.phase = "winner";
        this.winner = data.winner;

        if(this.mainStore.namespace == "user" && this.winner.find(user => user.id == this.mainStore.user.id)) {
            this.setBackground(null);
        } else {
            this.setBackground("wrong");
        }
        this.mainStore.setActivePage("knockout", true);
    }

    @action checkAnswer(answer) {
        console.log("active: ", this.activeAnswer)
        console.log("question: ", this.activeQuestion)
        if(this.activeAnswer.choice != this.activeQuestion.correct_answer) {
            this.setKnockedOut();
            this.setBackground("wrong");
        } else {
            this.setBackground("right");
        }
        this.setAnswerToChecked();
    }

    @action reboot(data) {
        console.log("rebooting with: ", data);
        if(data.knockout) {
            this.data = data.knockout;
        }
        if(data.questionIdx && data.phase == "multiple_choice") {
            this.setQuestionIdx(data.questionIdx);
        }
        if(data.players_left !== null) {
            this.players_left = data.players_left;
            this.final = true;
            if(this.mainStore.namespace == "display" && data.players_left == 0) {
                this.mainStore.setBackground('danger');
            }
        }
        console.log("reboot!", data);
        if(data.currentAnswer) {
            this.setAnswer(data.currentAnswer.choice);
        }
        if(data.locked) {
            this.checkAnswer();
        }
    }

    @action reset() {
        this.currentQuestionIdx = 0;
        this.knockedOut = false;
        this.final = null;
        this.phase = "multiple_choice";
        this.resetAnswer();
    }

    @action resetAnswer() {
        console.log("resetting answer..");
        this.activeAnswer = { choice: null, checked: false };
        this.setBackground(null);
    }

    @action setAnswer(answer) {
        const activeAnswer = {...this.activeAnswer};
        activeAnswer.choice = answer;
        this.activeAnswer = activeAnswer;
        console.log("current answer: ", activeAnswer);
        this.setBackground(answer);
    }

    @action setBackground(bg) {
        if(this.mainStore.namespace === "user") {
            this.mainStore.setBackground(bg);
        }
    }

    @action setAnswerToChecked() {
        const activeAnswer = {...this.activeAnswer};
        activeAnswer.checked = true;
        this.activeAnswer = activeAnswer;
    }

    @action showQuestion(data) {
        this.resetAnswer();
        this.setQuestionIdx(data.question_index);
        this.setBackground(null);
    }

    @action setKnockedOut() {
        this.knockedOut = true;
    }

    @action setQuestionIdx(idx) {
        if(!this.data && !this.data.questions) return;

        this.currentQuestionIdx = parseInt(idx);
    }

    @action setScore(data) {
        this.setAnswerToChecked();
        this.final = data.score;
        this.players_left = data.players_left;
        if(data.players_left == 0 && this.mainStore.namespace == "display") {
            this.mainStore.setBackground("danger");
        }
    }

    @action showFinal(final) {
        this.final = final;
    }

    @action submitAnswer(answer) {
        this.setAnswer(answer);
        this.socket.connection.emit(
            'knockout_submit_answer',
            {choice: answer, knockout_question_id: this.activeQuestion.id }
        );
    }

    @action submitDecidingAnswer(answer) {
        this.socket.connection.emit(
            'knockout_submit_deciding_answer',
            {answer}
        );
    }

    @action start(data) {
        this.reset();
        this.data = data.knockout;
        console.log("received data: ", data);
        if(this.mainStore.namespace == "display") {
            this.players_left = data.players_left;
        }
        if(data.reboot) {
            this.reboot(data.reboot);
        } else if(data.question_index) {
            this.setQuestionIdx(data.question_index);
        }
        this.mainStore.setActivePage("knockout", true);
    }

    @action stop() {
        this.mainStore.setBackground(null);
        if(this.mainStore.namespace == "user") {
            this.mainStore.closeTakeover();
        }
        this.mainStore.setActivePage("home");
        this.data = {};
        this.reset();
        this.activeAnswer = { choice: null, checked: false };
    }

    @action listen() {
        this.socket.connection.on('knockout_start', data => this.start(data));
        this.socket.connection.on('knockout_stop', () => this.stop());
        this.socket.connection.on('knockout_set_score', data => this.setScore(data));
        this.socket.connection.on('knockout_set_answer', answer => this.setAnswer(answer));
        this.socket.connection.on('knockout_set_knocked_out', () => this.setKnockedOut());
        this.socket.connection.on('knockout_show_question', data => this.showQuestion(data));
        this.socket.connection.on('knockout_check_answer', answer => this.checkAnswer(answer));
        this.socket.connection.on('knockout_show_final', data => this.showFinal(data));
        this.socket.connection.on('knockout_deciding_question', data => this.setDecidingQuestion(data));
        this.socket.connection.on('knockout_set_winner', data => this.setWinner(data));
        this.socket.connection.on('knockout_reset_answer', () => this.resetAnswer());

    }
}
