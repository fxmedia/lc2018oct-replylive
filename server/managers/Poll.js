const GameManager = require('./Game');

class PollManager extends GameManager {
    constructor(Admin) {
        super(Admin, "poll");
        this.result_users = [];
        this.result = {};
    }

    handle(action, data) {
        switch(action) {
            case 'poll_start':
                this.start(action, data);
                break;
            case 'poll_stop':
                this.stop(action, data);
                break;
        }
    }

    reset() {
        this.result = {};
    }

    start(action, data) {
        this.reset();
        this.setBroadcastLevel(data);
        const extraction = {
            user: this.extract(data, ["question", "choices", "id"]),
            display: this.extract(data, ["question", "choices", "result", "id"])
        }
        // init poll with data from backend
        if(data.result) {
            this.result = (data.result.constructor === Object) ? Object.assign({}, data.result) : data.result.slice();
        }
        if(data.result_users && data.result_users.length > 0) {
            this.result_users = data.result_users;
        }
        this.broadcastToUsers(action, extraction.user, data.result_users);
        this.Admin.broadcastToChannel("display", action, extraction.display);
        // and update nodejs server with the action, so new users/displays will start
        // with the correct action && data
        this.Admin.setActiveAction({ action, data: extraction });
    }

    status() {
        if(!this.Admin.activeAction || this.Admin.getManager(this.Admin.activeAction.action) != this.namespace) {
            return { poll_id: false };
        }
        return { poll_id: this.Admin.activeAction.data.user.id };
    }

    stop(action, data) {
        if(this.isActive()){
            this.broadcastToUsers(action, {});
            this.Admin.broadcastToChannel("display", action);
            this.result_users = [];
            this.Admin.setActiveAction(null);
        }
    }

    submitAnswer(answer, userSocket) {
        this.Admin.save('/poll/answer', answer, userSocket.id)
        .catch(err => {
            this.Admin.sendError(userSocket, "The backend failed..");
            const userId = this.Admin.getConnection("user", userSocket.id).id;
            this.Admin.broadcastToUsers([userId], 'poll_reset_answer');
        })
        .then(result => {
            if(result.data.success) {
                if(!this.result_users.find(user_id => user_id == result.data.answer.user_id)) {
                    this.Admin.channels.display.emit('poll_add_answer', result.data.answer.choice);
                    this.Admin.broadcastToUsers([result.data.answer.user_id], 'poll_set_answer', result.data.answer.choice);

                    // update node server
                    let currentChoice = this.result[result.data.answer.choice];
                    if(!currentChoice) {
                        this.result[result.data.answer.choice] = 1;
                    } else {
                        this.result[result.data.answer.choice] = parseInt(this.result[result.data.answer.choice]) + 1;
                    }

                    this.result_users.push(result.data.answer.user_id);
                }
            } else {
                this.Admin.sendError(userSocket, "The answer could not be saved correctly..");
                const userId = this.Admin.getConnection("user", userSocket.id).id;
                this.Admin.broadcastToUsers([userId], 'poll_reset_answer');
            }
        })
    }
}

module.exports = PollManager
