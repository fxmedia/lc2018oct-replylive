<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Quiz
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\QuizAnswer[] $answers
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quiz whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quiz whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quiz whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quiz whereUpdatedAt($value)
 * @property string|null $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quiz whereTags($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read mixed $group_ids
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\QuizQuestion[] $questions
 */
class Quiz extends Model
{

    protected $table = "quizzes";

    protected $fillable = ['name', 'tags'];

    protected $appends = ['group_ids'];

    protected $with = ['questions'];


    public function getGroupIdsAttribute(){
        return $this->groups()->pluck('id')->toArray();
    }

    public function questions(){
        return $this->hasMany('App\QuizQuestion')->orderBy('order');
    }

    public function groups(){
        return $this->belongsToMany('App\Group');
    }

    /**
     * isInGroup
     *
     * @param group $group
     * @return bool true if user is attached to given group
     */
    public function isInGroup(Group $group){
        $groups = [];
        foreach ($this->groups as $userGroup) {
            $groups[] = $userGroup->id;
        }

        return in_array($group->id, $groups);
    }


    public static function getAnswerStatus($id = null){
        $quiz = Quiz::find($id);

        if($quiz === null) return null;

        $data = [];
        $i = 0;
        foreach($quiz->questions as $question){
            $data[$i] = [];
            $data[$i]['total'] = count($question->answers);

            foreach($question->answers as $answer){
                if(!isset($data[$i][$answer->choice])) $data[$i][$answer->choice] = 0;

                $data[$i][$answer->choice]++;
            }

            $i++;
        }

        return $data;
    }
}
