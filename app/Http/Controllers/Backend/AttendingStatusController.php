<?php

namespace App\Http\Controllers\Backend;

use App\Attending;
use App\Events\UserSetAttending;
use App\Events\UserSetDeclinedByAdmin;
use App\Events\UserSetWaiting;
use App\NodeJS\Command;
use App\User;
//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttendingStatusController extends Controller
{
    //
    /**
     * @param string $token
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function adminAcceptedPendingUser(string $token, int $id)
    {
        $user = User::with('attending')->find($id);

        $message = 'Error accepting pending user: ';

        if($user === null){
            $message.= 'no user found with given id';

        } else if($user->attending){
            if($user->attending->status !== 'pending') {
                $message.= 'User '. $user->name .' ('. $user->email .')  has already been accepted or declined by another admin';

            } else {
                // where the magic happens
                $newStatus = Attending::determineAttendingStatus($user->id);

                if($user->attending->update(['status' => $newStatus])){

                    $message = 'User '. $user->name .' ('. $user->email .') has been accepted to the event with status '. $newStatus;

                    if($newStatus === 'attending'){
                        event(new UserSetAttending($user));

                    } else if($newStatus === 'waiting'){
                        event(new UserSetWaiting($user));
                    }
                    Command::make('user_update', $user->toArray());

                } else {
                    $message .= 'Error updating status user '. $user->name .' ('. $user->email .').';
                }
            }
        } else {
            $message .= 'no status set in the backend for user '. $user->name .' ('. $user->email .')';
        }

        return view('backend.attending_status.admin-attending-response', compact('message'));
    }

    /**
     * @param string $token
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function adminDeclinedPendingUser(string $token, int $id)
    {
        $user = User::with('attending')->find($id);
        $triggerEvent = false;

        $message = 'Error declining pending user: ';

        if($user === null){
            $message.= ' no user found with given id';

        } else if($user->attending){
            if($user->attending->status !== 'pending') {
                $message = 'User '. $user->name .' ('. $user->email .')  has already been accepted or declined by another admin';

            } else if($user->attending->update(['status' => 'declined'])) {
                $message = 'User '. $user->name . ' ('. $user->email .') has been declined from attending this event';
                $triggerEvent = true;

            } else {
                $message.= 'Error updating status user '. $user->name .' ('. $user->email .').';

            }
        } else {
            if($user->attending()->save(new Attending(['status' => 'declined']))){
                $message = 'User '. $user->name . ' ('. $user->email .') has been declined from attending this event';
                $triggerEvent = true;

            } else {
                $message.= 'Error updating status user '. $user->name .' ('. $user->email .').';

            }
        }

        if($triggerEvent){

            Command::make('user_update', $user->toArray());
            event(new UserSetDeclinedByAdmin($user));
        }

        return view('backend.attending_status.admin-attending-response', compact('message'));
    }
}

