(function(scope, $){
    console.log('pledges.js');

    class _Pledge {
        constructor() {
            this.routes = api_routes;
            this.toggleDisplayBtns = $('.toggle-display');
            this.toggleDisplayBtns.on('change', this.toggleDisplay.bind(this));
        }

        getRoute(pledge_id, key) {
            return this.routes[pledge_id][key];
        }

        toggleDisplay(event) {
            var route = this.getRoute('other', 'toggle_display');

            $.ajax(route, {
                method: 'POST',
                data: { showDisplay: event.currentTarget.value }
            }).success(data => {
                console.log("data: ", data);
                if(!data || !data['success']) return this.error(data);
            }).error(this.error);
        }

        error(xhr, status, errorThrown) {
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }

    }

    scope.pledge = new _Pledge();
})(this, $);
