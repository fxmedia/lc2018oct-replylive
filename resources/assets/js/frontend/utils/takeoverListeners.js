import Debug from './debug';

export default function initTakeOverListeners(mainStore) {
    const postTriggers = document.querySelectorAll(".reply-live-takeover-posts");
    if(postTriggers) {
        for(let trigger of postTriggers) {
            Debug.logS('font-weight:bold;color:tomato;', 'takeoverListener | initTakeoverListener() |',"trigger initialized..");
            trigger.addEventListener("click", () =>{ mainStore.setActivePage("post", true, true) });
        }
    }
}
