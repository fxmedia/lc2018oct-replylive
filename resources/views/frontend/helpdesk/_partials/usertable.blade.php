@if (count($users) > 0)

    <table id="user-table" class="table table-hover">

        <thead>
            <tr>
                <th>Name</th>
                <th>Status</th>
                <th>Scanned</th>
                <th>Emailaddress</th>
                <th>Phonenumber</th>
                <th>Groups</th>
                <th>Actions</th>
            </tr>
        </thead>

        <tbody>
        @foreach ( $users as $user )
            <tr data-token="{{$user->token}}">
                <td>{{$user->firstname}} {{$user->lastname}}</td>
                <td class="status">
                    @switch( $user->attending['status'] )
                        @case('attending')
                            <span class="badge badge-primary">Attending</span>
                            @break
                        @case('present')
                            <span class="badge badge-success">Present</span>
                            @break
                        @case('pending')
                            <span class="badge badge-danger">Pending</span>
                            @break
                        @case('waiting')
                            <span class="badge badge-warning">Waiting</span>
                            @break
                        @case('invited')
                            <span class="badge badge-info">Invited</span>
                            @break
                        @case('cancelled')
                            <span class="badge badge-secondary">Cancelled</span>
                            @break
                        @case('declined')
                            <span class="badge badge-dark">Declined</span>
                            @break
                    @endswitch
                </td>
                <td>
                    @if (isset($user->extra['scanned']))
                        @php
                            $scanned = \Carbon\Carbon::createFromFormat('U', $user->extra['scanned']);
                            $scanned->setTimezone($timezone);
                        @endphp
                        <strong>{{ $scanned->format('H:i\h') }}</strong> {{ $scanned->format('F jS') }}
                    @endif
                </td>
                <td>{{$user->email}}</td>
                <td>{{$user->phonenumber}}</td>
                <td>
                    @foreach ($user->groups as $i => $group)
                        {{ $i > 0 ? ', ' : '' }}{{$group->name}}
                    @endforeach
                </td>

                <td>
                    @if ($user->attending['status'] != 'present' || !isset($user->extra['scanned']))
                        <button data-token={{$user->token}} class="set-present btn btn-success">Set present</button>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
