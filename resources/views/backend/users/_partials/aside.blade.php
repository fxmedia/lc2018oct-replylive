<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-plus"></i> Create</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.users.create') }}">Add user</a></li>
        <li class="list-group-item"><a href="{{ route('backend.groups.create') }}">Add group</a></li>
    </ul>
    <div class="panel-heading"><i class="fa fa-upload"></i> Upload</div>
    <div class="panel-body panel-uploader">
        {!! Form::open(['route' => 'backend.users.import', 'files' => true]) !!}
        {!! Form::file( 'user_upload', ['class' => 'importfile'] ) !!}
        <br>
        {!! Form::submit( 'Upload', ['class' => 'btn btn-sm btn-primary'] ) !!}
        {!! Form::close() !!}
    </div>

    <div class="panel-heading"><i class="fa fa-download"></i> Download</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.users.export') }}">Download all users</a></li>
        <li class="list-group-item"><a href="{{ route('backend.users.export.test') }}">Download test users</a></li>
    </ul>


    <div class="panel-heading"><i class="glyphicon glyphicon-stats"></i> Statistics</div>
    <ul class="list-group">
        @if(isset($groups))
            <li class="list-group-item"><a href="{{ route('backend.groups.index') }}" class="pull-right badge">{{ count($groups) }}</a><a href="{{ route('backend.groups.index') }}">Total groups:</a> </li>
        @endif
        <li class="list-group-item"><span class="pull-right badge">{{ count($users) }}</span><a href="{{ route('backend.users.index') }}">Total users:</a> </li>
            <li class="list-group-item"><a href="{{ route('backend.groups.test') }}" class="pull-right badge">{{ count($users->where('test_user','=',1)) }}</a><a href="{{ route('backend.groups.test') }}">Test users:</a> </li>
    </ul>
</div>