(function(scope, $){
    console.log('Knockouts.js');

    class _Knockout {
        constructor() {
            this.routes = scope.api_routes;
            this.$modal = $('#knockoutModal');
            this.knockout = null;
            this.currentQuestion = 0;
            this.stateCheck = false;
            this.checkPlayersLeft = null;
            this.playersLeft = null;
            this.phase = "multiple_choice";

            this.$btnNext = this.$modal.find('.next');
            this.$btnCheckAnswer = this.$modal.find('.check-answer');

            $('.toggle-knockout-start-stop').on('click', this.toggleKnockoutStart.bind(this));

            this.$btnNext.on('click', this.showQuestion.bind(this));
            this.$btnCheckAnswer.on('click', this.checkAnswer.bind(this));

            this.$modal.find('.close').on('click', this.stopKnockout.bind(this));
            this.$modal.find('.end').on('click', this.stopKnockout.bind(this));
            this.$modal.find('.results').on('click', this.showResults.bind(this));
            this.$modal.find('.deciding-question').on('click', this.setDecidingQuestion.bind(this));
            this.$modal.find('.check-deciding-question').on('click', this.checkAnswer.bind(this));

            let active = $('.toggle-knockout-start-stop.btn-danger');
            if(active.length > 0){
                // oh noes ! dont know what to do...
                var knockoutId = active.parent().data('knockout-id');
                var questionIndex = parseInt(active.data('question-index'));
                this.stateCheck = (active.data('check-answer')) || false;

                if(isNaN(questionIndex) || !knockoutId){
                    // something is wrong with this.
                    this.error({error: 'Knockout '+knockoutId+' appears to be active, but data is set incorrectly on this page'});
                } else {
                    this.getCurrentKnockoutInfo(knockoutId, questionIndex);
                }
            }
        }

        getCurrentKnockoutInfo(knockoutId, questionIndex){
            var route = this.getRoute(knockoutId, 'getData');

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                console.log('current knockout info:', data);
                this.knockout = data.knockout;
                this.currentQuestion = questionIndex;
                //this.stateCheck = false;

                this.setModal();
                this.$modal.modal('show');
            }).error(this.error);
        }

        getRoute(knockout_id, key) {
            return this.routes[knockout_id][key];
        }

        toggleKnockoutStart(event){
            var el = $(event.currentTarget),
                knockoutId = el.parent().data('knockout-id'),
                isActive = el.hasClass('btn-danger');

            isActive
                ? this.stopKnockout()
                : this.startKnockout(el, knockoutId);
        }

        stopKnockout(){
            if(confirm('are you sure you want to stop this knockout?')){
                var route = this.getRoute('other', 'stop');

                $.ajax(route, {
                    method: 'Post'
                }).success(data => {
                    if(!data || !data['success']) return this.error(data);

                    this.knockout = null;
                    this.$modal.modal('hide');
                    clearInterval(this.checkPlayersLeft);
                    this.checkPlayersLeft = null;
                    document.location.reload(); // lol nice wallhack this is
                }).error(this.error);
            }
        }

        startKnockout(el, knockoutId){
            var route = this.getRoute(knockoutId, 'start'),
                questionIndex = el.data('question-index') || 0;

            if(!route){
                // something went wrong
                return this.error({success: false, error: 'No route found to start this knockout.'})
            }

            // this part should probably never happen
            var currentActive = $('.toggle-knockout-start-stop.btn-danger').length > 0;

            if(currentActive && !confirm('There is already a poll active, do you wish to proceed to this question?')) {
                return;
            }
            // end of part which probably never should happen

            $.ajax(route, {
                method: 'POST',
                data: {
                    question_index: questionIndex
                }
            }).success(data => {
                if(!data || !data['success']) return this.error(data);
                console.log('start click success', data);
                this.knockout = data.data.knockout;
                this.currentQuestion = data.data.question_index;
                this.stateCheck = false;

                this.setModal();
                this.$modal.modal('show');

            }).error(this.error);
        }

        checkAnswer(event){
            event.preventDefault();
            var route = this.getRoute('other', 'checkAnswer');

            if(!route){
                // something went wrong
                return this.error({success: false, error: 'Url to set the answer on check was set improperly.'})
            }

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                this.stateCheck = true;
                this.setModal();
                if(this.phase === "deciding_question") {
                    this.$modal.find('.end').removeClass("hide");
                }
            }).error(this.error)
        }

        setDecidingQuestion(event) {
            event.preventDefault();

            var el = $(event.currentTarget),
                route = this.getRoute('other','setDecidingQuestion');

            if(el.attr('disabled')){
                // not allowed to click this thing.
                return;
            }

            if(!route){
                // something went wrong
                return this.error({success: false, error: 'Url to switch question was set improperly.'})
            }

            this.phase = "deciding_question";
            this.$btnNext.addClass('hide');

            $.ajax(route, {
                method: 'POST',
                data: {}
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                this.$modal.find('.deciding-question').addClass('hide');
                this.$modal.find('.check-deciding-question').removeClass('hide');
                this.$modal.find('.question-counter').html("Shootout question");
                this.$modal.find('.question-name').html(this.knockout.deciding_question);
                this.$modal.find('.question-answer').html(this.knockout.deciding_answer);
                this.$modal.find('.question-answers').remove();
                this.$btnNext.addClass("hide");

                this.setModal();

            }).error(this.error);
        }

        showQuestion(event){
            event.preventDefault();

            var el = $(event.currentTarget),
                questionIndex = parseInt(el.data('question-index')),
                route = this.getRoute(this.knockout.id,'showQuestion');

            if(el.attr('disabled')){
                // not allowed to click this thing.
                return;
            }

            if(!route){
                // something went wrong
                return this.error({success: false, error: 'Url to switch question was set improperly.'})
            }
            if(isNaN(questionIndex) ){
                return this.error({success: false, error: 'Question index was set improperly'});
            }

            if(questionIndex > this.knockout.questions.length - 1) {
                return this.setDecidingQuestion(event);
            }

            console.log("question index: ", questionIndex);

            $.ajax(route, {
                method: 'POST',
                data: {
                    question_index: questionIndex
                }
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                this.stateCheck = false;
                this.currentQuestion = questionIndex;

                this.setModal();

            }).error(this.error);
        }

        showResults(event){
            event.preventDefault();
            console.log('showResult | don\'t know what to do here yet!');
        }

        startStatusCheck() {
            var route = this.getRoute('other', 'getStatus');

            $.ajax(route, {
                method: 'POST',
                data: {}
            }).success(data => {
                // console.log('check data', data);
                // console.log("current question: ", this.currentQuestion);
                if(!data || !data['success']) return this.error(data.error);
                $('.players-left').text(data.players_left);
                this.playersLeft = data.players_left;

                if(
                    ( parseInt(this.currentQuestion) === this.knockout['questions'].length && this.phase === "multiple_choice")
                ){
                    if(!this.hideDecidingQuestionButton) {
                        this.$modal.find('.deciding-question').removeClass('hide');
                        this.phase = "deciding_question";
                        this.$btnNext.addClass('hide');
                    }
                    this.hideDecidingQuestionButton = true;
                } else {
                    this.$modal.find('.deciding-question').addClass('hide');
                }
                this.setModal();
            }).error(err => {
                //console.warn('Ping answers error: ', err);
            });
        }


        setModal() {
            if(!this.knockout){
                // oh noes!
                return Notifications.error('Unable to set modal without current knockout');
            }

            if(!this.checkPlayersLeft) {
                this.checkPlayersLeft = setInterval(() => this.startStatusCheck(), 1000);
            }
            this.$modal.find('.knockout-name').html( this.knockout.name );
            var question = this.knockout.questions[ this.currentQuestion ];

            if(!question){
                // oh noes!
                return Notifications.error('Unable to set modal for non existing question ' + this.currentQuestion);
            }

            if(this.phase === "multiple_choice") {
                this.$modal.find('.question-number').html(this.currentQuestion+1);
                this.$modal.find('.question-numbers').html(this.knockout.questions.length);
                this.$modal.find('.question-name').html(question.title);
                this.$modal.find('.question-answer').html(question.correct_answer);
            }

            var listAnswers = $('<ol type="a">');
            for(var i=0; i< question.choices.length; i++){
                if(this.stateCheck && question.choices[i].choice === question.correct_answer){
                    listAnswers.append( $('<li class="correct">').html(question.choices[i].value) );
                } else {
                    listAnswers.append( $('<li>').html(question.choices[i].value) );
                }
            }
            this.$modal.find('.question-answers').html( listAnswers );

            console.log('set modal with currentQuestion', this.currentQuestion);

            if(this.phase === "multiple_choice") {
                this.$btnNext.data('question-index',this.currentQuestion+1).removeClass('hide');
                this.$btnNext.attr('disabled', false);
            }

            if(this.stateCheck){
                // knockout is in state of showing correct answer
                this.$btnCheckAnswer.addClass('hide');

                if(this.playersLeft <= 1){
                    console.log('locking last question!');

                    this.$modal.find('.end').removeClass('hide');
                    this.$btnNext.addClass('hide');
                }
            } else {
                // knockout is in state of ppl can do answer

                this.$btnCheckAnswer.removeClass('hide');
                this.$btnNext.attr('disabled','disabled');

                this.$modal.find('.results').addClass('hide');
                this.$modal.find('.end').addClass('hide');
            }
        }

        error(xhr, status, errorThrown) {
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }
    }

    scope.knockout = new _Knockout();


})(this, $);
