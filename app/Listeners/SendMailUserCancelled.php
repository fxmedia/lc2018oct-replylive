<?php

namespace App\Listeners;

use App\Events\UserSetCancelled;
use App\Senders\EmailSender;
//use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailUserCancelled
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserSetCancelled $event
     * @return void
     */
    public function handle(UserSetCancelled $event)
    {
        $user = $event->getUser();

        EmailSender::sendAction($user, 'cancelled');
    }
}
