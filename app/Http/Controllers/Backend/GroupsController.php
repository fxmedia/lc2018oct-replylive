<?php

namespace App\Http\Controllers\Backend;

use App\Events\WaitingListLimitChanged;
use App\Export\GroupExport;
use App\Export\UserExport;
use App\Import\GroupImport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Group;
use App\User;
use App\Setting;
use Session;
use Exception;

class GroupsController extends Controller
{
    //
    public function index()
    {
        $groups = Group::with('users:id')->get()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);
        //$groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);
        $amountTestUsers = count(User::getTestUsers());
        $amountWithoutGroup = count(User::getUsersNoGroup());
        $amountUsers = User::count();

        return view('backend.groups.index', compact('groups', 'amountTestUsers', 'amountWithoutGroup', 'amountUsers'));
    }

    public function create()
    {
        return view('backend.groups.create');
    }

    public function store(Request $request)
    {
        $validators = config('validation.groups');

        $data = $request->validate($validators);

        Group::create($data);

        return redirect()->route('backend.groups.index');
    }

    public function edit($id)
    {
        $group = Group::find($id);

        if ($group === null) {
            return redirect()->back()->withErrors(['message' => 'Unable to find selected group']);
        }

        return view('backend.groups.edit', compact('group'));
    }

    public function update(Request $request, $id)
    {
        $group = Group::find($id);

        if ($group === null) {
            return redirect()->route('backend.groups.index')->withErrors(['message' => 'Error saving updates to group']);
        }

        $validators = config('validation.groups');

        $data = $request->validate($validators);

        if(isset($data['limit']) && $data['limit'] > $group->limit){
            event(new WaitingListLimitChanged());
        }
        if(!isset($data['website'])) {
            $data['website'] = 0;
        }

        $group->fill($data);

        if ($group->save()) {
            return redirect()->route('backend.groups.index')->with('message', 'Successfully updated group ' . $group->name);
        }
        return redirect()->back()->withErrors(['message' => 'Oops, something went wrong with updating the group']);
    }

    public function remove($id)
    {
        $group = Group::find($id);

        if ($group === null) {
            return redirect()->route('backend.group.index')->withErrors(['message' => 'Unable to remove non existing group']);
        }


        try{
            if ( $group->delete()) {
                return redirect()->back()->with('message', 'User has been removed');
            }
        } catch(Exception $e){}

        return redirect()->back()->withErrors(['message' => 'Error removing user.']);
    }

    public function show($id)
    {
        if ($id == 0) {
            $users = User::getUsersNoGroup();
            $groupName = 'Users without group';
        } else {
            $group = Group::find($id);

            if ($group === null) {
                return redirect()->back()->withErrors(['message' => 'Unable to show users for non existing group']);
            }
            $users = $group->users;
            $groupName = $group->name;
        }
        $groups = Group::all();
        $eventDomain = Setting::getValue('event_domain') ?? route('frontend.client.index');
        $amountUsers = count($users);

        return view('backend.groups.show', compact('users', 'groups', 'groupName', 'eventDomain', 'amountUsers'));
    }

    public function showTestUsers()
    {
        $users = User::getTestUsers();
        $groups = Group::all();
        $groupName = 'Test Users';
        $eventDomain = Setting::getValue('event_domain') ?? route('frontend.client.index');
        $amountUsers = User::count();

        return view('backend.groups.show', compact('users', 'groups', 'groupName', 'eventDomain', 'amountUsers'));
    }


    public function perform(Request $request)
    {
        $message = '';
        $count = 0;
        $groups = $request->input('group');
        if ($groups && is_array($groups)) {
            switch ($request->input('perform')) {
                case 'export':
                    $message = 'Perform export';
                    // export per sheet??? nah, not for now
                    $arr = [];
                    foreach ($request->group as $value) {
                        $arr[] = $value;
                    }

                    $users = User::whereHas('groups', function ($query) use ($arr) {
                        $query->whereIn('id', $arr);
                    })->get();
                    //$count = count($users);

                    $file = new UserExport($users, 'groups-selection');
                    $file->export();
                    break;
                case 'empty':
                    $message = 'Emptied ';
                    foreach ($request->group as $value) {
                        if ($group = Group::find($value)) {
                            $group->users()->sync([]);
                        }
                    }
                    break;
                case 'delete':
                    $message = 'Deleted ';
                    foreach ($request->group as $value) {
                        if ($group = Group::find($value)) {
                            try {
                                if ($group->delete()) {
                                    $count++;
                                };
                            } catch(Exception $e){}
                        }
                    }
                    break;
            }
        }


//        if ($count > 0) {
            // send a command for updated user
            /**
             * TODO: send command to nodejs server
             */
//        }

        return redirect()->back()->with('message', $message . $count . ' groups');
    }

    public function exportAll()
    {
        // export just the groups them self, not the users connected to them
        $groups = Group::all();

        $file = new GroupExport($groups, 'all');
        $file->export();

        return redirect()->route('backend.groups')->with('message', count($groups) . ' Groups has been exported');
    }

    public function import(Request $request)
    {
        // import groups, not users to groups
        if ($request->hasFile('group_upload') && $request->file('group_upload')->isValid()) {
            $import = new GroupImport($request->file('group_upload'));

            if ($import->isValid()) {

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors(['no_file' => 'No or wrong file uploaded']);
    }
}
