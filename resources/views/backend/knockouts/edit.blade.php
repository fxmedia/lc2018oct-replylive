@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Knockouts</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.knockouts.index') }}">Knockouts</a></li>
        <li>{{ $knockout->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.knockouts._partials.knockout-form')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>
@endsection
