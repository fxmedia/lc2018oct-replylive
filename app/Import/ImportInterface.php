<?php

namespace App\Import;

interface ImportInterface
{
    public function isValid();

    public function getErrors();

    public function import();

    public function getData();

    public function getHeaders();
    
    
}