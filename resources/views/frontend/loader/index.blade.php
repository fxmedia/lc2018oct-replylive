<!DOCTYPE html>
<html>
<head>
    <title>{{ config('app.name', 'Reply.Live') }}</title>
</head>
<body>
&lt;script id="replyLiveLoader" type="text/javascript" data-rl="{{$dataPath}}" charset="UTF-8" src="{{$srcPath}}" &gt;&lt;/script&gt;
</body>
</html>