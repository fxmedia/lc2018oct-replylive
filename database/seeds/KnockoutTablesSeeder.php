<?php

use Illuminate\Database\Seeder;

class KnockoutTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->command->info('Seeding the database with random knockouts');
        factory('App\Knockout', 3)->create()->each(function($q){
            $q->questions()->save(factory('App\KnockoutQuestion')->make());
            $q->questions()->save(factory('App\KnockoutQuestion')->make());
            $q->questions()->save(factory('App\KnockoutQuestion')->make());
        });
    }
}
