import React from 'react';

export default function Error(props) {
    if(!props.show || !props.message) return null;

    return (
        <div className="ErrorMessage">
            <p>{props.message}</p>
        </div>
    )
}
