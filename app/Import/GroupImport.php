<?php

namespace App\Import;

use App\Group;
use Validator;

class GroupImport extends BaseImport
{
    public function getHeaders()
    {
        return ['name'];
    }

    /**
     * array['errors'] array of error fields.
     * array['warnings'] array of warnings
     * array['status'] array of statuses
     *
     * @return array $arr
     */
    public function import()
    {
        $skipCount = 0;
        $addCount = 0;
        $warnings = [];
        $errors = [];


        foreach($this->data as $key => $group){

            $existingGroup = Group::where('name','=',$group->name)->first();

            if($existingGroup !== null){
                $warnings[] = 'Skipped row '. ($key+1) . ': '. $group->name . ' already exists';
                $skipCount++;
                continue;
            }

            $data = [
                'name' => $group->name,
                'limit' => ($group->limit !== null)? $group->limit : 0
            ];

            $validation = Validator::make($data, config('validation.groups'));

            if($validation->fails()){

                $errors[] = 'Validation failed on row '. ($key+1) .': name or limit is invalid';
                $skipCount++;
                continue;
            }

            $newGroup = Group::create($data);

            if(!$newGroup->save()){
                // fail!
                $errors[] = 'Failed saving row'. ($key+1) .' for unknown reason';
                continue;
            }

            $addCount++;
        }


        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $addCount . ' Group'. ($addCount == 1? '' : 's') .' added'
            ]
        ];
    }
}