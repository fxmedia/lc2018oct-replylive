<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li><a href="{{ route('backend.index') }}" style="padding-left:0;"><img src="{{ asset('images/reply-live-logo.png') }}" style="height:22px;" /></a></li>
            <li><a href="{{ route('backend.users.index') }}">Users</a></li>
            <li><a href="{{ route('backend.groups.index') }}">Groups</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    Modules <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ route('backend.polls.index') }}">Re:Poll</a></li>
                    <li><a href="{{ route('backend.quizzes.index') }}">Re;Quiz</a></li>
                    <li><a href="{{ route('backend.open_questions.index') }}">Re:Question</a></li>
                    <li><a href="{{ route('backend.knockouts.index') }}">Re:Knockout</a></li>
                    <li><a href="{{ route('backend.surveys.index') }}">Re:Survey</a></li>
                    <li><a href="{{ route('backend.posts.index') }}">Re:Wall</a></li>
                    <li><a href="{{ route('backend.learning_curves.index') }}">Re:Assessment</a></li>
                    <li><a href="{{ route('backend.timer.index') }}">Re:Countdown</a></li>
                    <li><a href="{{ route('backend.pushes.index') }}">Re:Push</a></li>
                    <li><a href="{{ route('backend.pledges.index') }}">Pledges</a></li>
                </ul>
            </li>
            <li><a href="{{ route('backend.emails.index') }}">Emails</a></li>
            <li><a href="{{ route('backend.sms.index') }}">Sms</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    Welcome {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ route('backend.settings') }}"><i class="glyphicon glyphicon-edit"></i> Settings</a></li>
                    <li><a href="{{ route('backend.admins.index') }}"><i class="glyphicon glyphicon-user"></i> Manage admins</a></li>
                    <li><a href="{{ route('backend.admins.create') }}"><i class="glyphicon glyphicon-user"></i> Add new admin</a></li>
                    <li><a href="{{ route('backend.admins.edit',[Auth::user()->id]) }}"><i class="glyphicon glyphicon-user"></i> Change profile</a></li>
                    <li><a href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                           <i class="glyphicon glyphicon-log-out"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
