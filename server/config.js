const clArgs = require('command-line-args');
const env = require('dotenv').config({path: __dirname + '/../.env'}).parsed || {};

const clArgsOptions = [
    {
        name: 'host',
        alias: 'h',
        type: String,
        defaultValue: env.APP_URL || 'http://localhost'
    },
    {
        name: 'port',
        alias: 'p',
        type: Number,
        defaultValue: env.NODE_PORT || 5000
    },
    {
        name: 'token',
        alias: 't',
        type: String,
        defaultValue: env.API_TOKEN || 'appeltaart'
    }
];

const options = clArgs(clArgsOptions);
console.log('loading with options:');
console.dir(options, {depth: null, colors: true});

module.exports = {
    axios: {
        baseURL: options.host +'/api',
        timeout: 5000,
        headers: {'x-header-token': options.token || 'appeltaart'}
    },
    options
};
