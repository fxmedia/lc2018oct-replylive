(function(scope, $){
    console.log('OpenQuestions.js');

    class _OpenQuestion {
        constructor() {
            this.routes = api_routes;
            this.answerInterval = null;
            this.currentActiveId = null;

            $('.toggle-open_question-start-stop').on('click', this.toggleQuestion.bind(this));
            // $('.toggle-pause').on('click', this.togglePause.bind(this));
            // $('.reset-question-answers').on('click', this.resetQuestion.bind(this));
            let active = $('.toggle-open_question-start-stop.btn-danger');
            if(active.length > 0) {
                this.currentActiveId = active.parent().data('open_question-id');
                this.startUpdate();

            }
        }

        startUpdate(timeout = 2000) {
            if(!this.answerInterval) {
                this.answerInterval = setInterval(this.checkAnswers.bind(this), timeout);
            }
        }

        stopUpdate() {
            if(this.answerInterval) {
                this.answerInterval = clearInterval(this.answerInterval);
            }
        }

        getRoute(open_question_id, key) {
            return this.routes[open_question_id][key];
        }

        toggleQuestion(event) {
            var el = $(event.currentTarget),
                open_questionId = el.parent().data('open_question-id'),
                isActive = el.hasClass('btn-danger');

            isActive
                ? this.stopQuestion(el, open_questionId)
                : this.startQuestion(el, open_questionId);
        }

        stopQuestion(el, open_questionId) {
            var route = this.getRoute(open_questionId, 'stop');

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                el.toggleClass('btn-danger btn-primary')
                    .find('i').toggleClass('fa-stop fa-play')
                    .parent().find('span').text('Start Open Question')
                    .parent().parent().find('.toggle-pause')
                    .addClass('hidden')
                    .addClass('btn-info').removeClass('btn-success')
                    .find('i').removeClass('glyphicon-play').addClass('glyphicon-pause')
                    .parent().find('span').text('Pause');

                this.currentActiveId = null;
                this.stopUpdate();

            }).error(this.error);
        }

        startQuestion(el, open_questionId) {
            var route = this.getRoute(open_questionId, 'start');

            var currentActive = $('.toggle-open_question-start-stop.btn-danger').length > 0;

            if(currentActive && !confirm('There is already a open_question active, do you wish to proceed to this question?')) {
                return;
            }

            $.ajax(route, {
                method: 'POST',
                data: {results: true}
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                $('.toggle-open_question-start-stop')
                    .addClass('btn-primary').removeClass('btn-danger')
                    .find('i').addClass('fa-play').removeClass('fa-stop')
                    .parent().find('span').text('Start Open Question')
                    .parent().parent().find('.toggle-pause').addClass('hidden');

                el.toggleClass('btn-danger btn-primary')
                    .find('i').toggleClass('fa-stop fa-play')
                    .parent().find('span').text('Stop Open Question')
                    .parent().parent().find('.toggle-pause').removeClass('hidden');

                this.currentActiveId = open_questionId;
                this.startUpdate();

            }).error(this.error);
        }

        checkAnswers() {
            if(this.currentActiveId === null) return;

            var route = this.getRoute('other', 'answerCount');

            $.ajax(route, {
                method: 'POST',
                data: {id: this.currentActiveId}
            }).success(data => {
                console.log('check data', data);
                if(!data || !data['success']) return this.error(data.error);

                if(this.currentActiveId){
                    $('.answer-open_question-'+this.currentActiveId).text(data.total);

                    for(var key in data.result) {
                        if(data.result.hasOwnProperty(key))$('.answer-open_question-'+this.currentActiveId+'-' + key).text(data.result[key]);
                    }
                }

            }).error(err => {
                //console.warn('Ping answers error: ', err);
            });
        }

        error(xhr, status, errorThrown) {
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }


    }

    scope.open_question = new _OpenQuestion();
})(this, $);
