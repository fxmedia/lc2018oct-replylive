var jsQR = require('jsqr');
//var consoleLogToDiv = require('console-log-div');

(function() {
    var HIDE_MESSAGE_TIMEOUT_TIME = 5000;

    var form;
    var qrcode;
    var feedback;
    var period;
    var loader;
    var scanner;
    var scanning = false;
    var doingAjax = false;
    var video;
    var videoCanvas;
    var videoCanvasCtx;
    var hideMessageTimeout;
    var startScanningButton;
    var closeFeedbackButton;
    var animationFrameID;
    var cameraTrack;

    //_____Helper_Functions________________________________________________________________________________________/
    const searchToObject = function() {
        var pairs = window.location.search.substring(1).split("&"),
            obj = {},
            pair,
            i;

        for ( i in pairs ) {
            if ( pairs.hasOwnProperty(i) && pairs[i] === "" ) continue;

            pair = pairs[i].split("=");
            obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
        }
        return obj;
    };

    const _showOrHideLoader = function(show){
        if(show){
            loader.classList.remove('hidden');
        } else {
            loader.classList.add('hidden');
        }
    };

    const _showMessage = function(type, data){
        clearTimeout(hideMessageTimeout);

        switch (type) {
            case 'error':
                $('#scanFeedback #name').text('');
                $('#scanFeedback #message').html(data.error.message);
                break;
            case 'warning':
                $('#scanFeedback #name').text(data.name);
                $('#scanFeedback #message').insertAfter('#name').html('already checked in <br><strong>' + data.scanned + '</strong>');
                break;
            case 'success':
                $('#scanFeedback #message').text('Welcome');
                $('#scanFeedback #name').insertAfter('#scanFeedback #message').text( data.name );
                break;
        }
        $('#scanFeedback').addClass('show ' + type);
        hideMessageTimeout = setTimeout(_hideMessage, HIDE_MESSAGE_TIMEOUT_TIME);
    };

    const _hideMessage = function() {
        clearTimeout(hideMessageTimeout);
        feedback.className = '';
    };

    const _startScanning = function() {

        if ( typeof navigator.mediaDevices.getUserMedia === 'undefined' ) {
            navigator.getuserMedia({
                video: { facingMode: 'environment' },
                }, _streamHandler, _streamErrorHandler);
        }
        else {
            navigator.mediaDevices.getUserMedia({
                video: { facingMode: 'environment' }
            })
            .then(_streamHandler)
            .catch(_streamErrorHandler);
        }
    }

    const _streamHandler = function(stream) {
        cameraTrack = stream.getTracks()[0];
        video.srcObject = stream;
        video.setAttribute('playsinline', true);
        video.play();
        animationFrameID = requestAnimationFrame(_scanForQR);
    }

    const _streamErrorHandler = function(error) {
        alert( 'No suitable camera found' );
        console.log(error);
    }

    const _stopScanning = function() {
        cameraTrack.stop();
        video.srcObject = null;
        videoCanvasCtx.clearRect(0, 0, videoCanvas.width, videoCanvas.height);
        cancelAnimationFrame(animationFrameID);
    }

    const _scanForQR = function() {
        if (video.readyState === video.HAVE_ENOUGH_DATA) {
            scanning = true;
            scanner.classList.add('scanning');

            videoCanvas.width = video.videoWidth;
            videoCanvas.height = video.videoHeight;
            videoCanvasCtx.drawImage(video, 0, 0, videoCanvas.width, videoCanvas.height);
            var imageData = videoCanvasCtx.getImageData(0, 0, videoCanvas.width, videoCanvas.height);
            var code = jsQR(imageData.data, imageData.width, imageData.height);

            if (code && code.data != '') {
                console.log( 'Scanned code:: ', code);
                _submitQRCode(code.data);
            }
        }
        animationFrameID = requestAnimationFrame(_scanForQR);
    }

    const _submitQRCode = function(qrcode) {

        _stopScanning();

        var sendData = {
            method: 'POST',
            barcode: qrcode
        };
        if(period) sendData.period = period;

        if(!doingAjax){
            doingAjax = true;
            _showOrHideLoader(true);

            $.ajax('/api/scan', {
                method:'POST',
                data: sendData
            })
            .success(_onSubmitSuccess)
            .error(_onSubmitError);
        }
    }

    const _onSubmitSuccess = function(data){
        doingAjax = false;
        scanning = false;
        scanner.classList.remove('scanning');
        _showOrHideLoader(false);

        if(!data) return _onSubmitError('Error: Submit success, but no data return');
        if(!data.success) {
            return _showMessage('error', data);
        }

        if(data.warning){
            return _showMessage('warning', data);
        }

        _showMessage('success', data);
    };

    const _onSubmitError = function(data){
        doingAjax = false;
        scanning = false;
        scanner.classList.remove('scanning');
        _showOrHideLoader(false);

        console.warn('submit error!', data);

        _showMessage('error', 'Oops, something went wrong while scanning');
    };


    //_____Initialize______________________________________________________________________________________________/
    var _init = function() {
        period      = searchToObject()['period'];
        scanner     = document.getElementById('scanner');
        form        = document.getElementById('scanForm');
        feedback    = document.getElementById('scanFeedback');
        videoCanvas = document.getElementById('scanner-canvas');
        video       = document.getElementById('scanner-preview');
        videoCanvasCtx = videoCanvas.getContext('2d');
        qrcode      = document.getElementById('qrcode');
        loader      = document.getElementById('loader');
        startScanningButton = document.getElementById('start-scanning');
        closeFeedbackButton = document.getElementById('close-feedback');

        navigator.getUserMedia = (
            navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia
        );

        startScanningButton.addEventListener('click', _startScanning);
        closeFeedbackButton.addEventListener('click', _hideMessage);
    };
    _init();
})();