<?php

namespace App\Http\Controllers\Frontend;

use File;
use App\Helpers\VideoStream;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    //

    public function getImage($token, $type, $file){
        $path = storage_path('uploads/'.config('storage.'.$type).'/'.$file);

        if(!File::exists($path)){
            abort(404);
        }
        return response()->file($path);

    }

    public function getFile($file) {

    }

    public function streamVideo($file){
        $path = storage_path('uploads/video/'.$file);

        $stream = new VideoStream($path);
        $stream->start();
    }
}
