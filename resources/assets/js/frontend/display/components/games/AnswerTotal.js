import React from 'react';
import Odometer from 'react-odometerjs';

export default function AnswerTotal(props) {
    return (
        <div className="AnswerTotal">
            <div className="answer-total-wrapper">
                <div className="total-wrapper">
                    <Odometer value={props.total} format={"d"} duration={300} />
                    <p className="text">{(props.total === 1) ? "answer" : "answers"} given</p>
                </div>
            </div>
        </div>
    )
}
