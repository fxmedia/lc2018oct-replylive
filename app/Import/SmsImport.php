<?php

namespace App\Import;

use App\Sms;
use Validator;

class SmsImport extends BaseImport
{
    public function getHeaders(){
        return ['name', 'subject', 'action', 'body_text'];
    }

    public function import(){
        $skipCount = 0;
        $addCount = 0;
        $warnings = [];
        $errors = [];

        foreach($this->data as $key => $obj){

            if($obj->name === null){
                $warnings[] = 'Skipped row '. ($key+1) . ': missing name field';
                $skipCount++;
                continue;
            }

            $data = [];
            foreach($obj as $k => $v){
                if($v !== null && $k !== 'id')$data[$k] = $v;
            }

            $validation = Validator::make($data, config('validation.sms.store'));

            if($validation->fails()){
                $errors[] = 'Validation failed on row '. ($key+1) .' (' . $validation->messages()->first() . ')';
                $skipCount++;
                continue;
            }

            $sms = new Sms($validation->getData());

            if($sms->save()){
                $addCount++;
            } else {
                $errors[] = 'Unable to save sms on row '. ($key+1);
                $skipCount++;
            }
        }

        return [
            'errors' => $errors,
            'warnings' => $warnings,
            'messages' => [
                $skipCount . ' Row'. ($skipCount == 1? '' : 's') .' skipped',
                $addCount . ' Sms Message'. ($addCount == 1? '' : 's') .' added'
            ]
        ];
    }
}