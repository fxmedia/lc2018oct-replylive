(function(){
    var MY_ID = 'replyLiveLoader';
    var data;

    var currentScript;

    var DEBUG = true;
    var _debug = function(){
        if(DEBUG) console.log.apply(console, arguments);
    };

    //___Helper_Functions_____________________________________________________________________________________________________________________________________________________________________________/
    var PreLoaderHelper = function(){
        var _head = document.getElementsByTagName('head')[0];

        return {
            searchToObject: function() {
                var pairs = window.location.search.substring(1).split("&"),
                    obj = {},
                    pair,
                    i;

                for ( i in pairs ) {
                    if ( pairs.hasOwnProperty(i) && pairs[i] === "" ) continue;

                    pair = pairs[i].split("=");
                    obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
                }

                return obj;
            },
            getData: function(url, data, successCallback, failCallback){
                var _xmlHttpObject;
                var _type = 'POST';
                var _async = true;
                var _status = 0;
                var _txt;
                var _error;

                var _postVars = '';
                for(var k in data){
                    if(data.hasOwnProperty(k)){
                        if(_postVars) _postVars += '&';

                        _postVars += k.toString() + '=' + data[k].toString();
                    }
                }

                if(window.XMLHttpRequest){
                    _xmlHttpObject = new XMLHttpRequest();

                    // overridemimetype to json maybe?
                } else if(window.activeXObject){
                    _xmlHttpObject = new ActiveXObject("Microsoft.XMLHTTP");
                    if (_xmlHttpObject.overrideMimeType) {
                        _xmlHttpObject.overrideMimeType("text/xml");
                    }
                }

                _xmlHttpObject.onerror = function(evt){
                    // communication failed. error during communication
                    failCallback('Communication failed. Error during communication', evt);
                };
                _xmlHttpObject.onabort = function(evt){
                    // communication aborted
                    failCallback('Communication Aborted.', evt);
                };

                _xmlHttpObject.open(_type, url, _async);
                if(_type === 'POST'){
                    _xmlHttpObject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                }
                _xmlHttpObject.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                // var csrfToken = document.querySelector('meta[name=csrf-token]');
                // if(csrfToken){
                //     _xmlHttpObject.setRequestHeader('X-CSRF-TOKEN', csrfToken.getAttribute('content'));
                // }

                _xmlHttpObject.onreadystatechange = function(){
                    if(_xmlHttpObject.readyState === 4) {
                        _status = _xmlHttpObject.status;
                        _txt = "";

                        try {
                            _txt = _xmlHttpObject.responseText;
                        } catch(err){
                            _status = -1;
                            _error = String(err);
                            failCallback(_error);
                        }
                        if(_xmlHttpObject.status === 200){
                            successCallback(_txt);
                        } else {
                            failCallback('Request returned invalid status code: '+ _xmlHttpObject.status);
                        }

                    }
                };

                if(_type === "POST" && !_postVars){
                    _postVars = "noval=" + (Math.floor(Math.random() * 100000)).toString();
                }
                if(_postVars){
                    _xmlHttpObject.send(_postVars);
                } else {
                    _xmlHttpObject.send();
                }

            },
            loadCSS: function(url, callbackFunction){
                _debug('PreloaderHelper | loadCSS: ', url);
                var dlink = document.createElement('link');
                dlink.rel = 'stylesheet';
                dlink.type = 'text/css';
                dlink.href = url;

                var stylesheetLength = (document.styleSheets)? document.styleSheets.length : 0;
                var checkIfLoaded = setInterval(function(){
                    //_debug('trying...');
                    try {
                        if(
                            (dlink.sheet && dlink.sheet.hasOwnProperty('cssRules')) ||
                            (dlink.styleSheet && dlink.styleSheet.cssText.length > 0) ||
                            (dlink.innerHTML && dlink.innerHTML.length > 0) ||
                            (stylesheetLength < document.styleSheets.length)
                        ){
                            dlink.onload = null;
                            clearInterval(checkIfLoaded);
                            callbackFunction();
                            _debug('success!');
                        }
                    } catch(e){

                    }
                }, 20);

                dlink.onload = function(){
                    _debug('PreloaderHelper | loadCSS | dlink.onload()!');
                    dlink.onload = null;
                    clearInterval(checkIfLoaded);
                    callbackFunction();
                };

                _head.appendChild(dlink);
            },
            loadJS: function(url, callbackFunction){
                _debug('PreloaderHelper | loadJS: ', url);
                var script;
                var _loadJsComplete = function(){
                    script.onload = null;
                    script.onreadystatechange = null;
                    callbackFunction();
                };
                script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = url;
                script.onload = _loadJsComplete;
                script.onreadystatechange = function(){
                    if((script.readyState === 'loaded') || (script.readyState === 'complete')){
                        _loadJsComplete();
                    }
                };
                _head.appendChild(script);
            },
            json: function(string){
                var obj = false;
                try {
                    obj = JSON.parse(string);
                } catch(e) {
                    return false;
                }
                return obj;
            }
        };
    }();

    //___Initialize___________________________________________________________________________________________________________________________________________________________________________________/
    var _init = function(){
        document.removeEventListener("DOMContentLoaded", _init);

        var url = ('replyLiveSource' in window && typeof window['replyLiveSource'] === 'string')? window['replyLiveSource'] : '';
        if(!url){
            currentScript = document.currentScript || document.getElementById(MY_ID);
            if(!currentScript){
                // oh noes! major error
                console.error('Reply.Live Loader | Unable to find own script tag.');
                return;
            } else {
                url = currentScript.getAttribute('data-rl');
            }
            if(!url){
                // oh noes! double major error
                console.error('Reply.Live Loader | No source url found to fetch data.');
                return;
            }
        }

        var search = PreLoaderHelper.searchToObject();
        var data = {
            userAgent: navigator.userAgent,
            location: window.location.hostname
        };
        if(search.token){
            data.userToken = search.token;
        } else {
            data.userToken = currentScript.getAttribute('data-token') || '';
        }

        PreLoaderHelper.getData(url, data, _dataCallback, _dataFailCallback);
    };

    //___Load_Functions_______________________________________________________________________________________________________________________________________________________________________________/
    var _dataFailCallback = function(error){
        console.error('Reply.Live Loader | Data error:', error);
    };
    var _dataCallback = function(d){
        _debug('Preloader | _dataCallback:', d);
        var json = PreLoaderHelper.json(d);
        if(json){
            if(json.success){
                data = json.result;

                currentScript.setAttribute('data-json', JSON.stringify({
                    rl_domain    : data.rl_domain,
                    websocket    : data.websocket,
                    token        : data.token,
                    options : data.options
                }));

                var styleUrl = (typeof data.css === 'string')? data.css : data.css.shift();
                PreLoaderHelper.loadCSS(styleUrl, _onLoadCssComplete);
            } else {
                // oh noes!
                console.error('Reply.Live Loader | Unauthorized');
                if(json.errors){
                    for(var i=0; i<json.errors.length; i++){
                        console.warn('Error:', json.errors[i]);
                    }
                }
            }
        } else {
            console.error('Reply.Live Loader | Error parsing data.');
        }
    };
    var _onLoadCssComplete = function(){
        _debug('Preloader | _onloadCssComplete()');
        if(typeof data.css === 'string' || !data.css.length) {
            var jsUrl = (typeof data.js === 'string')? data.js : data.js.shift();
            PreLoaderHelper.loadJS(jsUrl, _onLoadJsComplete);

        } else {
            PreLoaderHelper.loadCSS(data.css.shift(), _onLoadCssComplete());
        }
    };

    var _onLoadJsComplete = function(){
        _debug('Preloader | _onloadJsComplete()');
        if(typeof data.js === 'string' || !data.js.length){
            _loadComplete();
        } else {
            PreLoaderHelper.loadJS(data.js.shift(), _onLoadJsComplete);
        }
    };
    var _loadComplete = function(){
        _debug('Preloader | _onloadComplete()');
        // do we need to do something? of does the js initialize automatically?
    };

    document.addEventListener("DOMContentLoaded", _init);
})();
