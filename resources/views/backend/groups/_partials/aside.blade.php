<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-plus"></i> Create</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.groups.create') }}">Add group</a></li>
    </ul>
    @if(!isset($users))
    <div class="panel-heading"><i class="fa fa-upload"></i> Upload</div>
    <div class="panel-body panel-uploader">
        {!! Form::open(['route' => 'backend.groups.import', 'files' => true]) !!}
        {!! Form::file( 'group_upload', ['class' => 'importfile'] ) !!}
        <br>
        {!! Form::submit( 'Upload', ['class' => 'btn btn-sm btn-primary'] ) !!}
        {!! Form::close() !!}
    </div>
    @endif
    <div class="panel-heading"><i class="fa fa-download"></i> Download</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.groups.export') }}">Download all groups</a></li>
    </ul>


    <div class="panel-heading"><i class="glyphicon glyphicon-stats"></i> Statistics</div>
    <ul class="list-group">
        <li class="list-group-item"><span class="pull-right badge">{{ count($groups) }}</span><a href="{{ route('backend.groups.index') }}">Total groups:</a> </li>
        <li class="list-group-item"><a href="{{ route('backend.users.index') }}" class="pull-right badge">{{ isset($users)? count($users) : isset($amountUsers) ? $amountUsers  : '??' }}</a><a href="{{ route('backend.users.index') }}" >Total users:</a> </li>
        @if(isset($amountTestUsers))
            <li class="list-group-item"><a href="{{ route('backend.groups.test') }}" class="pull-right badge">{{ $amountTestUsers }}</a><a href="{{ route('backend.groups.test') }}" >Test users:</a> </li>
        @endif
    </ul>
</div>