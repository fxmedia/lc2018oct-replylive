<?php

namespace App\Export;



class OpenQuestionExport extends BaseExport
{
    private $filename;
    private $headers = ['question', 'tags', 'multiple_answers', 'groups'];
    private $rows;

    /**
     * TODO: should this be a Report instead of an Export?
     */

    public function __construct($open_questions, $filename = 'all')
    {
        $this->filename = 'export-open_questions-'.$filename;


        foreach($open_questions as $open_question){
            $groups = '';
            foreach($open_question->groups as $group){
                $groups .= $group->name.', ';
            }
            if(strlen($groups) > 2) {
                $groups = substr($groups, 0, -2);
            }
            $this->rows[] = [
                $open_question->question,
                $open_question->tags,
                $open_question->multiple_answers,
                $groups
            ];
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}
