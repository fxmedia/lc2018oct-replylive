<?php

use Illuminate\Database\Seeder;
use App\Email;

class fillDefaultEmails extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(config('default-emails') as $email){
            if(!Email::where('name', '=', $email['name'])->exists()){
                $email['created_at'] = date('Y-m-d H:i:s');
                $email['updated_at'] = date('Y-m-d H:i:s');

                DB::table('emails')->insert($email);
            }
        }
    }
}