<?php

namespace App\Http\Controllers\Backend;

use Exception;
use App\Helpers\ImageHelper;
use App\Quiz;
use App\QuizQuestion;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuizQuestionsController extends Controller
{
    public function create($quiz_id){
        $quiz = Quiz::find($quiz_id);
        if($quiz == null){
            // cannot create question to non existing quiz
            return redirect()->back()->withErrors('Cannot create Question to non existing quiz');
        }

        $images = QuizQuestion::getImageList();
        return view('backend.quizzes.question.create', compact('quiz', 'images'));
    }

    public function store(Request $request, $quiz_id){
        if(!Quiz::whereId($quiz_id)->exists()){
            // cannot create question to non existing quiz
            return redirect()->back()->withErrors(['message' => 'Error storing question to non existing quiz']);
        }

        $validators = config('validation.quizzes.questions.store');
        $data = $request->validate($validators);

        $choices = [];
        foreach($data['choices'] as $c){
            if($c['value'] !== null) {
                $choices[] = [
                    'choice' => $c['choice'],
                    'value' => $c['value']
                ];
            }
        }
        $data['choices'] = $choices;

        $img = $data['image'] ?? null;
        unset($data['image']);

        $question = new QuizQuestion;
        $question->fill($data);
        $question->quiz_id = $quiz_id;

        $this->handleUploads($request, $question);

        if(!$question->save()){
            return redirect()->back()->witherrors(['message'=>'Error saving new question']);
        }

        return redirect()->route('backend.quizzes.edit',[$quiz_id])->with(['message' => "Successfully added a new question"]);
    }

    public function edit($id){
        $question = QuizQuestion::find($id);

        if($question == null){
            return redirect()->back()->withErrors('Unable to find Quiz Question to edit');
        }

        $quiz = $question->quiz;
        $images = QuizQuestion::getImageList();

        return view('backend.quizzes.question.edit', compact('question', 'quiz', 'images'));
    }

    public function update(Request $request, $id){
        $question = QuizQuestion::find($id);

        if($question == null){
            return redirect()->back()->withErrors('No Quiz Question found to edit');
        }

        $validators = config('validation.quizzes.questions.store');

        $data = $request->validate($validators);

        $choices = [];
        foreach($data['choices'] as $c){
            if($c['value'] !== null) {
                $choices[] = [
                    'choice' => $c['choice'],
                    'value' => $c['value']
                ];
            }
        }
        $data['choices'] = $choices;


        $question->fill($data);

        $this->handleUploads($request, $question);

        if($question->save()){
            return redirect()->route('backend.quizzes.edit',[$question->quiz_id])->with(['message' => 'Successfully updated question']);
        }

        return redirect()->back()->withErrors(['message' => 'Unable to save changes to quiz question']);
    }

    public function remove($id){
        $question = QuizQuestion::find($id);

        if($question === null){
            return redirect()->back()->withErrors(['message' => 'Unable to remove non existing question']);
        }


        try{
            $removed = $question->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Question has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing question.']);
    }

    public function reset($id){
        $question = QuizQuestion::find($id);

        if($question === null){
            return redirect()->back()->withErrors(['message'=>'Unable to reset answers to unknown question']);
        }

        $totalRemoved = 0;
        $totalFailed = 0;

        foreach ($question->answers as $answer) {
            try {
                $s = $answer->delete();
                if ($s == true) $totalRemoved++;
            } catch (Exception $e) {
                $totalFailed++;
            }
        }
        return redirect()->back()->with('message', 'Removed '.$totalRemoved. ' answers and skipped '. $totalFailed .' from question '. $question->id);
    }


    /**
     * @param Request $request
     * @param QuizQuestion $question
     */
    private function handleUploads(Request $request, QuizQuestion $question){
        foreach(['image_upload'] as $name){
            if($request->hasFile($name) && $request->file($name)->isValid()){
                $file = $request->file($name);
                $name = str_replace('_upload', '', $name);

                $imageHelper = new ImageHelper($file, 'quiz');

                if( $request->input('resize_'. $name) !== null){
                    $imageHelper->convert(false)
                        ->resizeWallUpload( Setting::getValue('upload_max_size') )
                        ->moveToStorage();
                } else {
                    $imageHelper->convert(false)
                        ->moveToStorage();
                }

                $question->{$name} = $imageHelper->getFilename();
            }
        }
    }
}
