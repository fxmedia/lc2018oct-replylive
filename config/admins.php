<?php
return [
    [
        'name' => 'Niels Janssen',
        'email' => 'niels.janssen@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Jeffrey von Grumbkow',
        'email' => 'jeffrey.vongrumbkow@fxmedia.nl',
        'password' => '$2y$10$GE8M2Ev7nd5j/IMRWCTIz.5c6WsTWignTCdoTwvwoVfD6ZlcXOq0e',
    ],
    [
        'name' => 'Willem-jan Straver',
        'email' => 'willem-jan.straver@fxmedia.nl',
        'password' => '$2y$10$O66RJqEOzsbXJwqf87X9R.g996bIGOpb0LhSp1T4D3Y2v8sO86THS'
    ],
    [
        'name' => 'Daan',
        'email' => 'daan@fxmedia.nl',
        'password' => '$2y$10$i5euIOZzGwHDHhNZI0UOFe6RXwMd1ToyTVsstacLMJ4FAknx2.8hC'
    ],
    [
        'name' => 'Anouk',
        'email' => 'anouk.vlasman@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Rogier',
        'email' => 'rogier@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Pleun',
        'email' => 'pleun.usman@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Stephan',
        'email' => 'stephan@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Mickey',
        'email' => 'mickey@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Mathilde',
        'email' => 'mathilde@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Boy',
        'email' => 'boy@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Floor',
        'email' => 'floor@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Jedidja',
        'email' => 'jedidja@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Marloes',
        'email' => 'marloes@fxmedia.nl',
        'password' => ''
    ],
    [
        'name' => 'Beau',
        'email' => 'beau@fxmedia.nl',
        'password' => ''
    ]
];