<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SurveyAnswer
 *
 * @property-read \App\Survey $survey
 * @property-read \App\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property int $survey_id
 * @property int $question_id
 * @property int $user_id
 * @property string $input
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyAnswer whereInput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyAnswer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyAnswer whereSurveyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyAnswer whereUserId($value)
 * @property int $survey_question_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SurveyAnswer whereSurveyQuestionId($value)
 */
class SurveyAnswer extends Model
{
    protected $fillable = ["survey_id", "survey_question_id", "user_id", "input"];
    protected $table = "survey_answers";

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function survey() {
        return $this->belongsTo('App\SurveyQuestion');
    }
}
