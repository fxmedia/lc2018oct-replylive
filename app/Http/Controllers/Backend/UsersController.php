<?php

namespace App\Http\Controllers\Backend;

use App\Events\UserSetAttending;
use App\Events\UserSetDeclinedByAdmin;
use App\Events\UserSetWaiting;
use App\Helpers\ImageHelper;
use App\NodeJS\Command;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Attending;
use App\Group;
use App\Setting;
use Illuminate\Support\Facades\DB;
use App\Export\UserExport;
use App\Import\UserImport;
use Session;

class UsersController extends Controller
{
    //

    public function index()
    {
        $users = User::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);//->paginate(20);
        $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);
        $eventDomain = Setting::getValue('event_domain') ?? route('frontend.client.index');
        if(strpos($eventDomain, 'http') === false) $eventDomain = 'http://' . $eventDomain;

        return view('backend.users.index', compact('users', 'groups', 'eventDomain'));
    }


    public function search(Request $request)
    {
        $searchQuery = $request->input('search');

        $users = User::where(DB::raw('CONCAT(firstname, " ", lastname)'), 'like', '%' . $searchQuery . '%')
            ->orWhere('phonenumber', 'like', '%' . $searchQuery . '%')
            ->orWhere('email', 'like', '%' . $searchQuery . '%')
            ->orWhereHas('attending', function($query) use ($searchQuery) {
                $query->where('status', 'like', '%' . $searchQuery . '%');
            } )
            ->orderBy('firstname', 'asc')->get()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

        $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);

        $eventDomain = Setting::getValue('event_domain') ?? route('frontend.client.index');
        if(strpos($eventDomain, 'http') === false) $eventDomain = 'http://' . $eventDomain;


        return view('backend.users.index', compact('users', 'groups', 'eventDomain'));
    }

    public function create()
    {
        $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);
        return view('backend.users.create', compact('groups'));
    }

    public function store(Request $request)
    {
        $success = false;
        $validators = config('validation.users.store');
        $data = $request->validate($validators);

        $img = $data['image'] ?? null;
        unset($data['image']);

        $user = new User;
        $user->fill($data);

        // do something with the image
        if (isset($img)) {
            $imageHelper = new ImageHelper($img, 'user');
            $user->image = $imageHelper->convert()
                ->resizeWallUpload( Setting::getValue('upload_max_size') )
                ->moveToStorage()->getFilename();
        }

        // do something with the extras
        if (isset($data['extra']) && is_array($data['extra'])) {
            $extraData = [];
            foreach ($data['extra'] as $extra) {
                if ($extra['key'] && strlen($extra['key']) > 0) {
                    $extraData[$extra['key']] = $extra['value'];
                }
            }
            $user->extra = $extraData;
        } else {
            $user->extra = [];
        }


        $user->token = User::createToken();
        if ($user->save()) {
            $attending = new Attending();
            $attending->user_id = $user->id;
            $attending->status = 'invited';

            if ( $attending->save() ) {
                $user = $user->fresh();

                $success = true;
            }
        }

        // should do something with groups here maybe as well....
        $groups = [];
        if ($request->input('groups')) {
            $groups = (array)$request->input('groups');
        }
        $user->groups()->sync($groups);

        if ($success) {

            Command::make('user_add', $user->toArray());
            return redirect()->route('backend.users.index')->with(['message', 'Successfully added new user']);
        }

        return redirect()->back()->withErrors(['message' => 'Oops, something went wrong with storing the user']);
    }

    public function edit($id)
    {
        $user = User::find($id);

        if ($user !== null) {
            $groups = Group::all()->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE);
            return view('backend.users.edit', compact('user', 'groups'));
        }
        return redirect()->back()->withErrors(['message' => 'Unable to find selected user']);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if ($user === null) {
            return redirect()->route('backend.users.index')->withErrors(['message' => 'Error saving updates to user']);
        }

        $validators = config('validation.users.store');

        $data = $request->validate($validators);
        $img = $data['image'] ?? null;
        unset($data['image']);


        $user->fill($data);

        if (isset($img)) {
            $imageHelper = new ImageHelper($img, 'user');
            $user->image = $imageHelper->convert()->resizeWallUpload( Setting::getValue('upload_max_size') )->moveToStorage()->getFilename();

        } else {
            $user->image = $user->getOriginal('image');
        }

        // do something with groups
        $groups = [];
        if ($request->input('groups')) {
            $groups = (array)$request->input('groups');
        }
        $user->groups()->sync($groups);

        // do something with the extras
        if (isset($data['extra']) && is_array($data)) {
            $extraData = [];
            foreach ($data['extra'] as $extra) {
                if ($extra['key'] && strlen($extra['key']) > 0) {
                    $extraData[$extra['key']] = $extra['value'];
                }
            }
            $user->extra = $extraData;
        } else {
            $user->extra = [];
        }

        if ($user->save()) {
            /**
             * TODO: Send command for new user!
             */
             $user = $user->fresh();
             Command::make('user_update', $user->toArray());

            return redirect()->route('backend.users.index')->with(['message', 'Successfully updated user ' . $user->name]);
        }

        return redirect()->back()->withErrors(['message' => 'Oops, something went wrong with updating the user']);
    }

    public function remove($id)
    {
        $user = User::find($id);

        if ($user === null) {
            return redirect()->route('backend.users.index')->withErrors(['message' => 'Unable to remove non existing user']);
        }

        if ($user->delete()) {
            /**
             * TODO: Send command for removing user
             */

            return redirect()->back()->with('message', 'User has been removed');
        }
        return redirect()->back()->withErrors(['message' => 'Error removing user.']);
    }

    public function perform(Request $request)
    {
        $message = '';
        $count = 0;
        $users = $request->input('user');
        $user_json = $request->input('user_json');
        if($user_json){
            $users = json_decode($user_json, true);
        }

        if ($users && is_array($users)) {
            switch ($request->input('perform')) {
                case 'export':
                    $message = 'Exporting ';

                    $arr = [];
                    foreach ($users as $val) {
                        $arr[] = $val;
                    }

                    $expUsers = User::whereIn('id', $arr)->get();

                    $export = new UserExport($expUsers, 'selection');
                    $export->export();

                    break;
                case 'add_to_group':
                    $message = 'Added to group: ';

                    $selectGroup = $request->input('select_group');
                    if ($selectGroup) {
                        $group = Group::find($selectGroup);
                        if ($group !== null) {
                            $group->users()->sync($users);

                            Command::make('user_add_group', ['group_id' => $group->id, 'user_ids' => $users]);
                        }
                    }
                    $count = count($users);

                    break;
                case 'remove_from_group':
                    $message = 'Removed from group: ';

                    $selectGroup = $request->input('select_group');
                    if ($selectGroup) {
                        $group = Group::find($selectGroup);
                        if ($group !== null) {
                            $group->users()->detach($users);

                            Command::make('user_remove_group', ['group_id' => $group->id, 'user_ids' => $users]);
                        }
                    }
                    $count = count($users);

                    break;
                case 'set_test_user':
                    $message = 'Set as test user: ';
                    foreach ($users as $value) {
                        if ($user = User::find($value)) {
                            $user->test_user = 1;
                            if ($user->save()) {
                                $count++;
                            }
                        }
                    }
                    Command::make('user_set_test', ['test_user' => 1, 'user_ids' => $users]);
                    break;
                case 'remove_test_user':
                    $message = 'Removed from test users: ';
                    foreach ($users as $value) {
                        if ($user = User::find($value)) {
                            $user->test_user = 0;
                            if ($user->save()) {
                                $count++;
                            }
                        }
                    }
                    Command::make('user_set_test', ['test_user' => 0, 'user_ids' => $users]);
                    break;
                case 'delete':
                    $message = 'Deleted ';
                    $ids = [];
                    foreach ($users as $value) {
                        if ($user = User::find($value)) {
                            if ($user->delete()) {
                                $count++;
                                $ids[] = $user->id;
                            }
                        }
                    }
                    Command::make('user_delete', ['user_ids' => $ids]);

                    break;

                case 'set_attending':
                    $message = 'Set attending: ';
                    foreach ($users as $user_id) {

                        if ($user = User::find($user_id)) {
                            if ($attending = Attending::whereUserId($user_id)->first() ?? new Attending(['user_id' => $user_id]) ) {
                                $oldStatus = $attending->status;

                                // Check waiting list and set waiting or attending
                                $attending->status = Attending::determineAttendingStatus( $user_id );
                                if ( $attending->save() ) {
                                    $count++;
                                    $user = $user->fresh();
                                    Command::make('user_update', $user->toArray());

                                    if($oldStatus === 'pending' || $oldStatus === 'declined'){
                                        // user comes from registration, and is now accepted for the first time.
                                        if($attending->status === 'attending'){
                                            event(new UserSetAttending($user));

                                        } else if($attending->status === 'waiting'){
                                            event(new UserSetWaiting($user));

                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;

                case 'set_cancelled':
                    $message = 'Set canceled: ';
                    foreach ($users as $user_id) {

                        if ($user = User::find($user_id)) {
                            if ($attending = Attending::whereUserId($user_id)->first() ?? new Attending(['user_id' => $user_id]) ) {

                                // Check waiting list and set waiting or attending
                                $attending->status = 'cancelled';
                                if ( $attending->save() ) {
                                    $count++;
                                    $user = $user->fresh();
                                    Command::make('user_update', $user->toArray());
                                }
                            }
                        }
                    }
                    break;

                case 'set_declined':
                    $message = 'Declined ';
                    foreach ($users as $user_id) {

                        if ($user = User::find($user_id)) {
                            if ($attending = Attending::whereUserId($user_id)->first() ?? new Attending(['user_id' => $user_id]) ) {
                                $oldStatus = $attending->status;
                                // Check waiting list and set waiting or attending
                                $attending->status = 'declined';
                                if ( $attending->save() ) {
                                    $count++;
                                    $user = $user->fresh();
                                    Command::make('user_update', $user->toArray());

                                    /**
                                     * TODO should user get email anyway if declined from backend?
                                     */
                                    if($oldStatus === 'pending'){
                                        // user comes from registration, and now is declined for the first time
                                        event(new UserSetDeclinedByAdmin($user));
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }

        return redirect()->route('backend.users.index')->with('message', $message . $count . ' users');
    }

    public function exportAll()
    {
        $users = User::all();
        $count = count($users);

        $export = new UserExport($users, 'all');
        $export->export();

        return redirect()->back()->with('message', 'Exporting ' . $count . ' users');
    }

    public function exportTest()
    {
        $users = User::getTestUsers();
        $count = count($users);

        $export = new UserExport($users, 'all');
        $export->export();

        return redirect()->back()->with('message', 'Exporting ' . $count . ' users');
    }

    public function import(Request $request)
    {
        if ($request->hasFile('user_upload') && $request->file('user_upload')->isValid()) {

            $import = new UserImport($request->file('user_upload'));

            if ($import->isValid()) {

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors(['no_file' => 'No or wrong file uploaded']);
    }
}
