<?php

namespace App\Http\Controllers\Frontend;

use App\Display;
use App\Http\Controllers\Controller;
use App\Setting;

class DisplayController extends Controller
{

    public function index($token) {

        $settings = Setting::getSettingsAsObject();

        if(!Setting::getIsOnline()){
            return redirect($settings->redirect_url);
        }


        if(!Display::where('token',$token)->exists()){
            if($settings->security_token !== $token){
                // no access allowed!
                return abort(404);
            }
        }

        $socketUrl = ($settings->nodejs_port == '443')
                ? 'https://'.$settings->nodejs_domain
                : 'http://'.$settings->nodejs_domain.':'.$settings->nodejs_port;
        $json = json_encode([
            'rl_domain' => $settings->reply_live_domain,
            'websocket' => $socketUrl,
            'token'     => $settings->security_token
        ]);

        return view('frontend.display.index', compact('json'));
    }
}
