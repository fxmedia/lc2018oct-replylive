@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Setup Email</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.emails.index') }}">Emails</a></li>
        <li>Setup {{ $email->name }}</li>
    </ol>
@endsection

@section('content')
    @include('backend._partials.message-setup-content')
    <div class="row">
        <div class="col-xs-6 breadcrumb breadcrumb-footer">
            <a id="setup-back-btn" class="btn btn-default"
               href="{{ URL::route('backend.emails.index') }}">Back</a>
        </div>
        <div class="col-xs-6 breadcrumb breadcrumb-footer text-right">
            {!! Form::open(['method' => 'post', 'route' => ['backend.emails.setup-preview', $email->id]]) !!}
            <input type="hidden" id="idList" name="id_list"/>
            {!! Form::submit('Ready!', ['class'=>'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>

    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/message-setup.js') }}"></script>
@endsection