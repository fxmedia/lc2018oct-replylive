<div class="panel panel-default">
    {{--<div class="panel-heading"><i class="fa fa-plus"></i> Create</div>--}}
    {{--<ul class="list-group">--}}
        {{--<li class="list-group-item"><a href="{{ route('backend.users.create') }}">Add user</a></li>--}}
        {{--<li class="list-group-item"><a href="{{ route('backend.groups.create') }}">Add group</a></li>--}}
    {{--</ul>--}}
    {{--<div class="panel-heading"><i class="fa fa-upload"></i> Upload</div>--}}
    {{--<div class="panel-body panel-uploader">--}}
        {{--{!! Form::open(['route' => 'backend.users.import', 'files' => true]) !!}--}}
        {{--{!! Form::file( 'user_upload', ['class' => 'importfile'] ) !!}--}}
        {{--<br>--}}
        {{--{!! Form::submit( 'Upload', ['class' => 'btn btn-sm btn-primary'] ) !!}--}}
        {{--{!! Form::close() !!}--}}
    {{--</div>--}}

    <div class="panel-heading"><i class="fa fa-download"></i> Download</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.posts.export') }}">Download all Posts</a></li>
        <li class="list-group-item"><a href="{{ route('backend.posts.export.comments') }}">Download all Comments</a></li>
        <li class="list-group-item"><a href="{{ route('backend.posts.export.questions') }}">Download all Questions</a></li>
        <li class="list-group-item"><a href="{{ route('backend.posts.export.images') }}">Download Images</a></li>
    </ul>


    <div class="panel-heading"><i class="glyphicon glyphicon-stats"></i> Statistics</div>
    <ul class="list-group">
        <li class="list-group-item"><span class="pull-right badge">{{ count($posts) }}</span>Total posts: </li>
        <li class="list-group-item"><span class="pull-right badge">{{ $commentAmount }}</span>Total comments: </li>
        <li class="list-group-item"><span class="pull-right badge">{{ $questionAmount }}</span>Total questions: </li>
    </ul>
</div>