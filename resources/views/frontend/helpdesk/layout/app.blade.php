<!DOCTYPE html>
<html>
<head>
    <title>{{ $eventName }} | Helpdesk</title>

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('stylesheets')
</head>
<body>

@yield('header')

@yield('content')

<script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/jquery-2.x-git.min.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'x-header-token': '{!! \App\Setting::getValue("media_token") !!}'
        }
    });
</script>
@yield('scripts')
</body>
</html>