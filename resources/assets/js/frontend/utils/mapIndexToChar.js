const characters = "abcdefghijklmnopqrstuvwyz";

const mapIndexToChar = (idx) => characters[idx];

module.exports = mapIndexToChar;
