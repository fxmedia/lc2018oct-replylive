<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
//        'App\Events\Event' => [
//            'App\Listeners\EventListener',
//        ],
        'App\Events\NewUserRegistered' => [
            'App\Listeners\SendMailUserRegistered',
            'App\Listeners\SendAdminUserRegistered'
        ],

        'App\Events\UserSetAttending' => [
            'App\Listeners\SendMailUserAttending'
            // maybe in future here do check if attending list limit is full and send email to admins
        ],

        'App\Events\UserSetWaiting' => [
            'App\Listeners\SendMailUserWaiting'
        ],

        'App\Events\UserSetCancelled' => [
            'App\Listeners\SendMailUserCancelled',
            'App\Listeners\CheckWaitingListAfterUserCancelled'
        ],

        'App\Events\UserSetDeclinedByAdmin' => [
            'App\Listeners\SendMailUserDeclined'
        ],

        'App\Events\WaitingListLimitChanged' => [
            'App\Listeners\FillWaitingListEmptySpots'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
