@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Emails</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li><a href="{{ URL::route('backend.emails.index') }}">Emails</a></li>
        <li>Create new email</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.emails._partials.email-form')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/textarea-tag-lists.js') }}"></script>
@endsection