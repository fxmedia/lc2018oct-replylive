@extends('frontend.scanner.layout.app')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome-all.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/scanner/qrscanner.css') }}" />
@endsection

@section('header')
@endsection

@section('content')

<div id="scanner">
    <div class="container">

        <h1>{{$eventName}}</h1>

        <button id="start-scanning">scan ticket</button>

        <div id="preview-container">
            <span id="intro">Scan ticket with camera</span>
            <video id="scanner-preview"></video>
            <canvas id="scanner-canvas"></canvas>
        </div>

    </div>
</div>

<div id="scanFeedback">
    <p id="message"></p>
    <p id="name"></p>
    <button id="close-feedback">return</button>
</div>

<div id="loader" class="hidden">
    <div class="loader-icon">
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('/js/qrscanner.js') }}"></script>
@endsection