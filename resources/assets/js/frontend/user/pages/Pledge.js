import React from 'react';
import { inject, observer } from 'mobx-react';

import Ad from '../components/layout/Ad';
import config from '../../config';

@inject("stores", "t") @observer
export default class PledgePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pledge: {
                name: "",
                amount: "",
                anonymous: false
            },
            submitted: false,
            error: null
         }
         this.lazyLoadName = true;
    }

    componentWillReceiveProps(props) {
        if(props.stores && props.stores.main.user && this.lazyLoadName) {
            let pledge = {...this.state.pledge};
            pledge.name = props.stores.main.user.name;
            this.lazyLoadName = false;
            this.setState({ pledge })
        }
    }

    handleChange(key, e) {
        if(key == "name" && e.target.value.length >= 70) {
            this.setState({ error: this.props.t.pledge.errors.name_too_long });
        } else {
            let pledge = {...this.state.pledge};
            pledge[key] = (key != "anonymous") ? e.target.value : e.target.checked;
            this.setState({ pledge, error: null });
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        if(this.state.pledge.amount === 0) {
            return this.setState({ error: this.props.t.pledge.errors.amount });
        }
        if(this.state.pledge.name.length === 0) {
            return this.setState({ error: this.props.t.pledge.errors.name });
        }
        this.props.stores.pledge.submit(this.state.pledge);
        this.setState({ submitted: true });
    }

    restartPledge() {
        this.setState({
            pledge: {
                name: this.state.pledge.name,
                amount: 0,
                anonymous: false
            },
            submitted: false
        });
    }

    renderError() {
        if(!this.state.error) return null;

        return (
            <div className="Error">
                <p>
                    {this.state.error}
                </p>
            </div>
        )
    }

    renderThanks() {
        return (
            <div className="PledgePage">
                <h2 className="title">Dank voor uw 'Pledge'</h2>
                <p className="Thanks">
                    Uw gift maakt het mogelijk voor jonge pianisten de reis van hun leven te maken!
                </p>
                <div className="button" onClick={this.restartPledge.bind(this)}>
                    <p>Nog een extra 'Pledge'?</p>
                </div>
                <Ad />
            </div>
        )
    }

    render() {
        if(this.state.submitted) return this.renderThanks();

        return (
            <div className="PledgePage game-wrapper">
                <h2 className="title">Dank voor uw 'Pledge'</h2>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div className="fields-wrapper">
                        <div className="name-wrapper">
                            <label htmlFor="name">Uw naam/bedrijfsnaam*</label>
                            <input
                                type="text"
                                value={this.state.pledge.name}
                                onChange={this.handleChange.bind(this, "name")}
                            />
                            {(this.state.pledge.name.length > 3) ? <img src={`${config.base_url}/images/check.png`} /> : null}
                        </div>
                        <div className="pledge">
                            <label>Uw 'pledge'</label>
                            <div className="amount-wrapper">
                                <span className="currency">&euro;</span>
                                <input
                                    type="number"
                                    value={this.state.pledge.amount}
                                    onChange={this.handleChange.bind(this, "amount")}
                                    placeholder={"Uw bedrag..."}
                                />
                            </div>
                            <div className="anonymous">
                                <label htmlFor="anonymous">Mijn 'Pledge' is anoniem</label>
                                <input id="anonymous" type="checkbox" checked={this.state.pledge.anonymous} onChange={this.handleChange.bind(this, "anonymous")} />
                                <p className="disclaimer">
                                    *Uw naam is noodzakelijk om uw 'Pledge' te registreren; als u liever een anonieme 'pledge' maakt zal uw naam niet verschijnen op het grote scherm; Het bedrag van uw 'Pledge' zal nooit zichtbaar zijn op het grote scherm.
                                </p>
                            </div>
                        </div>
                    </div>
                    <input type="submit" value="Bevestig 'Pledge'" />
                </form>
                <Ad />
                {this.renderError()}
            </div>
        )
    }
}
