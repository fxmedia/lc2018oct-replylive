<?php

return [
    [
        'name' => 'Default Invitation (please change or remove me)',
        'subject' => 'Invitation Reply.live',
        'reply_to' => 'info@fxmedia.nl',
        'from_name' => 'Reply.live',
        'from_email' => 'info@fxmedia.com',
        'action' => '',//'noaction',
        'view' => 'emails.client.default-template',
        'body_text' => 'Hi [name],<br><br>You are invited to our Reply.live event. Check out the details at your personal url here: [url]<br><br>Kind regards,<br><br>The Reply.live team',
        'disclaimer_text' => ''
    ],
    [
        'name' => 'Default Register (please change or remove me)',
        'subject' => 'Registration Reply.live',
        'reply_to' => 'info@fxmedia.nl',
        'from_name' => 'Reply.live',
        'from_email' => 'info@fxmedia.com',
        'action' => '',//'register',
        'view' => 'emails.client.default-template',
        'body_text' => 'Hi [name],<br><br>Thank you for registering to this event. Your registration first have to be approved by an admin. You will receive an email with your personal url for this event when this happens.<br><br>Kind regards,<br><br>The Reply.live team',
        'disclaimer_text' => ''
    ],
    [
        'name' => 'Default attending (please change or remove me)',
        'subject' => 'You are now attending to Reply.live',
        'reply_to' => 'info@fxmedia.nl',
        'from_name' => 'Reply.live',
        'from_email' => 'info@fxmedia.com',
        'action' => '',//'attending',
        'view' => 'emails.client.default-template',
        'body_text' => 'Hi [name],<br><br>We are happy to hear you come to our Reply.live event. Complete your profile or check for more info about the event here: [url]<br><br>Kind regards,<br><br>The Reply.live team',
        'disclaimer_text' => ''
    ],
    [
        'name' => 'Default waiting (please change or remove me)',
        'subject' => 'You are set on the waiting list for Reply.live',
        'reply_to' => 'info@fxmedia.nl',
        'from_name' => 'Reply.live',
        'from_email' => 'info@fxmedia.com',
        'action' => '',//'waiting',
        'view' => 'emails.client.default-template',
        'body_text' => 'Hi [name],<br><br>We are happy to hear you would like to come to our Reply.live event. Unfortunately all places are already taken. There is still a change you will be able to join when someone else will cancel their registration. We will send you an email when this happens.<br><br>Kind regards,<br><br>The Reply.live team',
        'disclaimer_text' => ''
    ],
    [
        'name' => 'Default cancelled (please change or remove me)',
        'subject' => 'Cancelled registration Reply.live',
        'reply_to' => 'info@fxmedia.nl',
        'from_name' => 'Reply.live',
        'from_email' => 'info@fxmedia.com',
        'action' => '',//'cancelled',
        'view' => 'emails.client.default-template',
        'body_text' => 'Hi [name],<br><br>We are sad to hear you are unable to come to our event. If you change your mind or still are able to come, please let us know on your personal url here [url]<br><br>Kind regards,<br><br>The Reply.live team',
        'disclaimer_text' => ''
    ],
    [
        'name' => 'Default moved from waiting to attending (please change or remove me)',
        'subject' => 'You are attending to Reply.live',
        'reply_to' => 'info@fxmedia.nl',
        'from_name' => 'Reply.live',
        'from_email' => 'info@fxmedia.com',
        'action' => '',//'moved_from_waiting_to_attending',
        'view' => 'emails.client.default-template',
        'body_text' => 'Hi [name],<br><br>We are happy to inform you you are able to come to our Reply.live event. You were set from the waiting list to the attending list after somebody else cancelled their registration. Complete your profile or check for more info about this event here: [url}<br><br>Kind regards,<br><br>The Reply.live team',
        'disclaimer_text' => ''
    ],
    [
        'name' => 'Default registration declined (please change or remove me)',
        'subject' => 'Registration Declined for Reply.live',
        'reply_to' => 'info@fxmedia.nl',
        'from_name' => 'Reply.live',
        'from_email' => 'info@fxmedia.com',
        'action' => '',//'register_declined',
        'view' => 'emails.client.default-template',
        'body_text' => 'Hi [name],<br><br>We are sorry to say your registration to our Reply.live event has been declined. We hope we have informed you well.<br><br>Kind regards,<br><br>The Reply.live team',
        'disclaimer_text' => ''
    ],
];