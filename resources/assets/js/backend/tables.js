(function () {
    var selectAll,
        selectables,
        deselectAll,
        performSelector,
        groupsSelector,
        formActions,
        remoteFormSubmitButtons;

    var allowedFormActions = ['delete', 'set_attending', 'set_declined'];

    var _clickSelectAll = function () {
        for (var i = 0; i < selectables.length; i++) {
            selectables[i].checked = true;
        }
    };
    var _clickDeselectAll = function () {
        for (var i = 0; i < selectables.length; i++) {
            selectables[i].checked = false;
        }
    };

    var _changePerformSelector = function () {
        var show = /group/.test(this.value);
        for (var i = 0; i < groupsSelector.length; i++) {
            if (show) groupsSelector[i].classList.remove('hidden');
            else  groupsSelector[i].classList.add('hidden');
        }
    };

    var _clickFormAction = function (e) {
        e.preventDefault();

        var action = this.getAttribute('data-action');
        var userName = this.getAttribute('data-username');
        var form = document.getElementById(this.getAttribute('data-form'));

        if (form && allowedFormActions.indexOf(action) > -1) {
            if (confirm('Are you sure you want to ' + action + ' ' + userName)) {

                var checkbox = document.getElementById('checkbox' + this.getAttribute('data-id'));

                if (checkbox) {
                    _clickDeselectAll();
                    checkbox.checked = true;

                    for (var i = 0; i < performSelector.length; i++) {
                        performSelector[0].value = action;
                    }

                    form.submit();
                }
            }
        }
        else {
            alert('Unable to perform task.');
        }


    };

    var _clickRemoteFormSubmitButton = function(e){
        e.preventDefault();

        var id = this.getAttribute('data-form');
        var c = this.getAttribute('data-confirm');
        if(id){
            var form = document.getElementById(id);
            if(form && !(c && !confirm(c)) ){
                form.submit();
            }
        }
    };

    var _triggerMore = function(e) {
        e.preventDefault();

        var openMore = document.querySelectorAll('.more.open');
        if ( openMore.length > 0 ) {
            openMore[0].classList.remove('open');
        }
        var openTr = document.querySelectorAll('tr.open');
        if ( openTr.length > 0 ) {
            openTr[0].classList.remove('open');
        }

        this.parentElement.classList.add('open');
        this.parentElement.nextElementSibling.classList.add('open');
    }

    var _init = function () {
        var i;
        selectAll = document.getElementsByClassName('select-all');
        for (i = 0; i < selectAll.length; i++) {
            selectAll[i].addEventListener('click', _clickSelectAll);
        }

        deselectAll = document.getElementsByClassName('deselect-all');
        for (i = 0; i < deselectAll.length; i++) {
            deselectAll[i].addEventListener('click', _clickDeselectAll);
        }
        selectables = document.getElementsByClassName('selectable');

        performSelector = document.getElementsByClassName('perform-selector');

        for (i = 0; i < performSelector.length; i++) {
            performSelector[i].addEventListener('change', _changePerformSelector);
        }
        groupsSelector = document.getElementsByClassName('groups-selector');

        formActions = document.getElementsByClassName('form-action');
        for (i = 0; i < formActions.length; i++) {
            formActions[i].addEventListener('click', _clickFormAction);
        }

        remoteFormSubmitButtons = document.getElementsByClassName('remote-form-submit');
        for (i=0; i < remoteFormSubmitButtons.length; i++) {
            remoteFormSubmitButtons[i].addEventListener('click', _clickRemoteFormSubmitButton);
        }

        triggerMore = document.getElementsByClassName('trigger-more');
        for (i = 0; i < triggerMore.length; i++) {
            triggerMore[i].addEventListener('mouseover', _triggerMore);
        }
    };
    _init();
})();