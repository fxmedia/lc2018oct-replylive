@extends('frontend.helpdesk.layout.app')

@section('stylesheets')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/helpdesk/helpdesk.css') }}" />
@endsection

@section('header')
    <header class="bg-primary">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <img src="{{ asset('images/reply-live-logo.png') }}" />
                    <h1 class="text-white">Helpdesk</h1>
                </div>
        </div>
    </header>
@endsection

@section('content')
<div class="container-fluid">

    @include( 'frontend.helpdesk._partials.new_user' )
    @include( 'frontend.helpdesk._partials.usertable' )

</div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('/js/helpdesk.js') }}"></script>
@endsection