import { observable, action, computed } from 'mobx';

export default class LearningCurveManagerStore {

    @observable data;
    @observable currentQuestionIdx;
    @observable activeAnswer = { choice: null, selected: null, checked: false };
    @observable prevAnswers = [];
    @observable phase = null;
    @observable display = { totalUsers: 0, totalAnswers: [] };

    constructor(socket, mainStore) {
        this.socket = socket;
        this.mainStore = mainStore;
        this.data = null;
        this.currentAnswers = [];
        this.currentQuestionIdx = 0;
        this.listen();
    }

    @computed get activeQuestion() {
        if(!this.data) return null;

        return this.data.questions[this.currentQuestionIdx];
    }

    @computed get prevAnswer() {
        if(!this.data || !this.prevAnswers) return null;

        return this.prevAnswers[this.currentQuestionIdx];
    }

    @computed get isLastQuestion() {
        if(!this.data) return null;

        return this.currentQuestionIdx == this.data.questions.length - 1;
    }

    @computed get isCorrect() {
        if(!this.activeAnswer.checked) return false;

        return this.activeQuestion.correct_answer == this.activeAnswer.choice;
    }

    @action setAnswerToChecked() {
        const activeAnswer = {...this.activeAnswer};
        activeAnswer.checked = true;
        this.activeAnswer = activeAnswer;
    }

    @action addAnswer(answer) {
        this.currentAnswers.push(answer.choice);
        this.setAnswer(answer);
        if(this.phase == "post") {
            this.setAnswerToChecked();
        } else {
            this.loadNextQuestion();
        }
    }

    @action addDisplayAnswer(data) {
        const display = {...this.display};
        const questionIndex = data.question_index;
        display.totalAnswers[questionIndex]++;
        this.display = display;
    }

    @action setAnswer(answer) {
        const activeAnswer = {...this.activeAnswer};
        activeAnswer.choice = answer.choice;
        activeAnswer.selected = null;
        this.activeAnswer = activeAnswer;
    }

    @action loadNextQuestion(data) {
        if(this.isLastQuestion && this.phase == "pre") {
            this.mainStore.closeTakeover();
            this.mainStore.setActivePage("home");
        }
        this.resetAnswer();
        this.currentQuestionIdx++;
    }

    @action selectAnswer(answer) {
        console.log("select answer in store", answer);
        const activeAnswer = {...this.activeAnswer};
        activeAnswer.selected = answer;
        this.activeAnswer = activeAnswer;
    }

    @action setPrevAnswers(answers) {
        this.prevAnswers = answers;
    }

    @action reset() {
        this.activeAnswer = { choice: null, selected: null, checked: false };
        this.prevAnswers = [];
        this.currentAnswers = [];
        this.display = { totalUsers: 0, totalAnswers: [] };
    }

    @action resetAnswer() {
        this.activeAnswer = { choice: null, selected: null, checked: false };
    }

    @action reboot(data) {
        console.log(data);
        if(data.question_index) {
            this.currentQuestionIdx = data.question_index;
        }
        if(data.answers) {
            this.prevAnswers = data.answers;
        }
    }

    @action rebootPrivateData(data) {
        console.log("private", data);
        this.display = data;
    }

    @action submitAnswer() {
        this.socket.connection.emit(
            'curve_submit_answer',
            {
                choice: this.activeAnswer.selected,
                learning_curve_question_id: this.activeQuestion.id,
                phase: this.phase,
                curve_id: this.data.id
            }
        );
        this.setAnswer( { choice: this.activeAnswer.selected } );
    }

    @action start(data) {
        this.reset();
        if(data.reboot) {
            this.reboot(data.reboot);
        }
        // block reboot if user has already all of the questions
        if(this.currentQuestionIdx >= data.curve.questions.length) return;
        if(data.mine) {
            this.setPrevAnswers(data.mine);
        }
        if(data.totalUsers) {
            const display = {...this.display};
            display.totalUsers = data.totalUsers;
            display.totalAnswers = data.totalAnswers;
            this.display = display;
        }
        if(data.private) {
            this.rebootPrivateData(data.private);
        }
        this.data = data.curve;
        this.phase = data.phase;
        this.mainStore.setActivePage("curve", true);
        if(this.phase === "post") {
            this.mainStore.setBackground("ios-overflow");
        }
    }

    @action stop() {
        this.mainStore.closeTakeover();
        this.mainStore.setBackground(null);
        this.mainStore.setActivePage("home");
        this.data = null;
        this.currentQuestionIdx = 0;
    }

    @action listen() {
        this.socket.connection.on('curve_start', (data) => this.start(data));
        this.socket.connection.on('curve_stop', () => this.stop());
        this.socket.connection.on('curve_display_add_answer', (data) => this.addDisplayAnswer(data));
        this.socket.connection.on('curve_add_answer', (answer) => this.addAnswer(answer));
        this.socket.connection.on('curve_reset_answer', () => this.resetAnswer());
    }
}
