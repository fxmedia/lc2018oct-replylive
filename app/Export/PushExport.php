<?php

namespace App\Export;

class PushExport extends BaseExport
{
    private $filename;
    private $headers = ['name', 'title', 'copy', 'groups', 'image', 'module'];
    private $rows;

    public function __construct($pushes, $filename = 'all')
    {
        $this->filename = 'export-pushes-'.$filename;


        foreach($pushes as $push){
            $groups = '';
            foreach($push->groups as $group){
                $groups .= $group->name.', ';
            }
            if(strlen($groups) > 2) {
                $groups = substr($groups, 0, -2);
            }
            $this->rows[] = [
                $push->name,
                $push->title,
                $push->copy,
                $groups,
                $push->image,
                $push->module
            ];
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}