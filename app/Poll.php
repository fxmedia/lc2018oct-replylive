<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Poll
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PollAnswer[] $answers
 * @mixin \Eloquent
 * @property int $id
 * @property string $question
 * @property array $choices
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Poll whereChoices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Poll whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Poll whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Poll whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Poll whereUpdatedAt($value)
 * @property string|null $tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Poll whereTags($value)
 * @property-read mixed $result
 * @property-read mixed $group_ids
 * @property-read mixed $result_users
 */
class Poll extends Model
{
    protected $fillable = ['question', 'choices', 'tags'];

    /**
     * Set database fields as different type.
     * E.g. Array will be stored as serialized array and will be unserialized when retrieved
     * @var array
     */
    protected $casts = [
        'choices' => 'array'
    ]; 

    protected $appends = ['result', 'result_users', 'group_ids'];


    public function getResultAttribute() {
        $obj = [];
//
        foreach($this->answers as $answer){
            if(!isset($obj[$answer->choice])) $obj[$answer->choice] = 0;

            $obj[$answer->choice]++;
        }

        if(count($obj) === 0) {
            return null;
        }

        return $obj;
    }

    public function getResultUsersAttribute(){
        return $this->answers()->pluck('user_id')->toArray();
    }

    public function getGroupIdsAttribute(){
        return $this->groups()->pluck('id')->toArray();
    }


    public function answers() {
        return $this->hasMany('App\PollAnswer');
    }

    public function groups(){
        return $this->belongsToMany('App\Group');
    }

    /**
     * isInGroup
     *
     * @param group $group
     * @return bool true if user is attached to given group
     */
    public function isInGroup(Group $group){
        $groups = [];
        foreach ($this->groups as $userGroup) {
            $groups[] = $userGroup->id;
        }

        return in_array($group->id, $groups);
    }
}
