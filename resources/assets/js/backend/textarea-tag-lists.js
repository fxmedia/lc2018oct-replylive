(function () {
    console.log('textarea-tag-list.js');

    var _clickTagItem = function () {
        var val = this.getAttribute('data-value');
        var forWho = this.parentNode.getAttribute('data-for');
        console.log('click li', val, forWho);
        if (val && forWho) {
            _insertAtCaret(forWho, '['+val+']');
        }
    };

    var _insertAtCaret = function (areaId, text) {
        var txtarea = document.getElementById(areaId);
        if (!txtarea) {
            return;
        }

        var scrollPos = txtarea.scrollTop;
        var strPos = 0;
        var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
            "ff" : (document.selection ? "ie" : false ) );
        if (br == "ie") {
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart('character', -txtarea.value.length);
            strPos = range.text.length;
        } else if (br == "ff") {
            strPos = txtarea.selectionStart;
        }

        var front = (txtarea.value).substring(0, strPos);
        var back = (txtarea.value).substring(strPos, txtarea.value.length);
        txtarea.value = front + text + back;
        strPos = strPos + text.length;
        if (br == "ie") {
            txtarea.focus();
            var ieRange = document.selection.createRange();
            ieRange.moveStart('character', -txtarea.value.length);
            ieRange.moveStart('character', strPos);
            ieRange.moveEnd('character', 0);
            ieRange.select();
        } else if (br == "ff") {
            txtarea.selectionStart = strPos;
            txtarea.selectionEnd = strPos;
            txtarea.focus();
        }

        txtarea.scrollTop = scrollPos;
    };

    var _init = function () {
        var tagLists = document.getElementsByClassName('tag-list');
        var items;

        console.log(tagLists);
        for (var i = 0; i < tagLists.length; i++) {
            items = tagLists[i].getElementsByTagName('li');
            console.log(items);
            for (var j = 0; j < items.length; j++) {
                items[j].addEventListener('click', _clickTagItem);
            }
        }
    };
    _init();

})();