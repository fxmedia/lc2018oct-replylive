import React from 'react';

export default class OpenQuestionAnswer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            styling: {
                opacity: 0,
                top: 0,
                left: 0
            },
        }
    }

    componentDidMount() {
        if(!this.answer) {
            return false;
        }
        const bounds = this.answer.getBoundingClientRect();
        const position = this.calcPosition(bounds.width, bounds.height);
        let styling = {
            opacity: (this.props.fade) ? 0.1 : 1,
            ...position
        }
        this.setState({ styling });
    }

    calcPosition(width, height) {
        const zone = document.querySelector(".zone");
        const question = document.querySelector(".Question");
        const bounds = zone.getBoundingClientRect();
        const questionHeight = question.getBoundingClientRect().height / 2;
        const top = Math.random() * (bounds.height - height - questionHeight) + questionHeight;
        const left = Math.random() * (bounds.width - width);
        return { top, left };
    }

    renderLayout() {
        if(!this.props.data) return null;
        let styling = "OpenQuestionAnswer";
        if(this.props.fade) {
            styling += " fade";
        } else if(this.props.highlight) {
            styling += " highlight";
        }
        return styling;
    }

    render() {
        if(this.props.hide || !this.props.data) return null;

        return (
            <div
                ref={(answer) => { this.answer = answer; }}
                style={this.state.styling}
                className={this.renderLayout()}
                onClick={this.props.handleClick}
            >
                <p>{this.props.data.answer}</p>
            </div>
        )
    }
}
