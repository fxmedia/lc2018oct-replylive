@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Pledges</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Pledges</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.pledges._partials.aside')
    </div>
    <div class="col-xs-9">
        <div class="show-wall">
            <h3>Show Pledges on Display</h3>
            <div class="form-switch">
                <input class="toggle-display on" id="pledgesOn" type="radio" name="showDisplay" value="1" @if($showDisplayPledges == '1') checked @endif>
                <input class="toggle-display off" id="pledgesOff" type="radio" name="showDisplay" value="0" @if(!$showDisplayPledges == '1') checked @endif>
                <div class="switch">
                    <label class="on" for="pledgesOff">On</label>
                    <label class="off" for="pledgesOn">Off</label>
                </div>
            </div>
        </div>
        @include('backend._partials.errors')
        <div class="pledges">
            <h3>Pledges by Users</h3>
            {!! Form::open(['route'=>['backend.pledges.perform'], 'id'=>'pledgesTable', 'method'=>'POST']) !!}
            <table class="table table-hover">
                <thead>
                    @include('backend._partials.table-top-actions')
                    <tr>
                        <th>&nbsp;</th>
                        <th>Name</th>
                        <th>Amount</th>
                        <th>User</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pledges as $pledge)
                        <tr id="pledge_{{ $pledge->id }}" class="">
                            <td>{!! Form::checkbox('pledge[]', $pledge->id, false, ['class' => 'selectable', 'id' => 'checkbox'.$pledge->id]) !!}</td>
                            <td>{{ $pledge->name }}</td>
                            <td>{{ $pledge->amount }}</td>
                            <td>{{ $pledge->user->name }}</td>
                            <td>
                                {{--<a class="btn btn-xs btn-success" href="{{ route('backend.pledges.edit', [$pledge->id] ) }}">--}}
                                    {{--<span class="glyphicon glyphicon-edit"></span>--}}
                                {{--</a>--}}
                                <a class="btn btn-xs btn-danger form-action" href="#"
                                   data-action="delete"
                                   data-username="{{ $pledge->user->name }}"
                                   data-id="{{ $pledge->id }}"
                                   data-form="pledgesTable">
                                    <span class="glyphicon glyphicon-trash"></span>
                                    Remove
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                @include('backend.pledges._partials.table-bottom-action')
            </table>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        var api_routes = {
            'other': {
                toggle_display: '{{ route('api.pledge.toggle-display') }}'
            }
        };
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/pledges.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/tables.js') }}"></script>
@endsection
