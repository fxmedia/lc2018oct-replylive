<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-plus"></i> Create</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.emails.create') }}">Add Email</a></li>
    </ul>
    <div class="panel-heading"><i class="fa fa-upload"></i> Upload</div>
    <div class="panel-body panel-uploader">
        {!! Form::open(['route' => 'backend.emails.import', 'files' => true]) !!}
        {!! Form::file( 'email_upload', ['class' => 'importfile'] ) !!}
        <br>
        {!! Form::submit( 'Upload', ['class' => 'btn btn-sm btn-primary'] ) !!}
        {!! Form::close() !!}
    </div>
    <div class="panel-heading"><i class="fa fa-download"></i> Download</div>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('backend.emails.export') }}">Download all Emails</a></li>
        <li class="list-group-item"><a href="{{ route('backend.emails.report') }}">Download Logs</a></li>
    </ul>

    <div class="panel-heading"><i class="glyphicon glyphicon-wrench"></i> Actions</div>
    <ul class="list-group">
        {{--<li class="list-group-item">--}}
            {{--<form action="{{ URL::route('backend.emails.reset') }}" method="post" onsubmit="return confirm('Are you sure you wish to reset all scores?')">--}}
                {{--{!! csrf_field() !!}--}}
                {{--<button type="submit" class="btn-link">Reset all quiz scores</button>--}}
            {{--</form>--}}
        {{--</li>--}}
    </ul>

    <div class="panel-heading"><i class="glyphicon glyphicon-stats"></i> Statistics</div>
    <ul class="list-group">
        <li class="list-group-item"><span class="pull-right badge">{{ count($emails) }}</span>Total email templates:</li>
        <li class="list-group-item"><span class="pull-right badge">{{ $sent }}</span>Emails sent:</li>
        <li class="list-group-item"><span class="pull-right badge">{{ $processing }}</span>Emails processing:</li>
        <li class="list-group-item"><span class="pull-right badge">{{ $failed }}</span>Emails failed:</li>
    </ul>
</div>