import React from 'react';
import { inject, observer } from 'mobx-react';

import AnswerTotal from '../components/games/AnswerTotal';
import ProgressBar from '../components/games/ProgressBar';
import Question from '../components/games/Question';
import GameLabel from '../components/games/Label';

@inject('stores', 't') @observer
export default class PollPage extends React.Component {

    render() {
        const { question, choices, result } = this.props.stores.poll.data;
        const { totalAnswers } = this.props.stores.poll;
        const { totalUsers } = this.props.stores.poll.mainStore;

        console.log(result);

        if(!question || !choices) return <p>Poll not found</p>;

        return (
            <div className="PollPage game-wrapper">
                <GameLabel text={this.props.t.poll.name} />
                <Question text={question} />
                <ol className="ABCD">
                    {choices.map((answer, idx) =>
                        <li className="answer-wrapper" key={answer.choice}>
                            <div className="choice"><span>{answer.choice}</span></div>
                            <ProgressBar value={(result && result[answer.choice]) || 0} total={totalAnswers} />
                        </li>
                    )}
                </ol>
            </div>
        )
    }
};
