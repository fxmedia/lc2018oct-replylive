import { observable, action } from 'mobx'

export default class PushStore {

    @observable data

    constructor(socket, mainStore) {
        this.socket = socket;
        this.mainStore = mainStore;
        this.data = null;
        this.listen();
    }

    @action start(data) {
        this.data = data;
        this.mainStore.setActivePage("push", true);
    }

    @action stop() {
        this.mainStore.closeTakeover();
        this.mainStore.setActivePage("home");
        this.data = null;
    }

    @action listen() {
        this.socket.connection.on('push_start', data => this.start(data));
        this.socket.connection.on('push_stop', () => this.stop());
    }
}
