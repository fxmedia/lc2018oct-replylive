import React from 'react';
import { inject, observer } from 'mobx-react';

import Post from '../components/post/Post';
import Loader from '../components/Loader';
import PostForm from '../components/post/PostForm';

@inject("stores", "t") @observer
export default class PostPage extends React.Component {

    constructor(props) {
        super(props);
        this.POST_LIMIT = 20;
    }

    componentDidMount() {
        this.props.stores.post.setIsLoading(true);
        this.props.stores.post.setContainer();
        this.props.stores.post.fetchAll();
    }

    componentDidUpdate() {
        this.props.stores.post.scrollToLast();
    }

    render() {
        const { isLoading } = this.props.stores.post;

        return (
            <div className="PostPage">
                {(isLoading) ? <Loader text={this.props.t.waiting.please_wait} /> : null}
                <div className="PostsContainer">
                    {this.props.stores.post.feed
                        .filter(post => post.visible == 1 || post.user_id == this.props.stores.main.user.id)
                        .slice(-this.POST_LIMIT)
                        .map(post => <Post key={post.id} data={post} mine={post.user_id == this.props.stores.main.user.id} />)
                    }
                </div>
                <PostForm />
            </div>
        )
    }
}
