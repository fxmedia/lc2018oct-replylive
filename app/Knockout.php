<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Knockout
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\KnockoutAnswer[] $answers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Knockout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Knockout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Knockout whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Knockout whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Knockout whereTags($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read mixed $group_ids
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\KnockoutQuestion[] $questions
 * @property string|null $deciding_question
 * @property int|null $deciding_answer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Knockout whereDecidingAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Knockout whereDecidingQuestion($value)
 */
class Knockout extends Model
{

    protected $table = "knockouts";

    protected $fillable = ['name', 'tags', 'deciding_question', 'deciding_answer'];

    protected $appends = ['group_ids'];

    protected $with = ['questions'];

    public function getGroupIdsAttribute(){
        return $this->groups()->pluck('id')->toArray();
    }

    public function questions(){
        return $this->hasMany('App\KnockoutQuestion')->orderBy('order');
    }

    public function groups(){
        return $this->belongsToMany('App\Group');
    }

    /**
     * isInGroup
     *
     * @param group $group
     * @return bool true if user is attached to given group
     */
    public function isInGroup(Group $group){
        $groups = [];
        foreach ($this->groups as $userGroup) {
            $groups[] = $userGroup->id;
        }

        return in_array($group->id, $groups);
    }


    public static function getAnswerStatus($id = null){
        $knockout = Knockout::find($id);

        if($knockout === null) return null;

        $data = [];
        $i = 0;
        foreach($knockout->questions as $question){
            $data[$i] = [];
            $data[$i]['total'] = count($question->answers);

            foreach($question->answers as $answer){
                if(!isset($data[$i][$answer->choice])) $data[$i][$answer->choice] = 0;

                $data[$i][$answer->choice]++;
            }

            $i++;
        }

        return $data;
    }
}
