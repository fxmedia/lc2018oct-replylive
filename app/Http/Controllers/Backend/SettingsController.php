<?php

namespace App\Http\Controllers\Backend;

use App\Events\WaitingListLimitChanged;
use App\Import\SettingImport;
use App\NodeJS\Command;
use App\Setting;
use App\Http\Controllers\Controller;
use App\SettingGroup;
use App\Export\SettingExport;
use Illuminate\Http\Request;
use Session;

class SettingsController extends Controller
{
    //

    public function index(){
        //$settings = Setting::all();
        $groups = SettingGroup::orderBy('order')->get();
        return view('backend.settings.index', compact('groups'))->with(['message' =>'go fuck yourself']);
    }

    public function update(Request $request){
        $settings = Setting::all();
        //dd($request);

        $validateArray = [];
        foreach($settings as $setting){
            $validateArray[$setting->key] = $setting->validation;
        }
        //dd($validateArray);
        $data = $request->validate($validateArray);
        $saved = false;
        foreach($settings as $setting){
            // but for now testing just save

            // app goes offline, so send command to nodejs server
            if( $setting->key === 'online'  &&
                $setting->value == 1        &&
                $data[ $setting->key ] == 0 ){

                Command::make('app_offline');
            }

            if($setting->key === 'event_attending_limit' &&
                $setting->value < $data[ $setting->key ] ){

                event(new WaitingListLimitChanged());
            }

            $setting->value = $data[ $setting->key ];
            $saved = $setting->save();

        }

        if($saved){
            // send command to njs server
            // Command::make('settings_update', [ 'data'=>['settings'=> Setting::getSettingsAsObject() ] ]);
        }


        return redirect()->route('backend.settings')->with(['message' => 'Changes have been saved']);
    }

    public function export(){
        $settings = Setting::all();

        $file = new SettingExport($settings);
        $file->export();

        return redirect()->route('backend.settings');
    }

    public function import(Request $request){
        if($request->hasFile('setting_upload') && $request->file('setting_upload')->isValid()){

            $import = new SettingImport($request->file('setting_upload'));

            if($import->isValid()){

                $result = $import->import();

                // send command to njs server
                //Command::make('settings_update', [ 'data'=>['settings'=> Setting::getSettingsAsObject() ] ]);

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors([ 'no_file' => 'No or wrong file uploaded' ]);
    }
}
