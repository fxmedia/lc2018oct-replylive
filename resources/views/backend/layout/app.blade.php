<!DOCTYPE html>
<html>
<head>
    <title>Reply.Live Backend</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" charset="UTF-8" href="{{ asset('css/app.css') }}">
    <link type="text/css" rel="stylesheet" charset="UTF-8" href="{{ asset('css/backend.css') }}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
</head>
<body>

@section( 'navigation' )
    @include( 'backend._partials.navigation' )
@show
<div class="container">
    @yield('header')

    @yield('content')
</div>
@yield('footer')

<div id="notifications"></div>
<div id="nodeServerStatusModal">
    <h2>Node Server</h2>
    <p>
        the server is currently: <span class="online"> offline</span>
    </p>
    <p>
        <span class="connected-users">..</span> users are connected
    </p>
    <p>
        the active module: <span class="active-module">none</span>
    </p>
</div>

{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>--}}
<script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/jquery-2.x-git.min.js') }}"></script>
<script>
    var statusRoute = '{{ URL::route('api.app.status') }}';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'x-header-token': '{!! \App\Setting::getValue("api_token") !!}'
        }
    });
</script>
<script src="{{ asset( 'js/backend/bootstrap.min.js' ) }}"></script>
<script src="{{ asset( 'js/backend/notifications.js' ) }}"></script>
{{--<script src="{{ asset( 'js/backend/statistics.js' ) }}"></script>--}}
<script src="{{ asset( 'js/backend/main.js') }}"></script>
@yield('scripts')

</body>
</html>
