<div id="new-user" class="row">
    <div class="col-12">
        <button data-toggle="modal" data-target="#new-user-modal" class="add-user btn btn-primary"><i class="fa fa-plus"></i> Add new user</button>
    </div>

<div id="new-user-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['id' => 'newUserForm', 'method' => 'POST', 'route' => ['api.helpdesk.register-user']]) !!}

            <div class="modal-header">
                <h5 class="modal-title">Add user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Firstname','', ['class' =>'control-label']) !!}
                    {!! Form::text('firstname', '', ['id' => 'firstname', 'class' => 'form-control', 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Lastname','', ['class' =>'control-label']) !!}
                    {!! Form::text('lastname', '', ['id' => 'lastname', 'class' => 'form-control', 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Emailaddress','', ['class' =>'control-label']) !!}
                    {!! Form::email('email', '', ['id' => 'email', 'class' => 'form-control', 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Phonenumber','', ['class' =>'control-label']) !!}
                    {!! Form::text('phonenumber', '', ['id' => 'phonenumber', 'class' => 'form-control', 'required']) !!}
                </div>

            </div>
            <div class="modal-footer">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary submit-scan']) !!}
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>

</div>