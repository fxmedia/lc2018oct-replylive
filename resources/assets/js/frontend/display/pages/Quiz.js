import React from 'react';
import { inject, observer } from 'mobx-react';

import AnswerTotal from '../components/games/AnswerTotal';
import Image from '../components/utils/Image';
import Question from '../components/games/Question';
import QuizAnswer from '../components/games/QuizAnswer';
import GameLabel from '../components/games/Label';

@inject('stores', 't') @observer
export default class QuizPage extends React.Component {

    renderImage(image_path) {
        if(!image_path) return null;

        return (
            <div className="img-wrapper">
                <Image filename={image_path} />
            </div>
        );
    }

    render() {
        const { activeQuestion, activeAnswer, currentQuestionIdx, data, answerCount } = this.props.stores.quiz;

        if(!activeQuestion || !data) return <p>Quiz not found</p>;

        const { totalUsers } = this.props.stores.main;
        const questionTotal = `${this.props.t.quiz.question} ${currentQuestionIdx + 1} ${this.props.t.quiz.of} ${data.questions.length}`;

        return (
            <div className={(activeQuestion.image_path) ? "QuizPage game-wrapper with-image" : "QuizPage game-wrapper"}>
                {this.renderImage(activeQuestion.image_path)}
                <div className="question-answers">
                    <GameLabel text={questionTotal} />
                    <Question text={activeQuestion.title} />
                    <ol className={(activeAnswer.checked) ? "ABCD checked" : "ABCD"}>
                        {activeQuestion.choices.map((answer) =>
                            <QuizAnswer
                                answer={answer.value}
                                key={answer.choice}
                                choice={answer.choice}
                                showResults={activeAnswer.checked}
                                correct={answer.choice == activeQuestion.correct_answer}
                            />
                        )}
                    </ol>
                </div>
                <AnswerTotal totalUsers={totalUsers} total={answerCount} />
            </div>
        );
    }
}
