<?php

return [
    /*
     * Here we say in what directory which images and files must be stored
     */
    'user' => 'users',      // e.g. profile image
    'post' => 'posts',      // images on message board / posts
    'push' => 'pushes',      // images from push modules
    'quiz' => 'quiz',
    'email_images' => 'email_images',     // header/footer images, but also attachments
    'email_attachments' => 'email_attachments',
    'other' => 'uploads'    // default directory
];
