@extends('frontend.client.layout.app')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/user/user.css').'?v='.$settings->css_cache_version }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/register/register.css').'?v='.$settings->css_cache_version }}" />
@endsection

@section('header')
@endsection

@section('content')
    <div class="App">
        <div class="Header">
            <div class="logo-wrapper">
                <img src={{ asset('images/logo.png') }} />
            </div>
        </div>
        <h2 class="title">Welkom</h2>

        {!! Form::open(['class' => 'register-form', 'route' => 'frontend.client.register']) !!}
        <div class="fields-wrapper">
            <div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
                {!! Form::label('Voornaam', '', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('firstname', Input::old('firstname'), ['class' => 'form-control']) !!}
                </div>
                <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('firstname') !!}</span>
            </div>
            <div class="form-group {{ $errors->has('lastname') ? 'has-error' : '' }}">
                {!! Form::label('Achternaam', '', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('lastname', Input::old('lastname'), ['class' => 'form-control']) !!}
                </div>
                <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('lastname') !!}</span>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                {!! Form::label('E-mail', '', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::email('email', Input::old('email'), ['class' => 'form-control']) !!}
                </div>
                <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('email') !!}</span>
            </div>
            <div class="form-group {{ $errors->has('company') ? 'has-error' : '' }}">
                {!! Form::label('Bedrijfsnaam (optioneel)', '', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('company', Input::old('company'), ['class' => 'form-control']) !!}
                </div>
                <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('company') !!}</span>
            </div>
            <div class="form-group {{ $errors->has('register_password') ? 'has-error' : '' }}">
                {!! Form::label('Wachtwoord', '', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('register_password', Input::old('register_password'),
                        [
                            'class' => 'form-control',
                            'autocapitalize' => 'off',
                            'autocomplete' => 'off',
                            'autocorrect' => 'off'
                        ])
                    !!}
                </div>
                <p class="help-block col-sm-12 col-md-offset-3">
                    {{ $errors->has('register_password') ? 'Het opgegeven wachtwoord is niet juist.' : '' }}
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12 text-right">
                <img id="loader" src="/images/loader.svg" style="display: none;" />
                {!! Form::submit('Ga door', ['class'=>'btn btn-success']) !!}
            </div>
        </div>


        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var form = document.querySelector(".register-form");
        var loader = document.getElementById("loader");
        form.addEventListener("submit", function() {
            document.querySelector("input[type=submit]").style.display = "none";
            loader.style.display = "inline-block";
        });
    </script>
@endsection
