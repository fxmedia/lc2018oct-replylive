<?php

use Faker\Generator as Faker;

$factory->define(App\Survey::class, function (Faker $faker) {
    return [
        'name' => $faker->lastName,
        'tags' => $faker->word,
        'takeover' => $faker->boolean(50),
        'intro' => $faker->text(200),
        'outro' => $faker->text(150)
    ];
});
