<?php

use Illuminate\Database\Seeder;

class PushesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->command->info('Seeding the database with random pushes');
        factory('App\Push', 4)->create();
    }
}
