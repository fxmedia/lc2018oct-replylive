<tfoot>
<tr>
    <td colspan="2">
        {!! Form::select('perform', [
            'set_test_user'     => 'Make test users',
            'remove_test_user'  => 'Undo test users',
            'add_to_group'      => 'Add to group:',
            'remove_from_group' => 'Remove from group:',
            'set_attending'     => 'Set users Attending',
            'set_cancelled'     => 'Set users Cancelled',
            'set_declined'      => 'Set users Declined',
            'export'            => 'Export users',
            'delete'            => 'Delete users'
        ],
        'send_text', [ 'class' => 'form-control pull-left perform-selector'] ) !!}
    </td>
    @if(isset($groups))
    <td class="groups-selector hidden">
        <select class="form-control pull-left" name="select_group">
            @foreach($groups as $group)
                <option value="{{ $group->id }}">{{ $group->name }}</option>
            @endforeach
        </select>
    </td>
    @endif
    <td>
        {!! Form::submit( 'Submit', ['class' => 'btn btn-md pull-left btn-primary'] ) !!}
    </td>
    <td colspan="1">&nbsp;</td>
</tr>
</tfoot>