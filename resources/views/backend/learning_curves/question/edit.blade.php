@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Learning Curve Questions</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('backend.index') }}">Backend</a></li>
        <li><a href="{{ route('backend.learning_curves.index') }}">Learning Curves</a></li>
        <li><a href="{{ route('backend.learning_curves.edit', [$learning_curve->id]) }}" >{{ $learning_curve->name }}</a></li>
        <li>{{ $question->title }}</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-12">
        @include('backend.learning_curves.question._partials.learning_curve-question-form')

    </div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/forms.js') }}"></script>
@endsection
