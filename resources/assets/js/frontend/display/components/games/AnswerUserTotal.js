import React from 'react';
import Odometer from 'react-odometerjs';

export default function AnswerUserTotal(props) {
    const progressPercentage = (props.totalAnswers > 0) ? (props.totalAnswers / props.totalUsers) * 100 : 0;

    return (
        <div className="AnswerUserTotal">
            <div className="ProgressBar">
                <div className="choice"><span>{props.questionNumber}</span></div>
                <div className="bar">
                    <div style={{ width: `${progressPercentage}%` }} className="completed" />
                </div>
                <div className={progressPercentage == 100 ? "odometer-wrapper full-styling-fix" : "odometer-wrapper"}>
                    <Odometer value={props.totalAnswers} format={"d"} duration={300} />
                    <p className="static-text-wrapper">/ {props.totalUsers}</p>
                </div>
            </div>
        </div>
    );
}