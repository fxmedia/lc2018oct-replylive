<?php

use Faker\Generator as Faker;

$factory->define(App\Push::class, function (Faker $faker) {
    return [
        'name' => $faker->lastName,
        'title' => $faker->text('20'),
        'copy' => $faker->text('500')
    ];
});
