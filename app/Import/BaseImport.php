<?php

namespace App\Import;

use Maatwebsite\Excel\Facades\Excel;

abstract class BaseImport implements ImportInterface
{
    protected $path;
    protected $valid = false;
    protected $errors = [];
    protected $data;

    public function __construct($file){
        if(  in_array( \PHPExcel_IOFactory::identify( $file ), array( 'Excel2007', 'Excel5' ) ) ){
            $this->path = $file->getRealPath();


            $this->data = Excel::load($this->path)->get();

            $headerRow =  $this->data->first()->keys()->toArray();
            $allThere = true;

            foreach($this->getHeaders() as $header){
                if(!in_array($header, $headerRow)){
                    $allThere = false;

                    $this->errors[] = ['Missing '.$header.' header in file'];
                }
            }

            $this->valid = $allThere;

        } else {
            $this->errors[] = ['Wrong file type'];
        }
    }

    public function isValid(){
        return $this->valid;
    }

    public function getErrors(){
        return $this->errors;
    }

    public function getData(){
        if($this->isValid()) return $this->data;
        return false;
    }

    
}