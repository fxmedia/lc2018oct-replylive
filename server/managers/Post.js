const GameManager = require('./Game');

class PostManager extends GameManager {
    constructor(Admin) {
        super(Admin, "post");
        this.currentState = { highlight: null };
    }

    getHighlightStatus() {
        return { post_id: this.currentState.highlight };
    }

    handle(action, data) {
        switch(action) {
            case 'post_toggle_visibility':
                this.toggleVisibility(action, data);
                break;
            case 'post_remove':
                this.removePost(action, data);
                break;
            case 'post_highlight':
                this.highlightPost(action, data);
                break;
        }
    }

    highlightPost(action, data) {
        this.currentState.highlight = data.post_id;
        this.Admin.broadcast(action, data);
    }

    fetchAll(channel, client) {
        this.Admin.fetch('/post/all')
        .then(result => this.Admin.channels[channel].to(client.id).emit('post_set_all', result.data.posts))
        .catch(err => console.log("Posts could not be fetched..", err))
    }

    submit(post, userSocket) {
        this.Admin.save('/post/submit', post, userSocket.id)
        .catch(err => {
            this.Admin.sendError(userSocket, err);
            const userId = this.Admin.getConnection("user", userSocket.id).id;
            this.Admin.broadcastToUsers([userId], 'post_reset');
        })
        .then(result => {
            if(result.data.success) {
                this.Admin.broadcast('post_add', result.data.post)
            } else {
                this.Admin.sendError(userSocket, "The post could not be saved..");
                const userId = this.Admin.getConnection("user", userSocket.id).id;
                this.Admin.broadcastToUsers([userId], 'post_reset');
            }
        })
    }

    removePost(action, data) {
        this.Admin.broadcast(action, data);
    }

    toggleVisibility(action, data) {
        this.Admin.broadcast(action, data);
    }
}

module.exports = PostManager;
