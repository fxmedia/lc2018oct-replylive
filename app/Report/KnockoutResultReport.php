<?php

namespace App\Report;

use App\User;
use Maatwebsite\Excel\Facades\Excel;

class KnockoutResultReport extends BaseReport{

    private $knockouts;

    public function __construct($knockouts)
    {
        $this->knockouts = $knockouts;
    }

    public function getFilename()
    {
        return 'knockout_report_' . date('Y_m_d_h_m_s');
    }

    public function report(){
        Excel::create($this->getFilename(), function($excel){

            foreach($this->knockouts as $knockout){

                $name = substr(trim(preg_replace('/[\*|\:|\\|\/|\?|\[|\]]/', '', $knockout->name)), 0, 31);
                $excel->sheet($name, function($sheet) use ($knockout){
                    $index = 1;

                    $sheet->row($index, ['Knockout Name: ' . $knockout->name ]);
                    $sheet->row($index, function ($row) {
                        $row->setFontSize(18);
                        $row->setFontWeight('bold');
                    });
                    $index++;

                    foreach($knockout->questions as $question) {
                        $sheet->row($index, ['Question: ' . $question->title ]);

                        $sheet->row($index, function ($row) {
                            $row->setFontSize(14);
                            $row->setFontWeight('bold');
                        });
                        $index++;

                        $sheet->row($index, ['', 'User', 'Choice', 'Correct?']);

                        $index++;

                        foreach($question->answers as $a){
                            $answer = json_decode($a);
                            $user = User::find($answer->user_id);
                            $correct = ($answer->choice === $question->correct_answer) ? "correct" : "wrong";

                            $name = $user->firstname . " " . $user->lastname;
                            $sheet->row($index,[ '', $name, $answer->choice,  $correct ]);
                            $index++;
                        }
                        $index++;
                    }
                });
            }

        })->export('xls');
    }
}
