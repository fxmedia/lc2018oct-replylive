import React from 'react'
import config from "../../../config";

export default (props) => {
    return (
        <header className="header__wrap">
            <div className="l-half logo__wrap">
                <img src={`${config.base_url}/images/logo.png`} alt="Logo" />
            </div>
            <div className="l-clear"></div>
        </header>
    )
}
