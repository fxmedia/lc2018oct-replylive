(function(scope, $){
    console.log('posts.js');

    class _Post {
        constructor() {
            this.routes = api_routes;
            this.highlightBtns = $('.toggle-post-highlight');
            this.visibilityBtns = $('.toggle-post-visibility');
            this.toggleDisplayBtns = $('.toggle-display');

            this.highlightBtns.on('click', this.toggleHighlight.bind(this));
            this.visibilityBtns.on('click', this.toggleVisibility.bind(this));
            this.toggleDisplayBtns.on('change', this.toggleDisplay.bind(this));
        }

        getRoute(post_id, key) {
            return this.routes[post_id][key];
        }

        toggleDisplay(event) {
            var route = this.getRoute('other', 'toggle_display');

            $.ajax(route, {
                method: 'POST',
                data: { showDisplay: event.currentTarget.value }
            }).success(data => {
                console.log("data: ", data);
                if(!data || !data['success']) return this.error(data);
            }).error(this.error);
        }

        toggleVisibility(event) {
            event.preventDefault();
            var el = $(event.currentTarget),
                postId = el.data('post-id'),
                isVisible = el.find("i").hasClass('fa-eye');

            var route = this.getRoute(postId, 'toggle_visibility');

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                if(isVisible) {
                    $(event.currentTarget).find("i").removeClass('fa-eye').addClass('fa-eye-slash');
                    $("#post_" + postId).addClass("greyed-out");
                } else {
                    $(event.currentTarget).find("i").removeClass('fa-eye-slash').addClass('fa-eye');
                    $("#post_" + postId).removeClass("greyed-out");
                }
            }).error(this.error);
        }

        toggleHighlight(event){
            var el = $(event.currentTarget),
                postId = el.data('post-id'),
                isHighlight = el.hasClass('btn-success');

            !isHighlight
                ? this.setHighlight(el, postId)
                : this.stopHighlight();
        }

        setHighlight(el, postId){
            var route = this.getRoute(postId, 'highlight');

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                this.highlightBtns.removeClass('btn-success').addClass('btn-primary');

                el.addClass('btn-success').removeClass('btn-primary');

            }).error(this.error);
        }

        stopHighlight(){

            var route = this.getRoute('other','lowlight');

            $.ajax(route, {
                method: 'POST'
            }).success(data => {
                if(!data || !data['success']) return this.error(data);

                this.highlightBtns.removeClass('btn-success').addClass('btn-primary');
            }).error(this.error);
        }


        error(xhr, status, errorThrown) {
            console.log(xhr, status, errorThrown);

            if(xhr && xhr.hasOwnProperty('success') && typeof xhr.success !== 'function') {
                if(xhr.hasOwnProperty('error')) {
                    xhr.error = (xhr.error && xhr.error.message)? xhr.error.message : xhr.error || 'Whoops, something went wrong';
                    return Notifications.error(xhr.error);
                }
            }

            if(xhr && xhr.readyState === 0) {
                return Notifications.error('Internet error, make sure you are connected to the internet');
            }

            if(xhr && xhr.readyState === 4) {
                return Notifications.error(errorThrown);
            }

            return Notifications.error('Whoops, something went wrong.');
        }

    }

    scope.post = new _Post();
})(this, $);
