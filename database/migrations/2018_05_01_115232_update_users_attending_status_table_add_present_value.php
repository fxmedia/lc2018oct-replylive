<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersAttendingStatusTableAddPresentValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE users_attending_status CHANGE status status ENUM('attending','present','invited','waiting','pending','cancelled','declined')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE users_attending_status CHANGE status status ENUM('attending','invited','waiting','pending','cancelled','declined')");
    }
}
