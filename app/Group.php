<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Group
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int $limit
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Poll[] $polls
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Knockout[] $knockouts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quiz[] $quizzes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Survey[] $surveys
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OpenQuestion[] $open_questions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attending[] $attending
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Push[] $pushes
 */
class Group extends Model
{
    //
    protected $fillable = ['name', 'limit', 'website'];

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function getUserIds() {
        return $this->users->pluck('user_id');
    }

    public function attending(){
        return $this->hasMany('App\Attending');
    }

    public function polls(){
        return $this->belongsToMany('App\Poll');
    }

    public function pushes(){
        return $this->belongsToMany('App\Push');
    }

    public function quizzes(){
        return $this->belongsToMany('App\Quiz');
    }

    public function surveys(){
        return $this->belongsToMany('App\Survey');
    }

    public function knockouts(){
        return $this->belongsToMany('App\Knockout');
    }

    public function open_questions(){
        return $this->belongsToMany('App\OpenQuestion');
    }

    static function printGroups($groups){
        $string = '';

        foreach($groups as $group){
            $string.= $group->name . ', ';
        }

        if( strlen($string) > 1){
            return substr($string, 0, -2);
        }

        return 'All groups';
    }
}
