const mapObjectToArray = (obj) => {
  return Object.keys(obj).filter(key => obj[key] !== "").map(key => obj[key])
};

module.exports = mapObjectToArray;
