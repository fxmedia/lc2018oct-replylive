import React from 'react';
import MultipleSelectAnswer from './MultipleSelectAnswer';
import SubmitButton from './SubmitButton';
import { inject } from 'mobx-react';

@inject("t")
export default class ABCDMultiple extends React.Component {

    constructor(props) {
        super(props);
        this.state = { selectedChoices: {} };
    }

    handleAnswer() {
        this.props.handleAnswer(JSON.stringify(this.state.selectedChoices));
        this.setState({ selectedChoices: {} })
    }

    handleSelect(choice) {
        let state = {...this.state};
        if(this.state.selectedChoices[choice]) {
            delete state.selectedChoices[choice];
        } else {
            state.selectedChoices[choice] = true;
        }
        this.setState(state);
    }

    renderChoice(data) {
        return (
            <MultipleSelectAnswer
                isSelected={this.state.selectedChoices[data.choice]}
                handleSelect={this.handleSelect.bind(this, data.choice)}
                choice={data.choice}
                text={data.value}
                key={data.choice}
            />
        )
    }

    render() {
        return (
            <div className="ABCDMultiple">
                {this.props.data.map(choice => this.renderChoice(choice))}
                <SubmitButton
                    text={(this.props.isLast) ? this.props.t.survey.submit : this.props.t.survey.next}
                    handleSubmit={this.handleAnswer.bind(this)}
                    disabled={!this.state.selectedChoices || Object.keys(this.state.selectedChoices).length === 0}
                />
            </div>
        )
    }
}
