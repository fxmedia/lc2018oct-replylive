<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\OpenQuestion::class, function (Faker $faker) {
    $user = User::inRandomOrder()->first();

    return [
        //
        'question' => $faker->text('80') . "?",
        'tags' => $faker->word,
        'multiple_answers' => $faker->boolean(50)

    ];
});
