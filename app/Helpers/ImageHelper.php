<?php namespace App\Helpers;

use Illuminate\Http\File;
use Storage;
use Image;
use Illuminate\Http\UploadedFile;

class ImageHelper {

    private $data;
    private $filename;
    private $folder = '';
    private $path = "images/";

    /**
     * ImageHelper constructor.
     * @param UploadedFile|string $data
     * @param string $pathGroup
     * @param boolean $base64
     */
    public function __construct($data, $pathGroup = 'other', $dataUrl = false) {

        if($dataUrl) {
            $this->data = $data['data-url'];
            $this->filename = $data['name'];
            $this->extension = $data['extension'];
        }
        else {
            $this->data = $data;
            $this->extension = strtolower($data->getClientOriginalExtension());
            $this->filename = $data->getClientOriginalName();
        }
        $this->image = null;
        $this->folder = config('storage.'.$pathGroup);
        $this->path = config('storage.'.$pathGroup) .'/';
    }

    public function storeOriginal(){
        if(!Storage::disk('uploads')->has($this->folder) ){
            Storage::disk('uploads')->makeDirectory($this->folder);
        }

        if(!Storage::disk('uploads')->has($this->folder.'/original')){
            Storage::disk('uploads')->makeDirectory($this->folder.'/original');
        }

        if($this->extension !== "gif") {
            Storage::disk('uploads')->put($this->path.'original/' . $this->filename, (string) $this->image->encode());
        } else {
            Storage::disk('uploads')->putFileAs($this->path.'original/', new File($this->data->getPathName()), $this->filename);
        }

        return $this;
    }

    /**
     * @param bool $randomizeName
     * @return $this
     */
    public function convert($randomizeName = true) {

        $this->image = Image::make($this->data)->orientate();
        if($randomizeName){
            $this->setFilename(time() . "-" . uniqid() . "." . $this->extension);
        } else {
            $prefix = substr( md5( rand() . time() ), 0, 3 ) . '_';
            $this->setFilename($prefix . $this->filename );
        }
        return $this;
    }

     /**
     * @param int $size
     * @return $this
     */
    public function setEventSiteUserImage($size = 640) {

        $this->image->resize(580, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $tmpFileName = $this->filename;
        $this->filename = 'original_' . $tmpFileName;
        $this->moveToStorage();
        $this->filename = $tmpFileName;
        $this->image->fit($size, $size);

        return $this;
    }

    /**
     * @param int $size
     * @return $this
     */
    public function resizeWallUpload($size = 640) {
        if($this->extension !== "gif") {
            $this->image->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function moveToStorage() {
        if(!Storage::disk('uploads')->has($this->folder)) {
            Storage::disk('uploads')->makeDirectory($this->folder);
        }
        if($this->extension !== "gif") {
            Storage::disk('uploads')->put($this->path . $this->filename, (string) $this->image->encode());
        } else {
            Storage::disk('uploads')->putFileAs($this->path, new File($this->data->getPathName()), $this->filename);
        }
        return $this;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function setFilename($name) {
        $this->filename = $name;
    }

    public function getFilename() {
        return $this->filename;
    }
}