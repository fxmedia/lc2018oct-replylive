<?php

namespace App\Http\Controllers\Backend;

use Exception;
use App\LearningCurve;
use App\LearningCurveQuestion;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LearningCurveQuestionsController extends Controller
{
    public function create($learning_curve_id){
        $learning_curve = LearningCurve::find($learning_curve_id);
        if($learning_curve == null){
            // cannot create question to non existing learning_curve
            return redirect()->back()->withErrors('Cannot create Question to non existing learning_curve');
        }

        return view('backend.learning_curves.question.create', compact('learning_curve'));
    }

    public function store(Request $request, $learning_curve_id){
        if(!LearningCurve::whereId($learning_curve_id)->exists()){
            // cannot create question to non existing learning_curve
            return redirect()->back()->withErrors(['message' => 'Error storing question to non existing learning_curve']);
        }

        $validators = config('validation.learning_curves.questions.store');
        $data = $request->validate($validators);

        $choices = [];
        foreach($data['choices'] as $c){
            if($c['value'] !== null) {
                $choices[] = [
                    'choice' => $c['choice'],
                    'value' => $c['value']
                ];
            }
        }
        $data['choices'] = $choices;

        $question = new LearningCurveQuestion;
        $question->fill($data);
        $question->learning_curve_id = $learning_curve_id;

        if(!$question->save()){
            return redirect()->back()->witherrors(['message'=>'Error saving new question']);
        }

        return redirect()->route('backend.learning_curves.edit',[$learning_curve_id])->with(['message' => "Successfully added a new question"]);
    }

    public function edit($id){
        $question = LearningCurveQuestion::find($id);

        if($question == null){
            return redirect()->back()->withErrors('Unable to find LearningCurve Question to edit');
        }

        $learning_curve = $question->learning_curve;

        return view('backend.learning_curves.question.edit', compact('question', 'learning_curve'));
    }

    public function update(Request $request, $id){
        $question = LearningCurveQuestion::find($id);

        if($question == null){
            return redirect()->back()->withErrors('No LearningCurve Question found to edit');
        }

        $validators = config('validation.learning_curves.questions.store');

        $data = $request->validate($validators);

        $choices = [];
        foreach($data['choices'] as $c){
            if($c['value'] !== null) {
                $choices[] = [
                    'choice' => $c['choice'],
                    'value' => $c['value']
                ];
            }
        }
        $data['choices'] = $choices;

        $question->fill($data);

        if($question->update($data)){
            return redirect()->route('backend.learning_curves.edit',[$question->learning_curve_id])->with(['message' => 'Successfully updated question']);
        }

        return redirect()->back()->withErrors(['message' => 'Unable to save changes to learning_curve question']);
    }

    public function remove($id){
        $question = LearningCurveQuestion::find($id);

        if($question === null){
            return redirect()->back()->withErrors(['message' => 'Unable to remove non existing question']);
        }


        try{
            $removed = $question->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Question has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing question.']);
    }

    public function reset($id){
        $question = LearningCurveQuestion::find($id);

        if($question === null){
            return redirect()->back()->withErrors(['message'=>'Unable to reset answers to unknown question']);
        }

        $totalRemoved = 0;
        $totalFailed = 0;

        foreach ($question->answers as $answer) {
            try {
                $s = $answer->delete();
                if ($s == true) $totalRemoved++;
            } catch (Exception $e) {
                $totalFailed++;
            }
        }
        return redirect()->back()->with('message', 'Removed '.$totalRemoved. ' answers and skipped '. $totalFailed .' from question '. $question->id);
    }
}
