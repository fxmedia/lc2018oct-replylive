import React from 'react';
import { inject } from 'mobx-react';

@inject("stores")
export default class Image extends React.Component {

    render() {
        if(!this.props.filename) return null;

        return <img src={ this.props.filename } />
    }

}
