let loaderScript = document.getElementById('replyLiveLoader');

let rl_domain = '';
let websocket = 'http://192.168.0.51:5000';
let token = 'appeltaart';
let otherOptions = {};

if(!loaderScript){
    // oh noes!
} else {
    let jsonString = loaderScript.getAttribute('data-json');
    let json;
    try {
        json = JSON.parse(jsonString);
    } catch(error){
        console.error('Reply.Live Config | Unable to parse Loader data');
        json = false;
    }

    if(json){
        rl_domain = json.rl_domain;
        websocket = json.websocket;
        token = json.token;
        otherOptions = json.options || {};
    }
}


module.exports = {
    allowedFileTypes: ["image/png", "image/jpeg", "image/gif", "video/quicktime", "video/mp4", "video/webm", "video/ogg", "video/mpeg"],
    allowedImageTypes: ["image/png", "image/jpeg", "image/gif"],
    maxMessageSize: 280,
    rl_domain: rl_domain,
    websocket: websocket,
    token: token,
    maps_key: "AIzaSyB0TEDHmNWHTKl6q74YNqkMMtVNvD2l2pA",
    api_token: token,
    api_base_url: rl_domain + "/api",
    base_url: rl_domain,
    image_endpoint: "post/image",
    video_endpoint: "post/video",
    otherOptions: otherOptions
};
