<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Pledge
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $amount
 * @property int $anonymous
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereAnonymous($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereUserId($value)
 * @mixin \Eloquent
 */
class Pledge extends Model
{
    protected $fillable = ["user_id", "name", "amount"];

    protected $with = ['user'];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
