import { observable, action } from 'mobx';

export default class TimerStore {

    @observable audio
    @observable remainingSeconds = 0;

    constructor(socket, mainStore) {
        this.socket = socket;
        this.mainStore = mainStore;
        this.startTime = null;
        this.totalSeconds = null;
        this.audio = null;
        this.update = null;
        this.listen();
    }

    @action setRemainingSeconds(seconds) {
        this.remainingSeconds = seconds;
    }

    @action updateTimer() {
        const delta = Date.now() - this.startTime;
        const elapsed = (Math.floor(delta / 1000));
        const remaining = this.totalSeconds - elapsed;

        if(remaining < 0) {
            window.clearInterval(this.update);
            this.setRemainingSeconds(0);
            this.stopAudio();
        } else {
            this.setRemainingSeconds(remaining);
        }
    }

    @action start(data) {
        if(data.audio_url) {
            this.audio = new Audio(data.audio_url);
            this.audio.play();
        }
        this.startTime = Date.now();
        this.totalSeconds = parseInt(data.total_seconds);
        this.update = window.setInterval(this.updateTimer.bind(this), 250);
        this.mainStore.setActivePage("timer");
    }

    @action stopAudio() {
        if(this.audio) {
            this.audio.pause();
        }
    }

    @action stopTimer() {
        window.clearInterval(this.update);
    }

    @action stop() {
        this.stopAudio();
        this.stopTimer();
        this.mainStore.setActivePage("home");
    }

    @action listen() {
        this.socket.connection.on('timer_start', (data) => this.start(data));
        this.socket.connection.on('timer_stop', (data) => this.stop(data));
    }
}
