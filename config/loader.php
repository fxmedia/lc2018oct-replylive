<?php
return [
    'loader_file_path' => 'js/loader.js',
    'client_file_path' => 'js/user.js',
    'css_file_path' => 'css/user/user.css',
    'polyfill_file_path' => 'js/polyfill.js'
];
