<?php

// use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/loader/data', 'Frontend\LoaderController@data')->name('frontend.loader.data');

Route::namespace('Api')->group(function () {

    // CLIENT CALLS
    Route::group(['middleware' => 'verifyUserToken'], function(){
        Route::post('post/video', ['as' => 'api.wall.video', 'uses' => 'PostController@storeVideo']);
        Route::post('post/image', ['as' => 'api.wall.image', 'uses' => 'PostController@storeImage']);
        Route::post('user/picture', ['as' => 'api.user.picture', 'uses' => 'UserController@uploadPicture']);
    });

    // SCANNER CALLS
    // /**
    //  * TODO do i want a middleware on this?
    //  */
    Route::post('scan','ScannerController@scan')->name('api.scanner.scan');

    // HELPDESK
    // @todo ADD MIDDLEWARE
    Route::post('helpdesk/register-user','UserController@eventSiteRegisterUser')->name('api.helpdesk.register-user');
    //Route::post('helpdesk/register-user','UserController@helpdeskRegisterUser')->name('api.helpdesk.register-user');


    Route::group(['middleware' => 'verifyApiToken'], function(){

        // EVENT SITE CALLS
        Route::post('event-site/token', 'UserController@eventSiteTokenCheck')->name('api.event-site.token-check');
        Route::post('event-site/register-user', 'UserController@eventSiteRegisterUser')->name('api.event-site.register-user');
        Route::post('event-site/update-user', 'UserController@eventSiteUpdateUser')->name('api.event-site.update-user');
        Route::post('event-site/update-user-image', 'UserController@eventSiteUpdateUserImage')->name('api.event-site.update-user-image');
        Route::post('event-site/cancel-user', 'UserController@eventSiteCancelUser')->name('api.event-site.cancel-user');
        Route::get('event-site/get-guestlist', 'UserController@eventSiteGetGuestlist')->name('api.event-site.get-guestlist');
        Route::get('event-site/online', 'AppController@isOnline')->name('api.event-site.online');

        // NODE JS CALLS
        Route::get('app/status', 'AppController@status')->name('api.app.status');

        Route::post('open-question/answer', ['as' => 'api.open_question.answer', 'uses' => 'OpenQuestionController@storeAnswer']);
        Route::post('knockout/answer', ['as' => 'api.knockout.answer', 'uses' => 'KnockoutController@storeAnswer']);
        Route::post('poll/answer', ['as' => 'api.poll.answer', 'uses' => 'PollController@storeAnswer']);
        Route::post('quiz/answer', ['as' => 'api.quiz.answer', 'uses' => 'QuizController@storeAnswer']);
        Route::post('survey/answer', ['as' => 'api.survey.answer', 'uses' => 'SurveyController@storeAnswer']);
        Route::post('curve/answer', ['as' => 'api.curve.answer', 'uses' => 'LearningCurveController@storeAnswer']);

        Route::post('post/submit', ['as' => 'api.posts.post', 'uses' => 'PostController@storePost']);
        Route::post('pledge/submit', ['as' => 'api.pledges.submit', 'uses' => 'PledgeController@storePledge']);

        Route::get('post/all', ['as' => 'api.wall.posts', 'uses' => 'PostController@fetchPosts']);
        Route::get('pledge/all', ['as' => 'api.pledges.all', 'uses' => 'PledgeController@fetchPledges']);
        Route::get('user/{token}', ['as' => 'api.user', 'uses' => 'UserController@fetch']);
        Route::get('display/{token}', ['as' => 'api.display', 'uses' => 'DisplayController@fetch']);
        Route::get('users', ['as' => 'api.user', 'uses' => 'UserController@fetchAll']);

        // BACKEND CALLS

        // KNOCKOUTS
        Route::group(['prefix' => 'knockout'], function(){
            Route::post('get-data/{id}', 'KnockoutController@getData')->name('api.knockout.get-data');
            Route::post('start/{id}', 'KnockoutController@start')->name('api.knockout.start');
            Route::post('check-answer', 'KnockoutController@checkAnswer')->name('api.knockout.check-answer');
            Route::post('show-question/{id}','KnockoutController@showQuestion')->name('api.knockout.show-question');
            Route::post('stop', 'KnockoutController@stop')->name('api.knockout.stop');
            Route::post('get-status', 'KnockoutController@getStatus')->name('api.knockout.status');
            Route::post('set-deciding-question', 'KnockoutController@setDecidingQuestion')->name('api.knockout.set-deciding-question');
        });

        // LEARNING CURVES
        Route::group(['prefix' => 'learning-curve'], function(){
            Route::post('get-data/{id}', 'LearningCurveController@getData')->name('api.learning_curve.get-data');
            Route::post('start/{id}/{phase}', 'LearningCurveController@start')->name('api.learning_curve.start');
            Route::post('check-answer', 'LearningCurveController@checkAnswer')->name('api.learning_curve.check-answer');
            Route::post('show-question/{id}','LearningCurveController@showQuestion')->name('api.learning_curve.show-question');
            Route::post('toggle-results','LearningCurveController@toggleResults')->name('api.learning_curve.toggle-results');
            Route::post('stop', 'LearningCurveController@stop')->name('api.learning_curve.stop');
        });

        // OPEN QUESTIONS
        Route::group(['prefix' => 'open-question'], function(){
            Route::post('status', 'OpenQuestionController@getStatus')->name('api.open_question.status');
            Route::post('start/{id}', 'OpenQuestionController@start')->name('api.open_question.start');
            Route::post('stop', 'OpenQuestionController@stop')->name('api.open_question.stop');
            Route::post('highlight/{id?}', 'OpenQuestionController@highLightAnswer')->name('api.open_question.highlight');
        });

        // PLEDGE
        Route::post('pledge/toggle-display', 'PledgeController@toggleDisplay')->name('api.pledge.toggle-display');

        // POLLS
        Route::post('poll/start/{id}', 'PollController@start')->name('api.poll.start');
        Route::post('poll/stop', 'PollController@stop')->name('api.poll.stop');
        Route::post('poll/status', 'PollController@getStatus')->name('api.poll.status');

        // POSTS
        Route::post('post/highlight/{id?}', 'PostController@highLightPost')->name('api.post.highlight');
        Route::post('post/toggle-visibility/{id}', 'PostController@toggleVisibility')->name('api.post.toggle-visibility');
        Route::post('post/toggle-display', 'PostController@toggleDisplay')->name('api.post.toggle-display');

        // PUSHES
        Route::post('push/start/{id}', 'PushController@start')->name('api.push.start');
        Route::post('push/stop', 'PushController@stop')->name('api.push.stop');

        // QUIZZES
        Route::group(['prefix' => 'quiz'], function(){
            Route::post('get-data/{id}', 'QuizController@getData')->name('api.quiz.get-data');
            Route::post('start/{id}', 'QuizController@start')->name('api.quiz.start');
            Route::post('check-answer', 'QuizController@checkAnswer')->name('api.quiz.check-answer');
            Route::post('show-question/{id}','QuizController@showQuestion')->name('api.quiz.show-question');
            Route::post('toggle-results','QuizController@toggleResults')->name('api.quiz.toggle-results');
            Route::post('stop', 'QuizController@stop')->name('api.quiz.stop');
        });

        // SURVEYS
        Route::group(['prefix' => 'survey'], function(){
            Route::post('start/{id}', 'SurveyController@start')->name('api.survey.start');
            Route::post('stop/{id}', 'SurveyController@stop')->name('api.survey.stop');
        });
    });
});
