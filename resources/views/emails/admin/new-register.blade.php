<!DOCTYPE html>
<html>
<head>

</head>
<body>
    Hi,
    <br/><br/>
    {{ $user->name }} <a href="mailto:{{ $user->email }}">({{ $user->email }})</a> has signed up to attend the event, but we don't have this user in our database. Please let us know if this e-mail address is approved or not. When you approve, this user will be added to the attending or waiting list.
    <br/><br/>
    <a href="{{ url(route('backend.pending-user.accept', [$token, $user->id])) }}">Approve this user</a>
    <br/>
    <br/>
    <a href="{{ url(route('backend.pending-user.declined', [$token, $user->id])) }}">Decline this user</a>
    <br/><br/>Best regards, <br/>FX team.<br/><br/>
</body>
</html>