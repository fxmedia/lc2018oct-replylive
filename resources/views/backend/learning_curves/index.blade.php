@extends('backend.layout.app')

@section('header')
    <h1>Reply.live Learning Curves</h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('backend.index') }}">Backend</a></li>
        <li>Learning Curves</li>
    </ol>
@endsection

@section('content')
    <div class="col-xs-3">
        @include('backend.learning_curves._partials.aside')
    </div>

    <div class="col-xs-9">
        @include('backend._partials.errors')
        @foreach($learning_curves as $learning_curve)
            <ul class="list-group panel-default">
                <li class="list-group-item panel-heading">
                    {{ $learning_curve->name }}
                </li>
                <li class="list-group-item list-grou-heading" data-learning_curve-id="{{ $learning_curve->id }}">
                    @if($learning_curve->isActive && $phase == "pre")
                        <a class="toggle-learning_curve-start-stop btn btn-danger btn-xs" data-phase="pre" data-question-index="{{ $learning_curve->activeData['question_index'] }}" @if( $learning_curve->activeData['locked'] ) data-check-answer="{{ $learning_curve->activeData['locked'] }}" @endif><i class="fa fa-stop"></i> <span>Stop Pre Learning Curve</span></a>
                    @else
                        <a class="toggle-learning_curve-start-stop btn btn-primary btn-xs" data-phase="pre"><i class="fa fa-play"></i> <span>Start Pre Learning Curve</span></a>
                    @endif
                    @if($learning_curve->isActive && $phase == "post")
                        <a class="toggle-learning_curve-start-stop btn btn-danger btn-xs" data-phase="post" data-question-index="{{ $learning_curve->activeData['question_index'] }}" @if( $learning_curve->activeData['locked'] ) data-check-answer="{{ $learning_curve->activeData['locked'] }}" @endif><i class="fa fa-stop"></i> <span>Stop Post Learning Curve</span></a>
                    @else
                        <a class="toggle-learning_curve-start-stop btn btn-primary btn-xs" data-phase="post"><i class="fa fa-play"></i> <span>Start Post Learning Curve</span></a>
                    @endif

                    <span>Groups: {{ App\Group::printGroups( $learning_curve->groups ) }}</span>

                    {!! Form::open(['method' => 'post', 'route' => ['backend.learning_curves.remove', $learning_curve->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Please confirm to remove the selected learning_curve?")']) !!}
                    <button class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> Remove</button>
                    {!! Form::close() !!}

                    {!! Form::open(['method' => 'post', 'route' => ['backend.learning_curves.reset.single', $learning_curve->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm("Are you sure you want to reset the score?")']) !!}
                    <button class="btn btn-xs btn-info"><span class="glyphicon glyphicon-retweet"></span> Reset</button>
                    {!! Form::close() !!}

                    <a href="{{ route('backend.learning_curves.edit', [$learning_curve->id]) }}" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                </li>

                @foreach($learning_curve->questions as $key => $question)
                    <li class="list-group-item">
                        <span class="badge pull-left">{{ $question->order }}</span>
                        &nbsp;&nbsp;&nbsp;{{ str_limit($question->title, 60) }}
                    </li>
                @endforeach
            </ul>
        @endforeach
        @if(count($learning_curves) === 0)
            <h3>No Learning Curves in the database.</h3>
            <p>Click on "Add Learning Curve" in the menu on the left to create a new learning curve</p>
        @endif
    </div>

@endsection

@section('scripts')
    <script>
        var api_routes = {
            other: {
                stop: '{{ route('api.learning_curve.stop') }}',
            }
        };
        @foreach($learning_curves as $learning_curve)
            api_routes['{{ $learning_curve->id }}'] = {
                start_pre: '{{ route('api.learning_curve.start', [$learning_curve->id, "pre"]) }}',
                start_post: '{{ route('api.learning_curve.start', [$learning_curve->id, "post"]) }}',
            };
        @endforeach
    </script>
    <script type="text/javascript" charset="UTF-8" src="{{ asset('js/backend/learning_curves.js') }}"></script>
@endsection
