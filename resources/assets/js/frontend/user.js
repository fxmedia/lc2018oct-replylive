import Debug from './utils/debug'
Debug.setOptions({
    enabled: true,
    enabledLevels: {
        mediaPreLoader: 'color: purple;',
        Store: 'font-weight:bold;color:brown;'
    }
});
window._Debug = Debug;

import React from 'react';
import ReactDOM from 'react-dom';
import { inject, observer, Provider } from 'mobx-react';

import Header from './user/components/layout/Header';
import PageContainer from './user/pages/PageContainer';
import translations from './translations/en';
import ErrorMessage from './user/components/Error';

import createStores from './stores';
const userStores = createStores("users");

import takeoverListeners from './utils/takeoverListeners';

@inject('stores', 't') @observer
class App extends React.Component {

    componentDidMount() {
        takeoverListeners(this.props.stores.main);
    }

    handleClose() {
        this.props.stores.main.closeTakeover();
        this.props.stores.main.setActivePage("home");
    }

    getStyling(bg) {
        if(!bg) return "App";

        return `App custom-bg ${bg}`;
    }

    renderPage() {
        const { activePage } = this.props.stores.main;
        if(!activePage) return null;

        const ActivePageComponent = (PageContainer[activePage])
            ? PageContainer[activePage].module
            : PageContainer["home"].module;

        return <ActivePageComponent />
    }

    render() {
        const {
            user,
            showPoints,
            closablePage,
            activePage,
            background,
            error
        } = this.props.stores.main;
        const title = (PageContainer[activePage]) ? PageContainer[activePage].title : "";

        return (
            <div className={this.getStyling(background)}>
                <Header
                    handleClose={this.handleClose.bind(this)}
                    points={(showPoints && user.extra) ? user.extra.points : false}
                    closable={closablePage}
                    title={title}
                />
                <div className={`page-wrapper ${activePage}`}>
                    {this.renderPage()}
                </div>
                <ErrorMessage show={!!error} message={error}/>
            </div>
        )
    }
}

let app = document.getElementById('replyLive');
if(!app) {
    app = document.createElement('div');
    app.id = 'replyLive';
    document.body.appendChild(app);
}

ReactDOM.render(
    <Provider stores={userStores} t={translations}>
        <App />
    </Provider>,
    app
);
