<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\LearningCurveQuestion
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\LearningCurveAnswer[] $answers
 * @property-read \App\LearningCurve $learning_curve
 * @mixin \Eloquent
 * @property int $id
 * @property int $learning_curve_id
 * @property string $title
 * @property string $correct_answer
 * @property int $points
 * @property array $choices
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveQuestion whereChoices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveQuestion whereCorrectAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveQuestion wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveQuestion whereLearningCurveId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveQuestion whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveQuestion whereUpdatedAt($value)
 * @property int $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LearningCurveQuestion whereOrder($value)
 */
class LearningCurveQuestion extends Model
{
    protected $fillable = ["learning_curve_id", "title", "correct_answer", "choices","order"];
    protected $table = "learning_curve_questions";
    protected $casts = [
        'choices' => 'array'
    ];

    public function answers() {
        return $this->hasMany('App\LearningCurveAnswer');
    }

    public function learning_curve() {
        return $this->belongsTo('App\LearningCurve');
    }

}
