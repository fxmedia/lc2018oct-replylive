<?php

namespace App\Http\Controllers\Frontend;

use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use App\User;

class LoaderController extends Controller
{
    //
    public function index(){
        $dataPath = route('frontend.loader.data');
        $srcPath = asset( config('loader.loader_file_path') );

        return view('frontend.loader.index', compact('dataPath', 'srcPath'));
    }

    public function data(Request $request){
        $settings = Setting::getSettingsAsObject();
        if(!$request->ajax()){
            // not allowed!
            return redirect($settings->redirect_url);
        }

        $validators = [
            'userAgent' => 'string|nullable',
            'location' => 'string|required',
            'userToken' => 'string|nullable'
        ];

        $validatorData = $request->validate($validators);

        if(!$settings->online){
            return response()->json(['success' => false, 'errors' => ['Event is offline']]);
        }

        if( !empty($settings->event_name) && ($validatorData['location'] !== $settings->event_domain && strpos($settings->reply_live_domain, $validatorData['location']) === false )){
            // request is NOT allowed here
            return response()->json(['success' => false, 'errors' => ['Request Host not authorized']]);
        }

//        if( ($user = User::whereToken($validatorData['userToken'])) === null ){
        if( User::whereToken($validatorData['userToken'])->count() === 0 ){
            return response()->json(['success' => false, 'errors' => ['Request Token not authorized']]);
        }

        $socketUrl = ($settings->nodejs_port == '443')
                ? 'https://'.$settings->nodejs_domain
                : 'http://'.$settings->nodejs_domain.':'.$settings->nodejs_port;

        if($settings->nodejs_port == '80') $socketUrl = 'http://'.$settings->nodejs_domain;


        $result = [
            'css' => [
                asset( config('loader.css_file_path') ) . '?v=' . $settings->css_cache_version
            ],
            'js' => [
                asset( config('loader.polyfill_file_path') ).'?v=' . $settings->js_cache_version,
                asset( config('loader.client_file_path') ) . '?v=' . $settings->js_cache_version
            ],
            'token' => isset($validatorData['userToken']) ? $validatorData['userToken'] : '',
            'rl_domain' => $settings->reply_live_domain,
            'websocket' => $socketUrl,
            'options' => [
                'upload_max_size' => $settings->upload_max_size
            ]
        ];
        //$errors = [];
        return response()->json(['success' => true,'result' => $result ]);
    }
}
