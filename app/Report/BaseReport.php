<?php

namespace App\Report;


abstract class BaseReport implements ReportInterface
{
    protected $index = 0;

    public function incrementRow()
    {
        return ++$this->index;
    }

    protected function remove_emoji($string) {

        // Match Emoticons
        $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clear_string = preg_replace($regex_emoticons, '', $string);

        // Match Miscellaneous Symbols and Pictographs
        $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clear_string = preg_replace($regex_symbols, '', $clear_string);

        // Match Transport And Map Symbols
        $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clear_string = preg_replace($regex_transport, '', $clear_string);

        // Match Miscellaneous Symbols
        $regex_misc = '/[\x{2600}-\x{26FF}]/u';
        $clear_string = preg_replace($regex_misc, '', $clear_string);

        // Match Dingbats
        $regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
        $clear_string = preg_replace($regex_dingbats, '', $clear_string);

        return $clear_string;
    }

    function report()
    {
//        \Excel::create($this->getFilename(), function($excel){
//            $excel->sheet('Sheet1', function($sheet){
//
//                $sheet->row(1, $this->getHeaderRow());
//
//                $row = 3;
//
//                $this->getStyling($sheet);
//
//                do {
//                    $sheet->row($row++, $this->getRow());
//                    $this->incrementRow();
//                } while ($this->hasNextRow() === true);
//            });
//        })->export('xls');
    }

    public function getStyling($sheet)
    {
        return null;
    }
}