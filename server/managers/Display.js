class DisplayManager {
    constructor(socket, Admin) {
        this.socket = socket;
        this.Admin = Admin;
        this.currentState = { page: "idle" };
        this.socket.on('connection', display => this.waitForAuthentication(display));
    }

    authenticate(display, token) {
        this.Admin.fetch(`/display/${token}`)
        .catch(err => {
            console.log("Backend is not working correctly: ", err);
            this.socket.to(display.id).emit(
                "display_error",
                { message: "The backend is throwing an error: ", err}
            )
        })
        .then(result => {
            if(result.data.success) {
                this.init(display, result)
            } else {
                console.log(`Display with token (${token}) was not found.. `);
                this.socket.to(display.id).emit(
                    "display_error",
                    { message: `The Display with token ${token} is not found in the database, have you added it to the displays table with the right token?`}
                )
            }
        })
    }

    handle(action, data) {
        switch(action) {
            case 'display_set_page':
                this.currentState.page = data.name;
                this.Admin.broadcastToChannel("display", action, data);
                break;
        }
    }

    setPage(name) {
        this.currentState.page = name;
    }

    status() {
        return {page: this.currentState.page};
    }

    init(display, result) {
        this.socket.to(display.id).emit('display_set', { data: result.data, page: this.currentState.page } );
        this.setListeners(display);
        if(this.Admin.activeAction) {
            this.reboot(display);
        }
    }

    reboot(display) {
        const manager = this.Admin.getManager(this.Admin.activeAction.action);
        const { result, currentState, privateState } = this.Admin.managers[manager];
        let rebootData = Object.assign({}, this.Admin.activeAction.data.display);
        // if a current question or other relevant info is present, use it to kickstart correctly
        // @TODO: Create better reboot implementation for various states
        if(currentState) {
            rebootData.reboot = Object.assign({}, currentState);
        }
        if(privateState) {
            rebootData.private = Object.assign({}, privateState);
        }
        // if results are present on the nodejs server, use those because they are most recent
        if(result) {
            console.log("reboot: ", result);
            rebootData.result = (result.constructor === Object) ? Object.assign({}, result) : result.slice();
            this.socket.to(display.id).emit(this.Admin.activeAction.action, rebootData);
        } else {
            // else use the data which was sent by the backend
            this.socket.to(display.id).emit(this.Admin.activeAction.action, rebootData);
        }
    }

    setListeners(display) {
        display.on('post_fetch_all', () => this.Admin.managers.post.fetchAll("display", display))
        display.on('pledge_fetch_all', () => this.Admin.managers.pledge.fetchAll("display", display))
    }

    waitForAuthentication(display) {
        display.on('token_set', token => this.authenticate(display, token))
    }
}

module.exports = DisplayManager
