<?php

namespace App\Http\Controllers\Backend;

use App\Report\SmsLogReport;
use App\Senders\SmsSender;
use App\Sms;
use App\SmsLog;
use App\Export\SmsExport;
use App\Group;
use App\Import\SmsImport;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Session;
use MessageBird\Client as MessageBirdClient;
use stdClass;

class SmsController extends Controller
{
    //
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $sms = Sms::all();
        $sent = SmsLog::whereStatus('done')->whereNull('error')->count();
        $processing = SmsLog::where('status' ,'!=' ,'done')->count();
        $failed = SmsLog::whereNotNull('error')->count();

        try {
            $messageBird = new MessageBirdClient(config('messagebird.token'));
            $balance = $messageBird->balance->read();
        } catch (Exception $e) {
            $balance = new stdClass();
            $balance->amount = 'Unable to retrieve';
        }

        return view('backend.sms.index', compact('sms', 'balance', 'sent', 'processing', 'failed'));
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function setup($id)
    {
        $sms = Sms::find($id);

        if ($sms !== null) {
            $users = User::with('groups:id')->get();
            //$users = User::all();
            $groups = Group::all();

            return view('backend.sms.setup', compact('sms', 'users', 'groups'));
        }
        return redirect()->back()->withErrors(['message' => 'unable to setup non existing sms']);
    }

    public function preview(Request $request, $id){
        $sms = Sms::find($id);

        if($sms === null){
            return redirect()->back()->withErrors(['message' => 'Oops, something went wrong with the selected sms']);
        }


        $idListString = $request->input('id_list');
        if($idListString === null){
            return redirect()->back()->withErrors(['message' => 'No users are selected to send an sms to.']);
        }

        $users = User::find(explode(',', $idListString));
        if($users === null){
            return redirect()->back()->withErrors(['message' => 'Error finding selected users in the database.']);
        }

        $sendUsers = [];
        $skipUsers = [];
        $sendUserIds = [];
        foreach($users as $user){
            if($user->phonenumber === null){
                $skipUsers[] = $user;
            } else {
                $sendUsers[] = $user;
                $sendUserIds[] = $user->id;
            }
        }

        if(count($sendUsers) === 0){
            return redirect()->back()->withErrors(['message' => 'No users were selected with an email address']);
        }

        $request->session()->put('sms.users', $sendUserIds);

        return view('backend.sms.preview', compact('sms','sendUsers', 'skipUsers'));
    }

    public function log($id)
    {
        $sms = Sms::find($id);

        if($sms !== null){
            $data = [];
            $lg = SmsLog::with('user')->where('sms_id', $sms->id)->get();
            foreach ( $lg as $log ) {
                if ( ! isset( $data[ $log->batch ] ) ) {
                    $data[ $log->batch ] = [
                        'created_at' => $log->created_at,
                        'users'      => 0,
                        'processing' => 0,
                        'failures'   => 0,
                        'rows'       => []
                    ];
                }
                if ( $log->error ) {
                    $data[ $log->batch ]['failures'] ++;
                }
                if ( $log->status !== 'done') {
                    $data[ $log->batch ]['processing'] ++;
                }
                $data[ $log->batch ]['users'] ++;
                $data[ $log->batch ]['rows'][] = $log;
            }

            $logs = array_reverse($data, true);
            return view('backend.sms.log', compact('sms', 'logs'));
        }

        return redirect()->back()->withErrors(['message' => 'unable to read log of non existing sms']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $actions = Sms::getActions();
        $defaultSender = Setting::getValue('sms_default_subject');

        return view('backend.sms.create', compact('actions', 'defaultSender'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validators = config('validation.sms.store');

        $data = $request->validate($validators);

        $sms = new Sms($data);

        if (!$sms->save()) {
            return redirect()->back()->withErrors(['message' => 'Error saving new sms']);
        }

        return redirect()->route('backend.sms.index')->with(['message' => 'Successfully created new sms']);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $sms = Sms::find($id);

        if($sms !== null){
            $actions = Sms::getActions();
            $defaultSender = Setting::getValue('sms_default_subject');

            return view('backend.sms.edit', compact('sms','actions', 'defaultSender'));
        }

        return redirect()->back()->withErrors(['message' => 'Unable to edit non existing Sms']);
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function update(Request $request, $id)
    {
        $sms = Sms::find($id);

        if($sms === null){
            return redirect()->back()->withErrors(['message' => 'Unable to update non existing Sms']);
        }

        $validators = config('validation.sms.store');

        $data = $request->validate($validators);

        if($sms->update($data)){
            return redirect()->route('backend.sms.index')->with(['message' => 'Successfully updated sms']);
        }

        return redirect()->back()->withErrors(['message' => 'Error saving updates to the sms']);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function remove($id)
    {
        $sms = Sms::find($id);

        if($sms === null){
            return redirect()->route('backend.sms.index')->withErrors(['message' => 'Unable to remove non existing sms']);
        }

        try{
            $removed = $sms->delete();
        } catch(Exception $e){
            $removed = false;
        }

        if($removed) return redirect()->back()->with('message', 'Email has been removed');

        return redirect()->back()->withErrors(['message'=>'Error removing sms.']);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function export()
    {
        $sms = Sms::all();
        $count = count($sms);

        if($count > 0){
            $export = new SmsExport($sms, 'all');
            $export->export();
        }

        return redirect()->back()->with('message','Exporting '.$count.' sms messages');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function import(Request $request)
    {
        if ($request->hasFile('sms_upload') && $request->file('sms_upload')->isValid()) {

            $import = new SmsImport($request->file('sms_upload'));

            if ($import->isValid()) {

                $result = $import->import();

                Session::flash('status', $result);

                return redirect()->back();
            } else {
                return redirect()->back()->withErrors($import->getErrors());
            }
        }

        return redirect()->back()->withErrors(['no_file' => 'No or wrong file uploaded']);

    }

    public function report()
    {
        $sms = Sms::all();

        if(count($sms) > 0){
            $report = new SmsLogReport($sms);

            $report->report();
        }

        return redirect()->back()->with(['message'=>'No Sms to report results of']);

    }

    public function reportSingle($id)
    {
        $sms = Sms::find($id);

        if($sms !== null){
            $report = new SmsLogReport([$sms]);
            $report->report();
        }

        return redirect()->back()->with(['message'=>'No Sms to report results of']);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxSend(Request $request, $id){
        $userIds = $request->session()->get('sms.users');

        if($userIds === null || count($userIds) === 0){
            return response()->json(['success' => false, 'error' => ['message' => 'No users found in session to send an sms to']]);
        }

        $users = User::find($userIds);
        if($users === null){
            return response()->json(['success' => false, 'error' => ['message' => 'Unable to find selected users in the database']]);
        }

        $sms = Sms::find($id);
        if($sms === null){
            return response()->json(['success' => false, 'error' => ['message' => 'Oops, sms is invalid']]);
        }

        if($batch = SmsSender::batch($sms, $users)){
            return response()->json(['success' => true,
                'batch' => $batch,
                'sms_id' => $sms->id,
                'statusUrl' => route('backend.sms.ajax-batch-status'),
                'logUrl' => route('backend.sms.log', [$sms->id]),
                'totalUsers' => count($users)
            ]);
        }

        return response()->json(['success'=>false, 'error'=> ['message' => 'Failed creating an sms batch']]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxBatchStatus(Request $request){
        $sms_id = $request->input('sms_id');
        $batch  = $request->input('batch');
        $logs = SmsLog::with('user:id,firstname,lastname,phonenumber')
            ->whereSmsId($sms_id)
            ->whereBatch($batch)
            ->get();

        if(count($logs)){
            return response()->json(['success' => true, 'logs' => $logs->toArray()]);
        }

        return response()->json(['success' => false, 'error' => ['message' => 'Unable to get logs from sms '.$sms_id.' with batch '.$batch]]);
    }
}
