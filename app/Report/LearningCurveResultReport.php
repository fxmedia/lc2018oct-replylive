<?php

namespace App\Report;

use App\User;
use Maatwebsite\Excel\Facades\Excel;

class LearningCurveResultReport extends BaseReport{

    private $learning_curves;

    public function __construct($learning_curves)
    {
        $this->learning_curves = $learning_curves;
    }

    public function getFilename()
    {
        return 'learning_curve_report_' . date('Y_m_d_h_m_s');
    }

    public function report(){
        Excel::create($this->getFilename(), function($excel){

            foreach($this->learning_curves as $learning_curve){

                $name = substr(trim(preg_replace('/[\*|\:|\\|\/|\?|\[|\]]/', '', $learning_curve->name)), 0, 31);
                $excel->sheet($name, function($sheet) use ($learning_curve){
                    $index = 1;

                    $sheet->row($index, ['LearningCurve Name: ' . $learning_curve->name ]);
                    $sheet->row($index, function ($row) {
                        $row->setFontSize(18);
                        $row->setFontWeight('bold');
                    });
                    $index++;

                    $totals = [
                        "pre" => ["total" => 0, "correct" => 0],
                        "post" => ["total" => 0, "correct" => 0]
                    ];

                    foreach($learning_curve->questions as $question) {
                        $sheet->row($index, ['Question: ' . $question->title ]);

                        $sheet->row($index, function ($row) {
                            $row->setFontSize(14);
                            $row->setFontWeight('bold');
                        });
                        $index++;

                        $sheet->row($index, ['', 'User', 'Choice', 'Pre/Post', 'Correct?']);

                        $index++;
                        
                        foreach($question->answers as $a){
                            $answer = json_decode($a);
                            $user = User::find($answer->user_id);
                            $answerIsCorrect = $answer->choice === $question->correct_answer;
                            $correct = ($answerIsCorrect) ? "correct" : "wrong";
                            $phase = $answer->phase;
                            $totals[$phase]["total"]++;
                            if($answerIsCorrect) {
                                $totals[$phase]["correct"]++;
                            }

                            $name = $user->firstname . " " . $user->lastname;
                            $sheet->row($index,[ '', $name, $answer->choice, $phase, $correct ]);
                            $index++;
                        }
                        $index++;
                    }
                    $index++;

                    $sheet->row($index, [
                        "Total",
                        "Before", 
                        $totals["pre"]["correct"] . "/" . $totals["pre"]["total"] . " answers correct",
                        "After",
                        $totals["post"]["correct"] . "/" . $totals["post"]["total"] . " answers correct",
                    ]);

                });
            }

        })->export('xls');
    }
}
