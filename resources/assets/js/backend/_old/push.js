$(function() {

    console.log('Init push.js');

    /**
     * Init fileuploader
     *
     * **/
    if( $('#fileupload').length > 0 ) {
        $('#fileupload').fileupload({
            dataType: 'json',
            formData: $('#pushForm').serializeArray(),
            done: function (e, data) {
                console.log('Upload ready ::', e, data );
                var response = data._response.result,
                    imageUri = false;
                // only on new items
                if( response.fileData ) {
                    $('input[name="image_name"]').val(response.fileData.name);
                    imageUri = response.fileData.uri;
                } else {
                    imageUri = response.image_url;
                }

                $('#content').attr('src', imageUri );
            }
        });
    }


    /**
     * Switch
     *
     * **/

    if( $("input[name='my-checkbox']").length > 0 ) {
        $("input[name='my-checkbox']")
            .bootstrapSwitch()
            .on('switchChange.bootstrapSwitch', function(event, state) {

                var $pushID = $(this).val(),
                    $pushState = state,
                    $toggleUri = toggleUri.replace( '0/toggle', $pushID+'/toggle' );

                $.ajax({
                    url: $toggleUri,
                    method: 'post',
                    data: { state: $pushState },
                    success: function( response ) {
                        console.log('succes ::', response);
                    }
                });
            });
    }

});