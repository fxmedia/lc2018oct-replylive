<?php

namespace App\Export;

class KnockoutExport extends BaseExport
{
    private $filename;
    private $headers = ['id', 'knockout', 'tags', 'groups', 'question' , 'question_id', 'order', 'correct_answer' ,'choice', 'value'];
    private $rows;

    public function __construct($knockouts, $filename = 'all')
    {
        $this->filename = 'export-knockouts-'.$filename;


        foreach($knockouts as $knockout){
            $groups = '';
            foreach($knockout->groups as $group){
                $groups .= $group->name.', ';
            }
            if(strlen($groups) > 2) {
                $groups = substr($groups, 0, -2);
            }
            $this->rows[] = [
                $knockout->id,
                $knockout->name,
                $knockout->tags,
                $groups
            ];

            foreach($knockout->questions as $question){
                $this->rows[] = [
                    $knockout->id,
                    '', // knockout_name
                    '', // knockout_tags
                    '', // knockout_groups
                    $question->title,
                    $question->id,
                    $question->order,
                    $question->correct_answer
                ];

                foreach($question->choices as $c){
                    $this->rows[] = [
                        '', // knockout_id
                        '', // knockout_name
                        '', // knockout_tags
                        '', // knockout_groups
                        '', // question_title
                        $question->id,
                        '', // question_order
                        '', // question_correct_answer
                        $c['choice'],
                        $c['value']
                    ];
                }
            }
        }
    }

    public function getFileName()
    {
        return $this->filename;
    }

    public function getHeaderRow()
    {
        return $this->headers;
    }

    public function getRow()
    {
        return $this->rows[$this->index];
    }

    public function hasNextRow()
    {
        return array_has($this->rows, ($this->index));
    }
}
