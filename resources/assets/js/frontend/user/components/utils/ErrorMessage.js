import React from 'react';

export default function ErrorMessage(props) {
    return (
        <p className="ErrorMessage">
            {props.text}
        </p>
    )
}
