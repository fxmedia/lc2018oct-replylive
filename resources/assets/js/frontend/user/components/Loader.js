import React from 'react';

export default function Loader(props) {
    return (
        <div className="loader-wrapper">
            <div className="lds-css ng-scope">
                <div style={{width:"100%", height: "100%"}} className="lds-ripple"><div></div><div></div></div>
            </div>
            <span>{props.text}</span>
        </div>
    )
}
