<?php

namespace App;

use App\Group;
use App\Setting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Attending
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $group_id
 * @property string $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Group $group
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attending whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attending whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attending whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attending whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attending whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attending whereUserId($value)
 * @mixin \Eloquent
 */
class Attending extends Model
{
    protected $table = 'users_attending_status';

    protected $fillable = ['user_id', 'group_id', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group() {
        return $this->belongsTo('App\Group');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * @param $user_id
     * @return string
     */
    public static function determineAttendingStatus($user_id) {

        // Check if the event has a spot available
        if (!self::eventSpotAvailable()) {
            return 'waiting';
        }
        else {
            // Check if user is in any group that is full.
            $userGroups = DB::table('group_user')->where('user_id', $user_id)->pluck( 'group_id');
            $groupsWithLimit = Group::whereIn('id', $userGroups)->where('limit', '!=', 0)->get();
            if ( count($groupsWithLimit) > 0 ) {
                foreach ($groupsWithLimit as $group) {

                    $groupUserIds = $group->users->pluck('id');
                    $groupAttendingUsers = User::whereIn('id', $groupUserIds)->whereHas('attending', function($query) {
                        $query->where('status', '=', 'attending');
                    })->get();
                    if ( count($groupAttendingUsers) >= $group->limit ) {
                        return 'waiting';
                    }
                }
            }
        }

        return 'attending';
    }

    /**
     * @return bool
     */
    public static function eventSpotAvailable() {
        $eventLimit = Setting::getValue('event_attending_limit');
        $attending = Attending::where('status', 'attending')->get();

        if (
            $eventLimit == 0 ||
            ( $eventLimit - count($attending) ) > 0 ) {
            return true;
        }
        return false;
    }

    /**
     * @param int|null $group_id
     * @return Model|null|static
     */
    public static function getFirstWaiting(int $group_id=null){
        return Attending::with('user')
            ->whereGroupId($group_id)
            ->whereStatus('waiting')
            ->orderBy('updated_at', 'asc')
            ->first();
    }
}
