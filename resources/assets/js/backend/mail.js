tinymce.init({
    selector: '#body_text',
    menubar: false,
    plugins: 'link',
    toolbar: 'undo redo | bold italic underline | alignleft aligncenter alignright | bullist numlist | link | custombutton',
    setup: function(editor) {
        editor.addButton('custombutton', {
            type: 'menubutton',
            text: 'Data',
            icon: false,
            menu: [{
                text: 'First name',
                onclick: function () {
                    editor.insertContent('[firstname]');
                }
            },{
                text: 'Last name',
                onclick: function () {
                    editor.insertContent('[lastname]');
                }
            },{
                text: 'Token',
                onclick: function () {
                    editor.insertContent('[token]');
                }
            },{
                text: 'URL',
                onclick: function () {
                    editor.insertContent('[url]');
                }
            },{
                text: 'Custom',
                onclick: function () {
                    editor.insertContent('[extra_<span id="cursor_position"></span>]');
                    var node = editor.dom.select('#cursor_position')[0];
                    editor.selection.select(node);
                    node.remove();
                }
            }]
        });
    }
});

tinymce.init({
    selector: '#disclaimer_text',
    menubar: false,
    toolbar: 'undo redo | bold italic underline | alignleft aligncenter alignright | bullist numlist',
    force_br_newlines: false,
    force_p_newlines: false,
    forced_root_block: '',
});