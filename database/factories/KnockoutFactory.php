<?php

use Faker\Generator as Faker;

$factory->define(App\Knockout::class, function (Faker $faker) {
    return [
        'name' => $faker->lastName,
        'tags' => $faker->word
    ];
});
