<?php namespace App\Http\Controllers\Api;

use App\Setting;
use Illuminate\Http\Request;
use App\NodeJS\Command;
use App\Http\Controllers\Controller;

class AppController extends ApiBaseController {

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function isOnline(){
        return $this->respond(["success" => true, "online" => Setting::getIsOnline()]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function status() {
        $status = json_decode( Command::status('app_status') ,true);
        $success = false;

        if(isset($status['online'])) {
            $success = true;
        }

        return $this->respond(["success" => $success, "status" => $status]);
    }
}
