import React from 'react';
import config from '../../../config';
import Image from '../utils/Image';
import ImageUpload from '../utils/ImageUpload';

export default function UploadPicture(props) {
    return (
        <div className="UploadPicture">
            <h2>Upload your picture here</h2>
            <ImageUpload
                progress={props.progress}
                loading={props.loading}
                allowed={config.allowedImageTypes}
                preview={false}
                handleUpload={props.handleUpload}
            />
            <Image filename={props.image} />
        </div>
    )
}
