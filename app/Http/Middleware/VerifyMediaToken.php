<?php

namespace App\Http\Middleware;

use App\Setting;
use Closure;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VerifyMediaToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!Setting::getIsOnline()) {
            throw new NotFoundHttpException('Reply.live for this event is offline');
        }
        if(Setting::verifyMediaToken( $request->route('token') )){
            return $next($request);
        }

        throw new NotFoundHttpException('Token is invalid');
    }
}
