class UserManager {
    constructor(socket, Admin) {
        this.socket = socket;
        this.Admin = Admin;
        this.users = [];

        // load users, then open the socket connection for authenticating users
        this.fetchUsers()
        .catch(err => console.log("Users could not be fetched, did you use the correct --host & --port?"))
        .then(() => this.socket.on('connection', user => this.waitForAuthentication(user)))
    }

    addPoints(userId, points) {
        let userIdx = this.users.findIndex(user => user.id == userId);
        let user = this.users[userIdx];
        if(Array.isArray(user.extra)) {
            this.users[userIdx].extra = {};
        }
        this.users[userIdx].extra = this.users[userIdx].extra || {};
        this.users[userIdx].extra.points = user.extra.points || 0;
        this.users[userIdx].extra.points += points;
    }

    addGroup(action, data) {
        data.user_ids.forEach(user_id => {
            // update the group connections/namespaces
            let connIds = this.Admin.getUserConnectionIds(user_id);
            connIds.forEach(connId => {
                let userConnection = this.Admin.getConnection("user", connId);
                let conn = userConnection.conn;
                conn.join(data.group_id);
                userConnection.group_ids.push(data.group_id);
                console.log("added group: ", userConnection);
            })

            // update the cached users
            let userIdx = this.users.findIndex(user => user.id == user_id);
            if(!this.users[userIdx].groups) this.users[userIdx].groups = [];
            this.users[userIdx].groups.push({id: data.group_id});
        });

        this.Admin.broadcastToUsers(data.user_ids, action, { group_id: data.group_id });
    }

    authenticate(user, token) {
        const userData = this.users.find(user => user.token === token);

        if(userData) {
            this.init(user, userData);
        } else {
            console.log("User was probably not found with this token: ", token);
            this.socket.to(user.id).emit("user_error", { message: "User could not be found on the NodeJS server :(" });
        }
    }

    deleteUser(action, data) {
        data.user_ids.forEach(user_id => {
            let connIds = this.Admin.getUserConnectionIds(user_id);
            connIds.forEach(connId => this.socket.to(connId).emit(action, { user_id }));
        });
        this.users = this.users.filter(user => data.user_ids.indexOf(user.id) == -1);
    }

    disconnect(user) {
        const userData = this.Admin.getConnection("user", user.id);
        this.leaveGroupRooms(user, userData);
        this.Admin.removeConnection("user", user.id);
    }

    getUserSpecificData(rebootData, state, user, manager) {
        delete rebootData.reboot;
        state.filter.forEach(filter => {
            if(state[filter][user.id]) {
                rebootData.reboot = (rebootData.id)
                    ? state[filter][user.id][rebootData.id]
                    : state[filter][user.id][rebootData[manager].id]
            }
        });
        return rebootData;
    }

    getRebootData(currentState, privateState, user, manager) {
        var rebootData = Object.assign({}, this.Admin.activeAction.data.user);

        if(currentState) {
            const state = Object.assign({}, currentState);
            if(state.filter) {
                rebootData = this.getUserSpecificData(rebootData, state, user, manager);
            } else {
                rebootData.reboot = state;
                if(privateState && privateState.answers) {
                    rebootData.reboot.currentAnswer = privateState.answers.find(_answer => _answer.user_id == user.id)
                }
            }
        }
        return rebootData;
    }

    getUsers() {
        return this.users;
    }

    fetchUsers() {
        return this.Admin.fetch('users').then(users => this.users = users.data);
    }

    handle(action, data) {
        switch(action) {
            case 'user_add':
                this.addUser(action, data);
                break;
            case 'user_update':
                this.updateUser(action, data);
                break;
            case 'user_delete':
                this.deleteUser(action, data);
                break;
            case 'user_add_group':
                this.addGroup(action, data);
                break;
            case 'user_remove_group':
                this.removeGroup(action, data);
                break;
            case 'user_set_test':
                this.setTestUser(action, data);
                break;
            case 'user_set_attending':
                this.setUserAttending(action, { user_ids: data.user_ids, status: "attending" });
                break;
            case 'user_scan':
                console.log('trigger scan even to the specific connected user!', data.token);
                this.triggerUserScanned(action, data);
                break;
        }
    }

    init(userSocket, data) {
        console.log("user connected with data: ", data);
        this.socket.to(userSocket.id).emit('user_set', { data } );
        this.joinGroupRooms(userSocket, data);
        this.Admin.setConnection("user", userSocket.id, {
            id: data.id,
            group_ids: (data.groups) ? (data.groups.map(group => group.id)) : [],
            conn: userSocket,
            attending: (data.attending) ? (data.attending.status) : null
        });
        this.setListeners(userSocket);
        if(this.Admin.activeAction && this.isAttending(data)) {
            this.reboot(userSocket, data);
        }
        if(data.attending) {
            this.rebootBackground(userSocket);
        }
    }

    hasMatchingGroup(targetGroups, userGroups) {
        if(targetGroups && targetGroups.length > 0 && userGroups && userGroups.length > 0) {
            // check if user is in one of the right groups
            const userIsInGroup = targetGroups.some(targetGroup => userGroups.find(userGroup => userGroup == targetGroup));
            // if not, do not trigger reboot
            if(!userIsInGroup) return false;
        } else if (targetGroups && targetGroups.length > 0 && (!userGroups || userGroups.length === 0)) {
            return false;
        }
        return true;
    }

    isAttending(user) {
        return user.attending && (user.attending == "attending" || user.attending.status == "attending" || user.attending.status == "present");
    }

    joinGroupRooms(user, data) {
        if(data && data.groups) {
            data.groups.forEach(group => user.join(group.id));
        }
    }

    leaveGroupRooms(user, data) {
        if(data && data.groups) {
            data.groups.forEach(group => user.leave(group.id));
        }
    }

    reboot(user, userData) {
        const manager = this.Admin.getManager(this.Admin.activeAction.action);
        const { currentState, result_users, group_ids, privateState } = this.Admin.managers[manager];

        // @todo: give better feedback by only sending if the user was knocked out or not instead of the whole knockout map
        if(privateState && privateState.knockouts && privateState.knockouts.indexOf(userData.id) > -1) return false;
        // check if user has already answered or if no results of users were set in current action
        if((result_users && result_users.indexOf(userData.id) === -1) || !result_users) {
            const userGroupsIds = (userData.groups) ? userData.groups.map(group => group.id) : [];
            if(!this.hasMatchingGroup(group_ids, userGroupsIds)) return false;
            // activate active action on node server
            // check if there is an ongoing action + state first
            const rebootData = this.getRebootData(currentState, privateState, userData, manager);
            this.socket.to(user.id).emit(this.Admin.activeAction.action, rebootData);
        }
        // no reboot, because user has already answered
        return false;
    }

    getBackgroundData(manager, backgroundState, userId) {
        const { currentState } = this.Admin.managers[manager];
        let backgroundData = [...backgroundState[manager]];
        backgroundData.forEach((data, idx) => {
            if(currentState && currentState.users && currentState.users[userId]) {
                // create deep pure clone
                let dataObj = JSON.parse(JSON.stringify(backgroundData[idx]));
                dataObj.question_index = (currentState.users[userId][data.id])
                    ? currentState.users[userId][data.id].question_index
                    : 0;
                backgroundData[idx] = dataObj;
            }
        });
        return backgroundData;
    }

    rebootBackground(user) {
        const { backgroundState } = this.Admin;
        const userId = this.Admin.getConnection("user", user.id).id;

        if(backgroundState && Object.keys(backgroundState).length > 0) {
            for(let manager in backgroundState) {
                const backgroundData = this.getBackgroundData(manager, backgroundState, userId);
                this.socket.to(user.id).emit(`${manager}_reboot_background`, backgroundData);
            }
        }
    }

    removeGroup(action, data) {
        data.user_ids.forEach(user_id => {
            let connIds = this.Admin.getUserConnectionIds(user_id);
            connIds.forEach(connId => {
                let userConnection = this.Admin.getConnection("user", connId);
                let conn = userConnection.conn;
                conn.leave(data.group_id);
                const groupIdx = userConnection.group_ids.indexOf(data.group_id)
                if (groupIdx > -1) {
                    userConnection.group_ids.splice(groupIdx, 1);
                }
            });

            // update the cached users
            let userIdx = this.users.findIndex(user => user.id == user_id);
            if(this.users[userIdx].groups) {
                const groupIdx = this.users[userIdx].groups.indexOf(data.group_id);
                if(groupIdx > -1) {
                    this.users[userIdx].groups.splice(groupIdx, 1);
                }
            }
        });

        this.Admin.broadcastToUsers(data.user_ids, action, { group_id: data.group_id });
    }

    setListeners(userSocket) {
        userSocket.on('knockout_submit_answer', answer => this.Admin.managers.knockout.submitAnswer(answer, userSocket));
        userSocket.on('knockout_submit_deciding_answer', answer => this.Admin.managers.knockout.submitDecidingAnswer(answer, userSocket));
        userSocket.on('oq_submit_answer', answer => this.Admin.managers.oq.submitAnswer(answer, userSocket));
        userSocket.on('poll_submit_answer', answer => this.Admin.managers.poll.submitAnswer(answer, userSocket));
        userSocket.on('post_fetch_all', () => this.Admin.managers.post.fetchAll("user", userSocket));
        userSocket.on('post_submit', post => this.Admin.managers.post.submit(post, userSocket));
        userSocket.on('pledge_submit', pledge => this.Admin.managers.pledge.submit(pledge, userSocket));
        userSocket.on('quiz_submit_answer', answer => this.Admin.managers.quiz.submitAnswer(answer, userSocket));
        userSocket.on('survey_submit_answer', answer => this.Admin.managers.survey.submitAnswer(answer, userSocket));
        userSocket.on('survey_save_next_question_idx', data => this.Admin.managers.survey.saveNextQuestion(data, userSocket));
        userSocket.on('curve_submit_answer', answer => this.Admin.managers.curve.submitAnswer(answer, userSocket));
        userSocket.on('user_update', userSocket => this.updateUser("user_update", userSocket));
        userSocket.on('user_add', userSocket => this.addUser("user_add", userSocket));
    }

    setTestUser(action, data) {
        data.user_ids.forEach(user_id => {
            // update the cached users
            let userIdx = this.users.findIndex(user => user.id == user_id);
            this.users[userIdx].test_user = data.test_user;
        });

        this.Admin.broadcastToUsers(data.user_ids, action, { test_user: data.test_user });
    }

    setUserAttending(action = "user_set_attending", data) {
        data.user_ids.forEach(user_id => {
            // update the cached users
            const userIdx = this.users.findIndex(user => user.id == user_id);
            let user = this.users[userIdx];
            if(user) {
                let _user = Object.assign({}, user);
                _user.attending = { status: data.status};
                this.users[userIdx] = _user;
                this.Admin.getUserConnectionIds(user_id)
                .forEach(connId => {
                    this.Admin.setConnectionAttribute("user", connId, "attending", data.status);
                });
            } else {
                this.Admin.getUserConnectionIds(user_id)
                .forEach(connId => {
                    this.socket.to(connId).emit(
                        "user_error",
                        { message: "User could not be set to attending, because it wasn't present on the NodeJS server" }
                    )
                });
            }

        });

        this.Admin.broadcastToUsers(data.user_ids, action, { attending: data.status });
    }

    setUsers(users) {
        this.users = users;
    }

    addUser(action, user) {
        this.users.push(user);
    }

    updateUser(action, data) {

        const updatedIdx = this.users.findIndex(user => user.id == data.id);
        if(updatedIdx > -1) {
            this.users[updatedIdx] = data;
            this.setUserAttending(
                "set_user_attending",
                {
                    user_ids: [this.users[updatedIdx].id],
                    status: (data.attending) ? data.attending.status : null
                }
            );
            const updatedConnections = this.Admin.getUserConnectionIds(data.id);
            updatedConnections.forEach(connId => {
                if(data.groups && data.groups.length > 0) {
                    let userConnection = this.Admin.getConnection("user", connId);
                    let conn = userConnection.conn;
                    data.groups.forEach(group => {
                        conn.join(group.id);
                        userConnection.group_ids.push(group.id);
                    });
                }
                this.socket.to(connId).emit(action, data)
            });
        } else {
            this.Admin.getUserConnectionIds(data.id)
            .forEach(connId =>
                this.socket.to(connId).emit(
                    "user_error",
                    { message: "User could not be updated, because it was not found" }
                )
            );
        }
    }

    waitForAuthentication(user) {
        user.on('token_set', token => this.authenticate(user, token));
        user.on('disconnect', data => this.disconnect(user));
    }

    /**
     * TODO daan can you check if i do this correctly?
     * Jeffrey: It works.
     * @param action
     * @param data
     */
    triggerUserScanned(action, data){
        const userData = this.users.find(user => user.token === data.token);
        const updatedConnections = this.Admin.getUserConnectionIds(userData.id);
        updatedConnections.forEach(connId => this.socket.to(connId).emit(action, data));

    }
}

module.exports = UserManager;
