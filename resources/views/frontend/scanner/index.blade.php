@extends('frontend.scanner.layout.app')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome-all.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/scanner/scanner.css') }}" />
@endsection

@section('header')
@endsection

@section('content')
<div class="container-fluid container--scanner header__wrapper">
    <div class="scan-content">
        <h1>{{ $eventName }}</h1>
        <br><br>

        {!! Form::open(['id' => 'scanForm', 'method' => 'POST', 'route' => ['api.scanner.scan']]) !!}

        <div class="form-group">
            {!! Form::label('e-Ticket Number','', ['class' =>'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('barcode', '', ['id' => 'barcode','class' => 'form-control allow-click', 'autocomplete' => 'off', 'autofocus'=>'']) !!}
            </div>

            <div class="col-sm-3">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary submit-scan']) !!}
            </div>
        </div>

        {!! Form::close() !!}
    </div>
    <div id="scanResult">
        hey dit is een text ofzo er in...
    </div>
</div>
<div id="loader" class="hidden">
    <div class="loader-icon">

    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" charset="UTF-8" src="{{ asset('/js/scanner.js') }}"></script>
@endsection