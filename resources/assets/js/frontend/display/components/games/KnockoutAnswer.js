import React from 'react';

export default function KnockoutAnswer(props) {
    return (
        <div className={(!props.correct) ? `KnockoutAnswer incorrect ${props.choice}` : `KnockoutAnswer ${props.choice}`}>
            <p className="answer">{props.answer}</p>
        </div>
    )
}
