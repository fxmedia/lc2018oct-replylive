(function(){
    console.log('forms.js');
    var letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    var _addButtons,
        _removeButtons,
        _arrayRows,
        _questionTypeSelect;

    var _currentRow = 1;

    window.templates = {
        user: '<div class="col-sm-4"><input type="text" class="key form-control" name="|keyname|" value="|keyvalue|"/></div>' +
                '<div class="col-sm-7"><input type="text" class="value form-control" name="|valuename|" value="|valuevalue|"/></div>' +
                '<div class="col-sm-1"><div class="btn btn-danger remove-row" data-type="user" data-row="|c|">Delete</div></div>',

        poll: '<div class="col-sm-1"><input type="text" class="key form-control" name="|choicename|" value="|choicevalue|" readonly/></div>' +
                '<div class="col-sm-10"><input type="text" class="value form-control" name="|valuename|" value="|valuevalue|"/></div>' +
                '<div class="col-sm-1"><div class="btn btn-danger remove-row" data-type="poll" data-row="|c|">Delete</div></div>',

        quiz_question: '<div class="col-sm-1"><input type="text" class="key correct-key form-control" name="|choicename|" value="|choicevalue|" readonly/></div>' +
                '<div class="col-sm-10"><input type="text" class="value form-control" name="|valuename|" value="|valuevalue|"/></div>' +
                '<div class="col-sm-1"><div class="btn btn-danger remove-row" data-type="quiz_question" data-row="|c|">Delete</div></div>',

        learning_curve_question: '<div class="col-sm-1"><input type="text" class="key correct-key form-control" name="|choicename|" value="|choicevalue|" readonly/></div>' +
                '<div class="col-sm-10"><input type="text" class="value form-control" name="|valuename|" value="|valuevalue|"/></div>' +
                '<div class="col-sm-1"><div class="btn btn-danger remove-row" data-type="quiz_question" data-row="|c|">Delete</div></div>',

        knockout_question: '<div class="col-sm-1"><input type="text" class="key correct-key form-control" name="|choicename|" value="|choicevalue|" readonly/></div>' +
                '<div class="col-sm-10"><input type="text" class="value form-control" name="|valuename|" value="|valuevalue|"/></div>' +
                '<div class="col-sm-1"><div class="btn btn-danger remove-row" data-type="knockout_question" data-row="|c|">Delete</div></div>',

        survey_abcd: '<div class="col-sm-1"><input type="text" class="key correct-key form-control" name="|choicename|" value="|choicevalue|" readonly/></div>' +
                '<div class="col-sm-10"><input type="text" class="value form-control" name="|valuename|" value="|valuevalue|"/></div>' +
                '<div class="col-sm-1"><div class="btn btn-danger remove-row" data-type="survey_abcd" data-row="|c|">Delete</div></div>'
    };


    var _fixRowKeysPoll = function(){
        var i, inp;
        for(i=0; i<_arrayRows.length ;i++){
            inp = _arrayRows[i].getElementsByClassName('key')[0];
            inp.value = letters[i];
        }

        var _extraRows = document.getElementsByClassName('extra-empty');
        for(var j=0; j<_extraRows.length; j++){
            inp = _extraRows[j].getElementsByClassName('key')[0];
            inp.value = letters[i];
            i++;
        }
    };

    var _updateCorrectAnswerSelect = function(){
        var select = document.getElementsByClassName('correct-answer-select');
        if(select) {
            select = select[0];
            var originalValue = select.value;
            select.innerHTML = '';
            console.log('correct-answer-select');

            var choices = document.getElementsByClassName('correct-key');
            var option, val;
            for(var i=0; i<choices.length;i++){
                val = choices[i].value;
                console.log('val', val);
                option = document.createElement('option');
                option.setAttribute('value', val);
                option.innerHTML = val;

                if(val === originalValue) option.selected = true;

                select.appendChild(option);
            }
        }
    };

    var _changeQuestionType = function(){
        var type = this.value;
        var typeClass = this.getAttribute('data-class');

        if(type && typeClass){
            var groups = document.getElementsByClassName(typeClass);
            var i,j,
                groupType = '',
                inputs,
                shouldDisable = false;
            for(i=0; i<groups.length; i++){
                groupType = groups[i].getAttribute('data-type');
                inputs = groups[i].getElementsByTagName('input');

                if(type.indexOf(groupType) > -1){
                    // show them!
                    groups[i].classList.remove('hidden');
                    shouldDisable = false;
                } else {
                    // hide them!
                    groups[i].classList.add('hidden');
                    shouldDisable = true;
                }

                for(j=0; j<inputs.length; j++){

                    if(shouldDisable) inputs[j].setAttribute('disabled','disabled');

                    else inputs[j].removeAttribute('disabled');
                }
            }
        }
    };


    var _clickAddRow = function(){
        var type = this.getAttribute('data-type');
        var dom = templates[type];

        var extraGroup = this.parentNode.parentNode;

        if(dom){
            var inputs = extraGroup.getElementsByTagName('input');
            window.inputs = inputs;
            var rep, name, value;

            for(var i=0; i<inputs.length; i++){
                rep = inputs[i].getAttribute('data-replace');
                name = inputs[i].getAttribute('name');
                value = inputs[i].value;

                dom = dom.replace('|'+rep+'name|', name);
                dom = dom.replace('|'+rep+'value|', value);

                inputs[i].value = '';
                switch(type){
                    case 'user':
                        inputs[i].setAttribute('name', 'extra['+ _currentRow +']['+rep+']');
                        break;
                    case 'quiz_question':
                    case 'learning_curve_question':
                    case 'knockout_question':
                    case 'poll':
                        if(inputs[i].classList.contains('key') ) inputs[i].value = letters[_currentRow];
                        inputs[i].setAttribute('name', 'choices['+ _currentRow +']['+rep+']');
                        break;
                    case 'survey_abcd':
                        if(inputs[i].classList.contains('key') ) inputs[i].value = letters[_currentRow];
                        inputs[i].setAttribute('name', 'data['+ _currentRow +']['+rep+']');
                        break;
                }
            }

            dom.replace(/\|c\|/g, _currentRow);

            var formGroup = document.createElement('div');
            formGroup.className = 'form-group array-row';

            formGroup.setAttribute('data-row', _currentRow.toString());

            formGroup.innerHTML = dom;
            window.formGroup = formGroup;

            _currentRow++;

            var remove = formGroup.getElementsByClassName('remove-row')[0];
            remove.addEventListener('click', _clickRemoveRow);

            extraGroup.parentNode.insertBefore(formGroup, extraGroup);

            switch(type){
                case 'poll':
                case 'survey_abcd':
                    _fixRowKeysPoll();
                    break;
                case 'quiz_question':
                case 'learning_curve_question':
                case 'knockout_question':
                    _fixRowKeysPoll();
                    _updateCorrectAnswerSelect();
                    break;
            }
        }
    };

    var _clickRemoveRow = function(){
        var type = this.getAttribute('data-type');

        this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);

        switch(type){
            case 'poll':
            case 'survey_abcd':
                _fixRowKeysPoll();
                break;
            case 'quiz_question':
            case 'learning_curve_question':
            case 'knockout_question':
                _fixRowKeysPoll();
                _updateCorrectAnswerSelect();
                break;
        }
    };

    var _init = function(){
        var i;

        _addButtons = document.getElementsByClassName('add-row');
        for(i=0; i< _addButtons.length; i++){
            var c = _addButtons[i].getAttribute('data-key');
            if(c && c > _currentRow) _currentRow = c;

            _addButtons[i].addEventListener('click', _clickAddRow);
        }

        _removeButtons = document.getElementsByClassName('remove-row');
        for(i=0; i< _removeButtons.length; i++){
            _removeButtons[i].addEventListener('click', _clickRemoveRow);
        }

        _arrayRows = document.getElementsByClassName('array-row');

        _questionTypeSelect = document.getElementsByClassName('type-select');
        for(i=0; i<_questionTypeSelect.length; i++){
            _questionTypeSelect[i].addEventListener('change',_changeQuestionType);
            _changeQuestionType.call(_questionTypeSelect[i]);
        }
    };
    _init();
})();
