@if( isset($knockout) )
    {!! Form::model( $knockout, ['class'=>'form-horizontal user-admin', 'method'=>'patch', 'route' => ['backend.knockouts.update', $knockout->id]] ) !!}
@else
    {!! Form::open(['class'=>'form-horizontal user-admin', 'route' => 'backend.knockouts.store']) !!}
@endif

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {!! Form::label('Knockout Name','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('name', Input::old('name'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('name') !!}</span>
</div>

<div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
    {!! Form::label('tags','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('tags', Input::old('tags'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('tags') !!}</span>
</div>

<div class="form-group {{ $errors->has('deciding_question') ? 'has-error' : '' }}">
    {!! Form::label('deciding_question','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('deciding_question', Input::old('deciding_question'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('deciding_question') !!}</span>
</div>

<div class="form-group {{ $errors->has('deciding_answer') ? 'has-error' : '' }}">
    {!! Form::label('deciding_answer','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::number('deciding_answer', Input::old('deciding_answer'),['class'=>'form-control']) !!}
    </div>
    <span class="help-block col-sm-12 col-md-offset-3">{!! $errors->first('deciding_answer') !!}</span>
</div>

<div class="form-group">
    {!! Form::label('Groups','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        @forelse ($groups as $group )
            <label>
                @if( isset($knockout) )
                    {!! Form::checkbox('groups[' . $group->id . ']', $group->id, $knockout->isInGroup($group) ) !!}

                @else
                    {!! Form::checkbox('groups[' . $group->id . ']', $group->id ) !!}
                @endif
                {{ $group->name }}
            </label><br>
        @empty
            No user groups
        @endforelse
    </div>
</div>


@if( isset($knockout) )
<div class="well form-group">
    {!! Form::label('questions','', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <ul class="list-group">
            @foreach($knockout->questions as $question)
                <li class="order-draggable list-group-item" data-question-id="{{ $question->id }}" data-order="{{ $question->order }}">
                    <span class="pull-right">
                        <span>&nbsp;Answers: {{ count($question->answers) }}&nbsp;&nbsp;&nbsp;</span>
                        <a href="{{ route('backend.knockouts.questions.edit', [$question->id]) }}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                        <a href="#" class="btn btn-info btn-xs remote-form-submit" data-form="question_reset_{{ $question->id }}" data-confirm="Are you sure you want to reset the answers to this question?"><span class="glyphicon glyphicon-retweet"></span> Reset</a>
                        <a href="#" class="btn btn-danger btn-xs remote-form-submit" data-form="question_remove_{{ $question->id }}" data-confirm="Are you sure you want to remove this question?"><span class="glyphicon glyphicon-trash"></span> Remove</a>
                    </span>
                    {{ $question->title }}
                </li>
            @endforeach
        </ul>
        <a href="{{ route('backend.knockouts.questions.create',[$knockout->id]) }}" class="pull-right btn btn-default btn-primary">Add Question</a>
    </div>
</div>
@endif

<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
        {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
    </div>
</div>

{!! Form::close() !!}

@if( isset($knockout) )
    <div class="hidden">
        @foreach($knockout->questions as $question)
            {!! Form::open(['method' => 'POST', 'id' => 'question_reset_'.$question->id, 'route' => ['backend.knockouts.questions.reset', $question->id]]) !!}
            {!! Form::close() !!}
            {!! Form::open(['method' => 'POST', 'id' => 'question_remove_'.$question->id , 'route' => ['backend.knockouts.questions.remove', $question->id]]) !!}
            {!! Form::close() !!}
        @endforeach
    </div>
@endif
