<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailAndSmsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $logStatus = config('logs.status');
        //
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('subject');
            $table->string('reply_to')->nullable();
            $table->string('from_name');
            $table->string('from_email');
            $table->string('action')->nullable();
            $table->string('view');
            $table->string('attachment')->nullable();
            $table->text('body_text')->nullable();
            $table->text('disclaimer_text')->nullable();
            $table->string('header_image')->nullable();
            $table->string('footer_image')->nullable();
            $table->timestamps();
        });

        Schema::create('email_log', function (Blueprint $table) use ($logStatus) {
            //
            $table->increments('id');
            $table->integer( 'email_id' )->unsigned();
            $table->foreign( 'email_id' )->references( 'id' )->on( 'emails' )->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('batch')->nullable();
            $table->enum('status',$logStatus )->default( $logStatus[0] );
            $table->text('error')->nullable();
            $table->timestamps();
        });

        Schema::create('sms', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('subject',11);
            $table->string('body_text');
            $table->string('action')->nullable();
            $table->timestamps();
        });

        Schema::create('sms_log', function (Blueprint $table) use ($logStatus) {
            //
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('sms_id')->unsigned();
            $table->foreign('sms_id')->references('id')->on('sms')->onDelete('cascade');
            $table->integer('batch')->nullable();
            $table->enum('status', $logStatus )->default( $logStatus[0] );
            $table->text('error')->nullable();
            $table->text('result')->nullable();
            $table->timestamps();
        });

        Artisan::call('db:seed', [
            '--class' => 'fillDefaultEmails',
            '--force' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('sms_log');

        Schema::dropIfExists('sms');

        Schema::dropIfExists('email_log');

        Schema::dropIfExists('emails');
    }
}
